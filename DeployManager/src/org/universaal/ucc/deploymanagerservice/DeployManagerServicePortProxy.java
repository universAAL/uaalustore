package org.universaal.ucc.deploymanagerservice;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;

public class DeployManagerServicePortProxy{

    protected Descriptor _descriptor;

    public class Descriptor {
        private org.universaal.ucc.deploymanagerservice.DeployManagerService _service = null;
        private org.universaal.ucc.deploymanagerservice.DeployManagerServicePortType _proxy = null;
        private Dispatch<Source> _dispatch = null;

        public Descriptor() {
            init();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new org.universaal.ucc.deploymanagerservice.DeployManagerService(wsdlLocation, serviceName);
            initCommon();
        }

        public void init() {
            _service = null;
            _proxy = null;
            _dispatch = null;
            _service = new org.universaal.ucc.deploymanagerservice.DeployManagerService();
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getDeployManagerServicePort();
        }

        public org.universaal.ucc.deploymanagerservice.DeployManagerServicePortType getProxy() {
            return _proxy;
        }

        public Dispatch<Source> getDispatch() {
            if (_dispatch == null ) {
                QName portQName = new QName("http://deploymanagerservice.ucc.universAAL.org/", "DeployManagerServicePort");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if (!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if (_dispatch != null ) {
                bp = (BindingProvider) _dispatch;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }

    }

    public DeployManagerServicePortProxy() {
        _descriptor = new Descriptor();
    }

    public DeployManagerServicePortProxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
    }

    public Descriptor _getDescriptor() {
        return _descriptor;
    }

    public String getInstalledServices(String arg0) {
        return _getDescriptor().getProxy().getInstalledServices(arg0);
    }

    public void update(String arg0, String arg1, String arg2) {
        _getDescriptor().getProxy().update(arg0,arg1,arg2);
    }

    public void install(String arg0, String arg1, String arg2) {
        _getDescriptor().getProxy().install(arg0,arg1,arg2);
    }

    public void uninstall(String arg0, String arg1) {
        _getDescriptor().getProxy().uninstall(arg0,arg1);
    }

    public String getUserProfile(String arg0) {
        return _getDescriptor().getProxy().getUserProfile(arg0);
    }

    public String getSessionKey(String arg0, String arg1) {
        return _getDescriptor().getProxy().getSessionKey(arg0,arg1);
    }

    public String getInstalledUnitsForService(String arg0, String arg1) {
        return _getDescriptor().getProxy().getInstalledUnitsForService(arg0,arg1);
    }

    public String getAALSpaceProfile() {
        return _getDescriptor().getProxy().getAALSpaceProfile();
    }

}