/*	
	Copyright 2008-2014 ProSyst AG, http://www.prosyst.com
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.ustore.cms;

import java.io.File;

/**
 * A container class that represents and contains the main properties of a maven artifact.
 * 
 * @author i.bakalov@prosyst.com
 */
public class ArtifactDescriptor {

	protected String groupId;
    protected String artifactId;
    protected String version;
    protected String packaging;
    protected String description; //if no ready pom file & generatePom=true
    protected File file;
    protected File pomFile;
    protected boolean generatePom;
    protected String classifier;

    /**
     * Setter for the group id property.
     * @param groupId the new group id
     */
    public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

    /**
     * Retrieves the group id of the artifact. 
	 * @return the group id
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
     * Setter for the artifact id property.
     * @param artifactId the new artifact id
	 */
	public void setArtifactId(String artifactId) {
		this.artifactId = artifactId;
	}

	/**
     * Retrieves the artifact id.
	 * @return the artifact id
	 */
	public String getArtifactId() {
		return artifactId;
	}

	/**
     * Setter for the version property.
     * @param version the new version
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	/**
     * Retrieves the version of the artifact.
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	/**
     * Setter for the packaging property.
	 * @param packaging the new packaging type
	 */
	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	/**
     * Retrieves the packaging type of the artifact.
	 * @return the packaging type
	 */
	public String getPackaging() {
		return packaging;
	}

	/**
     * Setter for the description property.
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
     * Retrieves the description of the artifact.
	 * @return the description.
	 */
	public String getDescription() {
		return description;
	}
	/**
     * Setter for the content file property.
	 * @param file the new content file
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
     * Retrieves the content file of the artifact.
	 * @return the content file
	 */
	public File getFile() {
		return file;
	}

	/**
     * Setter for the pom file property.
	 * @param pomFile the new pom file
	 */
	public void setPomFile(File pomFile) {
		this.pomFile = pomFile;
	}

	/**
     * Retrieves the pom file of the artifact.
	 * @return the pom file
	 */
	public File getPomFile() {
		return pomFile;
	}

	/**
     * Setter for the property indicating if a pom file should be automatically generated when uploading. 
     * Usually useful when there is no pom file and none is provided by <code>setPomFile</code> method.
	 * @param generatePom specifies if it is necessary to generate a pom file.
	 */
	public void setGeneratePom(boolean generatePom) {
		this.generatePom = generatePom;
	}

	/**
     * Retrieves the value of the property that specifies whether automatic pom file generation is necessary.
	 * @return true if the automatic pom file generation is necessary, and false otherwise
	 */
	public boolean getGeneratePom() {
		return generatePom;
	}

	/**
     * Setter for the classifier property.
	 * @param classifier
	 */
	public void setClassifier(String classifier) {
		this.classifier = classifier;
	}

	/**
     * Retrieves classifier property of the artifact.
	 * @return the classifier
	 */
	public String getClassifier() {
		return classifier;
	}

}
