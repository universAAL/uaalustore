/*	
	Copyright 2008-2014 ProSyst AG, http://www.prosyst.com
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.ustore.cms;

import java.util.List;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.deployer.ArtifactDeploymentException;
import org.apache.maven.artifact.installer.ArtifactInstallationException;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.transform.ArtifactTransformationManager;

/**
 * A default implementation of ArtifactTransformationManager. Since in ustore transformations aren't needed it is empty.
 * 
 * @author i.bakalov@prosyst.com
 */
public class EmptyTransformationManager implements
		ArtifactTransformationManager {

	public void transformForDeployment(Artifact artifact,
			ArtifactRepository remoteRepository,
			ArtifactRepository localRepository)
			throws ArtifactDeploymentException {

	}

	public void transformForInstall(Artifact artifact,
			ArtifactRepository localRepository)
			throws ArtifactInstallationException {

	}

	public void transformForResolve(Artifact artifact, List remoteRepositories,
			ArtifactRepository localRepository)
			throws ArtifactResolutionException, ArtifactNotFoundException {

	}

}
