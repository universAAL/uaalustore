/*	
	Copyright 2008-2014 ProSyst AG, http://www.prosyst.com
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.ustore.cms;

import java.util.HashMap;
import java.util.Map;

import org.apache.maven.artifact.handler.ArtifactHandler;
import org.apache.maven.artifact.handler.DefaultArtifactHandler;
import org.apache.maven.artifact.handler.manager.ArtifactHandlerManager;

/**
 * An implementation of the ArtifactHandlerManager that returns the default artifact handler for all types.
 * @author i.bakalov@prosyst.com
 *
 */
public class HandlerManagerImpl implements ArtifactHandlerManager {

	private HashMap types = new HashMap();
	
	/**
	 * Does nothing since only the default handler is needed.
	 */
	public void addHandlers(Map handlers) {
	  //do nothing
	}

	/**
	 * Returns the default artifact handler for the type.
	 */
	public ArtifactHandler getArtifactHandler(String type) {
		ArtifactHandler stored  = (ArtifactHandler)types.get(type);
		if (stored == null) {
			stored = new DefaultArtifactHandler(type);
			types.put(type, stored);
		}
		return stored;
	}

}
