/*	
	Copyright 2008-2014 ProSyst AG, http://www.prosyst.com
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.ustore.cms;

import java.io.InputStream;

import org.apache.maven.wagon.InputData;
import org.apache.maven.wagon.ResourceDoesNotExistException;
import org.apache.maven.wagon.TransferFailedException;
import org.apache.maven.wagon.authorization.AuthorizationException;
import org.apache.maven.wagon.providers.http.HttpWagon;
import org.apache.maven.wagon.resource.Resource;

/**
 * Extension of the normal maven wagon that additionaly exposes the resources' streams.
 * 
 * @author i.bakalov@prosyst.com
 *
 */
public class HttpWagonExt extends HttpWagon {


    /**
     * Retrieves the stream to the content of a resource.
     * @param resourceName the name of the resource to get the stream for.
     * @return the resource stream.
     */
    public InputStream getStream(String resourceName) throws TransferFailedException, ResourceDoesNotExistException, AuthorizationException {
	    InputData inputData = new InputData();
	    Resource resource = new Resource( resourceName );
	    inputData.setResource( resource );
	    fillInputData( inputData );
	    InputStream is = inputData.getInputStream();
	    return is;
    }

	
}
