/*	
	Copyright 2008-2014 ProSyst AG, http://www.prosyst.com
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.ustore.cms;

import java.io.IOException;
import java.io.InputStream;

import org.apache.maven.wagon.ConnectionException;

/**
 * A wrapper of the InputStream that additionally disconnects the associated wagon when the stream is closed.
 *
 * @author i.bakalov@prosyst.com
 *
 */
public class StreamWrapper extends InputStream {

	private HttpWagonExt wagon;
	private InputStream stream;

	/**
	 * @param stream the stream that is being wrapped.
	 * @param wagon the wagon that need sto be disconnected when the stream is closed.
	 */
	public StreamWrapper(InputStream stream, HttpWagonExt wagon) {
		this.wagon = wagon;
		this.stream = stream;
	}

	/* (non-Javadoc)
	 * @see java.io.InputStream#read()
	 */
	public int read() throws IOException {
      return stream.read();
    }

    /* (non-Javadoc)
     * @see java.io.InputStream#read(byte[])
     */
    public int read(byte b[]) throws IOException {
        return stream.read(b);
    }

    /* (non-Javadoc)
     * @see java.io.InputStream#read(byte[], int, int)
     */
    public int read(byte b[], int off, int len) throws IOException {
        return stream.read(b, off, len);
    }

    /* (non-Javadoc)
     * @see java.io.InputStream#skip(long)
     */
    public long skip(long n) throws IOException {
        return stream.skip(n);
    }

    /* (non-Javadoc)
     * @see java.io.InputStream#available()
     */
    public int available() throws IOException {
        return stream.available();
    }

    /* (non-Javadoc)
     * @see java.io.InputStream#mark(int)
     */
    public synchronized void mark(int readlimit) {
        stream.mark(readlimit);
    }

    /* (non-Javadoc)
     * @see java.io.InputStream#reset()
     */
    public synchronized void reset() throws IOException {
    	stream.reset();
    }

    /* (non-Javadoc)
     * @see java.io.InputStream#markSupported()
     */
    public boolean markSupported() {
        return stream.markSupported();
    }

	/**
	 * Closes the InputStream, and also disconnects the associated wagon (the one that was passed in the constructor). 
	 */
	public void close() throws IOException {
		try {
			wagon.disconnect();
		} catch (ConnectionException e) {
			throw new IOException("Cannot disonnect wagon", e);
		}
	}
}
