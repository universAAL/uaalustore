package org.universaal.ustore.cms;

import java.io.File;
import java.io.InputStream;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * A class that is solely for testing purposes, contains examples for usage of the main API methods.
 * @author ibakalov
 *
 */
public class TestMainClass {

	public static void main (String[] args) {
		
		//Example 1 - Deploying an artifact in Nexus
		UStoreCMS repo = new UStoreCMS("UStore Nexus repository", "http://localhost:8081/nexus/content/repositories/UStore/", "admin", "admin123");
		ArtifactDescriptor descriptor = new ArtifactDescriptor();

		descriptor.setArtifactId("CalendarService");
		descriptor.setGroupId("com.prosyst.osami.calendar");
		descriptor.setVersion("1.0.0");

		descriptor.setDescription("My test deployment artifact");
		descriptor.setPackaging("jar");
		descriptor.setFile(new File("D:/develop/clientprojects/osami/svn/components/application/trunk/ClinicServer/CalendarService/target/CalendarService-1.0.0.jar"));
//		descriptor.setPomFile(new File("D:/develop/clientprojects/osami/svn/components/application/trunk/ClinicServer/CalendarService/pom.xml"));
		descriptor.setGeneratePom(true);
		try {
			repo.deployArtifact(descriptor);
		} catch (MojoExecutionException e) {
			e.printStackTrace();
		} catch (MojoFailureException e) {
			e.printStackTrace();
		}

		//Example 2 - Downloading an artifact from Nexus to a file - Using the same descriptor from Example 1, just adding a file to it
		descriptor.setFile(new File("D:/develop/clientprojects/universAAL/Maven Deploy Plugin/CalendarService-1.0.0.jar"));
		Artifact a = null;
		try {
			a = repo.downloadArtifact(descriptor);
		} catch (MojoExecutionException e) {
			e.printStackTrace();
		} catch (MojoFailureException e) {
			e.printStackTrace();
		}
		System.out.println("----------------------------------");
		System.out.println(a.getArtifactId() );
		System.out.println(a.getGroupId() );
		System.out.println(a.getVersion() );
		System.out.println(a.getDownloadUrl() );
		System.out.println(a.getType() );
		System.out.println(a.getScope() );
		System.out.println(a.getClassifier() );
		System.out.println("----------------------------------");
		
		//Example 3 - Getting the stream of an artifact from Nexus - Using the same descriptor from Example 1
		try {
			InputStream is = repo.getArtifactStream(descriptor);
			int size = 0;
			int read = 0;
			byte[] buf = new byte[2048];
			do {
			  read = is.read(buf);
			  size += read;
			}
			while(read != -1);
			size ++;
			System.out.println("Bytes read from stream: " + size);
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
