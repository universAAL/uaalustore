/*	
	Copyright 2008-2014 ProSyst AG, http://www.prosyst.com
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.ustore.cms;

import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.factory.DefaultArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.repository.ArtifactRepositoryFactory;
import org.apache.maven.artifact.repository.DefaultArtifactRepositoryFactory;
import org.apache.maven.artifact.repository.layout.ArtifactRepositoryLayout;
import org.apache.maven.artifact.repository.layout.DefaultRepositoryLayout;
import org.apache.maven.artifact.repository.metadata.DefaultRepositoryMetadataManager;
import org.apache.maven.artifact.repository.metadata.RepositoryMetadataDeploymentException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.deployer.ArtifactDeploymentException;
import org.apache.maven.artifact.metadata.ArtifactMetadata;
import org.apache.maven.model.Model;
import org.apache.maven.model.Parent;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.apache.maven.project.artifact.ProjectArtifactMetadata;
import org.apache.maven.project.validation.DefaultModelValidator;
import org.apache.maven.project.validation.ModelValidationResult;
import org.apache.maven.project.validation.ModelValidator;
import org.apache.maven.wagon.TransferFailedException;
import org.codehaus.plexus.util.IOUtil;
import org.codehaus.plexus.util.ReaderFactory;
import org.codehaus.plexus.util.StringUtils;
import org.codehaus.plexus.util.WriterFactory;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Field;
import java.util.Iterator;

/**
 * The main class of the UStore connection API. An instance of this class should be created in order to upload
 * or download maven artifacts in the UStore CMS.
 * @author i.bakalov@prosyst.com
 */
public class UStoreCMS {
	
	private String repositoryId ;
	private String repositoryUrl;

	private WagonManagerImpl wagonManager;
	private DefaultRepositoryMetadataManager metadataManager;
    protected ArtifactFactory artifactFactory;
    ArtifactRepositoryFactory repositoryFactory;
    ArtifactRepositoryLayout layout;
    private ModelValidator modelValidator;
    
    /**
     * Creates a connection to the UStore CMS that can be used for upload or download of artifacts.
     * @param repositoryId Identifier of the repository, could be any string and does not have any particular meaning. Used for tracking purposes.
     * @param repositoryUrl the actual URL of the Nexus repository where the artifacts are stored. Should be placed somewhere in the configuration 
     * of the applications that use the repository. 
     * @param username the username that is used to connect to Nexus. Should be placed somewhere in the configuration 
     * of the applications that use the repository. 
     * @param password the password that is used to connect to Nexus. Should be placed somewhere in the configuration 
     * of the applications that use the repository. 
     */
    public UStoreCMS(String repositoryId, String repositoryUrl, String username, String password) {
    	this.repositoryId = repositoryId;
    	this.repositoryUrl = repositoryUrl;
    	this.wagonManager = new WagonManagerImpl();
    	this.metadataManager = new DefaultRepositoryMetadataManager();
    	setField(metadataManager, "wagonManager", wagonManager);
    	wagonManager.addAuthenticationInfo(repositoryId, username, password, null, null);
        this.artifactFactory= new DefaultArtifactFactory();
        setField(this.artifactFactory, "artifactHandlerManager", new HandlerManagerImpl());
        this.repositoryFactory = new DefaultArtifactRepositoryFactory();
    	this.layout = new DefaultRepositoryLayout();
    	this.modelValidator = new DefaultModelValidator();
    }

    private void setField(Object obj, String field, Object value) {
        Field f = null;
		try {
			f = obj.getClass().getDeclaredField(field);
		} catch (SecurityException e1) {
			e1.printStackTrace();
		} catch (NoSuchFieldException e1) {
			e1.printStackTrace();
		}
        f.setAccessible(true);
        try {
			f.set(obj, value);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
    }
    
    private void initProperties(ArtifactDescriptor descriptor) throws MojoExecutionException {
        // Process the supplied POM (if there is one)
        if ( descriptor.getPomFile()!= null ) {
            descriptor.setGeneratePom(false);
            Model model = readModel( descriptor.getPomFile());
            processModel( model, descriptor );
        }
    }

    /**
     * Deploys an artifact in the Nexus repository. The artifact, and optionally its POM file should be available as local files before the deployment.
     * @param descriptor The <code>ArtifactDescriptor</code> that describes the artifact being deployed. Should be constructed by the caller, and some of its properties - set.
     * The artifact file is mandatory. Further, either the POM file should be specified, or the groupId/artefactId/version triple along with <code>generatePom = true</code>.
     * @throws MojoExecutionException if an error occurs during the deployment.
     * @throws MojoFailureException if the deployment fails for some reason.
     */
    public void deployArtifact(ArtifactDescriptor descriptor) throws MojoExecutionException, MojoFailureException {
        initProperties(descriptor);
        validateArtifactInformation(descriptor);
        if ( !descriptor.getFile().exists() ) {
            throw new MojoExecutionException( descriptor.getFile().getPath() + " not found." );
        }
        ArtifactRepository deploymentRepository = repositoryFactory.createDeploymentArtifactRepository( this.repositoryId , this.repositoryUrl, layout, false /* unique version for snapshots */ );
        String protocol = deploymentRepository.getProtocol();
        if ( StringUtils.isEmpty( protocol ) ) {
            throw new MojoExecutionException( "No transfer protocol found." );
        }
        if (descriptor.getPackaging() == null) {
        	String filename = descriptor.getFile().getName();
        	int ind = filename.lastIndexOf('.');
        	if (ind < filename.length() - 1) {
        		descriptor.setPackaging(filename.substring(ind + 1));
        	}
        }
        	descriptor.setPackaging("uaal");
        Artifact artifact = artifactFactory.createArtifactWithClassifier( descriptor.getGroupId(), descriptor.getArtifactId(), descriptor.getVersion(), descriptor.getPackaging(), descriptor.getClassifier() );
        // Upload the POM if requested, generating one if need be
        if ( !"pom".equals( descriptor.getPackaging()) ) {
            if ( descriptor.getPomFile() != null ) {
                ArtifactMetadata metadata = new ProjectArtifactMetadata( artifact, descriptor.getPomFile() );
                artifact.addMetadata( metadata );
            }
            else if (descriptor.getGeneratePom()) {
                ArtifactMetadata metadata = new ProjectArtifactMetadata( artifact, generatePomFile(descriptor) );
                artifact.addMetadata( metadata );
            }
        }
        
        System.out.println("Artifact type is " + artifact.getType());
        System.out.println("Artifact file is " + descriptor.getFile());
        System.out.println("Artifact handler is " + artifact.getArtifactHandler());
        System.out.println("Artifact handler type is " + artifact.getArtifactHandler().getExtension());

        try {

            try {

                wagonManager.putArtifact( descriptor.getFile(), artifact, deploymentRepository );
                // must be after the artifact is installed
                for ( Iterator i = artifact.getMetadataList().iterator(); i.hasNext(); )
                {
                    ArtifactMetadata metadata = (ArtifactMetadata) i.next();
                    metadataManager.deploy( metadata, deploymentRepository, deploymentRepository );
                }
            }
            catch ( TransferFailedException e )
            {
                throw new ArtifactDeploymentException( "Error deploying artifact: " + e.getMessage(), e );
            }
            catch ( RepositoryMetadataDeploymentException e )
            {
                throw new ArtifactDeploymentException( "Error installing artifact's metadata: " + e.getMessage(), e );
            }
        } catch ( ArtifactDeploymentException e ) {
          throw new MojoExecutionException( e.getMessage(), e );
        }
    }

    
    /**
     * Downloads an artifact from the Nexus repository to a local file.
     * @param descriptor the descriptor which specifies the artifact to download. It must contain the groupId/artifactId/version of the artifact to download, and
     * the local file where the result shall be stored.
     * @return A Maven <code>Artifact</code> object that describes the downloaded artifact. 
     * @throws MojoExecutionException if an error occurs during the download.
     * @throws MojoFailureException if the download fails for some reason.
     */
    public Artifact downloadArtifact(ArtifactDescriptor descriptor) throws MojoExecutionException, MojoFailureException {
        initProperties(descriptor);
        validateArtifactInformation(descriptor);

        ArtifactRepository deploymentRepository = repositoryFactory.createDeploymentArtifactRepository( this.repositoryId , this.repositoryUrl, layout, false /* unique version for snapshots */ );
        String protocol = deploymentRepository.getProtocol();
        if ( StringUtils.isEmpty( protocol ) ) {
            throw new MojoExecutionException( "No transfer protocol found." );
        }
        // Create the artifact
        Artifact artifact = artifactFactory.createArtifactWithClassifier( descriptor.getGroupId(), descriptor.getArtifactId(), descriptor.getVersion(), descriptor.getPackaging(), descriptor.getClassifier() );
        artifact.setFile(descriptor.getFile());
        
        System.out.println("2Artifact type is " + artifact.getType());
        System.out.println("2Artifact file is " + artifact.getFile());
        System.out.println("2Artifact handler is " + artifact.getArtifactHandler());
        System.out.println("2Artifact handler type is " + artifact.getArtifactHandler().getExtension());

        try {
        	wagonManager.getArtifact(artifact, deploymentRepository);
        }
        catch ( Exception e )
        {
            throw new MojoExecutionException( "Error downloading artifact: " + e.getMessage(), e );
        }
        return artifact;
        	
    }


    /**
     * Retrieves a download stream for an artifact from the Nexus repository..
     * @param descriptor the descriptor which specifies the artifact to download. It must contain the groupId/artifactId/version of the artifact to download.
     * @return An <code>InputStream</code> object that can be used to download the artifact. 
     * @throws MojoExecutionException if an error occurs while obtaining the stream.
     * @throws MojoFailureException if the obtaining of the stream fails for some reason.
     */
    public InputStream getArtifactStream(ArtifactDescriptor descriptor) throws MojoExecutionException, MojoFailureException {
        initProperties(descriptor);
        validateArtifactInformation(descriptor);

        ArtifactRepository deploymentRepository = repositoryFactory.createDeploymentArtifactRepository( this.repositoryId , this.repositoryUrl, layout, false /* unique version for snapshots */ );
        String protocol = deploymentRepository.getProtocol();
        if ( StringUtils.isEmpty( protocol ) ) {
            throw new MojoExecutionException( "No transfer protocol found." );
        }
        // Create the artifact
        Artifact artifact = artifactFactory.createArtifactWithClassifier( descriptor.getGroupId(), descriptor.getArtifactId(), descriptor.getVersion(), descriptor.getPackaging(), descriptor.getClassifier() );
        try {
        	return wagonManager.getArtifactStream(artifact, deploymentRepository);
        }
        catch ( Exception e ) {
            throw new MojoExecutionException( "Error downloading artifact: " + e.getMessage(), e );
        }
    }


    private void processModel( Model model , ArtifactDescriptor descriptor) {
        Parent parent = model.getParent();
        if ( descriptor.getGroupId() == null ) {
        	descriptor.setGroupId(model.getGroupId());
            if ( descriptor.getGroupId() == null && parent != null ) {
                descriptor.setGroupId(parent.getGroupId());
            }
        }
        if ( descriptor.getArtifactId() == null ) {
        	descriptor.setArtifactId(model.getArtifactId());
        }
        if ( descriptor.getVersion() == null ) {
            descriptor.setVersion(model.getVersion());
            if ( descriptor.getVersion() == null && parent != null ) {
                descriptor.setVersion(parent.getVersion());
            }
        }
        if ( descriptor.getPackaging() == null ) {
        	descriptor.setPackaging(model.getPackaging());
        }
    }

    Model readModel( File pomFile ) throws MojoExecutionException {
        Reader reader = null;
        try {
            reader = ReaderFactory.newXmlReader( pomFile );
            return new MavenXpp3Reader().read( reader );
        } catch ( FileNotFoundException e ) {
            throw new MojoExecutionException( "POM not found " + pomFile, e );
        } catch ( IOException e ) {
            throw new MojoExecutionException( "Error reading POM " + pomFile, e );
        } catch ( XmlPullParserException e ) { 
            throw new MojoExecutionException( "Error parsing POM " + pomFile, e );
        } finally {
            IOUtil.close( reader );
        }
    }

    private File generatePomFile(ArtifactDescriptor descriptor) throws MojoExecutionException {
        Model model = generateModel(descriptor);
        Writer fw = null;
        try {
            File tempFile = File.createTempFile( "mvndeploy", ".pom" );
            tempFile.deleteOnExit();
            fw = WriterFactory.newXmlWriter( tempFile );
            new MavenXpp3Writer().write( fw, model );
            return tempFile;
        }
        catch ( IOException e ) {
            throw new MojoExecutionException( "Error writing temporary pom file: " + e.getMessage(), e );
        } finally {
            IOUtil.close( fw );
        }
    }

    private void validateArtifactInformation(ArtifactDescriptor descriptor) throws MojoExecutionException {
        Model model = generateModel(descriptor);
        ModelValidationResult result = modelValidator.validate( model );
        if ( result.getMessageCount() > 0 ) {
            throw new MojoExecutionException( "The artifact information is incomplete or not valid:\n" + result.render( "  " ) );
        }
    }

    private Model generateModel(ArtifactDescriptor descriptor) {
        Model model = new Model();
        model.setModelVersion( "4.0.0" );
        model.setGroupId( descriptor.getGroupId() );
        model.setArtifactId( descriptor.getArtifactId());
        model.setVersion( descriptor.getVersion());
        model.setPackaging( descriptor.getPackaging());
        model.setDescription( descriptor.getDescription());
        return model;
    }

}


