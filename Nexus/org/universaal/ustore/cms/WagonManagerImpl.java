/*	
	Copyright 2008-2014 ProSyst AG, http://www.prosyst.com
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package org.universaal.ustore.cms;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.manager.ChecksumFailedException;
import org.apache.maven.artifact.manager.WagonConfigurationException;
import org.apache.maven.artifact.manager.WagonManager;
import org.apache.maven.artifact.metadata.ArtifactMetadata;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.repository.ArtifactRepositoryFactory;
import org.apache.maven.artifact.repository.ArtifactRepositoryPolicy;
import org.apache.maven.artifact.repository.DefaultArtifactRepository;
import org.apache.maven.wagon.ConnectionException;
import org.apache.maven.wagon.ResourceDoesNotExistException;
import org.apache.maven.wagon.TransferFailedException;
import org.apache.maven.wagon.UnsupportedProtocolException;
import org.apache.maven.wagon.Wagon;
import org.apache.maven.wagon.authentication.AuthenticationException;
import org.apache.maven.wagon.authentication.AuthenticationInfo;
import org.apache.maven.wagon.authorization.AuthorizationException;
import org.apache.maven.wagon.events.TransferListener;
import org.apache.maven.wagon.observers.ChecksumObserver;
//import org.apache.maven.wagon.providers.http.HttpWagon;
import org.apache.maven.wagon.proxy.ProxyInfo;
import org.apache.maven.wagon.repository.Repository;
import org.apache.maven.wagon.repository.RepositoryPermissions;
import org.codehaus.plexus.PlexusConstants;
import org.codehaus.plexus.PlexusContainer;
import org.codehaus.plexus.component.configurator.ComponentConfigurationException;
import org.codehaus.plexus.component.configurator.ComponentConfigurator;
import org.codehaus.plexus.component.repository.exception.ComponentLifecycleException;
import org.codehaus.plexus.component.repository.exception.ComponentLookupException;
import org.codehaus.plexus.configuration.PlexusConfiguration;
import org.codehaus.plexus.configuration.xml.XmlPlexusConfiguration;
import org.codehaus.plexus.context.Context;
import org.codehaus.plexus.context.ContextException;
import org.codehaus.plexus.logging.AbstractLogEnabled;
import org.codehaus.plexus.personality.plexus.lifecycle.phase.Contextualizable;
import org.codehaus.plexus.util.FileUtils;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the WagonManager that provides ustore specific functionalities. In general it works with the extension <code>HttpWagonExt</code>
 * instead of the normal http wagons. This allows the streams for the resources to be obtained rather than working only with files.
 * 
 * @author i.bakalov@prosyst.com
 *
 */
public class WagonManagerImpl
    extends AbstractLogEnabled
    implements WagonManager, Contextualizable
{
    private static final String WILDCARD = "*";

    private PlexusContainer container;

    private Map proxies = new HashMap();

    private Map authenticationInfoMap = new HashMap();

    private Map serverPermissionsMap = new HashMap();

    private Map mirrors = new HashMap();

    private Map serverConfigurationMap = new HashMap();

    private TransferListener downloadMonitor;

    private boolean online = true;

    private ArtifactRepositoryFactory repositoryFactory;

    private boolean interactive = true;

    private Map availableWagons = new HashMap();

    private RepositoryPermissions defaultRepositoryPermissions;

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#getWagon(org.apache.maven.wagon.repository.Repository)
     */
    public Wagon getWagon( Repository repository )
        throws UnsupportedProtocolException, WagonConfigurationException
    {
        String protocol = repository.getProtocol();

        if ( protocol == null )
        {
            throw new UnsupportedProtocolException( "The repository " + repository + " does not specify a protocol" );
        }

        Wagon wagon = getWagon( protocol );

        return wagon;
    }

    /**
     * Creates a new extended wagon that allows streams to be obtained.  
     * @return a http wagon extension.
     */
    public HttpWagonExt getWagon() {
    	HttpWagonExt wagon = new HttpWagonExt();
        wagon.setInteractive( interactive );
        return wagon;
    }

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#putArtifact(java.io.File, org.apache.maven.artifact.Artifact, org.apache.maven.artifact.repository.ArtifactRepository)
     */
    public void putArtifact( File source,
                             Artifact artifact,
                             ArtifactRepository deploymentRepository )
        throws TransferFailedException
    {
        putRemoteFile( deploymentRepository, source, deploymentRepository.pathOf( artifact ), downloadMonitor );
    }

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#putArtifactMetadata(java.io.File, org.apache.maven.artifact.metadata.ArtifactMetadata, org.apache.maven.artifact.repository.ArtifactRepository)
     */
    public void putArtifactMetadata( File source,
                                     ArtifactMetadata artifactMetadata,
                                     ArtifactRepository repository )
        throws TransferFailedException
    {
        debug( "Uploading " + artifactMetadata );
        putRemoteFile( repository, source, repository.pathOfRemoteRepositoryMetadata( artifactMetadata ), null );
    }

    private void putRemoteFile( ArtifactRepository repository,
                                File source,
                                String remotePath,
                                TransferListener downloadMonitor )
        throws TransferFailedException
    {
        failIfNotOnline();

        String protocol = repository.getProtocol();

        Wagon wagon;
        wagon = getWagon();

        if ( downloadMonitor != null )
        {
            wagon.addTransferListener( downloadMonitor );
        }

        Map checksums = new HashMap( 2 );
        Map sums = new HashMap( 2 );

        try
        {
            ChecksumObserver checksumObserver = new ChecksumObserver( "MD5" );
            wagon.addTransferListener( checksumObserver );
            checksums.put( "md5", checksumObserver );
            checksumObserver = new ChecksumObserver( "SHA-1" );
            wagon.addTransferListener( checksumObserver );
            checksums.put( "sha1", checksumObserver );
        }
        catch ( NoSuchAlgorithmException e )
        {
            throw new TransferFailedException( "Unable to add checksum methods: " + e.getMessage(), e );
        }

        try
        {
            Repository artifactRepository = new Repository( repository.getId(), repository.getUrl() );

            if ( serverPermissionsMap.containsKey( repository.getId() ) )
            {
                RepositoryPermissions perms = (RepositoryPermissions) serverPermissionsMap.get( repository.getId() );

                getLogger().debug(
                    "adding permissions to wagon connection: " + perms.getFileMode() + " " + perms.getDirectoryMode() );

                artifactRepository.setPermissions( perms );
            }
            else
            {
                if ( defaultRepositoryPermissions != null )
                {
                    artifactRepository.setPermissions( defaultRepositoryPermissions );
                }
                else
                {
                    debug( "not adding permissions to wagon connection" );
                }
            }

            wagon.connect( artifactRepository, getAuthenticationInfo( repository.getId() ), getProxy( protocol ) );

            wagon.put( source, remotePath );

            wagon.removeTransferListener( downloadMonitor );

            // Pre-store the checksums as any future puts will overwrite them
            for ( Iterator i = checksums.keySet().iterator(); i.hasNext(); )
            {
                String extension = (String) i.next();
                ChecksumObserver observer = (ChecksumObserver) checksums.get( extension );
                sums.put( extension, observer.getActualChecksum() );
            }

            // We do this in here so we can checksum the artifact metadata too, otherwise it could be metadata itself
            for ( Iterator i = checksums.keySet().iterator(); i.hasNext(); )
            {
                String extension = (String) i.next();

                File temp = File.createTempFile( "maven-artifact", null );
                temp.deleteOnExit();
                FileUtils.fileWrite( temp.getAbsolutePath(), (String) sums.get( extension ) );

                wagon.put( temp, remotePath + "." + extension );
            }
        }
        catch ( ConnectionException e )
        {
            throw new TransferFailedException( "Connection failed: " + e.getMessage(), e );
        }
        catch ( AuthenticationException e )
        {
            throw new TransferFailedException( "Authentication failed: " + e.getMessage(), e );
        }
        catch ( AuthorizationException e )
        {
            throw new TransferFailedException( "Authorization failed: " + e.getMessage(), e );
        }
        catch ( ResourceDoesNotExistException e )
        {
            throw new TransferFailedException( "Resource to deploy not found: " + e.getMessage(), e );
        }
        catch ( IOException e )
        {
            throw new TransferFailedException( "Error creating temporary file for deployment: " + e.getMessage(), e );
        }
        finally
        {
            disconnectWagon( wagon );

        }
    }

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#getArtifact(org.apache.maven.artifact.Artifact, java.util.List)
     */
    public void getArtifact( Artifact artifact,
                             List remoteRepositories )
        throws TransferFailedException, ResourceDoesNotExistException
    {
        boolean successful = false;
        for ( Iterator iter = remoteRepositories.iterator(); iter.hasNext() && !successful; )
        {
            ArtifactRepository repository = (ArtifactRepository) iter.next();

            try
            {
                getArtifact( artifact, repository );

                successful = artifact.isResolved();
            }
            catch ( ResourceDoesNotExistException e )
            {
                // This one we will eat when looking through remote repositories
                // because we want to cycle through them all before squawking.

                debug( "Unable to get resource '" + artifact.getId() + "' from repository " +
                    repository.getId() + " (" + repository.getUrl() + ")" );
            }
            catch ( TransferFailedException e )
            {
                debug( "Unable to get resource '" + artifact.getId() + "' from repository " +
                    repository.getId() + " (" + repository.getUrl() + ")" );
            }
        }

        // if it already exists locally we were just trying to force it - ignore the update
        if ( !successful && !artifact.getFile().exists() )
        {
            throw new ResourceDoesNotExistException( "Unable to download the artifact from any repository" );
        }
    }

    private void debug(String string) {
		System.out.println(string);
		
	}

	/* (non-Javadoc)
	 * @see org.apache.maven.artifact.manager.WagonManager#getArtifact(org.apache.maven.artifact.Artifact, org.apache.maven.artifact.repository.ArtifactRepository)
	 */
	public void getArtifact( Artifact artifact,
                             ArtifactRepository repository )
        throws TransferFailedException, ResourceDoesNotExistException
    {
        String remotePath = repository.pathOf( artifact );

        if ( repository.isBlacklisted() )
        {
            debug( "Skipping blacklisted repository " + repository.getId() );
        }
        else
        {
            debug( "Trying repository " + repository.getId() );
            
            getRemoteFile( repository, artifact.getFile(), remotePath, downloadMonitor, ArtifactRepositoryPolicy.CHECKSUM_POLICY_IGNORE /*policy.getChecksumPolicy()*/,
                           false );
            debug( "  Artifact resolved" );

            artifact.setResolved( true );
        }
    }

	/**
	 * A ustore specific method that allows obtaining the stream to the content of an artifact rather than downloading it to a local file.
	 * @param artifact the artifact to obtain the stream for
	 * @param repository the repository to obtain the stream from
	 * @return the stream to the specified artifact
	 * @throws TransferFailedException if the transter failed for some reason.
	 * @throws ResourceDoesNotExistException if the specified artifact does not exist in the specified repository.
	 */
	public InputStream getArtifactStream( Artifact artifact, ArtifactRepository repository ) throws TransferFailedException, ResourceDoesNotExistException {
		String remotePath = repository.pathOf( artifact );

		if ( repository.isBlacklisted() ) {
			debug( "Skipping blacklisted repository " + repository.getId() );
			throw new TransferFailedException("Repository is blacklisted");
		} else {
			debug( "Trying repository " + repository.getId() );
			return getRemoteFileStream( repository, remotePath );
		}
	}

    private InputStream getRemoteFileStream(ArtifactRepository repository, String remotePath) throws TransferFailedException, ResourceDoesNotExistException {
        failIfNotOnline();

        ArtifactRepository mirror = getMirror( repository.getId() );
        if ( mirror != null ) {
            repository = repositoryFactory.createArtifactRepository( mirror.getId(), mirror.getUrl(),
                                                                     repository.getLayout(), repository.getSnapshots(),
                                                                     repository.getReleases() );
        }
        HttpWagonExt wagon = getWagon();

        try {
			wagon.connect( new Repository( repository.getId(), repository.getUrl() ),
			               getAuthenticationInfo( repository.getId() ), (ProxyInfo)null );
	        return new StreamWrapper(wagon.getStream(remotePath), wagon);
		} catch (Exception e) {
			e.printStackTrace();
			throw new TransferFailedException("Cannot connect to repository or bad credentials: ", e);
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.maven.artifact.manager.WagonManager#getArtifactMetadata(org.apache.maven.artifact.metadata.ArtifactMetadata, org.apache.maven.artifact.repository.ArtifactRepository, java.io.File, java.lang.String)
	 */
	public void getArtifactMetadata( ArtifactMetadata metadata,
                                     ArtifactRepository repository,
                                     File destination,
                                     String checksumPolicy )
        throws TransferFailedException, ResourceDoesNotExistException
    {
        String remotePath = repository.pathOfRemoteRepositoryMetadata( metadata );

        getRemoteFile( repository, destination, remotePath, null, checksumPolicy, true );
    }

    private void getRemoteFile( ArtifactRepository repository,
                                File destination,
                                String remotePath,
                                TransferListener downloadMonitor,
                                String checksumPolicy,
                                boolean force )
        throws TransferFailedException, ResourceDoesNotExistException
    {

        failIfNotOnline();

        ArtifactRepository mirror = getMirror( repository.getId() );
        if ( mirror != null )
        {
            repository = repositoryFactory.createArtifactRepository( mirror.getId(), mirror.getUrl(),
                                                                     repository.getLayout(), repository.getSnapshots(),
                                                                     repository.getReleases() );
        }

        String protocol = repository.getProtocol();
        Wagon wagon;
        wagon = getWagon();

        if ( downloadMonitor != null )
        {
            wagon.addTransferListener( downloadMonitor );
        }

        ChecksumObserver md5ChecksumObserver;
        ChecksumObserver sha1ChecksumObserver;
        try
        {
            md5ChecksumObserver = new ChecksumObserver( "MD5" );
            wagon.addTransferListener( md5ChecksumObserver );

            sha1ChecksumObserver = new ChecksumObserver( "SHA-1" );
            wagon.addTransferListener( sha1ChecksumObserver );
        }
        catch ( NoSuchAlgorithmException e )
        {
            throw new TransferFailedException( "Unable to add checksum methods: " + e.getMessage(), e );
        }

        File temp = new File( destination + ".tmp" );
        temp.deleteOnExit();

        boolean downloaded = false;

        try
        {
            wagon.connect( new Repository( repository.getId(), repository.getUrl() ),
                           getAuthenticationInfo( repository.getId() ), getProxy( protocol ) );

            boolean firstRun = true;
            boolean retry = true;

            // this will run at most twice. The first time, the firstRun flag is turned off, and if the retry flag
            // is set on the first run, it will be turned off and not re-set on the second try. This is because the
            // only way the retry flag can be set is if ( firstRun == true ).
            while ( firstRun || retry )
            {
                // reset the retry flag.
                retry = false;

                // This should take care of creating destination directory now on
                if (destination.exists() && !force )
                {
                    try
                    {
                        downloaded = wagon.getIfNewer( remotePath, temp, destination.lastModified() );
                        if ( !downloaded )
                        {
                            // prevent additional checks of this artifact until it expires again
                            destination.setLastModified( System.currentTimeMillis() );
                        }
                    }
                    catch ( UnsupportedOperationException e )
                    {
                        // older wagons throw this. Just get() instead
                        wagon.get( remotePath, temp );
                        downloaded = true;
                    }
                }
                else
                {
                
                    wagon.get( remotePath, temp );
                    downloaded = true;
                }

                if ( downloaded )
                {
                    // keep the checksum files from showing up on the download monitor...
                    if ( downloadMonitor != null )
                    {
                        wagon.removeTransferListener( downloadMonitor );
                    }

                    // try to verify the SHA-1 checksum for this file.
                    try
                    {
                        verifyChecksum( sha1ChecksumObserver, destination, temp, remotePath, ".sha1", wagon );
                    }
                    catch ( ChecksumFailedException e )
                    {
                        // if we catch a ChecksumFailedException, it means the transfer/read succeeded, but the checksum
                        // doesn't match. This could be a problem with the server (ibiblio HTTP-200 error page), so we'll
                        // try this up to two times. On the second try, we'll handle it as a bona-fide error, based on the
                        // repository's checksum checking policy.
                        if ( firstRun )
                        {
//                            debug( "*** CHECKSUM FAILED - " + e.getMessage() + " - RETRYING" );
                            retry = true;
                        }
                        else
                        {
                            handleChecksumFailure( checksumPolicy, e.getMessage(), e.getCause() );
                        }
                    }
                    catch ( ResourceDoesNotExistException sha1TryException )
                    {
                        debug( "SHA1 not found, trying MD5", sha1TryException );

                        // if this IS NOT a ChecksumFailedException, it was a problem with transfer/read of the checksum
                        // file...we'll try again with the MD5 checksum.
                        try
                        {
                            verifyChecksum( md5ChecksumObserver, destination, temp, remotePath, ".md5", wagon );
                        }
                        catch ( ChecksumFailedException e )
                        {
                            // if we also fail to verify based on the MD5 checksum, and the checksum transfer/read
                            // succeeded, then we need to determine whether to retry or handle it as a failure.
                            if ( firstRun )
                            {
                                retry = true;
                            }
                            else
                            {
                                handleChecksumFailure( checksumPolicy, e.getMessage(), e.getCause() );
                            }
                        }
                        catch ( ResourceDoesNotExistException md5TryException )
                        {
                            // this was a failed transfer, and we don't want to retry.
                            handleChecksumFailure( checksumPolicy, "Error retrieving checksum file for " + remotePath,
                                                   md5TryException );
                        }
                    }

                    // reinstate the download monitor...
                    if ( downloadMonitor != null )
                    {
                        wagon.addTransferListener( downloadMonitor );
                    }
                }

                // unset the firstRun flag, so we don't get caught in an infinite loop...
                firstRun = false;
            }
        }
        catch ( ConnectionException e )
        {
            throw new TransferFailedException( "Connection failed: " + e.getMessage(), e );
        }
        catch ( AuthenticationException e )
        {
            throw new TransferFailedException( "Authentication failed: " + e.getMessage(), e );
        }
        catch ( AuthorizationException e )
        {
            throw new TransferFailedException( "Authorization failed: " + e.getMessage(), e );
        }
        finally
        {
            disconnectWagon( wagon );

        }

        if ( downloaded )
        {
        	if ( !temp.exists() )
            {
                throw new ResourceDoesNotExistException( "Downloaded file does not exist: " + temp );
            }

            // The temporary file is named destination + ".tmp" and is done this way to ensure
            // that the temporary file is in the same file system as the destination because the
            // File.renameTo operation doesn't really work across file systems.
            // So we will attempt to do a File.renameTo for efficiency and atomicity, if this fails
            // then we will use a brute force copy and delete the temporary file.

            if ( !temp.renameTo( destination ) )
            {
                try
                {
                    FileUtils.copyFile( temp, destination );

                    temp.delete();
                }
                catch ( IOException e )
                {
                    throw new TransferFailedException(
                        "Error copying temporary file to the final destination: " + e.getMessage(), e );
                }
            }
        }
    }

    private void debug(String string,
			Exception exc) {
		debug(string);
		exc.printStackTrace();
	}

	private void failIfNotOnline()
        throws TransferFailedException
    {
        if ( !isOnline() )
        {
            throw new TransferFailedException( "System is offline." );
        }
    }

    private void handleChecksumFailure( String checksumPolicy,
                                        String message,
                                        Throwable cause )
        throws ChecksumFailedException
    {
        if ( ArtifactRepositoryPolicy.CHECKSUM_POLICY_FAIL.equals( checksumPolicy ) )
        {
            throw new ChecksumFailedException( message, cause );
        }
        else if ( !ArtifactRepositoryPolicy.CHECKSUM_POLICY_IGNORE.equals( checksumPolicy ) )
        {
            // warn if it is set to anything other than ignore
            debug( "*** CHECKSUM FAILED - " + message + " - IGNORING" );
        }
        // otherwise it is ignore
    }

    private void verifyChecksum( ChecksumObserver checksumObserver,
                                 File destination,
                                 File tempDestination,
                                 String remotePath,
                                 String checksumFileExtension,
                                 Wagon wagon )
        throws ResourceDoesNotExistException, TransferFailedException, AuthorizationException
    {
        try
        {
            // grab it first, because it's about to change...
            String actualChecksum = checksumObserver.getActualChecksum();

            File tempChecksumFile = new File( tempDestination + checksumFileExtension + ".tmp" );
            tempChecksumFile.deleteOnExit();
            wagon.get( remotePath + checksumFileExtension, tempChecksumFile );

            String expectedChecksum = FileUtils.fileRead( tempChecksumFile );

            // remove whitespaces at the end
            expectedChecksum = expectedChecksum.trim();

            // check for 'MD5 (name) = CHECKSUM'
            if ( expectedChecksum.startsWith( "MD5" ) )
            {
                int lastSpacePos = expectedChecksum.lastIndexOf( ' ' );
                expectedChecksum = expectedChecksum.substring( lastSpacePos + 1 );
            }
            else
            {
                // remove everything after the first space (if available)
                int spacePos = expectedChecksum.indexOf( ' ' );

                if ( spacePos != -1 )
                {
                    expectedChecksum = expectedChecksum.substring( 0, spacePos );
                }
            }
            if ( expectedChecksum.equals( actualChecksum ) )
            {
                File checksumFile = new File( destination + checksumFileExtension );
                if ( checksumFile.exists() )
                {
                    checksumFile.delete();
                }
                FileUtils.copyFile( tempChecksumFile, checksumFile );
            }
            else
            {
                throw new ChecksumFailedException( "Checksum failed on download: local = '" + actualChecksum +
                    "'; remote = '" + expectedChecksum + "'" );
            }
        }
        catch ( IOException e )
        {
            throw new ChecksumFailedException( "Invalid checksum file", e );
        }
    }

    private void disconnectWagon( Wagon wagon )
    {
        try
        {
            wagon.disconnect();
        }
        catch ( ConnectionException e )
        {
            getLogger().error( "Problem disconnecting from wagon - ignoring: " + e.getMessage() );
        }
    }


    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#getProxy(java.lang.String)
     */
    public ProxyInfo getProxy( String protocol )
    {
        return (ProxyInfo) proxies.get( protocol );
    }

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#getAuthenticationInfo(java.lang.String)
     */
    public AuthenticationInfo getAuthenticationInfo( String id )
    {
        return (AuthenticationInfo) authenticationInfoMap.get( id );
    }

    /**
     * @param mirrorOf
     * @return
     */
    public ArtifactRepository getMirror( String mirrorOf )
    {
        ArtifactRepository repository = (ArtifactRepository) mirrors.get( mirrorOf );
        if ( repository == null )
        {
            repository = (ArtifactRepository) mirrors.get( WILDCARD );
        }
        return repository;
    }

    /**
     * Set the proxy used for a particular protocol.
     *
     * @param protocol      the protocol (required)
     * @param host          the proxy host name (required)
     * @param port          the proxy port (required)
     * @param username      the username for the proxy, or null if there is none
     * @param password      the password for the proxy, or null if there is none
     * @param nonProxyHosts the set of hosts not to use the proxy for. Follows Java system
     *                      property format: <code>*.foo.com|localhost</code>.
     * @todo [BP] would be nice to configure this via plexus in some way
     */
    public void addProxy( String protocol,
                          String host,
                          int port,
                          String username,
                          String password,
                          String nonProxyHosts )
    {
        ProxyInfo proxyInfo = new ProxyInfo();
        proxyInfo.setHost( host );
        proxyInfo.setType( protocol );
        proxyInfo.setPort( port );
        proxyInfo.setNonProxyHosts( nonProxyHosts );
        proxyInfo.setUserName( username );
        proxyInfo.setPassword( password );

        proxies.put( protocol, proxyInfo );
    }

    /* (non-Javadoc)
     * @see org.codehaus.plexus.personality.plexus.lifecycle.phase.Contextualizable#contextualize(org.codehaus.plexus.context.Context)
     */
    public void contextualize( Context context )
        throws ContextException
    {
        container = (PlexusContainer) context.get( PlexusConstants.PLEXUS_KEY );
    }

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#setDownloadMonitor(org.apache.maven.wagon.events.TransferListener)
     */
    public void setDownloadMonitor( TransferListener downloadMonitor )
    {
        this.downloadMonitor = downloadMonitor;
    }

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#addAuthenticationInfo(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    public void addAuthenticationInfo( String repositoryId,
                                       String username,
                                       String password,
                                       String privateKey,
                                       String passphrase )
    {
        AuthenticationInfo authInfo = new AuthenticationInfo();

        authInfo.setUserName( username );

        authInfo.setPassword( password );

        authInfo.setPrivateKey( privateKey );

        authInfo.setPassphrase( passphrase );

        authenticationInfoMap.put( repositoryId, authInfo );
    }

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#addPermissionInfo(java.lang.String, java.lang.String, java.lang.String)
     */
    public void addPermissionInfo( String repositoryId,
                                   String filePermissions,
                                   String directoryPermissions )
    {

        RepositoryPermissions permissions = new RepositoryPermissions();
        boolean addPermissions = false;

        if ( filePermissions != null )
        {
            permissions.setFileMode( filePermissions );
            addPermissions = true;
        }

        if ( directoryPermissions != null )
        {
            permissions.setDirectoryMode( directoryPermissions );
            addPermissions = true;
        }

        if ( addPermissions )
        {
            serverPermissionsMap.put( repositoryId, permissions );
        }
    }

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#addMirror(java.lang.String, java.lang.String, java.lang.String)
     */
    public void addMirror( String id,
                           String mirrorOf,
                           String url )
    {
        ArtifactRepository mirror = new DefaultArtifactRepository( id, url, null );

        mirrors.put( mirrorOf, mirror );
    }

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#setOnline(boolean)
     */
    public void setOnline( boolean online )
    {
        this.online = online;
    }

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#isOnline()
     */
    public boolean isOnline()
    {
        return online;
    }

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#setInteractive(boolean)
     */
    public void setInteractive( boolean interactive )
    {
        this.interactive = interactive;
    }

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#registerWagons(java.util.Collection, org.codehaus.plexus.PlexusContainer)
     */
    public void registerWagons( Collection wagons,
                                PlexusContainer extensionContainer )
    {
        for ( Iterator i = wagons.iterator(); i.hasNext(); )
        {
            availableWagons.put( i.next(), extensionContainer );
        }
    }


    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#addConfiguration(java.lang.String, org.codehaus.plexus.util.xml.Xpp3Dom)
     */
    public void addConfiguration( String repositoryId,
                                  Xpp3Dom configuration )
    {
        if ( repositoryId == null || configuration == null )
        {
            throw new IllegalArgumentException( "arguments can't be null" );
        }

        final XmlPlexusConfiguration xmlConf = new XmlPlexusConfiguration( configuration );

        serverConfigurationMap.put( repositoryId, xmlConf );
    }

    /* (non-Javadoc)
     * @see org.apache.maven.artifact.manager.WagonManager#setDefaultRepositoryPermissions(org.apache.maven.wagon.repository.RepositoryPermissions)
     */
    public void setDefaultRepositoryPermissions( RepositoryPermissions defaultRepositoryPermissions )
    {
        this.defaultRepositoryPermissions = defaultRepositoryPermissions;
    }

	/* (non-Javadoc)
	 * @see org.apache.maven.artifact.manager.WagonManager#getWagon(java.lang.String)
	 */
	public Wagon getWagon(String protocol) throws UnsupportedProtocolException {
		return getWagon();
	}

	/* (non-Javadoc)
	 * @see org.apache.maven.artifact.manager.WagonManager#getArtifactMetadataFromDeploymentRepository(org.apache.maven.artifact.metadata.ArtifactMetadata, org.apache.maven.artifact.repository.ArtifactRepository, java.io.File, java.lang.String)
	 */
	@Override
	public void getArtifactMetadataFromDeploymentRepository(
			ArtifactMetadata arg0, ArtifactRepository arg1, File arg2,
			String arg3) throws TransferFailedException,
			ResourceDoesNotExistException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see org.apache.maven.artifact.manager.WagonManager#getMirrorRepository(org.apache.maven.artifact.repository.ArtifactRepository)
	 */
	@Override
	public ArtifactRepository getMirrorRepository(ArtifactRepository arg0) {
		// TODO Auto-generated method stub
		return null;
	}
}

