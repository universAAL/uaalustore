package org.universaal.aal_usrv.matcher;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.universaal.aal_uapp.v1_0.CapabilityType;
import org.universaal.aal_uapp.v1_0.ContactType;
import org.universaal.aal_uapp.v1_0.ContactType.OtherChannel;
import org.universaal.aal_uapp.v1_0.LicenseCategoryType;
import org.universaal.aal_uapp.v1_0.LicenseType;
import org.universaal.aal_uapp.v1_0.LogicalCriteriaType;
import org.universaal.aal_uapp.v1_0.LogicalRelationType;
import org.universaal.aal_uapp.v1_0.ReqAtomType;
import org.universaal.aal_uapp.v1_0.ReqGroupType;
import org.universaal.aal_uapp.v1_0.ReqType;
import org.universaal.aal_uapp.v1_0.VersionType;
import org.universaal.aal_usrv.v1_0.AalUsrv;
import org.universaal.aal_usrv.v1_0.AalUsrv.Components;
import org.universaal.aal_usrv.v1_0.AalUsrv.ServiceCapabilities;
import org.universaal.aal_usrv.v1_0.AalUsrv.ServiceRequirements;
import org.universaal.aal_usrv.v1_0.AalUsrv.Srv;
import org.universaal.aal_usrv.v1_0.AalUsrv.Srv.Licenses;
import org.universaal.aal_usrv.v1_0.ApplicationType;
import org.universaal.aal_usrv.v1_0.HardwareType;
import org.universaal.aal_usrv.v1_0.HumanType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class AALUsrvParser {
	
	private final static String AAL_USRV_ROOT_TAG="usrv:aal-usrv";
	
	private final static String SERVICE_TAG_NAME="usrv:srv";
	private final static String SERVICE_NAME_TAG_NAME="usrv:name";
	private final static String SERVICE_VERSION_TAG_NAME="usrv:version";
	private final static String VERSION_MAJOR_TAG_NAME="uapp:major";
	private final static String VERSION_MINOR_TAG_NAME="uapp:minor";
	private final static String VERSION_MICRO_TAG_NAME="uapp:micro";
	private final static String VERSION_BUILD_TAG_NAME="uapp:build";
	
	private final static String CONTACT_ORGANIZATION_TAG_NAME="uapp:organizationName";
	private final static String CONTACT_CERTIFICATE_TAG_NAME="uapp:certificate";
	private final static String CONTACT_PERSON_TAG_NAME="uapp:contactPerson";
	private final static String CONTACT_ADDRESS_TAG_NAME="uapp:streetAddress";
	private final static String CONTACT_EMAIL_TAG_NAME="uapp:email";
	private final static String CONTACT_WEB_ADDRESS_TAG_NAME="uapp:webAddress";
	private final static String CONTACT_PHONE_TAG_NAME="uapp:phone";
	private final static String CONTACT_CHANNEL_TAG_NAME="uapp:otherChannel";
	private final static String CHANNEL_NAME_TAG_NAME="uapp:channelName";
	private final static String CHANNEL_DETAILS_TAG_NAME="uapp:channelDetails";
	
	private final static String SERVICE_ID_TAG_NAME="usrv:serviceId";
	private final static String SERVICE_DESCRIPTION_TAG_NAME="usrv:description"; 
	private final static String SERVICE_TAGS_TAG_NAME="usrv:tags";
	private final static String SERVICE_PROVIDER_TAG_NAME="usrv:serviceProvider";
	private final static String SERVICE_LICENSES_TAG_NAME="usrv:licenses";
	private final static String LICENSES_TAG_NAME="usrv:license";
	private final static String LICENSES_SLA_TAG_NAME="usrv:sla";
	private final static String LICENSE_NAME_TAG_NAME="uapp:name";
	private final static String LICENSE_CATEGORY_TAG_NAME="uapp:category";
	private final static String LICENSE_LINK_TAG_NAME="uapp:link";
	private final static String SLA_NAME_TAG_NAME="usrv:name";
	private final static String SLA_LINK_TAG_NAME="usrv:link";
	private final static String SERVICE_PROFILE_TAG_NAME="usrv:serviceProfile";
	
	private final static String CAPABILITIES_TAG_NAME="usrv:serviceCapabilities";
	private final static String CAPABILITY_TAG_NAME="usrv:capability";
	private final static String CAPABILITY_NAME_TAG_NAME="uapp:name";
	private final static String CAPABILITY_VALUE_TAG_NAME="uapp:value";
	
	private final static String REQUIREMENTS_TAG_NAME="usrv:serviceRequirements";
	private final static String REQUIREMENT_TAG_NAME="usrv:requirement";
	private final static String REQUIREMENT_ATOM_TAG_NAME="uapp:reqAtom";
	private final static String REQUIREMENT_GROUP_TAG_NAME="uapp:reqGroup";
	private final static String REQUIREMENT_OPTIONAL_TAG_NAME="uapp:optional";
	private final static String REQATOMTYPE_NAME_TAG_NAME="uapp:reqAtomName";
	private final static String REQATOMTYPE_VALUE_TAG_NAME="uapp:reqAtomValue";
	private final static String REQATOMTYPE_CRITERIA_TAG_NAME="uapp:reqCriteria";
	private final static String REQGROUPTYPE_LOGICAL_RELATION_TAG_NAME="uapp:logicalRelation";
	private final static String REQGROUPTYPE_REQUIREMENT_TAG_NAME="uapp:requirement";
	
	private final static String COMPONENTS_TAG_NAME="usrv:components";
	private final static String APPLICATION_COMPONENTS_TAG_NAME="usrv:application";
	private final static String HARDWARE_COMPONENTS_TAG_NAME="usrv:hardware";
	private final static String HUMAN_COMPONENTS_TAG_NAME="usrv:human";	
	private final static String APPLICATION_NAME_TAG_NAME="usrv:name";
	private final static String APPLICATION_VERSION_TAG_NAME="usrv:version";
	private final static String APPLICATION_CLASS_TAG_NAME="usrv:class";
	private final static String APPLICATION_ARTIFACT_ID_TAG_NAME="usrv:artifactID";
	private final static String APPLICATION_LOCATION_TAG_NAME="usrv:location";
	private final static String HARDWARE_NAME_TAG_NAME="usrv:name";
	private final static String HARDWARE_CLASS_TAG_NAME="usrv:class";
	private final static String HARDWARE_ARTIFACT_ID_TAG_NAME="usrv:artifactID";
	private final static String HARDWARE_CATEGORY_TAG_NAME="usrv:category";
	private final static String HUMAN_ROLE_TAG_NAME="usrv:rolerole";
	private final static String HUMAN_COMPANY_TAG_NAME="usrv:company";
	private final static String HUMAN_PHONE_TAG_NAME="usrv:phone";
	private final static String HUMAN_REFID_TAG_NAME="usrv:refID";
	private final static String HUMAN_FREQUENCY_NAME="usrv:frequency";
	private final static String HUMAN_QUALIFICATION_TAG_NAME="usrv:qualification";
	private final static String HUMAN_EXPERIENCE_TAG_NAME="usrv:experience";
	
	
	public static AalUsrv parse(InputStream input) throws Exception {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = factory.newDocumentBuilder();
		Document document = documentBuilder.parse(input);

		Element element = document.getDocumentElement();
		String rootName = element.getTagName();
		if (!AAL_USRV_ROOT_TAG.equals(rootName)) {
			throw new IllegalArgumentException("Expecting " + AAL_USRV_ROOT_TAG + ", found " + rootName);
		}

		AalUsrv aalUsrv=new AalUsrv();
		
		Srv srv=readService(element);
		aalUsrv.setSrv(srv);
		
		ServiceCapabilities capabilities=readCapabilities(element);
		aalUsrv.setServiceCapabilities(capabilities);
		
		ServiceRequirements requirements=readRequirements(element);
		aalUsrv.setServiceRequirements(requirements);
		
		Components components=readComponents(element);
		aalUsrv.setComponents(components);
		
		return aalUsrv;
	}
	
	private static Srv readService(Element rootElement) {
		Srv service = new Srv();
		for (Node node = rootElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(SERVICE_TAG_NAME)) {
					for (Node srvNode = nextElement.getFirstChild(); srvNode != null; srvNode = srvNode
							.getNextSibling()) {
						if (srvNode.getNodeType() == Node.ELEMENT_NODE) {
							Element srvElement = (Element) srvNode;
							String srvTagName = srvElement.getTagName();
							if (srvTagName
									.equalsIgnoreCase(SERVICE_NAME_TAG_NAME)) {
								String serviceName = getText(srvElement);
								service.setName(serviceName);

							} else if (srvTagName
									.equalsIgnoreCase(SERVICE_VERSION_TAG_NAME)) {
								VersionType version = readVersion(srvElement);
								service.setVersion(version);
							} else if (srvTagName
									.equalsIgnoreCase(SERVICE_ID_TAG_NAME)) {
								String serviceId = getText(srvElement);
								service.setServiceId(serviceId);
							} else if (srvTagName
									.equalsIgnoreCase(SERVICE_DESCRIPTION_TAG_NAME)) {
								String description = getText(srvElement);
								service.setDescription(description);
							} else if (srvTagName
									.equalsIgnoreCase(SERVICE_TAGS_TAG_NAME)) {
								String tags = getText(srvElement);
								service.setTags(tags);
							} else if (srvTagName
									.equalsIgnoreCase(SERVICE_PROVIDER_TAG_NAME)) {
								ContactType provider = readContactType(srvElement);
								service.setServiceProvider(provider);
							} else if (srvTagName
									.equalsIgnoreCase(SERVICE_LICENSES_TAG_NAME)) {
								Licenses lic = readLicenses(srvElement);
								service.getLicenses().add(lic);
							} else if (srvTagName
									.equalsIgnoreCase(SERVICE_PROFILE_TAG_NAME)) {
								String profile = getText(srvElement);
								service.setServiceProfile(profile);
							}
						}
					}
					// break;
				}
			}
		}
		return service;
	}

	private static Licenses readLicenses(Element srvElement) {
		Licenses licenses =new Licenses(); 
		for (Node node = srvElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(LICENSES_TAG_NAME)) {
					LicenseType license=readLicense(nextElement);
					licenses.getLicense().add(license);
				}
				else if(tagName.equalsIgnoreCase(LICENSES_SLA_TAG_NAME)){
					org.universaal.aal_usrv.v1_0.AalUsrv.Srv.Licenses.Sla sla = readServiceLevelArg(nextElement);
					licenses.setSla(sla);
				}
				
			}
		}
		return licenses;
	}
	
	private static LicenseType readLicense(Element licenseElement) {
		LicenseType license = new LicenseType();
		for (Node node = licenseElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(LICENSE_NAME_TAG_NAME)) {
					String licenseName = getText(nextElement);
					license.setName(licenseName);
				} else if (tagName.equalsIgnoreCase(LICENSE_CATEGORY_TAG_NAME)) {
					LicenseCategoryType categoryType = LicenseCategoryType
							.fromValue(getText(nextElement));
					license.setCategory(categoryType);
				} else if (tagName.equalsIgnoreCase(LICENSE_LINK_TAG_NAME)) {
					String licenseLink = getText(nextElement);
					license.setLink(licenseLink);
				}
			}
		}
		return license;
	}

	private static org.universaal.aal_usrv.v1_0.AalUsrv.Srv.Licenses.Sla readServiceLevelArg(
			Element slaElement) {
		org.universaal.aal_usrv.v1_0.AalUsrv.Srv.Licenses.Sla sla = new org.universaal.aal_usrv.v1_0.AalUsrv.Srv.Licenses.Sla();
		for (Node node = slaElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(SLA_NAME_TAG_NAME)) {
					String slaName = getText(nextElement);
					sla.setName(slaName);
				} else if (tagName.equalsIgnoreCase(SLA_LINK_TAG_NAME)) {
					String slaLink = getText(nextElement);
					sla.setLink(slaLink);
				}
			}
		}
		return sla;
	}

	private static VersionType readVersion(Element versionElement) {
		VersionType version = new VersionType();
		for (Node node = versionElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(VERSION_MAJOR_TAG_NAME)) {
					int major=Integer.parseInt(getText(nextElement));
					version.setMajor(major);
				}
				else if(tagName.equalsIgnoreCase(VERSION_MINOR_TAG_NAME)){
					int minor=Integer.parseInt(getText(nextElement));
					version.setMinor(minor);
				}
				else if(tagName.equalsIgnoreCase(VERSION_MICRO_TAG_NAME)){
					int micro=Integer.parseInt(getText(nextElement));
					version.setMicro(micro);
				}
				else if(tagName.equalsIgnoreCase(VERSION_BUILD_TAG_NAME)){
					String build = getText(nextElement);
					version.setBuild(build);
				}
			}
		}
		return version;
	}
	
	private static ContactType readContactType(Element contactElement) {
		ContactType contact =new ContactType();
		for (Node node = contactElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(CONTACT_ORGANIZATION_TAG_NAME)) {
					String organizationName=getText(nextElement);
					contact.setOrganizationName(organizationName);
				}
				else if(tagName.equalsIgnoreCase(CONTACT_CERTIFICATE_TAG_NAME)){
					String certificate=getText(nextElement);
					contact.setCertificate(certificate);
				}
				else if(tagName.equalsIgnoreCase(CONTACT_PERSON_TAG_NAME)){
					String contactPerson = getText(nextElement);
					contact.setContactPerson(contactPerson);
				}
				else if(tagName.equalsIgnoreCase(CONTACT_ADDRESS_TAG_NAME)){
					String streetAddress = getText(nextElement);
					contact.setStreetAddress(streetAddress);
				}
				else if(tagName.equalsIgnoreCase(CONTACT_EMAIL_TAG_NAME)){
					String email = getText(nextElement);
					contact.setEmail(email);
				}
				else if(tagName.equalsIgnoreCase(CONTACT_WEB_ADDRESS_TAG_NAME)){
					String webAddress = getText(nextElement);
					contact.setWebAddress(webAddress);
				}
				else if(tagName.equalsIgnoreCase(CONTACT_PHONE_TAG_NAME)){
					String phone = getText(nextElement);
					contact.setPhone(phone);
				}
				else if(tagName.equalsIgnoreCase(CONTACT_CHANNEL_TAG_NAME)){
					OtherChannel otherChannel = readChannel(nextElement);
					contact.setOtherChannel(otherChannel);
				}
			}
		}
		return contact;
	}

	private static OtherChannel readChannel(Element channelElement) {
		OtherChannel channel=new OtherChannel();
		for (Node node = channelElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(CHANNEL_NAME_TAG_NAME)) {
					String channelName=getText(nextElement);
					channel.setChannelName(channelName);
				}
				else if(tagName.equalsIgnoreCase(CHANNEL_DETAILS_TAG_NAME)){
					String channelDetails=getText(nextElement);
					channel.setChannelDetails(channelDetails);
				}
			}
		}
		return channel;
	}

	private static ServiceCapabilities readCapabilities(Element rootElement) {
		ServiceCapabilities serviceCapabilities = new ServiceCapabilities();
		for (Node node = rootElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(CAPABILITIES_TAG_NAME)) {
					for (Node capabilityNode = nextElement.getFirstChild(); capabilityNode != null; capabilityNode = capabilityNode
							.getNextSibling()) {
						if (capabilityNode.getNodeType() == Node.ELEMENT_NODE) {
							Element capabilityElement = (Element) capabilityNode;
							String capabilityTagName = capabilityElement
									.getTagName();
							if (capabilityTagName
									.equalsIgnoreCase(CAPABILITY_TAG_NAME)) {
								CapabilityType capability =readCapability(capabilityElement);
							    serviceCapabilities.getCapability().add(capability);
							}
						}
					}
				}
			}
		}
		return serviceCapabilities;
	}
	
	private static CapabilityType readCapability(Element capabilityElement) {
		CapabilityType capability = new CapabilityType();
		for (Node node = capabilityElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(CAPABILITY_NAME_TAG_NAME)) {
					String name =getText(nextElement);
					capability.setName(name);
				}
				else if(tagName.equalsIgnoreCase(CAPABILITY_VALUE_TAG_NAME)){
					String value = getText(nextElement);
					capability.setValue(value);
				}
			}
		}
		return capability;
	}

	private static ServiceRequirements readRequirements(Element rootElement) {
		ServiceRequirements serviceRequirements = new ServiceRequirements();
		for (Node node = rootElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(REQUIREMENTS_TAG_NAME)) {
					for (Node requirementNode = nextElement.getFirstChild(); requirementNode != null; requirementNode = requirementNode
							.getNextSibling()) {
						if (requirementNode.getNodeType() == Node.ELEMENT_NODE) {
							Element requirementElement = (Element) requirementNode;
							String requirementTagName = requirementElement
									.getTagName();
							if (requirementTagName
									.equalsIgnoreCase(REQUIREMENT_TAG_NAME)) {
								ReqType requirement=readRequrement(requirementElement);
								serviceRequirements.getRequirement().add(requirement);
							}
						}
					}
				}
			}
		}
		return serviceRequirements;
	}
	
	private static ReqType readRequrement(Element requirementElement) {
		ReqType requirement =new ReqType();
		for (Node node = requirementElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(REQUIREMENT_ATOM_TAG_NAME)) {
					ReqAtomType reqAtomType=readReqAtomType(nextElement);
//					requirement.setReqAtom(reqAtomType);
					requirement.setType(reqAtomType);
				}
				else if(tagName.equalsIgnoreCase(REQUIREMENT_GROUP_TAG_NAME)){
					ReqGroupType reqGroupType =readReqGroupType(nextElement);
//					requirement.setReqGroup(reqGroupType);
					requirement.setType(reqGroupType);
				}
				else if(tagName.equalsIgnoreCase(REQUIREMENT_OPTIONAL_TAG_NAME)){
					boolean isOptional=Boolean.parseBoolean(getText(nextElement));
					requirement.setOptional(isOptional);
				}
			}
		}
		return requirement;
	}

	private static ReqAtomType readReqAtomType(Element reqAtomTypeElement) {
		ReqAtomType reqAtomType = new ReqAtomType();
		for (Node node = reqAtomTypeElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(REQATOMTYPE_NAME_TAG_NAME)) {
					String reqAtomTypeName=getText(nextElement);
					reqAtomType.setReqAtomName(reqAtomTypeName);
				}
				else if(tagName.equalsIgnoreCase(REQATOMTYPE_VALUE_TAG_NAME)){
					String value =getText(nextElement);
					reqAtomType.setReqAtomValue(value);
//					reqAtomType.getReqAtomValue().add(value);
				}
				else if(tagName.equalsIgnoreCase(REQATOMTYPE_CRITERIA_TAG_NAME)){
					LogicalCriteriaType criteria =LogicalCriteriaType.fromValue(getText(nextElement));
					reqAtomType.setReqCriteria(criteria);
				}
			}
		}
		return reqAtomType;
	}

	private static ReqGroupType readReqGroupType(Element reqGroupTypeElement) {
		ReqGroupType reqGroupType=new ReqGroupType();
		for (Node node = reqGroupTypeElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(REQGROUPTYPE_LOGICAL_RELATION_TAG_NAME)) {
					LogicalRelationType logicalRelation=LogicalRelationType.fromValue(getText(nextElement));
					reqGroupType.setLogicalRelation(logicalRelation);
				}
				else if(tagName.equalsIgnoreCase(REQGROUPTYPE_REQUIREMENT_TAG_NAME)){
					ReqType requirement=readRequrement(nextElement);
					reqGroupType.getRequirement().add(requirement); //maxOccurs="2" ???
				}
			}
		}
		return reqGroupType;
	}
	
	private static Components readComponents(Element rootElement) {
		Components components = new Components();
		for (Node node = rootElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(COMPONENTS_TAG_NAME)) {
					for (Node componentNode = nextElement.getFirstChild(); componentNode != null; componentNode = componentNode
							.getNextSibling()) {
						if (componentNode.getNodeType() == Node.ELEMENT_NODE) {
							Element componentElement = (Element) componentNode;
							String componentTagName = componentElement
									.getTagName();
							if (componentTagName
									.equalsIgnoreCase(APPLICATION_COMPONENTS_TAG_NAME)) {
								ApplicationType application = readApplication(componentElement);
								components.getApplication().add(application);
							}
							else if (componentTagName
									.equalsIgnoreCase(HARDWARE_COMPONENTS_TAG_NAME)) {
								HardwareType hardwareComponent = readHardwareComponent(componentElement);
								components.getHardware().add(hardwareComponent);
							}
							else if (componentTagName
									.equalsIgnoreCase(HUMAN_COMPONENTS_TAG_NAME)) {
								HumanType humanComponent = readHumanComponent(componentElement);
								components.getHuman().add(humanComponent);
							}
						}
					}
				}
			}
		}
		return components;
	}
	
	private static ApplicationType readApplication(Element applicationElement) {
		ApplicationType application =new ApplicationType();
		for (Node node = applicationElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(APPLICATION_NAME_TAG_NAME)) {
					String appName =getText(nextElement);
					application.setName(appName);
				}
				else if(tagName.equalsIgnoreCase(APPLICATION_VERSION_TAG_NAME)){
					VersionType appVersion =readVersion(nextElement);
					application.setVersion(appVersion);
				}
				else if(tagName.equalsIgnoreCase(APPLICATION_CLASS_TAG_NAME)){
					String appClass =getText(nextElement);
					application.setClazz(appClass);
				}
				else if(tagName.equalsIgnoreCase(APPLICATION_ARTIFACT_ID_TAG_NAME)){
					String appArtifactID=getText(nextElement);
					application.setArtifactID(appArtifactID);
				}
				else if(tagName.equalsIgnoreCase(APPLICATION_LOCATION_TAG_NAME)){
					String location = getText(nextElement);
					application.setLocation(location);
				}
			}
		}
		return application;
	}
	
	private static HardwareType readHardwareComponent(Element hardwareElement) {
		HardwareType hardware = new HardwareType();
		for (Node node = hardwareElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(HARDWARE_NAME_TAG_NAME)) {
					String name = getText(nextElement);
					hardware.setName(name);
				} else if (tagName.equalsIgnoreCase(HARDWARE_CLASS_TAG_NAME)) {
					String clazz = getText(nextElement);
					hardware.setClazz(clazz);
				} else if (tagName
						.equalsIgnoreCase(HARDWARE_ARTIFACT_ID_TAG_NAME)) {
					String artifactID = getText(nextElement);
					hardware.setArtifactID(artifactID);
				} else if (tagName.equalsIgnoreCase(HARDWARE_CATEGORY_TAG_NAME)) {
					String category = getText(nextElement);
					hardware.setCategory(category);
				}
			}
		}
		return hardware;
	}

	private static HumanType readHumanComponent(Element humanTypeElement) {
		HumanType humanType =new  HumanType();
		for (Node node = humanTypeElement.getFirstChild(); node != null; node = node
				.getNextSibling()) {
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element nextElement = (Element) node;
				String tagName = nextElement.getTagName();
				if (tagName.equalsIgnoreCase(HUMAN_ROLE_TAG_NAME)) {
					String role =getText(nextElement);
					humanType.setRole(role);
				}
				else if(tagName.equalsIgnoreCase(HUMAN_COMPANY_TAG_NAME)){
					String company =getText(nextElement);
					humanType.setCompany(company);
				}
				else if(tagName.equalsIgnoreCase(HUMAN_PHONE_TAG_NAME)){
					String phone =getText(nextElement);
					humanType.setPhone(phone);
				}
				else if(tagName.equalsIgnoreCase(HUMAN_REFID_TAG_NAME)){
					String refID=getText(nextElement);
					humanType.setRefID(refID);
				}
				else if(tagName.equalsIgnoreCase(HUMAN_FREQUENCY_NAME)){
					String frequency = getText(nextElement);
					humanType.setFrequency(frequency);
				}
				else if(tagName.equalsIgnoreCase(HUMAN_QUALIFICATION_TAG_NAME)){
					String qualification=getText(nextElement);
					humanType.setQualification(qualification);
				}
				else if(tagName.equalsIgnoreCase(HUMAN_EXPERIENCE_TAG_NAME)){
					String experience = getText(nextElement);
					humanType.setExperience(experience);
				}
			}
		}
		return humanType;
	}

	private static String getText(Node parent) {
		String result = null;
		for (Node node = parent.getFirstChild(); node != null; node = node.getNextSibling()) {
			if (node.getNodeType() != Node.TEXT_NODE) {
				continue;
			}
			result = node.getNodeValue().trim();
			break;
		}

		return result;
	}
	
	
//    private ObjectFactory objectFactory = new  ObjectFactory();
//    
//    /**
//    * Converts given XML to instances of classes made before.
//    *
//    * @param file Input XML file
//    * @throws ParserConfigurationException
//    * @throws SAXException
//    * @throws IOException
//    * @throws XPathExpressionException
//    */
//    public void parse(File file) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
//        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
//        documentFactory.setNamespaceAware(true);
//        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
//        Document doc = documentBuilder.parse(file);
//        
//        XPathFactory xPathFactory = XPathFactory.newInstance();
//        XPath xpath = xPathFactory.newXPath();
//        XPathExpression expr = xpath.compile("/*");
//        Object result = expr.evaluate(doc, XPathConstants.NODESET);
//        
//        NodeList nodes = (NodeList) result;
//        if (nodes.getLength() > 1){
//            throw new SAXException("XML has more than one root element");
//        }
//        if (!(nodes.item(0).getNodeName().equals("aal-usrv"))){
//            throw new SAXException("XML doesn't match given schema");
//        }
//        
//        Node root = nodes.item(0);
//        AalUsrv aalUsrvInstance = new AalUsrv();
//    }
//    
//    private NodeList underElements(Node node) throws XPathExpressionException {
//        XPathFactory xPathFactory = XPathFactory.newInstance();
//        XPath xpath = xPathFactory.newXPath();
//        XPathExpression expr = xpath.compile("*");
//        Object result = expr.evaluate(node, XPathConstants.NODESET);
//        
//        NodeList nodes = (NodeList) result;
//        
//        return nodes;
//    }


}