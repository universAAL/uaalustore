/*
 * Copyright (c) Prosyst 2013. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.universaal.aal_usrv.matcher;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

import org.universAAL.ontology.profile.AALAppSubProfile;
import org.universAAL.ontology.profile.AALSpaceProfile;
import org.universAAL.ontology.profile.HWSubProfile;
import org.universAAL.middleware.rdf.Resource;
import org.universaal.aal_uapp.v1_0.LogicalCriteriaType;
import org.universaal.aal_uapp.v1_0.LogicalRelationType;
import org.universaal.aal_uapp.v1_0.ReqAtomType;
import org.universaal.aal_uapp.v1_0.ReqGroupType;
import org.universaal.aal_uapp.v1_0.ReqType;
import org.universaal.aal_uapp.v1_0.VersionType;
import org.universaal.aal_usrv.v1_0.AalUsrv;
import org.universaal.aal_usrv.v1_0.AalUsrv.Components;
import org.universaal.aal_usrv.v1_0.AalUsrv.ServiceRequirements;
import org.universaal.aal_usrv.v1_0.ApplicationType;
import org.universaal.aal_usrv.v1_0.HardwareType;

/**
 * This class holds the implementation of a service, which purpose is to match
 * the service requirements of an AAL service according to its components and
 * a given {@link org.universAAL.ontology.profile.AALSpaceProfile} and to find
 * the service requirements satisfaction level.
 * 
 * @author
 */
public class ServiceMatcher {
	
	/**
	 * Matches the service requirements of an AAL service by parsing its xml
	 * description and returns its requirements satisfaction level.
	 * 
	 * @param usrvXML the xml file, describing the AAL service
	 * @param spaceProfile the AAL space profile, which capabilities will be matched 
	 * @return the service requirements satisfaction level
	 * @throws Exception
	 */
	public static float matchServiceRequirements(InputStream usrvXML,
			AALSpaceProfile spaceProfile) throws Exception {

		AalUsrv aalUsrv = AALUsrvParser.parse(usrvXML);
		List<Resource> resources = initResources(aalUsrv, spaceProfile);
		ServiceRequirements requirements = aalUsrv.getServiceRequirements();
		List<ReqType> reqTypes = requirements.getRequirement();
		if (reqTypes != null) {
			ReqGroupType rootGroup = new ReqGroupType();
			rootGroup.setLogicalRelation(LogicalRelationType.AND);
			for (ReqType requirement : reqTypes) {
				rootGroup.getRequirement().add(requirement);
			}
			return matchGroup(rootGroup,  resources, spaceProfile);
		}
		return 0;
	}
	
	/**
	 * Checks if the group is composed of atomic requirements only or it
	 * contains at least one subgroup.
	 * 
	 * @param reqGroup
	 *            ReqGroupType object to be checked
	 * @return true if there is at least one subgroup
	 */
	private static boolean isMixedGroup(ReqGroupType reqGroup){
		if(reqGroup!=null){
			List<ReqType> requirements = reqGroup.getRequirement();
			for(ReqType requirement: requirements){
				Object type = requirement.getType();
				if(type instanceof ReqGroupType){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Matches a group of requirements to the capabilities, provided by list of
	 * resources, depending on whether it is group of atomic requirements only
	 * or not.
	 * 
	 * @param reqGroup group of requirements to be matched to the
	 *            capabilities, provided by the resources
	 * @param resources list of resources, providing capabilities to be matched
	 * @param spaceProfile the main resource
	 * @return the satisfaction level of the requirements group - real number between 0 and 1
	 */
	private static float matchGroup(ReqGroupType reqGroup, List<Resource> resources, AALSpaceProfile spaceProfile) {		
		if(isMixedGroup(reqGroup)){
			return matchMixedGroup(reqGroup, resources, spaceProfile);		
		}
		else{	
			return matchGroupOfAtoms(reqGroup, resources, spaceProfile);
		}
	}
	
	/**
	 * Returns the satisfaction level of a requirements group, composed of at
	 * least one other subgroup, according to the capabilities, provided by the
	 * resources.
	 * 
	 * The first step is for every requirement to be computed its satisfaction
	 * level by applying the appropriate computation according its type - atomic
	 * or group. If it is an atomic requirement, for every resource, including
	 * the space profile, the requirement satisfaction level is computed and the
	 * maximum one is chosen as resources-requirement result. After computing
	 * every requirement satisfaction level (if the operator is AND or NONE, it
	 * is taken into account whether the requirement is optional or not) the
	 * logical relation operator is applied over requirements result.
	 * 
	 * @param reqGroup
	 *            group of requirements to be matched to the capabilities,
	 *            provided by the resources
	 * @param resources
	 *            list of resources, providing capabilities
	 * @param spaceProfile
	 *            the main resource
	 * @return the satisfaction level of the requirements group - real number
	 *         between 0 and 1
	 */
	private static float matchMixedGroup(ReqGroupType reqGroup,
			List<Resource> resources, AALSpaceProfile spaceProfile) {
		
		List<Resource> allResources = new ArrayList<Resource>(resources);
		allResources.add(spaceProfile);
		
		float groupResult = 0;

		if (reqGroup != null) {
			List<ReqType> requirements = reqGroup.getRequirement();
			LogicalRelationType reqRelation = reqGroup.getLogicalRelation();
			if (reqRelation.equals(LogicalRelationType.OR)) {
				List<Float> weights = new ArrayList<Float>();

				for (ReqType requirement : requirements) {
					Object type = requirement.getType();
					float weight = 0;

					if (type instanceof ReqAtomType) {
						
					
						ReqAtomType reqAtom = (ReqAtomType) type;
						for (Resource resource : allResources) {
							if (matchReqAtom(reqAtom, resource)) {
								weight = 1;
								break;
							}
						}
					} else {
						ReqGroupType group = (ReqGroupType) type;
						weight = matchGroup(group, resources, spaceProfile);
					}
					weights.add(weight);
				}

				groupResult = Collections.max(weights);

			} else if (reqRelation.equals(LogicalRelationType.AND)
					|| reqRelation.equals(LogicalRelationType.NONE)) {
				List<Float> optionalWeights = new ArrayList<Float>();
				List<Float> nonOptionalWeights = new ArrayList<Float>();

				for (ReqType requirement : requirements) {
					Object type = requirement.getType();
					float weight = 0;

					if (type instanceof ReqAtomType) {
						ReqAtomType reqAtom = (ReqAtomType) type;
						for (Resource resource : allResources) {
							if (matchReqAtom(reqAtom, resource)) {
								weight = 1;
								break;
							}
						}

						if (reqRelation.equals(LogicalRelationType.NONE)) {
							weight = 1 - weight;
						}
					} else {
						ReqGroupType group = (ReqGroupType) type;
						weight = matchGroup(group, resources, spaceProfile);
					}

					if (requirement.isOptional()) {
						optionalWeights.add(weight);
					} else {
						nonOptionalWeights.add(weight);
					}
				}

				groupResult = requirementsAnd(optionalWeights,
						nonOptionalWeights);
			}
		}
		return groupResult;
	}
	
	/**
	 * Initializes the resources, which will provide capabilities.
	 * 
	 * @param aalUsrv
	 *            AAL service, which application and hardware components will be
	 *            added to the resources list
	 * @param spaceProfile
	 *            AAL space profile, which hardware and applications subprofiles
	 *            will be added to the resources list, the AAL space profile
	 *            itself is not added to it
	 * @return list of resources
	 */
	private static List<Resource> initResources(AalUsrv aalUsrv, AALSpaceProfile spaceProfile){
		List<Resource> resources = new ArrayList<Resource>(); 
		
		if (spaceProfile != null) {
			Object hwSubProfileObj = spaceProfile
					.getProperty(AALSpaceProfile.PROP_INSTALLED_HARDWARE);
			if (hwSubProfileObj instanceof HWSubProfile) {
				resources.add((HWSubProfile) hwSubProfileObj);
			} else if (hwSubProfileObj instanceof List<?>) {
				List<HWSubProfile> hwSubProfiles = (List<HWSubProfile>) hwSubProfileObj;
				resources.addAll(hwSubProfiles);
			}

			Object appSubProfileObj = spaceProfile
					.getProperty(AALSpaceProfile.PROP_INSTALLED_SERVICES);
			if (appSubProfileObj instanceof AALAppSubProfile) {
				resources.add((AALAppSubProfile) appSubProfileObj);
			} else if (appSubProfileObj instanceof List<?>) {
				List<AALAppSubProfile> appSubProfiles = (List<AALAppSubProfile>) appSubProfileObj;
				resources.addAll(appSubProfiles);
			}
		}
		
		Components components = aalUsrv.getComponents();
		if(components!=null){
			List<ApplicationType> appComponents = components.getApplication();
			for(ApplicationType appType:appComponents){
				Resource appResource = new Resource();
				appResource.changeProperty(AALAppSubProfile.PROP_ARTIFACT_DESCRIPTION, appType.getName());
				VersionType versionType = appType.getVersion();
				String version = versionType.getMajor()+"."+versionType.getMinor()+"."+versionType.getMicro();
				appResource.changeProperty(AALAppSubProfile.PROP_ARTIFACT_VERSION, version);
				appResource.changeProperty(AALAppSubProfile.PROP_ARTIFACT_GROUP_ID, appType.getClazz());
				appResource.changeProperty(AALAppSubProfile.PROP_ARTIFACT_FILE_URL, appType.getLocation());
				appResource.changeProperty(AALAppSubProfile.PROP_ARTIFACT_ID, appType.getArtifactID());		
				resources.add(appResource);
			}
			List<HardwareType> hwComponents = components.getHardware();
			for(HardwareType hwType:hwComponents){
				Resource hwResource = new Resource();
				hwResource.changeProperty(HWSubProfile.PROP_HW_NAME, hwType.getName());
				hwResource.changeProperty(HWSubProfile.PROP_HW_CATEGORY, hwType.getCategory());
				hwResource.changeProperty(HWSubProfile.PROP_HW_MANUFACTURER, hwType.getClazz());
				hwResource.changeProperty(HWSubProfile.PROP_HW_IDENTIFIER, hwType.getArtifactID());
				resources.add(hwResource);
			}
		}
		
		return resources;
	}
	
	/**
	 * Returns the satisfaction level of an atomic requirements group, according
	 * to the capabilities, provided by the resources.
	 * 
	 * For every resource from the list is calculated the level of its ability
	 * to satisfy the requirements group. The calculation is based on comparison
	 * between a single requirement satisfaction achieved by using the resource
	 * and the one achieved by using the AAL space profile resource, the maximum
	 * result is taken as the resource-requirement result (0 or 1). Next, the
	 * resource-group result is gotten by applying the group logical relation
	 * over the resource-requirement results.The final group result is the
	 * maximum resource-group result - the requirements satisfaction level,
	 * achieved by using the capabilities of the best resource (the highest
	 * satisfaction level).
	 * 
	 * @param groupOfAtoms
	 *            group of atomic requirements to be matched to the
	 *            capabilities, provided by the resources
	 * @param resources
	 *            list of resources, providing capabilities to be matched
	 * @param spaceProfile
	 *            AAL space profile, which requirement satisfaction level result
	 *            will be compared with every resource-requirement result
	 * @return the satisfaction level of the atomic requirements group 
	 */
	private static float matchGroupOfAtoms(ReqGroupType groupOfAtoms, List<Resource> resources,
			AALSpaceProfile spaceProfile) {

		List<ReqType> requirements = groupOfAtoms.getRequirement();
		LogicalRelationType reqRelation = groupOfAtoms.getLogicalRelation();
		
		float max_resource_result = 0;
		if(resources.isEmpty() && spaceProfile==null){
			if(reqRelation.equals(LogicalRelationType.NONE)){
				return 1;
			}
		}
		
		for (Resource resource : resources) {
			if (requirements != null) {
				float resource_result = 0;

				if (reqRelation.equals(LogicalRelationType.OR)) {
					List<Float> weights = new ArrayList<Float>();

					for (ReqType atomReq : requirements) {
						ReqAtomType reqAtom = (ReqAtomType) atomReq.getType();

						float weight = 0;
						if (matchReqAtom(reqAtom, resource)
								|| matchReqAtom(reqAtom, spaceProfile)) {
							weight = 1;
						}
						weights.add(weight);
					}

					resource_result = Collections.max(weights);

				} else if (reqRelation.equals(LogicalRelationType.AND)
						|| reqRelation.equals(LogicalRelationType.NONE)) {
					List<Float> optionalWeights = new ArrayList<Float>();
					List<Float> nonOptionalWeights = new ArrayList<Float>();

					for (ReqType atomReq : requirements) {
						ReqAtomType reqAtom = (ReqAtomType) atomReq.getType();

						float weight = 0;
						if (matchReqAtom(reqAtom, resource)
								|| matchReqAtom(reqAtom, spaceProfile)) {
							weight = 1;
						}

						if (reqRelation.equals(LogicalRelationType.NONE)) {
							weight = 1 - weight;
						}

						if (atomReq.isOptional()) {
							optionalWeights.add(weight);
						} else {
							nonOptionalWeights.add(weight);
						}
					}

					resource_result = requirementsAnd(optionalWeights,
							nonOptionalWeights);
				}

				if (resource_result > max_resource_result) {
					max_resource_result = resource_result;
				}
			}
		}

		return max_resource_result;
	}
	
	
	/**
	 * Computes the group satisfaction level after applying the AND logical
	 * relation operator.
	 * 
	 * @param optionalReqsWeights
	 *            list of the optional requirements satisfaction level
	 * @param nonOptionalReqsWeights
	 *            list of the non-optional requirements satisfaction level
	 * @return the group satisfaction level after applying the AND logical
	 *         relation operator
	 */
	private static float requirementsAnd(List<Float> optionalReqsWeights, List<Float> nonOptionalReqsWeights){
		float nonOptMin = 1;
		int nonOptCount = nonOptionalReqsWeights.size();
		float optSum = 0;
		int totalCount = optionalReqsWeights.size()+nonOptCount;
		
		if(nonOptionalReqsWeights!=null){
			if(!nonOptionalReqsWeights.isEmpty()){
				nonOptMin=Collections.min(nonOptionalReqsWeights);
			}
		}
		
		if (optionalReqsWeights != null) {
			for (Float weight : optionalReqsWeights) {
				optSum += weight;
			}
		}
		return nonOptMin * (nonOptCount + optSum) / totalCount;	
	}
	
	
	/**
	 * Matches an atomic requirement to capabilities, provided by a
	 * {@link org.universAAL.middleware.rdf.Resource} object.
	 * 
	 * @param reqAtom
	 *            atomic requirement to be matched
	 * @param resource
	 *            resource, providing capabilities
	 * @return true if the atomic requirement is satisfied
	 */
	private static boolean matchReqAtom(ReqAtomType reqAtom,
			Resource resource) {
		String atomName = reqAtom.getReqAtomName();
		String atomValue = reqAtom.getReqAtomValue();
		
		LogicalCriteriaType criteria = reqAtom.getReqCriteria();

		Object resourceValueObj = (resource!=null) ? resource.getProperty(atomName) : null;

		if (resourceValueObj == null) {
			if(LogicalCriteriaType.NOT.equals(criteria)|| LogicalCriteriaType.LESS.equals(criteria) || LogicalCriteriaType.LESS_EQUAL.equals(criteria)){
				return true;
			}
		} else {
			if (resourceValueObj instanceof String) {
				String resourceValue = (String) resourceValueObj;
				
				if (criteria == null) {
					if(atomName.contains("version")){
						return longVersion(resourceValue)==longVersion(atomValue) ? true : false;
					}
							
					return (resourceValue.equalsIgnoreCase(atomValue)) ? true
							: false;
				}
				switch (criteria) {
				case NOT:
					if(atomName.contains("version")){
						return longVersion(resourceValue)==longVersion(atomValue) ? false : true;
					}
					return (resourceValue.equalsIgnoreCase(atomValue)) ? false : true;
				case EQUAL:
					if(atomName.contains("version")){
						return longVersion(resourceValue)==longVersion(atomValue) ? true : false;
					}
					return (resourceValue.equalsIgnoreCase(atomValue)) ? true : false;
				case GREATER:
					if(atomName.contains("version")){
						return longVersion(resourceValue)>longVersion(atomValue) ? true : false;
					}
					return (resourceValue.compareTo(atomValue) > 0) ? true : false;
				case GREATER_EQUAL:
					if(atomName.contains("version")){
						return longVersion(resourceValue)>=longVersion(atomValue) ? true : false;
					}
					return (resourceValue.compareTo(atomValue) >= 0) ? true : false;
				case LESS:
					if(atomName.contains("version")){
						return longVersion(resourceValue)<longVersion(atomValue) ? true : false;
					}
					return (resourceValue.compareTo(atomValue) < 0) ? true : false;
				case LESS_EQUAL:
					if(atomName.contains("version")){
						return longVersion(resourceValue)<=longVersion(atomValue) ? true : false;
					}
					return (resourceValue.compareTo(atomValue) <= 0) ? true : false;
				case CONTAIN:
					return (resourceValue.contains(atomValue)) ? true : false;
				case DOESN_NOT_CONTAIN:
					return (!resourceValue.contains(atomValue)) ? true : false;
				case BEGIN:
					return (resourceValue.startsWith(atomValue)) ? true : false;
				case END:
					return (resourceValue.endsWith(atomValue)) ? true : false;
				default:
					return false;
				}
			} else if (resourceValueObj instanceof List<?>) {
				List<String> resourceValues = (List<String>) resourceValueObj;
				if (resourceValues != null) {

					if (criteria == null) {
						for (String resourceValue : resourceValues) {
							if(atomName.contains("version")){
								if (longVersion(resourceValue) == longVersion(atomValue)){
									return true;
								}
							}
							if (resourceValue.equalsIgnoreCase(atomValue)) {
								return true;
							}
						}
					}
					switch (criteria) {
					case NOT:
						for (String resourceValue : resourceValues) {
							if(atomName.contains("version")){
								if (longVersion(resourceValue) == longVersion(atomValue)){
									return false;
								}
							}
							if (resourceValue.equalsIgnoreCase(atomValue)) {
								return false;
							}
						}
						return true;
					case EQUAL:
						for (String resourceValue : resourceValues) {
							if(atomName.contains("version")){
								if (longVersion(resourceValue) == longVersion(atomValue)){
									return true;
								}
							}
							if (resourceValue.equalsIgnoreCase(atomValue)) {
								return true;
							}
						}
						return false;
					case GREATER:
						for (String resourceValue : resourceValues) {
							if(atomName.contains("version")){
								if (longVersion(resourceValue) > longVersion(atomValue)){
									return true;
								}
							}
							if (resourceValue.compareTo(atomValue) > 0) {
								return true;
							}
						}
						return false;
					case GREATER_EQUAL:
						for (String resourceValue : resourceValues) {
							if(atomName.contains("version")){
								if (longVersion(resourceValue) >= longVersion(atomValue)){
									return true;
								}
							}
							if (resourceValue.compareTo(atomValue) >= 0) {
								return true;
							}
						}
						return false;
					case LESS:
						for (String resourceValue : resourceValues) {
							if(atomName.contains("version")){
								if (longVersion(resourceValue) < longVersion(atomValue)){
									return true;
								}
							}
							if (resourceValue.compareTo(atomValue) < 0) {
								return true;
							}
						}
						return false;
					case LESS_EQUAL:
						for (String resourceValue : resourceValues) {
							if(atomName.contains("version")){
								if (longVersion(resourceValue) <= longVersion(atomValue)){
									return true;
								}
							}
							if (resourceValue.compareTo(atomValue) <= 0) {
								return true;
							}
						}
						return false;
					case CONTAIN:
						for (String resourceValue : resourceValues) {
							if (resourceValue.contains(atomValue)) {
								return true;
							}
						}
						return false;
					case DOESN_NOT_CONTAIN:
						for (String resourceValue : resourceValues) {
							if (resourceValue.contains(atomValue)) {
								return false;
							}
						}
						return true;
					case BEGIN:
						for (String resourceValue : resourceValues) {
							if (resourceValue.startsWith(atomValue)) {
								return true;
							}
						}
						return false;
					case END:
						for (String resourceValue : resourceValues) {
							if (resourceValue.endsWith(atomValue)) {
								return true;
							}
						}
						return false;
					}
				}

			}
		}

		return false;
	}
	
	/**
	 * Converts version from the form {major}.{minor}.{micro}.{qualifier} to long. 
	 * 
	 * @param version in the form {major}.{minor}.{micro}.{qualifier} to be converted to long 
	 * @return version as long
	 */
	private static long longVersion(String version) {
		if (version == null || version.equals("")) {
			return 0L;
		}
		StringTokenizer parser = new StringTokenizer(version, ".");
		int count = parser.countTokens();
		if (count > 4) {
			count = 4;
		}
		StringBuffer encoded = new StringBuffer();
		for (int i = 0; i < 4; i++) {
			String s = i < count ? parser.nextToken() : "0000";
			try {
				Integer.parseInt(s);
			} catch (NumberFormatException nfExc) {
				encoded.append("0000");
				break;
			}
			int vl = s.length();
			if (vl > 4) {
				encoded.append(s.substring(0, 4));
			} else {
				for (int j = 4; j > vl; j--) {
					encoded.append('0');
				}
				encoded.append(s);
			}
		}
		return Long.parseLong(encoded.toString());
	}
}
