<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<script>
	var cookieString = "UstoreTermsAndConditions";
	
	function setCookie() {

		// set cookie
		cookieValue = "Agreed";
		var exdate = new Date();
		exdate.setDate(exdate.getDate() + 3650);
		var c_value = escape(cookieValue)
				+  "; expires=" + exdate.toUTCString();
		document.cookie = cookieString + "=" + c_value + ";path=/";

		// find IP. Save it in a DB?
		var params = window.location.href.split("?")[1];
		var ip = decodeURIComponent(params.split("&")[0]);
		
		// redirect back to original 
		var encoded = params.split("&")[1];
		if (encoded != null && encoded != "") {
			window.location.href = decodeURIComponent(encoded);
		}		
	}
</script>

<body style="font-family: Verdana, Arial, Verdana, Helvetica, sans-serif; font-size:12px;">

	<H1 align="center">Terms of Use for uStore Website</H1>
	<br>
	
	<p>
	The following are terms and conditions ("Terms of Use") constituting a legal agreement between you 
	(or anyone using your computer or account) and the member-parties of the EU FP7 Consortium entitled 
	"UNIVERSAAL", who are co-owning this Website, (see list of each and all of the jointly and severally 
	bound member parties : http://www.universaal.org/index.php/en/about/consortium) (hereinafter together 
	referred to as "UNIVERSAAL") in whose name of and on whose behalf  IBM (Israel) Science And Technology 
	Ltd. ("IBM") as one of the member-parties of UNIVERSAAL  has been upon joint consent authorized to 
	technically control and host this Website in its capacity of delegated uStore Website service provider. 
	</p>
	
	<p>
	By accessing, browsing, or using the the uStore Prototype Website (hereinafter this "Website"), you 
	acknowledge that you have fully read, understood, and agreed to be bound by these terms and to comply 
	with all applicable laws and regulations, including export and re-export control laws and regulations. 
	If you do not agree to these terms, please do not access or use this Website.
	</p>
	
	<p>
	By using this Website, you represent and warrant that (a) all registration information you submit to create 
	an individual Account is truthful and accurate; (b) you will maintain the accuracy of such information; (c) 
	you are 18 years of age or older; and (d) your use of this Website does not violate any applicable laws 
	including the laws of the place where you have your domicile or your habitual residence.. 
	</p> 
	
	<p>
	This Website is a mere electronic platform that allows or may allow in the future interactions or transactions 
	via this Website between partners and/or members registered on this Website. UNIVERSAAL does not own nor can 
	be deemed to be an agent who is entitled to contract for any product or services listed on the Website. As a 
	result, UNIVERSAAL has no control over the truth, accuracy of quality, safety or legality of the products or 
	services individually offered or advertised or the advertisements. By no means or any circumstances can UNIVERSAAL 
	be deemed to be involved in an actual transaction concluded between an authorized partner-vendor and a registered 
	user as a result of accessing this Website nor can it become a party to such transaction or contract. UNIVERSAAL 
	cannot ensure that advertisers will actually complete a transaction. 
	</p>
	
	<p>
	No one other than a registered party to this Agreement shall have any right to enforce any of its terms against 
	UNIVERSAAL or ANY UNIVERSAAL MEMBER-PARTY.
	</p>
	
	<p>
	UNIVERSAAL may, without prior notice to you, at any time revise these Terms of Use and any other information 
	contained in this Website by updating this posting and these shall be accepted by your continued registration 
	and/or use. As this Website is currently intended as a prototype/beta version, UNIVERSAAL may also make improvements 
	or changes in the information, products, services, or programs described in this Website which all are offered on an 
	"as is" basis at any time without prior notice other than through this particular subpage of the Website. For this 
	reason please make sure that you regularly check these Terms of Use to ensure you are aware of any changes. Your 
	continued use of this Website after changes are posted means you agree to be bound by these conditions as updated 
	and/or amended. 
	</p>
	
	<H2 align="left"">General</H2>
	<br>
	
	<p>
	All information on the Website, unless otherwise explicitly stated, is owned by or licensed to UNIVERSAAL.The Website’s 
	content, texts, photos, graphics, illustrations, forms, lay-out, logos, icons, html code, databases, structure, choice, 
	arrangement of the various sections, organisation of data  and the names, images and logos identifying the Website, 
	UNIVERSAAL or third parties and their products and services, are protected by intellectual property rights, including 
	copyright, trade names, trademarks and the database rights of UNIVERSAAL and/or third parties who have contracted with 
	UNIVERSAAL. 
	</p> 

	<p>
	This Website and all content in this Website may not be copied, reproduced, republished, uploaded, posted, transmitted, 
	distributed, or used for the creation of derivative works without UNIVERSAAL's prior written consent, in any form or in any 
	manner, whether electronically or mechanically, except that UNIVERSAAL grants you non-exclusive, non-transferable, limited 
	permission to access and display the Web pages within this Website, solely on your computer and for your personal, 
	non-commercial use of this Website. Any commercial use of the content of the site is strictly prohibited. This permission 
	is conditioned on your not modifying the content displayed on this Website, your keeping intact all copyright, trademark, 
	and other proprietary notices, and your acceptance of these Terms of Use and any other or additional relevant terms, 
	conditions, and notices accompanying the content or otherwise set forth in this Website. 
	</p> 

	<p>
	Notwithstanding the foregoing, any software and other materials that are made available for downloading, access, or other 
	use from this Website with their own, separate license terms, conditions, and notices will be solely governed by such 
	express terms, conditions, and notices for any such parts or elements made accessible or linked via this Website.
	</p>

	<p>
	UNIVERSAAL shall not be liable towards users of the Website for any claim or action brought against them by a third 
	party who claims that the use of any of the content, products or services accessible on this Website infringes that 
	third party's intellectual property rights. Your failure to comply with the terms, conditions, and notices on this Website 
	will result in automatic termination of any rights granted to you, without prior notice, and you must immediately destroy 
	all copies of downloaded materials in your possession or control. Except for the limited permission in the preceding 
	paragraph, UNIVERSAAL does not grant you any express or implied rights or licenses under any patents, trademarks, 
	copyrights, or other proprietary or intellectual property rights. You may not mirror any of the content from this 
	Website on another web site or in any other media.
	</p>

	<p>
	You acknowledge that access to the Website, serving as a prototype of the future uStore, may be interrupted or withdrawn 
	for reasons outside UNIVERSAAL’s control. 
	</p> 

	<H2 align="left"">Disclaimers</H2>
	<br>

	<p>
	Although reasonable care shall be taken hereto, information on this Website is provided "AS IS" and on an "IS AVAILABLE" basis, 
	and as a result thereof is not promised or guaranteed to be correct, current, or complete, and this Website may contain 
	technical inaccuracies or typographical errors. UNIVERSAAL assumes no responsibility (and expressly disclaims responsibility) 
	for updating this Website to keep information current or to ensure the accuracy or completeness of any posted information. 
	Accordingly, you should confirm the accuracy and completeness of all posted information before making any decision related 
	to any services, products, or other matters described in this Website.
	</p>

	<p>
	You acknowledge and agree that the provision of any data, services or other matters in this Website does not constitute 
	the practice of medicine in any way. It is not the purpose of the Website to advice, to practice, or to assist you in, 
	any medical professional or any other person in the practice of, medicine; please consult an authorized medical professional 
	for any medical advice or assistance that you would be seeking for.
	</p>

	<p>
	UNIVERSAAL, including all the parties in UniversAAL consortium and their affiliates, will not be liable for any injury or 
	damages arising out of the use of any materials or other information found on the Website. 
	</p>
	
	<p>
	You represent and warrant that you will not practice medicine, nor will you make any medical-related decision, based on 
	any information provided on the Website; and you agree to defend, hold harmless (against all liability, demands, claims, 
	costs, losses, damages, recoveries, settlements and expenses) and indemnify UniversAAL, including all the parties in 
	UniversAAL consortium and their affiliates, against all resulting liability, loss, damage and/or expense if any third 
	party makes a claim against any of them based on an actual or alleged breach of such representation ,warranty,or any breach 
	or non-compliance by you of these Terms of use and any claims for injury or damage to person or property arising from a 
	transaction to which you are a party concluded (in)directly via the use of this Website.
	</p> 

	<H2 align="left"">Confidential and Personal information</H2>
	<br>

	<p>
	UNIVERSAAL does not want to receive confidential or proprietary information or personal health related information 
	(whether or not covered by medical secrecy) from you through this Website. Please note that any information or material 
	sent to UNIVERSAAL will be deemed NOT to be confidential. By sending UNIVERSAAL any information or material, you grant 
	UNIVERSAAL an unrestricted, irrevocable license to copy, reproduce, publish, upload, post, transmit, distribute, publicly 
	display, perform, modify, create derivative works from, and otherwise freely use, those materials or information. You also 
	agree that UNIVERSAAL is free to use any ideas, concepts, know-how, or techniques that you send us for any purpose. However, 
	we will not release your name or otherwise publicize the fact that you submitted materials or other information to us unless: 
	(a) we obtain your permission to use your name; or (b) we first notify you that the materials or other information you submit 
	to a particular part of this Website will be published or otherwise used with your name on it; or (c) we are required to do 
	so by law. 
	</p> 

	<p>
	By using the Website you represent and warrant that you will not provide any Personal Data through the Website (other than 
	at registration phase). "Personal Data" means any information as defined by Article 2 section (a) of Directive 95/46/EC 
	of the European Parliament and of the Council of 24 October 1995 on the protection of individuals with regard to the 
	processing of personal data and on the free movement of such data. For any such personal data UNIVERSAAL acting as a data 
	controller or its processors will act in compliance with the standards imposed by Directive 95/46/EC (as amened) or any privacy 
	laws of countries which have been recognized offering adequate personal data protection.  
	</p>  

	<H2 align="left"">Business relationships</H2>
	<br>

	<p>
	This Website may provide links or references to non-UNIVERSAAL owned or controlled web sites and resources. 
	</p> 

	<p>
	Our product presentation in the Website store is merely a non-binding invitation to you to order goods or services from 
	the respective party. By submitting the order via the Website you make a binding offer to conclude a purchase contract.
	</p>

	<p>
	UNIVERSAAL makes no representations, warranties, or other commitments whatsoever about any non-UNIVERSAAL owned or controlled 
	web sites or third-party resources that may be referenced, accessible from, or linked to this Website. A link to a non-UNIVERSAAL 
	web site does not mean that UNIVERSAAL endorses the content or use of such web site or its owner. In addition, UNIVERSAAL is not 
	a party to or responsible for any transactions you may enter into with third parties, even if you learn of such parties 
	(or use a link to such parties) from or via this Website. Accordingly, you acknowledge and agree that UNIVERSAAL is not responsible 
	for the availability of such external sites or resources, and is not responsible or liable for any content, services, products, 
	or other materials on or available from those sites or resources.
	</p>

	<p>
	You agree that no joint venture, partnership, employment, or agency relationship exists between you and UNIVERSAAL as a result 
	of your use of this Website. 
	</p> 

	<p>
	You agree that UNIVERSAAL, at its sole discretion, may terminate your access to and/or use of this Website and your account, 
	if any, at any time and without prior notice for any reason. UNIVERSAAL may also in its sole discretion and at any time 
	discontinue providing this Website, or any part thereof, with or without notice. Further, you agree that UNIVERSAAL shall not 
	be liable to you or any third-party for any termination of your access to this Website and/or your Account. 
	</p>

	<H2 align="left"">Linking to this Website</H2>
	<br>

	<p>
	All links to this Website must be approved on beforehand and in writing by UNIVERSAAL, except that UNIVERSAAL consents to 
	links in which the link and the pages that are activated by the link do not: (a) create frames around any page on this Website 
	or use other techniques that alter in any way the visual presentation or appearance of any content within this Website; (b) 
	misrepresent your relationship with UNIVERSAAL; (c) imply that UNIVERSAAL approves or endorses you, your web site, or your 
	service or product offerings; (d) present false or misleading impressions about UNIVERSAAL or its representatives or otherwise 
	damage the goodwill associated with UNIVERSAAL or its representatives’ names or trademarks and (e) resell or redistribute any 
	portion of this Website or its contents or provide access to the foregoing to any third party. As a further condition to being 
	permitted to link to this Website, you agree that UNIVERSAAL may at any time, in its sole discretion, terminate permission to 
	link to this Website. In such event, you agree to immediately remove all links to this Website and to cease using any 
	unauthorized trademark.
	</p>

	<p>
	The inclusion of a third party link within or to this Website does not necessarily imply any association with UNIVERSAAL 
	and the latter cannot be deemed to be endorsing, publishing, permitting or authorizing such websites or materials.
	</p>

	<H2 align="left"">DISCLAIMER OF WARRANTY</H2>
	<br>

	<p>
	USE OF THIS WEBSITE IS AT YOUR SOLE RISK. ALL MATERIALS, INFORMATION, PRODUCTS, SOFTWARE, PROGRAMS, AND SERVICES ARE PROVIDED 
	"AS IS," WITH NO WARRANTIES OR GUARANTEES WHATSOEVER. UNIVERSAAL AND ANY PARTY IN UNIVERSAAL CONSORTIUM OR ITS AFFILIATES 
	(TOGETHER HEREIN: "UNIVERSAAL PARTY")  EXPRESSLY DISCLAIM TO THE FULLEST EXTENT PERMITTED BY LAW ALL EXPRESS, IMPLIED, 
	STATUTORY, AND OTHER WARRANTIES, GUARANTEES, OR REPRESENTATIONS, INCLUDING, WITHOUT LIMITATION, THE WARRANTIES OF MERCHANTABILITY, 
	FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF PROPRIETARY AND INTELLECTUAL PROPERTY RIGHTS. WITHOUT LIMITATION, 
	UNIVERSAAL AND ANY UNIVERSAAL PARTY MAKE NO WARRANTY OR GUARANTEE THAT THIS WEBSITE WILL BE UNINTERRUPTED, TIMELY, SECURE, 
	OR ERROR-FREE.
	</p>

	<p>
	YOU UNDERSTAND AND AGREE THAT IF YOU DOWNLOAD OR OTHERWISE OBTAIN MATERIALS, INFORMATION, PRODUCTS, SOFTWARE, PROGRAMS, OR 
	SERVICES, YOU DO SO AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGES THAT MAY RESULT, 
	INCLUDING LOSS OF DATA OR DAMAGE TO YOUR COMPUTER SYSTEM OR DAMAGE RELATED TO BODILY INJURY (INCLUDING DEATH), TO THE FULLEST 
	EXTENT PERMITTED BY LAW. PUBLIC ORDER OR MANDATORY LAWS IN SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF WARRANTIES, SO 
	THE ABOVE EXCLUSIONS MAY NOT APPLY TO YOU.
	</p>

	<H2 align="left"">LIMITATION OF LIABILITY</H2>
	<br>

	<p>
	IN NO EVENT WILL UNIVERSAAL OR ANY UNIVERSAAL PARTY BE LIABLE TO ANY PARTY FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
	EXEMPLARY OR CONSEQUENTIAL DAMAGES OF ANY TYPE WHATSOEVER RELATED TO OR ARISING FROM THIS WEBSITE OR ANY USE OF THIS WEBSITE, 
	OR OF ANY SITE OR RESOURCE LINKED TO, REFERENCED, OR ACCESSED THROUGH THIS WEBSITE (INCLUDING WITHOUT LIMITATION ANY ENGINE 
	AND/OR DATABASE), OR FOR THE USE OR DOWNLOADING OF, OR ACCESS TO, ANY MATERIALS, INFORMATION, PRODUCTS, OR TRANSACTIONS CONCLUDED 
	VIA THIS WEBSITE, OR SERVICES OFFERED OR RECEIVED VIA THIS WEBSITE, INCLUDING, WITHOUT LIMITATION, ANY LOST PROFITS, BUSINESS 
	INTERRUPTION, LOST SAVINGS OR LOSS OF PROGRAMS OR OTHER DATA, EVEN IF UNIVERSAAL OR ANY UNIVERSAAL PARTY IS EXPRESSLY ADVISED 
	OF THE POSSIBILITY OF SUCH DAMAGES. THIS EXCLUSION AND WAIVER OF LIABILITY APPLIES TO ALL CAUSES OF ACTION, WHETHER BASED ON 
	CONTRACT, WARRANTY, TORT, OR ANY OTHER LEGAL THEORIES.
	</p>

	<p>
	Additional or different terms, conditions, and notices may apply to specific materials, information, products, software, 
	and services offered through this Website whereby in certain cases your co-contracting party will no longer be UNIVERSAAL, 
	but another party as identified therein. In the event of any conflict, such additional or different terms, conditions, and 
	notices will prevail over these Terms of Use. Please consult any such applicable agreement or notice.
	</p>

	<p>
	UNIVERSAAL will not be liable or deemed to be in default for any delay or failure in performance or interruption of the delivery 
	of the Website or parts thereof resulting directly or indirectly from any unforeseeable cause or circumstance beyond its 
	reasonable control, including but not limited to failure of electronic or mechanical equipment or communication lines, 
	telephone or other interconnection problems, power loss, computer viruses, unauthorised access, theft, operator errors, 
	severe weather, earthquakes or natural disasters, strikes or other labour problems (industrial disputes, lock-outs, etc), 
	wars, (cyber)terrorism, riots, civil unrest, governmental acts or restrictions ("fait du Prince"),or shortcoming of suppliers 
	of UNIVERSAAL (hereinafter "Force Majeure event"). It is up to you to take precautions to protect yourself from viruses, worms, 
	trojan horses, and other potentially destructive programs, and to protect your information as you deem appropriate.
	</p>

	<H2 align="left"">GOVERNING LAW AND JURISDICTION</H2>
	<br>

	<p>
	This Agreement and any dispute or claim arising out of or in connection with it or its subject matter or formation 
	(including non-contractual disputes or claims) will be exclusively  governed, construed and interpreted by Belgian law which 
	will apply to relationships between a UNIVERSAAL PARTY and you. The applicability of the United Nations Convention on 
	Contracts for the International Sale of Goods (CISG) is explicitly excluded herein.  When you are a consumer (i.e. acting 
	for a purpose which can be regarded as being outside any trade or profession), you may, without prejudice to this choice 
	of applicable law, also in addition thereto take recourse to the legal protection of applicable mandatory provisions of 
	the country of your habitual residence. 
	</p>

	<p>
	Save as to the exception determined hereinafter, any dispute between a UNIVERSAAL PARTY and you which may arise out of, 
	under, or in connection with this Website, these Terms of Use (as might be amended over time) or any additional terms and 
	conditions or the legal relationship established by them, will be exclusively brought before the competent court in Brussels 
	(Belgium). When you are a consumer domiciled in the European Union/EEA (i.e. European Economic Area), you may submit any 
	dispute against between a  UNIVERSAAL PARTY before the courts in Brussels (Belgium) or before the courts of the place where 
	you have your domicile. When you are a consumer domiciled in the European Union/EEA, UNIVERSAAL PARTY shall be entitled to 
	submit any dispute to the courts of the country of your domicile.
	</p>

	<p>
	Notices to UNIVERSAAL PARTY must be given in writing by letter and sent to: Mr. Joe Gorman, STIFTELSEN SINTEF ICT, S.P. 
	Andersens vei 15 B, 7031 NORDHEIM, NORWAY and to Roni Ram, IBM R&D Labs in Israel, Haifa University Campus, Mount 
	Carmel, Haifa, 3498825, Israel.
	</p>

	<p>All rights reserved, 2013 © UNIVERSAAL</p> 

	<button onclick=setCookie()>I accept</button>

</body>
</html>
