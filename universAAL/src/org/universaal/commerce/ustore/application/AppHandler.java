/*	
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.application;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.utilities.CatalogUtilities;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.ibm.commerce.catalog.facade.datatypes.CatalogDescriptionType;
import com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType;
import com.ibm.commerce.catalog.objects.CatalogEntryRelationAccessBean;

/**
 * This class retrieves and extracts information from an application file.
 *  
 * @author roni
 */
public class AppHandler 
{
	/**
	 * The name of the config directory in a uapp file
	 */
	private static final String CONFIG_DIR = "config";

	/**
	 * The name of the doc directory in a uapp file
	 */
	private static final String DOC_DIR = "doc";
	
	/**
	 * The name of the elements in a uapp configuration file
	 */
	public static final String UAAP_ROOT_ELEMENT = "aal-uapp";
	public static final String UAAP_APP_ELEMENT = "app";
	public static final String UAAP_APP_REQUIREMENTS_ELEMENT = "applicationRequirements";
	public static final String UAAP_APP_PARTS_ELEMENT = "applicationPart";
	public static final String UAAP_APP_CAPABILITIES_ELEMENT = "applicationCapabilities";
	public static final String UAAP_APP_LICENSES_ELEMENT = "licenses";
	public static final String UAAP_APP_NAME_ELEMENT = "name";
	public static final String UAPP_APP_DESCRIPTION = "description";
	public static final String UAPP_APP_TAGS = "tags";
	public static final String UAAP_APPLICATION_PROVIDER_ELEMENT = "applicationProvider";
	public static final String UAPP_ORGANIZATION_NAME = "organizationName";
	public static final String UAPP_CERTIFICATE = "certificate";
	public static final String UAPP_CONTACT_PERSON = "contactPerson";
	public static final String UAPP_EMAIL = "email";
	public static final String UAPP_WEB_ADDRESS = "webAddress";
	public static final String UAPP_PHONE = "phone";
	public static final String UAAP_APP_REQUIREMENT_ELEMENT = "requirement";
	public static final String UAAP_APP_REQUIREMENT_GROUP_ELEMENT = "reqGroup";
	public static final String UAAP_APP_REQUIREMENT_LOGICAL_ELEMENT = "logicalRequirement";
	public static final String UAAP_APP_REQUIREMENT_ATOM_ELEMENT = "reqAtom";
	public static final String UAAP_APP_REQUIREMENT_ATOM_NAME_ELEMENT = "reqAtomName";
	public static final String UAAP_APP_REQUIREMENT_ATOM_VALUE_ELEMENT = "reqAtomValue";
	public static final String UAAP_APP_LICENSE_ELEMENT = "license";
	public static final String UAAP_APP_SLA_ELEMENT = "sla";
	public static final String UAAP_APP_LICENSE_CATEGORY_ELEMENT = "category";
	public static final String UAAP_APP_LICENSE_NAME_ELEMENT = "name";
	public static final String UAAP_APP_LICENSE_LINK_ELEMENT = "link";
	public static final String UAAP_APP_CAPABILITY_ELEMENT = "capability";
	public static final String UAAP_APP_CAPABILITY_NAME_ELEMENT = "name";
	public static final String UAAP_APP_CAPABILITY_VALUE_ELEMENT = "value";
	public static final String UAAP_APP_PART_ELEMENT = "part";
	public static final String UAAP_APP_PART_REQUIREMENTS_ELEMENT = "partRequirements";
	public static final String UAAP_APP_PART_BUNDLE_ID_ELEMENT = "bundleId";
	public static final String UAAP_APP_PART_BUNDLE_VERSION_ELEMENT = "bundleVersion";
	
	/**
	 * The keys of the extracted parameters that are stored in the 
	 * mUappConfigParameters map
	 */
	public static final String APP_NAME = "app-name";
	public static final String APP_DESCRIPTION = "app-description";
	public static final String APP_KEYWORDS = "app-keywords";
	public static final String APP_ORGANIZATION_NAME = "app-organization-name";
	public static final String APP_ORGANIZATION_URL = "app-organization-url";
	public static final String APP_ORGANIZATION_CERTIFICATE = "app-organization-certificate";
	public static final String APP_DEVELOPER_NAME = "app-developer-name";
	public static final String APP_DEVELOPER_EMAIL = "app-developer-email";
	public static final String APP_DEVELOPER_PHONE = "app-developer-phone";
	public static final String APP_REQUIREMENTS = "app-requirements";
	public static final String APP_PART_REQUIREMENTS = "app-part-requirements";
	public static final String APP_PART_BUNDLE = "app-part-bundle";
	public static final String APP_LICENSES = "app-licenses";
	public static final String APP_SLA = "app-sla";
	public static final String APP_CAPABILITIES = "app-capabilities";

	/**
	 * The application file as a byte array
	 */
	private byte[] mAppFile = null;
	
	/**
	 * The catalog entry identifier of this application
	 */
	private String mAppId = null;
	
	/**
	 * The application name
	 */
	private String mAppName = null;

	/**
	 * The application version (as stored in the Nexus repository)
	 */
	private String mVersion = null;
	
	/**
	 * The application file type (uaap, zip, jar, ...)
	 */
	private String mFileType = null;

	/**
	 * The uapp configuration file as an XML document
	 */
	private Document mUappConfigFile = null;
	
	/**
	 * The configuration parameters that were extracted from the uapp 
	 * configuration file  
	 */
	private Map mUappConfigParameters = new HashMap();
	
	/**
	 * The license files that are included in the uapp file as byte arrays 
	 * organized in the map by their name
	 */
	private Map mUappLicenseFiles = new HashMap();

	/**
	 * The uapp SLA file as a byte array
	 */
	private byte[] mUappSLAFile = null;
	
	/**
	 * The document files that are included in the uapp file as byte arrays 
	 * organized in the map by their name
	 */
	private Map mUappDocFiles = new HashMap();
	
	/**
	 * Number of application parts that are defined in the uapp configuration
	 * file
	 */
	private int mAppParts = 0;
	
	/**
	 * Extract the information stored in the application file.
	 * 
	 * @param pInputStream
	 * 		The input stream of the application file
	 * 
	 * @throws uAALException
	 * 		If an error has occurred during the file extraction process
	 */
	public AppHandler(InputStream pInputStream)
		throws uAALException
	{
		if(null == pInputStream)
		{
			throw new IllegalArgumentException("ERROR: Invalid input stream");
		}

		try
		{
			mAppFile = getInputStreamAsByteArray(pInputStream);
			extractUaapConfigFile();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve the application file (" + 
					ex.getMessage() + ")");	
		}
		finally
		{
			try
			{
				pInputStream.close();
			}
			catch(IOException ex) {} // ignore
		}
	}
	
	/**
	 * Extract the information stored in the application file that is associated 
	 * with the given item and service.
	 * 
	 * @param pService 
	 * 		The service as a <code>CatalogEntryType</code> 
	 * @param pItem 
	 * 		The application item as a <code>CatalogEntryType</code>
	 *  
	 * @throws uAALException 
	 * 		If an error has occurred during the file extraction process
	 */
	public AppHandler(CatalogEntryType pService, CatalogEntryType pItem)
		throws uAALException
	{
		if(null == pItem || null == pService)
		{
			throw new IllegalArgumentException("ERROR: Invalid input parameters");
		}

		String itemId = pItem.getCatalogEntryIdentifier().getUniqueID();
		String serviceId = pService.getCatalogEntryIdentifier().getUniqueID();
		
		InputStream input = null;
		try
		{
			// retrieve the version and the file name of the application from the 
			// 'FIELD1' and 'FIELD3' columns
			CatalogEntryRelationAccessBean bean = 
				CatalogUtilities.getCatalogEntryRelationAccessBean(itemId, serviceId);
			String fileName = bean.getField1();
			String version = bean.getField3();
	
			if(null == fileName || null == version)
			{
				throw new uAALException("ERROR: Missing file name or version for the application file");
			}
		
			// set the file type
			int index = fileName.lastIndexOf(".");
			if(index != -1)
			{
				mFileType = fileName.substring(index+1, fileName.length());
			}
			
			version = String.valueOf(Double.parseDouble(version));
			
			System.out.println("-->> Retrieve from nexus server (" + 
					itemId + " > " + itemId + " > " + version + ")");
			input = NexusHandler.getInstance().getArtifactFromNexusServer(itemId, itemId, version);

			mAppFile = getInputStreamAsByteArray(input);
			
			mAppName = pItem.getCatalogEntryIdentifier().getExternalIdentifier().getPartNumber();
			mAppId = itemId;
			mVersion = version;
			mUappConfigParameters.put(APP_NAME, mAppName);
			
			if(!pItem.getDescription().isEmpty())
			{
				CatalogDescriptionType description = (CatalogDescriptionType)pItem.getDescription().get(0);
				if(null != description)
				{
					mUappConfigParameters.put(APP_KEYWORDS, description.getKeyword());
					mUappConfigParameters.put(APP_DESCRIPTION, description.getShortDescription());
				}
			}
			
			Map parameters = pItem.getUserData().getUserDataField();
			mUappConfigParameters.put(APP_SLA, (String)parameters.get("appservicelevelagreement"));
			mUappConfigParameters.put(APP_LICENSES, (String)parameters.get("licenses"));
			mUappConfigParameters.put(APP_REQUIREMENTS, (String)parameters.get("requirements"));
			mUappConfigParameters.put(APP_ORGANIZATION_NAME, (String)parameters.get("organizationname"));
			mUappConfigParameters.put(APP_ORGANIZATION_URL, (String)parameters.get("organizationurl"));
			mUappConfigParameters.put(APP_ORGANIZATION_CERTIFICATE, (String)parameters.get("organizationcertificate"));
			mUappConfigParameters.put(APP_DEVELOPER_NAME, (String)parameters.get("developername"));
			mUappConfigParameters.put(APP_DEVELOPER_EMAIL, (String)parameters.get("developeremail"));
			mUappConfigParameters.put(APP_DEVELOPER_PHONE, (String)parameters.get("developerephone"));
			
			extractUaapConfigFile();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve the application file (" + 
					ex.getMessage() + ")");	
		}
		finally
		{
			if(null != input)
			{
				try
				{
					input.close();
				}
				catch(IOException ex) {} // ignore
			}
		}
	}

	/**
	 * Initializes the handler with all the information that can be extracted 
	 * from the uapp file
	 * 
	 * @throws uAALException 
	 * 		If an error has occurred during this initialization process
	 */
	public void init()
		throws uAALException
	{
		extractParametersFromConfigFile();
		extractUappLicenseFiles();
		extractUappSLA();
		extractUappDocFiles();
	}
	
	/**
	 * @return The type of the application (uaap, zip, jar, ...)
	 */
	public String getFileType()
	{
		return mFileType;
	}

	/**
	 * @return The catalog entry identifier of this application
	 */
	public String getAppId()
	{
		return mAppId;
	}

	/**
	 * @return The application name
	 */
	public String getAppName()
	{
		return mAppName;
	}

	/**
	 * @return The application version (as stored in the Nexus repository)
	 */
	public String getVersion()
	{
		return mVersion;
	}
	
	/**
	 * @return The number of application parts that are defined in the uapp 
	 * 		configuration file
	 */
	public int getNumAppParts()
	{
		return mAppParts;
	}
	
	/**
	 * @return A new input stream to the application file.
	 */
	public InputStream getAppFileInputStream()
	{
		if(null != mAppFile)
		{
			return new ByteArrayInputStream(mAppFile);
		}
		return null;
	}

	/**
	 * @return The configuration parameters that were extracted from the uapp 
	 * 		configuration file  
	 */
	public Map getUappConfigParameters()
	{
		return mUappConfigParameters;
	}
	
	/**
	 * Return a new input stream to the application license file. 
	 * 
	 * @param pLicenseName 
	 * 		The name of the license file
	 * 
	 * @return A new input stream to the requested application license file, or
	 *		<code>null</code> if there is no matching license. 
	 */
	public InputStream getUappLicenseInputStream(String pLicenseName)
	{
		if(null != pLicenseName)
		{
			byte[] buffer = (byte[])mUappLicenseFiles.get(pLicenseName);
			if(null != buffer)
			{
				return new ByteArrayInputStream(buffer);
			}
		}
		return null;
	}

	/**
	 * @return The document files that are included in the uapp file as byte 
	 * 		arrays organized in the map by their name
	 */
	public Map getUappDocs()
	{
		return mUappDocFiles;
	}
	
	/**
	 * Extract the uapp configuration file from the application file.
	 * 
	 * @throws uAALException 
	 * 		If an error has occurred during the extraction process
	 */
	private void extractUaapConfigFile()
		throws uAALException
	{
		ZipInputStream in = new ZipInputStream(getAppFileInputStream());

		try
		{
			ZipEntry entry = in.getNextEntry();
			while(null != entry)
			{
				// look for the uaap configuration file
				if(!entry.isDirectory() && entry.getName().startsWith(CONFIG_DIR) && entry.getName().endsWith(".xml"))
				{
					byte[] buffer = getInputStreamAsByteArray(in);
			        DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					InputSource src = new InputSource(new ByteArrayInputStream(buffer));
					
			        Document doc = dBuilder.parse(src);
			        doc.getDocumentElement().normalize();
                    
                    if(null != doc.getDocumentElement() && null != doc.getDocumentElement().getNodeName() &&
                    		doc.getDocumentElement().getNodeName().indexOf(UAAP_ROOT_ELEMENT) != -1)
                    {
        				in.closeEntry();
        				mUappConfigFile = doc;
                    	mFileType = "uapp";	
                    	return;
                    }
				}
				in.closeEntry();
				entry = in.getNextEntry();
			}
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to read the uapp config file (" + ex.getMessage() + ")");	
		}
		finally
		{
			try
			{
				in.close();
			}
			catch(IOException ex) {} // ignore
		}
	}
	
	/**
	 * Extracts the license files if included in the uapp file.
	 * 
	 * @throws uAALException 
	 * 		If an exception has occurred during the extraction process
	 */
	private void extractUappLicenseFiles()
		throws uAALException
	{
		StringBuffer buffer = new StringBuffer();
		
		int counter = 1;
		String licenseName = (String)mUappConfigParameters.get(AppHandler.APP_LICENSES + "-name-" + counter);
		String licenseLink = (String)mUappConfigParameters.get(AppHandler.APP_LICENSES + "-link-" + counter);
		while(null != licenseName && null != licenseLink)
		{
			if(licenseLink.startsWith("file:"))
			{
				String path = licenseLink.substring(5);
				if(path.startsWith("../"))
				{
					path = path.substring(3);
				}
				else if(path.startsWith("//../"))
				{
					path = path.substring(5);
				}
				else
				{
					path = "config/" + path;
				}
				
				ZipInputStream in = new ZipInputStream(getAppFileInputStream());
				try
				{
					ZipEntry entry = in.getNextEntry();
					while(null != entry)
					{
						String path1 = path.replace('/', '\\');
						if(!entry.isDirectory() && (entry.getName().equals(path) || entry.getName().equals(path1)))
						{
							mUappLicenseFiles.put(licenseName, getInputStreamAsByteArray(in));
							
		       				in.closeEntry();
		       				break;
						}
						in.closeEntry();
						entry = in.getNextEntry();
					}
				}
				catch(Exception ex)
				{
					System.out.println("WARNING: Failed to read license file for '" + licenseName + "'");
				}
				finally
				{
					try
					{
						in.close();
					}
					catch(IOException ex) {} // ignore
				}
			}

			String licenseCategory = (String)mUappConfigParameters.get(AppHandler.APP_LICENSES + "-category-" + counter);
			buffer.append(licenseName);
			if(null != licenseCategory && licenseCategory.length() > 0)
			{
				buffer.append(" [" + licenseCategory + "]");
			}
			buffer.append("\n");
			byte[] licenseFile = (byte[])mUappLicenseFiles.get(licenseName);
			if(null != licenseFile)
			{
				buffer.append(new String(licenseFile));
			}
			else
			{
				buffer.append(licenseLink);
			}
			buffer.append("\n\n");

			counter ++;
			licenseName = (String)mUappConfigParameters.get(AppHandler.APP_LICENSES + "-name-" + counter);
			licenseLink = (String)mUappConfigParameters.get(AppHandler.APP_LICENSES + "-link-" + counter);
		}
		mUappConfigParameters.put(AppHandler.APP_LICENSES, buffer.toString());
	}

	/**
	 * Extracts the SLA file if included in the uapp file.
	 * 
	 * @throws uAALException 
	 * 		If an exception has occurred during the extraction process
	 */
	private void extractUappSLA()
		throws uAALException
	{
		StringBuffer buffer = new StringBuffer();
		
		String name = (String)mUappConfigParameters.get(APP_SLA + "-name");
		String link = (String)mUappConfigParameters.get(APP_SLA + "-link");
		if(null != name && null != link)
		{
			if(link.startsWith("file:"))
			{
				String path = link.substring(5);
				if(path.startsWith("../"))
				{
					path = path.substring(3);
				}
				else if(path.startsWith("//../"))
				{
					path = path.substring(5);
				}
				else
				{
					path = "config/" + path;
				}
				
				ZipInputStream in = new ZipInputStream(getAppFileInputStream());
				try
				{
					ZipEntry entry = in.getNextEntry();
					while(null != entry)
					{
						String path1 = path.replace('/', '\\');
						if(!entry.isDirectory() && (entry.getName().equals(path) || entry.getName().equals(path1)))
						{
							mUappSLAFile = getInputStreamAsByteArray(in);
							
		       				in.closeEntry();
		       				break;
						}
						in.closeEntry();
						entry = in.getNextEntry();
					}
				}
				catch(Exception ex)
				{
					System.out.println("WARNING: Failed to read SLA file");
				}
				finally
				{
					try
					{
						in.close();
					}
					catch(IOException ex) {} // ignore
				}
			}
			
			if(null != mUappSLAFile)
			{
				buffer.append(new String(mUappSLAFile));
			}
			else
			{
				buffer.append(link);
			}
			buffer.append("\n\n");
		}
		mUappConfigParameters.put(AppHandler.APP_SLA, buffer.toString());
	}
	
	/**
	 * Extracts the document files if included in the uapp file.
	 * 
	 * @throws uAALException 
	 * 		If an exception has occurred during the extraction process
	 */
	private void extractUappDocFiles()
		throws uAALException
	{
		ZipInputStream in = new ZipInputStream(getAppFileInputStream());
		
		try
		{
			ZipEntry entry = in.getNextEntry();
			while(null != entry)
			{
				if(!entry.isDirectory() && entry.getName().startsWith(DOC_DIR))
				{
					mUappDocFiles.put(entry.getName(), getInputStreamAsByteArray(in));
       				in.closeEntry();
       				break;
				}
				in.closeEntry();
				entry = in.getNextEntry();
			}
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to extract the uapp doc files (" + ex.getMessage() + ")");	
		}
		finally
		{
			try
			{
				in.close();
			}
			catch(Exception ex) {} // ignore
		}
	}

	/**
	 * Extracts parameters of the application from the uapp configuration file 
	 * that is located in the config directory and put them in a 
	 * <code>java.util.Map</code> that holds the extracted parameters with their 
	 * key identifiers.
	 * 
	 * @throws uAALException 
	 * 		If an exception has occurred during the extraction process
	 */
	private void extractParametersFromConfigFile()
		throws uAALException
	{
		if(null == mUappConfigFile)
		{
			throw new uAALException("ERROR: Application config file was not initialized");
		}
		
		NodeList nodes = mUappConfigFile.getDocumentElement().getChildNodes();
		for(int i=0; i<nodes.getLength(); i++)
		{
			Node node = nodes.item(i);
			if(node.getNodeName().indexOf(UAAP_APP_CAPABILITIES_ELEMENT) != -1)
			{
				extractCapabilitiesParametersFromConfigFile(node);
			}
			else if(node.getNodeName().indexOf(UAAP_APP_REQUIREMENTS_ELEMENT) != -1)
			{
				extractRequirementsParametersFromConfigFile(node, APP_REQUIREMENTS);
			}
			else if(node.getNodeName().indexOf(UAAP_APP_PARTS_ELEMENT) != -1)
			{
				extractPartRequirementsParametersFromConfigFile(node);
			}
			else if(node.getNodeName().indexOf(UAAP_APP_ELEMENT) != -1)
			{
				NodeList subnodes = node.getChildNodes();
				for(int j=0; j<subnodes.getLength(); j++)
				{
					Node subnode = subnodes.item(j);
					if(subnode.getNodeName().indexOf(UAAP_APP_NAME_ELEMENT) != -1)
					{
						mUappConfigParameters.put(APP_NAME, subnode.getTextContent());
					}
					else if(subnode.getNodeName().indexOf(UAPP_APP_DESCRIPTION) != -1)
					{
						mUappConfigParameters.put(APP_DESCRIPTION, subnode.getTextContent());
					}
					else if(subnode.getNodeName().indexOf(UAPP_APP_TAGS) != -1)
					{
						mUappConfigParameters.put(APP_KEYWORDS, subnode.getTextContent());
					}
					else if(subnode.getNodeName().indexOf(UAAP_APPLICATION_PROVIDER_ELEMENT) != -1)
					{
						extractApplicationProviderParametersFromConfigFile(subnode);
						
					}
					else if(subnode.getNodeName().indexOf(UAAP_APP_LICENSES_ELEMENT) != -1)
					{
						extractLicensesParametersFromConfigFile(subnode);
						extractSLAParametersFromConfigFile(subnode);
					}
				}
			}
		}
	}
	
	/**
	 * Extracts the application provider parameters from the uapp configuration 
	 * file and put them in a <code>java.util.Map</code> that holds the 
	 * extracted parameters with their key identifiers.
	 * 
	 * @param pProviderNode 
	 * 		The root <code>org.w3c.dom.Node</code> of the application provider 
	 * 		in the uapp configuration file 
	 */
	private void extractApplicationProviderParametersFromConfigFile(Node pProviderNode)
	{
		NodeList subsubnodes = pProviderNode.getChildNodes();
		for(int k=0; k<subsubnodes.getLength(); k++)
		{
			Node subsubnode = subsubnodes.item(k);
			if(subsubnode.getNodeName().indexOf(UAPP_ORGANIZATION_NAME) != -1)
			{
				mUappConfigParameters.put(APP_ORGANIZATION_NAME, subsubnode.getTextContent());
			}
			else if(subsubnode.getNodeName().indexOf(UAPP_CONTACT_PERSON) != -1)
			{
				mUappConfigParameters.put(APP_DEVELOPER_NAME, subsubnode.getTextContent());
			}
			else if(subsubnode.getNodeName().indexOf(UAPP_EMAIL) != -1)
			{
				mUappConfigParameters.put(APP_DEVELOPER_EMAIL, subsubnode.getTextContent());
			}
			else if(subsubnode.getNodeName().indexOf(UAPP_PHONE) != -1)
			{
				mUappConfigParameters.put(APP_DEVELOPER_PHONE, subsubnode.getTextContent());
			}
			else if(subsubnode.getNodeName().indexOf(UAPP_WEB_ADDRESS) != -1)
			{
				mUappConfigParameters.put(APP_ORGANIZATION_URL, subsubnode.getTextContent());
			}
			else if(subsubnode.getNodeName().indexOf(UAPP_CERTIFICATE) != -1)
			{
				mUappConfigParameters.put(APP_ORGANIZATION_CERTIFICATE, subsubnode.getTextContent());
			}
		}
	}

	/**
	 * Extracts the licenses parameters from the uapp configuration file and put 
	 * them in a <code>java.util.Map</code> that holds the extracted parameters 
	 * with their key identifiers.
	 * 
	 * @param pLicensesNode 
	 * 		The root <code>org.w3c.dom.Node</code> of the licenses in the uapp 
	 * 		configuration file 
	 */
	private void extractLicensesParametersFromConfigFile(Node pLicensesNode)
	{
		int counter = 1;
		NodeList licensesNodes = pLicensesNode.getChildNodes();
		for(int z=0; z<licensesNodes.getLength(); z++)
		{
			Node licenseNode = licensesNodes.item(z);
			if(licenseNode.getNodeName().indexOf(UAAP_APP_LICENSE_ELEMENT) != -1)
			{
				NodeList licenseNodes = licenseNode.getChildNodes();
				for(int k=0; k<licenseNodes.getLength(); k++)
				{
					Node tmp = licenseNodes.item(k);
					if(tmp.getNodeName().indexOf(UAAP_APP_LICENSE_CATEGORY_ELEMENT) != -1)
					{
						mUappConfigParameters.put(APP_LICENSES + "-category-" + counter, tmp.getTextContent());
					}
					else if(tmp.getNodeName().indexOf(UAAP_APP_LICENSE_NAME_ELEMENT) != -1)
					{
						mUappConfigParameters.put(APP_LICENSES + "-name-" + counter, tmp.getTextContent());
					}
					else if(tmp.getNodeName().indexOf(UAAP_APP_LICENSE_LINK_ELEMENT) != -1)
					{
						mUappConfigParameters.put(APP_LICENSES + "-link-" + counter, tmp.getTextContent());
					}
				}
				counter ++;
			}
		}				
	}

	/**
	 * Extracts the SLA parameters from the uapp configuration file and put them 
	 * in a <code>java.util.Map</code> that holds the extracted parameters with 
	 * their key identifiers.
	 * 
	 * @param pLicensesNode 
	 * 		The root <code>org.w3c.dom.Node</code> of the SLA in the uapp 
	 * 		configuration file 
	 */
	private void extractSLAParametersFromConfigFile(Node pLicensesNode)
	{
		NodeList licensesNodes = pLicensesNode.getChildNodes();
		for(int z=0; z<licensesNodes.getLength(); z++)
		{
			Node licenseNode = licensesNodes.item(z);
			if(licenseNode.getNodeName().indexOf(UAAP_APP_SLA_ELEMENT) != -1)
			{
				NodeList licenseNodes = licenseNode.getChildNodes();
				for(int k=0; k<licenseNodes.getLength(); k++)
				{
					Node tmp = licenseNodes.item(k);
					if(tmp.getNodeName().indexOf(UAAP_APP_LICENSE_NAME_ELEMENT) != -1)
					{
						mUappConfigParameters.put(APP_SLA + "-name", tmp.getTextContent());
					}
					else if(tmp.getNodeName().indexOf(UAAP_APP_LICENSE_LINK_ELEMENT) != -1)
					{
						mUappConfigParameters.put(APP_SLA + "-link", tmp.getTextContent());
					}
				}
			}
		}				
	}
	
	/**
	 * Extracts the capabilities parameters from the uapp configuration file and 
	 * put them in a <code>java.util.Map</code> that holds the extracted 
	 * parameters with their key identifiers.
	 * 
	 * @param pCapabilitiesNode 
	 * 		The root <code>org.w3c.dom.Node</code> of the capabilities in the 
	 * 		uapp configuration file 
	 */
	private void extractCapabilitiesParametersFromConfigFile(Node pCapabilitiesNode)
	{
		StringBuffer buffer = new StringBuffer();
		int counter = 1;
		NodeList capabilitiessNodes = pCapabilitiesNode.getChildNodes();
		for(int i=0; i<capabilitiessNodes.getLength(); i++)
		{
			Node capabililtyNode = capabilitiessNodes.item(i);
			if(capabililtyNode.getNodeName().indexOf(UAAP_APP_CAPABILITY_ELEMENT) != -1)
			{
				String name = null;
				String value = null;
				NodeList nodes = capabililtyNode.getChildNodes();
				for(int k=0; k<nodes.getLength(); k++)
				{
					Node tmp = nodes.item(k);
					if(tmp.getNodeName().indexOf(UAAP_APP_CAPABILITY_NAME_ELEMENT) != -1)
					{
						name = tmp.getTextContent();
					}
					else if(tmp.getNodeName().indexOf(UAAP_APP_CAPABILITY_VALUE_ELEMENT) != -1)
					{
						value = tmp.getTextContent();
					}
				}
				if(null != name && null != value)
				{
					mUappConfigParameters.put(APP_CAPABILITIES + "-name-" + counter, name);
					mUappConfigParameters.put(APP_CAPABILITIES + "-value-" + counter, value);
					buffer.append(counter + ". " + name + "=" + value + "\n");
				}
				counter ++;
			}
		}			
		mUappConfigParameters.put(APP_CAPABILITIES, buffer.toString());
	}
	
	/**
	 * Extracts the requirements parameters from the uapp configuration file and 
	 * put them in a <code>java.util.Map</code> that holds the extracted 
	 * parameters with their key identifiers initialized with the given prefix.
	 * 
	 * @param pRequirementsNode 
	 * 		The root <code>org.w3c.dom.Node</code> of the requirements in the 
	 * 		uapp configuration file
	 * @param pPrefix 
	 * 		The prefix of the requirements parameters in the configuration 
	 * 		<code>java.util.Map</code>
	 */
	private void extractRequirementsParametersFromConfigFile(
			Node pRequirementsNode, String pPrefix)
	{
		StringBuffer buffer = new StringBuffer();
		int counter = 1;
		NodeList requirementsNodes = pRequirementsNode.getChildNodes();
		for(int j=0; j<requirementsNodes.getLength(); j++)
		{
			Node requirementNode = requirementsNodes.item(j);
			if(requirementNode.getNodeName().indexOf(UAAP_APP_REQUIREMENT_ELEMENT) != -1)
			{
				NodeList requirementAtomsNodes = requirementNode.getChildNodes();
				for(int k=0; k<requirementAtomsNodes.getLength(); k++)
				{
					Node requirementAtomNode = requirementAtomsNodes.item(k);
					if(requirementAtomNode.getNodeName().indexOf(UAAP_APP_REQUIREMENT_ATOM_ELEMENT) != -1)
					{
						String name = null;
						String value = null;
						NodeList requirementAtomNodes = requirementAtomNode.getChildNodes();
						for(int p=0; p<requirementAtomNodes.getLength(); p++)
						{
							Node tmp = requirementAtomNodes.item(p);
							if(tmp.getNodeName().indexOf(UAAP_APP_REQUIREMENT_ATOM_NAME_ELEMENT) != -1)
							{
								name = tmp.getTextContent();
							}
							else if(tmp.getNodeName().indexOf(UAAP_APP_REQUIREMENT_ATOM_VALUE_ELEMENT) != -1)
							{
								value = tmp.getTextContent();
							}
						}
						if(null != name && null != value)
						{
							mUappConfigParameters.put(pPrefix + "-name-" + counter, name);
							mUappConfigParameters.put(pPrefix + "-value-" + counter, value);
							buffer.append(counter + ". " + name + "=" + value + "\n");
						}
					}
				}
				counter ++;
			}
		}				
		mUappConfigParameters.put(pPrefix, buffer.toString());
	}

	/**
	 * Extracts the part requirements parameters from the uapp configuration 
	 * file and put them in a <code>java.util.Map</code> that holds the 
	 * extracted parameters with their key identifiers.
	 * 
	 * @param pNode 
	 * 		The root <code>org.w3c.dom.Node</code> of the part requirements in 
	 * 		the uapp configuration file
	 */
	private void extractPartRequirementsParametersFromConfigFile(Node pNode)
	{
		int counter = 1;
		NodeList partNodes = pNode.getChildNodes();
		for(int i=0; i<partNodes.getLength(); i++)
		{
			Node partNode = partNodes.item(i);
			if(partNode.getNodeName().indexOf(UAAP_APP_PART_ELEMENT) != -1)
			{
				NodeList nodes = partNode.getChildNodes();
				for(int j=0; j<nodes.getLength(); j++)
				{
					Node node = nodes.item(j);
					if(node.getNodeName().indexOf(UAAP_APP_PART_BUNDLE_ID_ELEMENT) != -1)
					{
						String bundleId = node.getTextContent();;
						mUappConfigParameters.put(APP_PART_BUNDLE + "-id-" + counter, bundleId);
					}
					else if(node.getNodeName().indexOf(UAAP_APP_PART_BUNDLE_VERSION_ELEMENT) != -1)
					{
						String bundleVersion = node.getTextContent();;
						mUappConfigParameters.put(APP_PART_BUNDLE + "-version-" + counter, bundleVersion);
					}
					else if(node.getNodeName().indexOf(UAAP_APP_PART_REQUIREMENTS_ELEMENT) != -1)
					{
						extractRequirementsParametersFromConfigFile(node, APP_PART_REQUIREMENTS + "-" + counter);
					}
				}
				counter ++;
				mAppParts ++;
			}
		}
	}
	
	/**
	 * Return the input stream of the file as a byte array.
	 * 
	 * @param pInputStream 
	 * 		The input stream of the file
	 * 
	 * @return A byte array of the input file
	 * 
	 * @throws IOException 
	 * 		If an error has occurred while reading the file
	 */
	private byte[] getInputStreamAsByteArray(InputStream pInput)
		throws IOException
	{
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		byte[] buffer = new byte[2048];
		int len = 0;
        while((len = pInput.read(buffer)) > 0)
        {
            output.write(buffer, 0, len);
        }
        return output.toByteArray();
	}
}