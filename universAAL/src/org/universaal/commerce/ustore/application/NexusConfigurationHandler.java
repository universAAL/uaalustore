/*	
	Copyright 2008-2014 ProSyst AG, http://www.prosyst.com
	
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.application;

import java.io.InputStream;
import java.util.Properties;

import com.ibm.commerce.util.Base64Coder;

/**
 * This class holds the configuration parameters of the Nexus server.
 *  
 * @author i.bakalov@prosyst.com
 * @author roni
 */
public class NexusConfigurationHandler 
{
	/**
	 * An object that is being used as a lock
	 */
	private static Object mLock = new Object();
	
	/**
	 * A number that is being used to generate unique temporary names 
	 */
	private static long mLast = 0;

	/**
	 * The single instance of this class
	 */
	private static NexusConfigurationHandler mInstance = new NexusConfigurationHandler();
	
	/**
	 * The name of the Nexus configuration file 
	 */
	private static final String NEXUS_CFG_FILE = "/nexus.properties";
	
	/**
	 * The URL to access the Nexus repository
	 */
	private String mRepoURL = null;
	
	/**
	 * The REST URL to access the Nexus repository
	 */
	private String mRestURL = null;
	
	/**
	 * The basic encoding for the Nexus repository URL
	 */
	private String mHttpBasic = null;

	/**
	 * The username to access the Nexus server 
	 */
	private String mUser = null;

	/**
	 * The password to access the Nexus server 
	 */
	private String mPass = null;
	
	/**
	 * Constructor.
	 */
	private NexusConfigurationHandler()
	{
		Properties p = new Properties();
		try 
		{
			// load the properties file from the classpath root
			InputStream input = getClass().getResourceAsStream(NEXUS_CFG_FILE);
			if(null != input)
			{
				p.load(input);
			}
		} 
		catch(Exception ex) 
		{
			System.out.println("WARNING: Cannot open Nexus configuration, using default one.");
		}

		String host = p.getProperty("nexus.host", "localhost");
		String port = p.getProperty("nexus.port", "8081");
		String store = p.getProperty("nexus.repository", "UStore");
		mUser = p.getProperty("nexus.user", "admin");
		mPass = p.getProperty("nexus.pass", "admin123");
		mRepoURL = "http://" + host + ':' + port + "/nexus/content/repositories/" + store;
		mRestURL = "http://" + host + ':' + port + "/nexus/service/local/repositories/" + store + "/content/";
		mHttpBasic = "Basic " + Base64Coder.base64Encode(mUser + ':' + mPass);
	}
	
	/**
	 * @return The single instance of the configuration handler
	 */
	public static NexusConfigurationHandler getInstance()
	{
		return mInstance;
	}
	
	/**
	 * @return The username to access the Nexus server 
	 */
	public String getUser()
	{
		return mUser;
	}
	
	/**
	 * @return The password to access the Nexus server 
	 */
	public String getPassword()
	{
		return mPass;
	}
	
	/**
	 * @return The URL to access the Nexus repository
	 */
	public String getRepoURL()
	{
		return mRepoURL;
	}
	
	/**
	 * @return The REST URL to access the Nexus repository
	 */
	public String getRestURL()
	{
		return mRestURL;
	}
	
	/**
	 * @return The basic encoding for the Nexus repository URL  
	 */
	public String getHttpBasic()
	{
		return mHttpBasic;
	}
	
	/**
	 * Generates a unique temporary file name.
	 * 
	 * @return The generated file name
	 */
	public static String generateTmpName() 
	{
		synchronized(mLock) 
		{
			long l = System.currentTimeMillis();
			if(l == mLast) 
			{
				l++;
			}
			mLast = l;
			return String.valueOf(l);
		}
	}
}
