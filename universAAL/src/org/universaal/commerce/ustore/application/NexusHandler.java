/*	
	Copyright 2008-2014 ProSyst AG, http://www.prosyst.com
	
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.application;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.StringTokenizer;

import org.universaal.commerce.ustore.uAALException;
import org.universaal.ustore.cms.ArtifactDescriptor;
import org.universaal.ustore.cms.UStoreCMS;

/**
 * This class accesses the Nexus server.
 * 
 * @author i.bakalov@prosyst.com
 * @author roni
 */
public class NexusHandler 
{
	/**
	 * The single instance of this class.
	 */
	private static NexusHandler mInstance = new NexusHandler();

	/**
	 * Constructor.
	 */
	private NexusHandler()
	{
	}
	
	/**
	 * @return The single instance of the nexus handler.
	 */
	public static NexusHandler getInstance()
	{
		return mInstance;
	}
	
	/**
	 * Deploys the application file in the Nexus repository.
	 * 
	 * @param pInputStream 
	 * 		The input stream of the application file to be deployed
	 * @param pGroupId 
	 * 		The group identifier of the artifact to be deployed
	 * @param pArtifactId 
	 * 		The artifact identifier of the artifact to be deployed
	 * @param pVersion 
	 * 		The version of the artifact to be deployed
	 * @param pVersionNotes 
	 * 		The version description of the artifact to be deployed
	 * @param pFileType 
	 * 		The application type (uaal, uapp, ...) to be deployed 
	 * 
     * @throws uAALException if an error occurs during the deployment process
	 */
	public void deployArtifactToNexusServer(
			InputStream pInputStream,
			String pGroupId, 
			String pArtifactId, 
			String pVersion,
			String pVersionNotes,
			String pFileType)
		throws uAALException
	{
		if(null == pInputStream) return;
		
		NexusConfigurationHandler configHandler = NexusConfigurationHandler.getInstance();
		UStoreCMS nexus = new UStoreCMS(
				"UStore Nexus repository", 
				configHandler.getRepoURL(), 
				configHandler.getUser(), 
				configHandler.getPassword());
		
		ArtifactDescriptor descriptor = new ArtifactDescriptor();
		descriptor.setArtifactId(pArtifactId);
		descriptor.setGroupId(pGroupId);
		descriptor.setVersion(pVersion);
		descriptor.setDescription(pVersionNotes);
		descriptor.setPackaging(pFileType);
		
		FileOutputStream out = null;
		try
		{
			File temp = File.createTempFile(NexusConfigurationHandler.generateTmpName(), "." + pFileType);
			out = new FileOutputStream(temp);
			byte[] buf = new byte[4096];
			int read;
	        while((read = pInputStream.read(buf)) != -1) 
	        {
	        	out.write(buf, 0, read);
	        }
			descriptor.setFile(temp);
			descriptor.setGeneratePom(true);

			nexus.deployArtifact(descriptor);
			temp.delete();
		}
		catch(Exception ex)
		{
			System.out.println("ERROR: Failed to deploy file in the Nexus repository [" + ex.getMessage() + "]");
			throw new uAALException(ex.getMessage());
		}
		finally
		{
			if(null != out)
			{
				try
				{
					out.flush();
					out.close();
				}
				catch(IOException ex) {} // ignore
			}
		}
	}
	
	/**
	 * Deletes an application file from the Nexus repository.
	 * 
	 * @param pGroupId 
	 * 		The group identifier of the artifact to be deleted
	 * @param pArtifactId 
	 * 		The artifact identifier of the artifact to be deleted
	 * @param pVersion 
	 * 		The version of the artifact to be deleted
	 * 
     * @throws uAALException if an error occurs during the removal process
	 */
	public void deleteArtifactFromNexusServer(
			String pGroupId, 
			String pArtifactId, 
			String pVersion)
		throws uAALException
	{
		NexusConfigurationHandler configHandler = NexusConfigurationHandler.getInstance();
		StringBuffer url = new StringBuffer(configHandler.getRestURL());
		StringTokenizer st = new StringTokenizer(pGroupId, ".");
		int ct = st.countTokens();
		for(int i = 0; i < ct - 1; i++) 
		{
			url.append(st.nextToken()).append('/');
		}
		url.append(st.nextToken()).append('/').append(pArtifactId).append('/').append(pVersion);
		
	    try 
	    {
	    	String stringURL = url.toString();
	    	HttpURLConnection http = (HttpURLConnection)((new URL(stringURL)).openConnection());
	    	http.setRequestMethod("DELETE");
	    	http.setRequestProperty("Authorization", configHandler.getHttpBasic());
	    	http.connect();
	    	if(http.getResponseCode() != 204) 
	    	{
	    		// DO NOTHING but deletion not successful
	    		http.disconnect();
	    		throw new IOException("ERROR: Failed to delete file from the Nexus repository [" +
	    				"responseCode = " + http.getResponseCode() + "]");
	    	}
	    	http.disconnect();
	    } 
	    catch(IOException ex) 
	    {
	    	System.out.println("ERROR: Failed to delete file from the Nexus repository [" + ex.getMessage() + "]");
	    	throw new uAALException("ERROR: Failed to delete file from the Nexus repository [" + ex.getMessage() + "]");
	    }
	    
		// Should be done via REST API with HTTP request like this:
		// "http://localhost:8081/nexus/service/local/repositories/ustore/content/"
		// DELETE /nexus/service/local/repositories/ustore/content/org/universAAL/samples/smp.tta.risk/0.0.1-SNAPSHOT/smp.tta.risk-0.0.1-SNAPSHOT.bundle HTTP/1.1
		// Host: localhost
		// User-Agent: Mozilla/5.0 (Windows NT 5.1; rv:8.0) Gecko/20100101 Firefox/8.0
		// Accept: application/json
		// Accept-Language: en-gb,en;q=0.5
		// Accept-Encoding: gzip, deflate
		// Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
		// Connection: keep-alive
		// X-Requested-With: XMLHttpRequest
		// Referer: http://localhost/nexus/index.html
		// Cookie: THEME=Default
		// Authorization: Basic YWRtaW46YWRtaW4xMjM=
	}
	
	/**
	 * Retrieve an application file from the Nexus repository.
	 * 
	 * @param pGroupId 
	 * 		The group identifier of the artifact to be retrieved
	 * @param pArtifactId 
	 * 		The artifact identifier of the artifact to be retrieved
	 * @param pVersion 
	 * 		The version of the artifact to be retrieved
	 * 
	 * @return The input stream of the artifact
	 * 
     * @throws uAALException if an error occurs during the retrieval process
	 */
	public InputStream getArtifactFromNexusServer(
			String pGroupId,
			String pArtifactId,
			String pVersion)
	throws uAALException
	{
		 NexusConfigurationHandler configHandler = NexusConfigurationHandler.getInstance();
		 UStoreCMS nexus = new UStoreCMS(
				 "UStore Nexus repository",
				 configHandler.getRepoURL(),
				 configHandler.getUser(),
				 configHandler.getPassword());
		
		 ArtifactDescriptor descriptor = new ArtifactDescriptor();
		 descriptor.setArtifactId(pGroupId);
		 descriptor.setGroupId(pArtifactId);
		 descriptor.setVersion(pVersion);
		 descriptor.setPackaging("uaal");

		 try
		 {
			 return nexus.getArtifactStream(descriptor);
		 }
		 catch(Exception ex)
		 {
			 System.out.println("ERROR: Failed to retrieve file from the Nexus repository [" + ex.getMessage() + "]");
			 throw new uAALException("ERROR: Failed to retrieve file from the Nexus repository [" + ex.getMessage() + "]");
		 }
	}
}
