/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.application.action;

import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.universaal.commerce.ustore.application.NexusHandler;

import com.ibm.commerce.foundation.client.facade.bod.servlet.struts.BusinessObjectDocumentAction;

/**
 * This class is being called when there is a request for downloading the 
 * application file from the Management Center by developers/service providers 
 * (for testing purposes). 
 *  
 * @author roni 
 */
public class RetrieveApplicationFileAction extends BusinessObjectDocumentAction  
{
	/**
	 * Override the execute method.
	 * 
	 * Retrieve the application file from the Nexus server with the parameters 
	 * that identify the file and are passing in the request. Store the file in 
	 * the response output stream.  
	 * 
	 * @param pActionMapping 
	 * 		The configuration of the struts action
	 * @param pActionForm 
	 * 		The action form of the struts action
	 * @param pRequest 
	 * 		The HTTP request object that contains the information pertaining to 
	 * 		the request
	 * @param pResponse 
	 * 		The HTTP response object that will contain the information 
	 * 		pertaining to the response
	 * 
	 * @return The ActionForward instance describing where and how control 
	 * 		should be forwarded
	 * 
	 * @throws <code>java.lang.Exception</code> in case a problem occurred while 
	 * 		parsing the HTTP request or executing the business object document. 
	 * 		These exceptions can range from server side validation which are 
	 * 		recoverable to server errors where are not recoverable
	 */
	 public ActionForward execute(
			 ActionMapping pActionMapping, 
			 ActionForm pActionForm, 
			 HttpServletRequest pRequest, 
			 HttpServletResponse pResponse) 
	 	throws Exception
	{
		String itemId = pRequest.getParameter("itemid");
		String itemType = pRequest.getParameter("itemtype");
		String version = pRequest.getParameter("version");
		String fileName = pRequest.getParameter("filename");
		
		System.out.println("-->>RetrieveApplicationFileAction itemId = " + itemId + 
				" itemType = " + itemType + " version = " + 
				version + " fileName = " + fileName);
		
		if(null == itemId || null == itemType || !itemType.equals("AAL_APPLICATION") || 
				null == version || null == fileName)
		{
			System.out.println("ERROR: The input parameters are invalid");
			return null;
		}
		
		OutputStream output = null;
		InputStream input = null;
		try
		{
			output = pResponse.getOutputStream();
			version = String.valueOf(Double.parseDouble(version));
			input = NexusHandler.getInstance().getArtifactFromNexusServer(itemId, itemId, version);

			fileName = fileName.replaceAll(" ", "_");
			String fileType = "uaal";
			int index = fileName.indexOf(".");
			if(index != -1)
			{
				fileType = fileName.substring(index+1);
			}
			if(fileType.equalsIgnoreCase("apk"))
			{
				pResponse.setContentType("application/vnd.android.package-archive");
			}
			else
			{
				pResponse.setContentType("application/" + fileType);
			}
			pResponse.setHeader("Content-disposition","attachment; filename=" + fileName);

			byte[] buff = new byte[2048];
			int bytesRead = -1;
			long total = 0;
				 
			// simple read/write loop
			while(-1 != (bytesRead = input.read(buff, 0, buff.length)))
			{
				output.write(buff, 0, bytesRead);
				total += bytesRead;
			}
		}
		catch(Exception ex)
		{
			System.out.println("ERROR: Failed to retrieve the application file (" + ex.getMessage() + ")");
			return null;
		}
		finally
		{
			if(null != input)
			{
				input.close();
			}
			if(null != output) 
			{
				output.flush();
				output.close();
			}
		}
		return null; 
	 }
}
