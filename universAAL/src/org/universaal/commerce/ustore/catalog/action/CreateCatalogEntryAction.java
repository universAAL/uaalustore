/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.catalog.action;

import java.io.InputStream;
import java.util.Map;

import javax.security.auth.callback.CallbackHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.application.AppHandler;
import org.universaal.commerce.ustore.application.NexusHandler;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.user.UserSessionInfo;
import org.universaal.commerce.ustore.utilities.CatalogUtilities;
import org.universaal.commerce.ustore.utilities.ServletRequestWrapper;

import com.ibm.commerce.datatype.TypedProperty;
import com.ibm.commerce.foundation.client.facade.bod.servlet.struts.BusinessObjectDocumentAction;
import com.ibm.commerce.foundation.common.datatypes.BusinessContextType;
import com.ibm.commerce.server.UploadedFile;

/**
 * This action class is responsible for all the operations that have to be done
 * when adding an item (AAL Application/Hardware/Human Resource) or a service to 
 * the catalog.
 * - Store the username that creates this catalog entry in the CATENTRY table 
 *   in 'FIELD5'
 * - If the item is of type 'AAL Application' and the uploaded file is a uapp 
 *   file then some of the parameters will be extracted from the file (instead 
 *   of the UI) and populate the DB tables accordingly 
 * - If the item is of type 'AAL Application' then the file is deployed to the 
 *   Nexus repository
 *  
 * @author roni
 */
public class CreateCatalogEntryAction extends BusinessObjectDocumentAction 
{
    /**
	 * Override the execute method.
	 * 
	 * @param pActionMapping 
	 * 		The configuration of the struts action
	 * @param pActionForm 
	 * 		The action form of the struts action
	 * @param pRequest 
	 * 		The HTTP request object that contains the information pertaining to 
	 * 		the request
	 * @param pResponse 
	 * 		The HTTP response object that will contain the information 
	 * 		pertaining to the response
	 * @param pBusinessContext 
	 * 		The business context of the current request 
	 * @param pCallbackHandler 
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return A <code>java.util.Map</code> that is used in the result of the 
	 * 		current request
	 * 
	 * @throws <code>java.lang.Exception</code> in case a problem occurred while 
	 * 		parsing the HTTP request or executing the business object document. 
	 * 		These exceptions can range from server side validation which are 
	 * 		recoverable to server errors where are not recoverable
	 */
	protected Map executeBusinessObjectDocument(
			ActionMapping pActionMapping, 
			ActionForm pActionForm, 
			HttpServletRequest pRequest, 
			HttpServletResponse pResponse, 
			BusinessContextType pBusinessContext, 
			CallbackHandler pCallbackHandler)
     throws Exception
     {
		String storeId = pRequest.getParameter("storeId");
		
		// no store id
		if(null == storeId)
		{
			// carry on with BusinessObjectDocumentAction's executing
			return super.executeBusinessObjectDocument(
					pActionMapping, 
					pActionForm, 
					pRequest, 
					pResponse, 
					pBusinessContext, 
					pCallbackHandler);
		}
		
		// check that the item is of type AAL_APPLICATION
		String itemType = pRequest.getParameter("x_itemtype");
		System.out.println("-->> CreateCatalogEntryAction itemType = " + itemType);

		AppHandler handler = null;
		Map appParams = null;
		if(null != itemType && itemType.equals("AAL_APPLICATION"))
		{
			TypedProperty value = (TypedProperty)pRequest.getAttribute("DecryptedReqProp");
			if(null != value && value.containsKey("Filename") && value.containsKey("UpLoadedFile"))
			{
				UploadedFile file = (UploadedFile)value.get("UpLoadedFile");
				
				handler = new AppHandler(file.getInputStream());
				
				// extract the uapp configuration file
				try
				{
					handler.init();
					appParams = handler.getUappConfigParameters();
				}
				catch(Exception ex) {} // not a uapp file - ignore
			}
		}
		
		// update the parameter request with the new application parameters
		ServletRequestWrapper request = new ServletRequestWrapper(pRequest);
		if(null != appParams)
		{
			request.addParameter("x_filetype", "uapp");
			request.addParameter("name", (String)appParams.get(AppHandler.APP_NAME));
			request.addParameter("sDesc", (String)appParams.get(AppHandler.APP_DESCRIPTION));
			request.addParameter("keyword", (String)appParams.get(AppHandler.APP_KEYWORDS));
			request.addParameter("x_organizationname",  (String)appParams.get(AppHandler.APP_ORGANIZATION_NAME));
			request.addParameter("x_organizationurl",  (String)appParams.get(AppHandler.APP_ORGANIZATION_URL));
			request.addParameter("x_developername", (String)appParams.get(AppHandler.APP_DEVELOPER_NAME));
			request.addParameter("x_developeremail", (String)appParams.get(AppHandler.APP_DEVELOPER_EMAIL));
			request.addParameter("x_developerephone", (String)appParams.get(AppHandler.APP_DEVELOPER_PHONE));
			request.addParameter("x_organizationcertificate", (String)appParams.get(AppHandler.APP_ORGANIZATION_CERTIFICATE));
			request.addParameter("x_requirements", (String)appParams.get(AppHandler.APP_REQUIREMENTS));
			request.addParameter("x_licenses", (String)appParams.get(AppHandler.APP_LICENSES));
			request.addParameter("x_appservicelevelagreement", (String)appParams.get(AppHandler.APP_SLA));
		}

		// carry on with BusinessObjectDocumentAction's executing
		Map result = super.executeBusinessObjectDocument(
				pActionMapping, 
				pActionForm, 
				request,  
				pResponse, 
				pBusinessContext, 
				pCallbackHandler);
		
		if(null != result)
		{
			String catEntryId = (String)result.get("catentryId");
			if(null != catEntryId && catEntryId.length() > 0)
			{
				UserSessionInfo info = UserSessionHandler.getUserSessionInfoBySessionKey(pRequest.getSession().getId());
				System.out.println("-->> CreateCatalogEntryAction userSessionInfo = " + info);
				if(null == info) // user session info was not found 
				{
					// failed to retrieve the user session info --> delete the new 
					// item from the catalog
					CatalogUtilities.deleteItem(catEntryId, 
							storeId, 
							pRequest.getSession().getServletContext(), 
							pBusinessContext, 
							pCallbackHandler);
	
					throw new Exception("ERROR: Session id in invalid. Please re-login!");
				}

				try
				{
					// store the user name that created this catalog entry in FIELD5 of the CATENTRY table 
					CatalogUtilities.updateCatalogEntryAccessBeanUserName(catEntryId, info.getUserName());
					
					// check that the item is of type AAL_APPLICATION
					if(null != handler)
					{
						// create a new AAL Application
						addFileToNexusServer(pRequest, handler.getAppFileInputStream(), catEntryId);
					}
				}
				catch(uAALException ex)
				{
					// delete the new item from the catalog
					CatalogUtilities.deleteItem(catEntryId, 
							storeId, 
							pRequest.getSession().getServletContext(), 
							pBusinessContext, 
							pCallbackHandler);
					
					throw ex;
				}
			}
		}
		return result;
     }

	/**
	 * Add a file to the Nexus server by using the unique identifiers of the 
	 * item (group id, artifact id ane version).
	 * 
	 * @param pRequest 
	 * 		The HTTP request object that contains the information of the new 
	 * 		file to be created
	 * @param pInputStream 
	 * 		The input stream of the file to be stored
	 * @param pItem 
	 * 		The unique identifier of the new item
	 * 
	 * @throws uAALException 
	 * 		In case a problem occurred during the upload process
	 */
	private void addFileToNexusServer(
			HttpServletRequest pRequest, 
			InputStream pInputStream,
			String pItemId)
		throws uAALException
	{
		String version = pRequest.getParameter("x_version");
		String versionNotes = pRequest.getParameter("x_versionnotes");
				
		System.out.println("-->> CreateCatalogEntryAction version = " + version);
		System.out.println("-->> CreateCatalogEntryAction versionNotes = " + versionNotes);
		
		if(null != pInputStream &&	null != version && version.length() > 0 && 
				null != versionNotes && versionNotes.length() > 0)
		{
			version = String.valueOf(Double.parseDouble(version));
			String[] val = new String[1];
			val[0] = version;
			pRequest.getParameterMap().put("x_version", val);
			String fileType = "uaal";
			System.out.println("-->> CreateCatalogEntryAction - Create in nexus server (" + 
					pItemId + " > " + pItemId + " > " + version + ") file type is " + fileType);
			NexusHandler.getInstance().deployArtifactToNexusServer(
					pInputStream, pItemId, pItemId, version, versionNotes, fileType);
		}
	}
}
