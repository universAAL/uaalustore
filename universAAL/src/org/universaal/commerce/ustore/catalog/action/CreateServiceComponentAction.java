/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.catalog.action;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.security.auth.callback.CallbackHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.message.ApplicationUseMessageNotification;
import org.universaal.commerce.ustore.message.GenericMessageNotification;
import org.universaal.commerce.ustore.message.MessageSender;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.user.UserSessionInfo;
import org.universaal.commerce.ustore.utilities.CatalogUtilities;

import com.ibm.commerce.catalog.facade.datatypes.CatalogDescriptionType;
import com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType;
import com.ibm.commerce.catalog.objects.CatalogEntryAccessBean;
import com.ibm.commerce.foundation.client.facade.bod.servlet.struts.BusinessObjectDocumentAction;
import com.ibm.commerce.foundation.common.datatypes.BusinessContextType;

/**
 * This action class is responsible for all the operations that have to be done
 * when adding an item (AAL Application/Hardware/Human Resource) to the service 
 * components list.
 * - Verify that the user who adds items to the service components list is the 
 *   one that created the service, otherwise throw an exception
 * - In case the added item is marked as 'not for purchase' then mark also the
 *   service as 'not for purchase'
 * - If the item is an AAL application then the version and the file name fields 
 *   are being saved in the 'FIELD1' and 'FIELD3' columns in the CATENTREL table 
 *   for retrieval time. In addition, the developer is notified that his 
 *   application in now included in the service
 * - Update the service components list that is stored in the CATENTRY table in
 *   'FIELD4'  
 * - Items can be added to the service components list even if the service has 
 *   already been purchased by users. The service provider is responsible to 
 *   manage the items of the service and to take into consideration the status 
 *   of the service
 *  
 * @author roni
 */
public class CreateServiceComponentAction extends BusinessObjectDocumentAction 
{
	/**
	 * Override the execute method.
	 * 
	 * @param pActionMapping 
	 * 		The configuration of the struts action
	 * @param pActionForm 
	 * 		The action form of the struts action
	 * @param pRequest 
	 * 		The HTTP request object that contains the information pertaining to 
	 * 		the request
	 * @param pResponse 
	 * 		The HTTP response object that will contain the information 
	 * 		pertaining to the response
	 * @param pBusinessContext 
	 * 		The business context of the current request 
	 * @param pCallbackHandler 
	 * 		The authentication callback handler to associate with the client library
	 * 
	 * @return A <code>java.util.Map</code> that is used in the result of the 
	 * 		current request
	 * 
	 * @throws <code>java.lang.Exception</code> in case a problem occurred while 
	 * 		parsing the HTTP request or executing the business object document. 
	 * 		These exceptions can range from server side validation which are 
	 * 		recoverable to server errors where are not recoverable
	 */
	protected Map executeBusinessObjectDocument(
			ActionMapping pActionMapping, 
			ActionForm pActionForm, 
			HttpServletRequest pRequest, 
			HttpServletResponse pResponse, 
			BusinessContextType pBusinessContext, 
			CallbackHandler pCallbackHandler)
     throws Exception
     {
		String serviceId = pRequest.getParameter("parentCatentryId");
		String storeId = pRequest.getParameter("storeId");
		String itemId = pRequest.getParameter("componentId");
		
		if(null == storeId || null == serviceId || null == itemId)
		{
			// carry on with BusinessObjectDocumentAction's executing.
			return super.executeBusinessObjectDocument(
					pActionMapping, 
					pActionForm, 
					pRequest, 
					pResponse, 
					pBusinessContext, 
					pCallbackHandler);
		}

		System.out.println("-->> CreateServiceComponentAction serviceId = " + serviceId +
				" itemId = " + itemId);
		
		CatalogEntryType item = CatalogUtilities.getCatalogEntryById(
				itemId,
				storeId, 
				pRequest.getSession().getServletContext(), 
				pBusinessContext, 
				pCallbackHandler);

		CatalogEntryType service = CatalogUtilities.getServiceWithComponentsById(
				serviceId,
				storeId, 
				pRequest.getSession().getServletContext(), 
				pBusinessContext, 
				pCallbackHandler);
		
		// no item or service (catalog entry) was found
		if(null == item || null == service) 
		{
			// carry on with BusinessObjectDocumentAction's executing
			return super.executeBusinessObjectDocument(
					pActionMapping, 
					pActionForm, 
					pRequest, 
					pResponse, 
					pBusinessContext, 
					pCallbackHandler);
		}

		UserSessionInfo info = UserSessionHandler.getUserSessionInfoBySessionKey(
				pRequest.getSession().getId());
		System.out.println("-->> CreateServiceComponentAction userSessionInfo = " + info);
		if(null == info)
		{
			throw new Exception("ERROR: Session id in invalid. Please re-login!");
		}
		
		// check that the user that creates this service component is the one that created the service
		String userName = CatalogUtilities.getCatalogEntryAccessBean(serviceId).getField5();
		System.out.println("-->> CreateServiceComponentAction The userName that created the service = " + userName +
				" The userName that wants to create a service component = " + info.getUserName());
		if(null == userName || null == info.getUserName() || !userName.equals(info.getUserName()))
		{
			throw new Exception("ERROR: User " + info.getUserName() + 
					" is not authorized to update this service");
		}
		
		// mark the service for 'not for purchase' in case the status of the 
		// added item is 'not for purchase'
		CatalogEntryAccessBean itemBean = CatalogUtilities.getCatalogEntryAccessBean(itemId);
		if(itemBean.getBuyable().equals("0")) // the item is 'not for purchase'
		{
			CatalogEntryAccessBean serviceBean = CatalogUtilities.getCatalogEntryAccessBean(serviceId);
			if(serviceBean.getBuyable().equals("1")) // the service is 'for purchase'
			{
				serviceBean.setBuyable("0"); // mark the service as 'not for purchase'
				serviceBean.commitCopyHelper();
			}
		}
		
		// carry on with BusinessObjectDocumentAction's executing
		Map result = super.executeBusinessObjectDocument(
				pActionMapping, 
				pActionForm, 
				pRequest, 
				pResponse, 
				pBusinessContext, 
				pCallbackHandler);

		String itemType = (String)item.getUserData().getUserDataField().get("itemtype");
		System.out.println("-->> UpdateServiceComponentAction itemType = " + itemType);

		// store the version and the file name in FIELD1 and FIELD3 of the 
		// CATENTREL table 
		if(null != itemType && itemType.equals("AAL_APPLICATION"))
		{
			String version = (String)item.getUserData().getUserDataField().get("version");
			String fileName = (String)item.getUserData().getUserDataField().get("filename");
			version = String.valueOf(Double.parseDouble(version));
			System.out.println("-->> CreateServiceComponentAction version = " + version + " file name = " + fileName);
			
			CatalogUtilities.updateCatalogEntryRelationAccessBean(itemId, serviceId, fileName, version);

			// notify the developer
			try
			{
				CatalogEntryType serviceInfo = CatalogUtilities.getCatalogEntryById(
						serviceId,
						storeId, 
						pRequest.getSession().getServletContext(), 
						pBusinessContext, 
						pCallbackHandler);

				Map messageParams = new HashMap();
				messageParams.put(ApplicationUseMessageNotification.PARAM_APPLICATION_ID, 
						item.getCatalogEntryIdentifier().getUniqueID());
				if(item.getDescription().size() > 0)
				{
					messageParams.put(ApplicationUseMessageNotification.PARAM_APPLICATION_NAME,
							((CatalogDescriptionType)item.getDescription().get(0)).getName());
				}
				messageParams.put(ApplicationUseMessageNotification.PARAM_DEVELOPER_NAME, 
						(String)item.getUserData().getUserDataField().get("developername"));
				messageParams.put(ApplicationUseMessageNotification.PARAM_DEVELOPER_EMAIL, 
						(String)item.getUserData().getUserDataField().get("developeremail"));
				messageParams.put(ApplicationUseMessageNotification.PARAM_SERVICE_ID, 
						service.getCatalogEntryIdentifier().getUniqueID());
				if(service.getDescription().size() > 0)
				{
					messageParams.put(ApplicationUseMessageNotification.PARAM_SERVICE_NAME,
							((CatalogDescriptionType)service.getDescription().get(0)).getName());
				}
				messageParams.put(ApplicationUseMessageNotification.PARAM_SERVICE_PROVIDER_NAME, 
						(String)serviceInfo.getUserData().getUserDataField().get("serviceprovidername"));
				messageParams.put(ApplicationUseMessageNotification.PARAM_SERVICE_PROVIDER_EMAIL, 
						(String)serviceInfo.getUserData().getUserDataField().get("serviceprovideremail"));
				messageParams.put(ApplicationUseMessageNotification.PARAM_SERVICE_PROVIDER_PHONE, 
						(String)serviceInfo.getUserData().getUserDataField().get("serviceproviderphone"));
				messageParams.put(ApplicationUseMessageNotification.PARAM_SERVICE_PROVIDER_ORGANIZATION, 
						(String)serviceInfo.getUserData().getUserDataField().get("serviceorganizationname"));
				messageParams.put(ApplicationUseMessageNotification.PARAM_SERVICE_PROVIDER_USER_NAME,
						info.getUserName());
				messageParams.put(ApplicationUseMessageNotification.PARAM_TIME, new Date().toString());
				
				sendMessage(messageParams);
			}
			catch(Exception ex)
			{
				System.out.println(ex.getMessage());
			}
		}

		// add the new item into the FIELD4 for of the CATENTRY table
		// for the service record
		CatalogEntryAccessBean serviceBean = CatalogUtilities.getCatalogEntryAccessBean(serviceId);
		String items = serviceBean.getField4();
		if(null == items || items.length() == 0 || items.equals(";"))
		{
			items = itemId;
		}
		else
		{
			if(items.endsWith(";"))
			{
				items += itemId;
			}
			else 
			{
				items += (";" + itemId);
			}
		}
		CatalogUtilities.updateCatalogEntryAccessBeanItemsList(serviceId, items);
		
		return result;
     }

	/**
	 * Send a message to the developer detailing information of the service and
	 * the service provider.
	 * 
	 * @param pMessageParams
	 * 		The message information
	 * 
	 * @throws uAALException
	 * 		If there is a problem with the sending process
	 */
  	private void sendMessage(Map pMessageParams)
  		throws uAALException
  	{
  		GenericMessageNotification message = 
  			new ApplicationUseMessageNotification(pMessageParams);
		MessageSender.sendMessage(message);
  	}
}


