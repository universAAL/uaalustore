/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.catalog.action;

import java.util.Map;

import javax.security.auth.callback.CallbackHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.application.NexusHandler;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.user.UserSessionInfo;
import org.universaal.commerce.ustore.utilities.CatalogUtilities;

import com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType;
import com.ibm.commerce.foundation.client.facade.bod.servlet.struts.BusinessObjectDocumentAction;
import com.ibm.commerce.foundation.common.datatypes.BusinessContextType;

/**
 * This action class is responsible for all the operations that have to be done
 * when deleting an item (AAL Application/Hardware/Human Resource) or a service
 * from the catalog.
 * - Verify that the user who deletes the catalog entry is the one that created 
 *   it, otherwise throw an exception
 * - If the item has already been included in a service then it can't be deleted
 * - If the item is of type 'AAL Application' then the file is being deleted 
 *   from the Nexus repository 
 *  
 * @author roni
 */
public class DeleteCatalogEntryAction extends BusinessObjectDocumentAction 
{
	/**
	 * Override the execute method.
	 * 
	 * @param pActionMapping 
	 * 		The configuration of the struts action
	 * @param pActionForm 
	 * 		The action form of the struts action
	 * @param pRequest 
	 * 		The HTTP request object that contains the information pertaining to 
	 * 		the request
	 * @param pResponse 
	 * 		The HTTP response object that will contain the information 
	 * 		pertaining to the response
	 * @param pBusinessContext 
	 * 		The business context of the current request 
	 * @param pCallbackHandler 
	 * 		The authentication callback handler to associate with the client library
	 * 
	 * @return A <code>java.util.Map</code> that is used in the result of the 
	 * 		current request
	 * 
	 * @throws <code>java.lang.Exception</code> in case a problem occurred while 
	 * 		parsing the HTTP request or executing the business object document. 
	 * 		These exceptions can range from server side validation which are 
	 * 		recoverable to server errors where are not recoverable
	 */
	protected Map executeBusinessObjectDocument(
			ActionMapping pActionMapping, 
			ActionForm pActionForm, 
			HttpServletRequest pRequest, 
			HttpServletResponse pResponse, 
			BusinessContextType pBusinessContext, 
			CallbackHandler pCallbackHandler)
     throws Exception
     {
		String catEntryId = pRequest.getParameter("catentryId");
		String storeId = pRequest.getParameter("storeId");
		System.out.println("-->> catEntryId = " + catEntryId + " storeId = " + storeId);	

		// no item id or store id
		if(null == storeId || null == catEntryId || catEntryId.length() == 0)
		{
			// carry on with BusinessObjectDocumentAction's executing
			return super.executeBusinessObjectDocument(
					pActionMapping, 
					pActionForm, 
					pRequest, 
					pResponse, 
					pBusinessContext, 
					pCallbackHandler);
		}
		
		CatalogEntryType catEntry = CatalogUtilities.getCatalogEntryById(catEntryId,
					storeId, 
					pRequest.getSession().getServletContext(), 
					pBusinessContext, 
					pCallbackHandler);
			
		// no catalog entry was found
		if(null == catEntry) 
		{
			// carry on with BusinessObjectDocumentAction's executing
			return super.executeBusinessObjectDocument(
					pActionMapping, 
					pActionForm, 
					pRequest, 
					pResponse, 
					pBusinessContext, 
					pCallbackHandler);
		}
			
		UserSessionInfo info = UserSessionHandler.getUserSessionInfoBySessionKey(
				pRequest.getSession().getId());
		System.out.println("-->> DeleteCatalogEntryAction userSessionInfo = " + info);
		if(null == info)
		{
			throw new Exception("ERROR: Session id in invalid. Please re-login!");
		}
		
		// check that the user that deletes this catalog entry is the one that created it
		String userName = CatalogUtilities.getCatalogEntryAccessBean(catEntryId).getField5();
		System.out.println("-->> DeleteCatalogEntryAction The userName that created the entry = " + userName +
				" The userName that wants to delete the entry = " + info.getUserName());
		if(null == userName || null == info.getUserName() || !userName.equals(info.getUserName()))
		{
			throw new Exception("ERROR: User " + info.getUserName() + 
					" is not authorized to delete this catalog entry");
		}

		CatalogEntryType[] services = CatalogUtilities.getServicesContainReferenceToItemId(
				catEntryId, 
				storeId, 
				pRequest.getSession().getServletContext(), 
				pBusinessContext, 
				pCallbackHandler);
		
		if(null != services)
		{
			System.out.println("-->> DeleteCatalogEntryAction #services contain a reference to item = " + 
					services.length);
			if(services.length > 0)
			{
				throw new Exception("ERROR: Item can not be deleted since it is already included in a service");
			}
		}
			
		// check that the item is of type AAL_APPLICATION
		String itemType = (String)catEntry.getUserData().getUserDataField().get("itemtype");
		System.out.println("-->> DeleteCatalogEntryAction itemType = " + itemType);

		if(null != itemType && itemType.equals("AAL_APPLICATION"))
		{
			try
			{
				deleteFileFromNexusServer(catEntry);
			}
			catch(uAALException ex)
			{
				System.out.println("ERROR (ignore): Failed to delete the file from the Nexus server");
			}
		}

		// carry on with BusinessObjectDocumentAction's executing
		return super.executeBusinessObjectDocument(
				pActionMapping, 
				pActionForm, 
				pRequest, 
				pResponse, 
				pBusinessContext, 
				pCallbackHandler);
     }

	/**
	 * Delete a file from the Nexus server.
	 * 
	 * @param pItem 
	 * 		The <code>com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType</code>
	 * 		that holds the identifiers of the file to be deleted
	 * 
	 * @throws uAALException 
	 * 		In case a problem occurred during the removal process
	 */
	private void deleteFileFromNexusServer(CatalogEntryType pItem)
		throws uAALException
	{
		// retrieve the version
		Map parameters = pItem.getUserData().getUserDataField();
		String id = pItem.getCatalogEntryIdentifier().getUniqueID();
		String version = (String)parameters.get("version");
		System.out.println("-->> DeleteCatalogEntryAction id = " + id + " version = " + version);

		if(null != id && id.length() > 0 && null != version && version.length() > 0)
		{
			version = String.valueOf(Double.parseDouble(version));
			System.out.println("-->> DeleteCatalogEntryAction - Delete from the nexus server (" + 
					id + " > " + id + " > " + version + ")");
			NexusHandler.getInstance().deleteArtifactFromNexusServer(id, id, version);
		}
	}
}
