/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.catalog.action;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;

import javax.security.auth.callback.CallbackHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.application.AppHandler;
import org.universaal.commerce.ustore.application.NexusHandler;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.user.UserSessionInfo;
import org.universaal.commerce.ustore.utilities.CatalogUtilities;
import org.universaal.commerce.ustore.utilities.ServletRequestWrapper;

import com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType;
import com.ibm.commerce.catalog.facade.datatypes.KitComponentType;
import com.ibm.commerce.catalog.objects.CatalogEntryAccessBean;
import com.ibm.commerce.datatype.TypedProperty;
import com.ibm.commerce.foundation.client.facade.bod.servlet.struts.BusinessObjectDocumentAction;
import com.ibm.commerce.foundation.common.datatypes.BusinessContextType;
import com.ibm.commerce.server.UploadedFile;

/**
 * This action class is responsible for all the operations that have to be done
 * when updating an item (AAL Application/Hardware/Human Resource) or a service 
 * in the catalog.
 * - Verify that the user who updates the catalog entry is the one that created 
 *   it, otherwise throw an exception
 * - If the item has already been included in a service and it is being marked 
 *   as 'not for purchase' then all the services that include this item are also
 *   marked as 'not for purchase'
 * - If the service is being marked as 'for purchase' then check that all its 
 *   items (components) are also marked as 'for purchase'. Otherwise, throw an 
 *   exception
 * - If the catalog entry is a service then check that the service version has 
 *   been increased, otherwise throw an exception
 * - If the item is of type 'AAL_APPLICATION" then check that the application
 *   version has been increased, otherwise throw an exception
 * - If the item is of type 'AAL Application' and a new uapp file was uploaded  
 *   then some of the parameters will be extracted from the file and populate 
 *   the DB tables accordingly 
 * - If the item is of type 'AAL Application' then the file is being added to 
 *   the Nexus server (under the same application code and a different version)
 * - All the fields that describe this catalog entry can be updated even if the
 *   catalog entry is an item and this item is already included in a service or 
 *   if it's a service and the service has already been purchased by end users. 
 *   The service provider is responsible to manage this information and to take 
 *   into consideration the status of the catalog entry 
 *  
 * @author roni
 */
public class UpdateCatalogEntryAction extends BusinessObjectDocumentAction 
{
	/**
	 * Override the execute method.
	 * 
	 * @param pActionMapping 
	 * 		The configuration of the struts action
	 * @param pActionForm 
	 * 		The action form of the struts action
	 * @param pRequest 
	 * 		The HTTP request object that contains the information pertaining to 
	 * 		the request
	 * @param pResponse 
	 * 		The HTTP response object that will contain the information 
	 * 		pertaining to the response
	 * @param pBusinessContext 
	 * 		The business context of the current request 
	 * @param pCallbackHandler 
	 * 		The authentication callback handler to associate with the client library
	 * 
	 * @return A <code>java.util.Map</code> that is used in the result of the 
	 * 		current request
	 * 
	 * @throws <code>java.lang.Exception</code> in case a problem occurred while 
	 * 		parsing the HTTP request or executing the business object document. 
	 * 		These exceptions can range from server side validation which are 
	 * 		recoverable to server errors where are not recoverable
	 */
	protected Map executeBusinessObjectDocument(
			ActionMapping pActionMapping, 
			ActionForm pActionForm, 
			HttpServletRequest pRequest, 
			HttpServletResponse pResponse, 
			BusinessContextType pBusinessContext, 
			CallbackHandler pCallbackHandler)
     throws Exception
     {
		String catEntryId = pRequest.getParameter("catentryId");
		String storeId = pRequest.getParameter("storeId");

		// no item id or store id
		if(null == storeId || null == catEntryId || catEntryId.length() == 0)
		{
			// carry on with BusinessObjectDocumentAction's executing
			return super.executeBusinessObjectDocument(
					pActionMapping, 
					pActionForm, 
					pRequest, 
					pResponse, 
					pBusinessContext, 
					pCallbackHandler);
		}
		
		CatalogEntryType catEntry = CatalogUtilities.getCatalogEntryById(
				catEntryId,
				storeId, 
				pRequest.getSession().getServletContext(), 
				pBusinessContext, 
				pCallbackHandler);
		
		// no catalog entry was found
		if(null == catEntry) 
		{
			// carry on with BusinessObjectDocumentAction's executing
			return super.executeBusinessObjectDocument(
					pActionMapping, 
					pActionForm, 
					pRequest, 
					pResponse, 
					pBusinessContext, 
					pCallbackHandler);
		}

		UserSessionInfo info = UserSessionHandler.getUserSessionInfoBySessionKey(pRequest.getSession().getId());
		System.out.println("-->> UpdateCatalogEntryAction userSessionInfo = " + info);
		if(null == info)
		{
			throw new Exception("ERROR: Session id in invalid. Please re-login!");
		}

		// check that the user that updates this catalog entry is the one that created it
		String userName = CatalogUtilities.getCatalogEntryAccessBean(catEntryId).getField5();
		System.out.println("-->> UpdateCatalogEntryAction The userName that created the entry = " + userName +
				" The userName that wants to update the entry = " + info.getUserName());
		if(null == userName || null == info.getUserName() || !userName.equals(info.getUserName()))
		{
			throw new Exception("ERROR: User " + info.getUserName() + 
					" is not authorized to update this catalog entry");
		}

		CatalogEntryType[] services = CatalogUtilities.getServicesContainReferenceToItemId(
				catEntryId, 
				storeId,	 
				pRequest.getSession().getServletContext(), 
				pBusinessContext, 
				pCallbackHandler);

		// check the 'for purchase' field  
		String buyable = pRequest.getParameter("xprop_buyable");
		if(null != buyable && buyable.equals("0")) // not for purchase
		{
			System.out.println("-->> UpdateCatalogEntryAction - the item is marked as 'not for purchase'");
			if(null != services)
			{
				for(int i=0; i<services.length; i++) // go through all the services that include this item
				{
					CatalogEntryAccessBean bean = 
						CatalogUtilities.getCatalogEntryAccessBean(services[i].getCatalogEntryIdentifier().getUniqueID());
					if(bean.getBuyable().equals("1")) // the service is 'for purchase'
					{
						System.out.println("-->> UpdateCatalogEntryAction - the service is marked as 'for purchase'");
						bean.setBuyable("0"); // mark the service as 'not for purchase'
						bean.commitCopyHelper();
						System.out.println("-->> UpdateCatalogEntryAction - the service was updated as 'not for purchase'");
					}
				}
			}
		}
		else if(null != buyable && buyable.equals("1")) // for purchase
		{
			System.out.println("-->> UpdateCatalogEntryAction - the service is marked as 'for purchase'");
			
			// check the 'For purchase' status of all the included items (components)
			CatalogEntryType service = CatalogUtilities.getServiceWithComponentsById(
					catEntryId, 
					storeId, 
					pRequest.getSession().getServletContext(), 
					pBusinessContext, 
					pCallbackHandler);
		
			if(null != service)
			{
				Iterator iter = service.getKitComponent().iterator();
				while(iter.hasNext())
				{
					KitComponentType comp = (KitComponentType)iter.next();
					
					CatalogEntryAccessBean bean = CatalogUtilities.getCatalogEntryAccessBean(
							comp.getCatalogEntryReference().getCatalogEntryIdentifier().getUniqueID());
					if(bean.getBuyable().equals("0")) // the item is 'not for purchase'
					{
						System.out.println("-->> UpdateCatalogEntryAction - the item is marked as 'not for purchase'");
						throw new Exception("ERROR: The service can not be marked as 'For purchase' since the item " +
								comp.getCatalogEntryReference().getDisplayName() + " is not for purchase'");
					}
				}
			}
		}

		String itemType = (String)catEntry.getUserData().getUserDataField().get("itemtype");
		System.out.println("-->> UpdateCatalogEntryAction itemType = " + itemType);

		// check that the service version has been increased
		if(null == itemType) 
		{
			// check the version field
			Map parameters = catEntry.getUserData().getUserDataField();
			String oldVersion = (String)parameters.get("serviceversion");
			String oldVersionNotes = (String)parameters.get("serviceversionnotes");
			System.out.println("-->> UpdateCatalogEntryAction oldVersion = " + oldVersion + 
					" oldVersionNotes = " + oldVersionNotes);
			if(null != oldVersion && oldVersion.length() > 0 && null != oldVersionNotes && oldVersionNotes.length() > 0)
			{
				String version = pRequest.getParameter("x_serviceversion");
				System.out.println("-->> UpdateCatalogEntryAction version = " + version);
				if(null != version && version.length() > 0)
				{
					String versionNotes = pRequest.getParameter("x_serviceversionnotes");
					System.out.println("-->> UpdateCatalogEntryAction versionNotes = " + versionNotes);
					if(null == versionNotes || versionNotes.length() == 0)
					{
						versionNotes = oldVersionNotes;
					}
					if(Double.parseDouble(oldVersion) > Double.parseDouble(version))
					{
						throw new Exception("ERROR: The new version must be increased comparing to '" + oldVersion + "'");
					}
					version = String.valueOf(Double.parseDouble(version));
					String[] val = new String[1];
					val[0] = version;
					pRequest.getParameterMap().put("x_serviceversion", val);
				}			
			}
		}

		// update the parameter request with the new application parameters
		ServletRequestWrapper request = new ServletRequestWrapper(pRequest);

		// check that the item is of type AAL_APPLICATION
		if(null != itemType && itemType.equals("AAL_APPLICATION"))
		{
			AppHandler handler = null;
			
			// new application file is uploaded
			TypedProperty value = (TypedProperty)pRequest.getAttribute("DecryptedReqProp");
			if(null != value && value.containsKey("Filename") && value.containsKey("UpLoadedFile"))
			{
				UploadedFile file = (UploadedFile)value.get("UpLoadedFile");
				
				handler = new AppHandler(file.getInputStream());
				
				Map appParams = null;
				try
				{
					handler.init();
					appParams = handler.getUappConfigParameters();
				}
				catch(Exception ex) {} // not a uapp file - ignore
				
				// update the parameter request with the new application parameters
				request.addParameter("x_filetype", null == appParams ? "" : "uapp");
				request.addParameter("name", null == appParams ? "" : (String)appParams.get(AppHandler.APP_NAME));
				request.addParameter("sDesc", null == appParams ? "" : (String)appParams.get(AppHandler.APP_DESCRIPTION));
				request.addParameter("keyword", null == appParams ? "" : (String)appParams.get(AppHandler.APP_KEYWORDS));
				request.addParameter("x_organizationname",  null == appParams ? "" : (String)appParams.get(AppHandler.APP_ORGANIZATION_NAME));
				request.addParameter("x_organizationurl",  null == appParams ? "" : (String)appParams.get(AppHandler.APP_ORGANIZATION_URL));
				request.addParameter("x_developername", null == appParams ? "" : (String)appParams.get(AppHandler.APP_DEVELOPER_NAME));
				request.addParameter("x_developeremail", null == appParams ? "" : (String)appParams.get(AppHandler.APP_DEVELOPER_EMAIL));
				request.addParameter("x_developerphone", null == appParams ? "" : (String)appParams.get(AppHandler.APP_DEVELOPER_PHONE));
				request.addParameter("x_organizationcertificate", null == appParams ? "" : (String)appParams.get(AppHandler.APP_ORGANIZATION_CERTIFICATE));
				request.addParameter("x_requirements", null == appParams ? "" : (String)appParams.get(AppHandler.APP_REQUIREMENTS));
				request.addParameter("x_licenses", null == appParams ? "" : (String)appParams.get(AppHandler.APP_LICENSES));
				request.addParameter("x_appservicelevelagreement", null == appParams ? "" : (String)appParams.get(AppHandler.APP_SLA));

				changeFileInNexusServer(catEntry, handler.getAppFileInputStream(), pRequest);
			}
		}
		
		// carry on with BusinessObjectDocumentAction's executing
		return super.executeBusinessObjectDocument(
				pActionMapping, 
				pActionForm, 
				request, 
				pResponse, 
				pBusinessContext, 
				pCallbackHandler);
     }

	/**
	 * Add a file to the Nexus repository under the same application code and a 
	 * different version.
	 * 
	 * @param pItem 
	 * 		The <code>com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType</code>
	 * 		that holds the identifiers of the file to be updated
	 * @param pInputStream 
	 * 		The input stream of the file to be stored
	 * @param pRequest 
	 * 		The HTTP request object that contains the new file
	 * 
	 * @throws uAALException 
	 * 		In case a problem occurred during the update process
	 */
	private void changeFileInNexusServer(
			CatalogEntryType pItem, 
			InputStream pInputStream,
			HttpServletRequest pRequest)
		throws uAALException
	{
		// retrieve the version
		Map parameters = pItem.getUserData().getUserDataField();
		String oldVersion = (String)parameters.get("version");
		String oldVersionNotes = (String)parameters.get("versionnotes");
		System.out.println("-->> UpdateCatalogEntryAction oldVersion = " + oldVersion + 
				" oldVersionNotes = " + oldVersionNotes);

		String version = pRequest.getParameter("x_version");
		System.out.println("-->> UpdateCatalogEntryAction version = " + version);
		if(null == version || version.length() == 0)
		{
			throw new uAALException("ERROR: The 'version' field must be updated");
		}
				
		String versionNotes = pRequest.getParameter("x_versionnotes");
		System.out.println("-->> UpdateCatalogEntryAction versionNotes = " + versionNotes);
		if(null == versionNotes || versionNotes.length() == 0)
		{
			versionNotes = oldVersionNotes;
			if(null == versionNotes || versionNotes.length() == 0)
			{
				throw new uAALException("ERROR: The 'version notes' field must be updated");
			}
		}

		if(null != oldVersion)
		{
			if(Double.parseDouble(oldVersion) >= Double.parseDouble(version))
			{
				throw new uAALException("ERROR: The new version must be increased comparing to '" + oldVersion + "'");
			}
		}
			
		String id = pItem.getCatalogEntryIdentifier().getUniqueID();
		version = String.valueOf(Double.parseDouble(version));
		String[] val = new String[1];
		val[0] = version;
		pRequest.getParameterMap().put("x_version", val);

		String fileType = "uaal";
		System.out.println("-->> UpdateCatalogEntryAction - Update file in the nexus server (" + 
				id + " > " + id + " > " + version + ") file type is " + fileType);
		NexusHandler.getInstance().deployArtifactToNexusServer(
				pInputStream, id, id, version, versionNotes, fileType);
	}
}
