/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.catalog.action;

import java.util.Map;

import javax.security.auth.callback.CallbackHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.user.UserSessionInfo;
import org.universaal.commerce.ustore.utilities.CatalogUtilities;

import com.ibm.commerce.foundation.client.facade.bod.servlet.struts.BusinessObjectDocumentAction;
import com.ibm.commerce.foundation.common.datatypes.BusinessContextType;

/**
 * This action class is responsible for all the operations that have to be done
 * when updating an item (AAL Application/Hardware/Human Resource) in the 
 * service components list.
 * - Verify that the user who updates the service components list is the one 
 *   that created the service, otherwise throw an exception
 * - Items can be updated in the service components list even if the service has 
 *   already been purchased by users. The service provider is responsible to 
 *   manage the items of the service and to take into consideration the status 
 *   of the service
 *  
 * @author roni
 */
public class UpdateServiceComponentAction extends BusinessObjectDocumentAction 
{
	/**
	 * Override the execute method.
	 * 
	 * @param pActionMapping 
	 * 		The configuration of the struts action
	 * @param pActionForm 
	 * 		The action form of the struts action
	 * @param pRequest 
	 * 		The HTTP request object that contains the information pertaining to 
	 * 		the request
	 * @param pResponse 
	 * 		The HTTP response object that will contain the information 
	 * 		pertaining to the response
	 * @param pBusinessContext 
	 * 		The business context of the current request 
	 * @param pCallbackHandler 
	 * 		The authentication callback handler to associate with the client library
	 * 
	 * @return A <code>java.util.Map</code> that is used in the result of the 
	 * 		current request
	 * 
	 * @throws <code>java.lang.Exception</code> in case a problem occurred while 
	 * 		parsing the HTTP request or executing the business object document. 
	 * 		These exceptions can range from server side validation which are 
	 * 		recoverable to server errors where are not recoverable
	 */
	protected Map executeBusinessObjectDocument(
			ActionMapping pActionMapping, 
			ActionForm pActionForm, 
			HttpServletRequest pRequest, 
			HttpServletResponse pResponse, 
			BusinessContextType pBusinessContext, 
			CallbackHandler pCallbackHandler)
     throws Exception
     {
		String serviceId = pRequest.getParameter("parentCatentryId");
		String storeId = pRequest.getParameter("storeId");
		String componentId = pRequest.getParameter("componentId");
		
		if(null == storeId || null == serviceId || null == componentId)
		{
			// carry on with BusinessObjectDocumentAction's executing
			return super.executeBusinessObjectDocument(
					pActionMapping, 
					pActionForm, 
					pRequest, 
					pResponse, 
					pBusinessContext, 
					pCallbackHandler);
		}

		UserSessionInfo info = UserSessionHandler.getUserSessionInfoBySessionKey(
				pRequest.getSession().getId());
		System.out.println("-->> UpdateServiceComponentAction userSessionInfo = " + info);
		if(null == info)
		{
			throw new Exception("ERROR: Session id in invalid. Please re-login!");
		}
		
		// check that the user that updates this service component is the one that created the service
		String userName = CatalogUtilities.getCatalogEntryAccessBean(serviceId).getField5();
		System.out.println("-->> UpdateServiceComponentAction The userName that created the service = " + userName + 
				" The userName that wants to update a service component = " + info.getUserName());
		if(null == userName || null == info.getUserName() || !userName.equals(info.getUserName()))
		{
			throw new Exception("ERROR: User " + info.getUserName() + 
					" is not authorized to update this service");
		}
		
		// carry on with BusinessObjectDocumentAction's executing
		return super.executeBusinessObjectDocument(
				pActionMapping, 
				pActionForm, 
				pRequest, 
				pResponse, 
				pBusinessContext, 
				pCallbackHandler);
     }
}
