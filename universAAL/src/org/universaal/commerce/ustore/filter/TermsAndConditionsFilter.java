/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.filter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.universaal.commerce.ustore.utilities.CommonUtilities;

/**
 * This class implements a filer for the incoming requests to the store.
 * The filter redirects the user to accept the terms and conditions of the store 
 * in case he hasn't done that before.
 * 
 * @author sivan
 * @author roni
 */
public class TermsAndConditionsFilter 
	implements Filter 
{
	/**
	 * The name of the cookie that is associated with the terms and conditions 
	 * acceptance process  
	 */
	private static final String COOKIE_STRING = "UstoreTermsAndConditions";
	
	/**
	 * The filter configuration
	 */
	private FilterConfig mFilterConfig = null;

	/**
	 * Called by the web container to indicate to a filter that it is being 
	 * placed into service.
	 * 
	 * @param pFilterConfig 
	 * 		The filter configuration
	 * 
	 * @throws <code>javax.servlet.ServletException</code> 
	 * 		If an exception has occurred during the initialization
	 */
	@Override
	public void init(FilterConfig pFilterConfig) 
		throws ServletException 
	{
		mFilterConfig = pFilterConfig;
	}

	/**
	 * Called by the web container to indicate to a filter that it is being 
	 * taken out of service.
	 */
	@Override
	public void destroy() 
	{
	}

	/**
	 * Examines the request and based on the existence of the cookie decides 
	 * whether to invoke the next entity in the chain or to redirect to the 
	 * terms and conditions page.
	 * 
	 * @param pRequest 
	 * 		The HTTP servlet request being processed
	 * @param pResponse 
	 * 		The HTTP response being created
	 * @param pFilterChain 
	 * 		The filter chain allows the filter to pass on the request and 
	 * 		response to the next entity in the chain
	 * 
	 *  @throws <code>java.io.IOException</code>
	 *  @throws <code>javax.servlet.ServletException</code>
	 */
	@Override
	public void doFilter(
			ServletRequest pRequest, 
			ServletResponse pResponse,
			FilterChain pFilterChain) 
		throws IOException, ServletException 
	{
		HttpServletRequest httpRequest = (HttpServletRequest)pRequest;
		HttpServletResponse httpResponse = (HttpServletResponse)pResponse;

		// check if terms and conditions cookie exists
		Cookie[] allCookies = httpRequest.getCookies();
		if(null != allCookies) 
		{
			for(int i=0; i<allCookies.length; i++) 
			{
				String name = allCookies[i].getName();
				if(COOKIE_STRING.equals(name)) 
				{
					// continue normally
					pFilterChain.doFilter(pRequest, pResponse);
					return;
				}
			}
		}

		// cookie not found, redirect to sign terms and conditions
		String url = httpRequest.getRequestURL().toString();
		System.out.println("-->> TermsAndConditionsFilter: url = " + url);

		// continue normally in case there is a request to retrieve the service 
		// file (will be checked then with the provided session key)
		if(null != url && url.indexOf("StoreRetrieveServiceFile") != -1)
		{
			System.out.println("-->> TermsAndConditionsFilter: " +
					"request to retrieve a service file - continue normally");

			// continue normally
			pFilterChain.doFilter(pRequest, pResponse);
			return;
		}
		
		String serverName = httpRequest.getServerName();
		
		// should we add remote IP to redirection?
		String ip = httpRequest.getRemoteAddr();

		System.out.println("-->> TermsAndConditionsFilter: serverName = " +	serverName + " ip = " + ip);

		if(null == ip)
		{
			// continue normally
			pFilterChain.doFilter(pRequest, pResponse);
			return;
		}

		// write to the log
		FileOutputStream outputStream = new FileOutputStream("/TermsAndConditions.log", true);
		PrintWriter printWriter = new PrintWriter(outputStream);
		printWriter.print(ip);
		printWriter.print('\t');
		printWriter.print(new Date().toString());
		printWriter.print('\t');
		printWriter.print(url);
		printWriter.print('\n');
		printWriter.close();

		String port = CommonUtilities.getInstance().getProperty("termsAndConditionsPort", "80");

		System.out.println("-->> TermsAndConditionsFilter: send redirect to http://" + 
				serverName + ":" + port + "/universAAL?" + ip + "&" + url);

		String encodedIp = URLEncoder.encode(ip, "UTF-8");
		url = URLEncoder.encode(url, "UTF-8");

		httpResponse.sendRedirect("http://" + serverName + ":" + port + "/universAAL?" + 
				encodedIp + "&" + url);
	}
}
