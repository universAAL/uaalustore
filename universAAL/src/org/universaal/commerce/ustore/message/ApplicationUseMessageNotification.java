/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.message;

import java.text.MessageFormat;
import java.util.Map;

import org.universaal.commerce.ustore.uAALException;

/**
 * This class holds the content of the message that will be sent to a developer 
 * when one of his applications is included in a service that is offered in 
 * uStore.  
 * 
 * @author roni
 */
public class ApplicationUseMessageNotification 
	extends GenericMessageNotification
{
	/**
	 * The extracted parameters for the composition of the message to the 
	 * developer
	 */
	public static final String PARAM_DEVELOPER_NAME = "developerName";
	public static final String PARAM_DEVELOPER_EMAIL = "developerEmail";
	public static final String PARAM_SERVICE_PROVIDER_USER_NAME = "serviceProviderUserName";
	public static final String PARAM_SERVICE_PROVIDER_NAME = "serviceProviderName";
	public static final String PARAM_SERVICE_PROVIDER_PHONE = "serviceProviderPhone";
	public static final String PARAM_SERVICE_PROVIDER_EMAIL = "serviceProviderEmail";
	public static final String PARAM_SERVICE_PROVIDER_ORGANIZATION = "serviceProviderOrganization";
	public static final String PARAM_APPLICATION_ID = "appId";
	public static final String PARAM_APPLICATION_NAME = "appName";
	public static final String PARAM_SERVICE_NAME = "serviceName";
	public static final String PARAM_SERVICE_ID = "serviceId";
	public static final String PARAM_TIME = "time";
	
	/**
	 * The subject of the email message sent to the developer
	 */
	private static final String DEFAULT_MESSAGE_SUBJECT = 
		"Application use notication [uStore]";
	
	/**
	 * Place holder for the developer name in the email message
	 */
	private static final String DEFAULT_DEVELOPER_NAME = 
		"Developer";

	/**
	 * Place holder for the service name in the email message
	 */
	private static final String DEFAULT_UNKNOWN = "Unknown";
	
	/**
	 * The first part of message that is sent to the developer
	 */
	private final static String MSG_PART1 =
		"Dear {0},\r\n\r\n"  +
		"We would like to inform you that your application {1} (id #{2}) has been included " +
		"in service {3} (id #{4}) by service provider {5} on {6}.\r\n\r\n";

	/**
	 * The second part of message that is sent to the developer with the service 
	 * provider information
	 */
	private final static String MSG_PART2 =
		"Please contact the service provider for additional information:\r\n" +
		"Name: {0}\r\n" +
		"Phone number: {1}\r\n" +
		"Email address: {2}\r\n" +
		"Organization: {3}\r\n\r\n";

   /**
	 * The third part of message that is sent to the developer
	 */
	private final static String MSG_PART3 =
		"Best Regards,\r\n" +
		"The uStore team.\r\n";

	/**
	 * The content of the message to be sent
	 */
	private String mMessage = null;
	
	/**
	 * The email address of the developer
	 */
	private String mToEmailMessage = null;
	
	/**
	 * Compose a message to the developer detailing information on the service 
	 * provider and on the service.
	 * 
	 * @param pMessageParams
	 * 		The message information
	 * 
	 * @return a <code>String</code> containing the message
	 * 
	 * @throws uAALException
	 * 		If there is missing information required to sending the message
	 */
	public ApplicationUseMessageNotification(Map pMessageParams)
		throws uAALException
	{
		String developerName = DEFAULT_DEVELOPER_NAME;
		if(null != pMessageParams.get(PARAM_DEVELOPER_NAME))
		{
			developerName = (String)pMessageParams.get(PARAM_DEVELOPER_NAME); 
		}

		String appName = "<" + DEFAULT_UNKNOWN + ">";
		if(null != pMessageParams.get(PARAM_SERVICE_NAME))
		{
			appName = "<" + (String)pMessageParams.get(PARAM_APPLICATION_NAME) + ">"; 
		}
		
		String serviceName = "<" + DEFAULT_UNKNOWN + ">";
		if(null != pMessageParams.get(PARAM_SERVICE_NAME))
		{
			serviceName = "<" + (String)pMessageParams.get(PARAM_SERVICE_NAME) + ">"; 
		}

		String[] valuesPart1 = {
				developerName, 
				appName,
				(String)pMessageParams.get(PARAM_APPLICATION_ID),
				serviceName,
				(String)pMessageParams.get(PARAM_SERVICE_ID),
				"<" + (String)pMessageParams.get(PARAM_SERVICE_PROVIDER_USER_NAME) + ">",
				(String)pMessageParams.get(PARAM_TIME)};
		
		String msgPart1 = MessageFormat.format(MSG_PART1, (Object[])valuesPart1);
		
		String serviceProviderName = DEFAULT_UNKNOWN;
		if(null != pMessageParams.get(PARAM_SERVICE_PROVIDER_NAME))
		{
			serviceProviderName = (String)pMessageParams.get(PARAM_SERVICE_PROVIDER_NAME); 
		}
		String serviceProviderPhoneNumber = DEFAULT_UNKNOWN;
		if(null != pMessageParams.get(PARAM_SERVICE_PROVIDER_PHONE))
		{
			serviceProviderPhoneNumber = (String)pMessageParams.get(PARAM_SERVICE_PROVIDER_PHONE); 
		}
		String serviceProviderEmailAddress = DEFAULT_UNKNOWN;
		if(null != pMessageParams.get(PARAM_SERVICE_PROVIDER_EMAIL))
		{
			serviceProviderEmailAddress = (String)pMessageParams.get(PARAM_SERVICE_PROVIDER_EMAIL); 
		}
		String serviceProviderOrganization = DEFAULT_UNKNOWN;
		if(null != pMessageParams.get(PARAM_SERVICE_PROVIDER_ORGANIZATION))
		{
			serviceProviderOrganization = (String)pMessageParams.get(PARAM_SERVICE_PROVIDER_ORGANIZATION); 
		}
		
		String[] valuesPart2 = {
				serviceProviderName,
				serviceProviderPhoneNumber,
				serviceProviderEmailAddress,
				serviceProviderOrganization};
		
		String msgPart2 = MessageFormat.format(MSG_PART2, (Object[])valuesPart2);

		String msgPart3 = MSG_PART3;
		
		mMessage = msgPart1 + msgPart2 + msgPart3;
		
		mToEmailMessage = (String)pMessageParams.get(PARAM_DEVELOPER_EMAIL);
	}
	
	/**
	 * @return A <code>String</code> containing the message subject
	 */
	public String getMessageSubject()
	{
		return DEFAULT_MESSAGE_SUBJECT;
	}
	
	/**
	 * @return A <code>String</code> containing the message itself
	 */
	public String getMessage()
	{
		return mMessage;
	}
	
	/**
	 * @return A <code>String</code> containing the TO email address
	 */
	public String getToEmailAddress()
	{
		return mToEmailMessage;
	}
}

