/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.message;

/**
 * This is an abstract class for all the email messages that uStore sends as
 * notifications.
 * Any new message that extends this class should implement the abstract methods
 * and then it can be sent by the <code>MessageSender</code>.
 *   
 * @author roni
 */
public abstract class GenericMessageNotification 
{
	/**
	 * @return A <code>String</code> containing the message subject
	 */
	public abstract String getMessageSubject();
	
	/**
	 * @return A <code>String</code> containing the message itself
	 */
	public abstract String getMessage();

	/**
	 * @return A <code>String</code> containing the TO email address
	 */
	public abstract String getToEmailAddress();
}

