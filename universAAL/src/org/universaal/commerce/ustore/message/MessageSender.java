/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.message;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.utilities.CommonUtilities;

/**
 * This class sends an email message.
 * 
 * @author roni
 */
public class MessageSender 
{
	/**
	 * Sends an email message.
	 * 
	 *  @param pMessage
	 *  	The message to send
	 *  
	 * @throws uAALException
	 * 		If there is missing information required to sending the message or 
	 * 		there is a problem with the sending process
	 */
	public static void sendMessage(GenericMessageNotification pMessage)
		throws uAALException
	{
  		if(null == pMessage.getToEmailAddress())
  		{
  			throw new uAALException("ERROR: The email address is not defined");
  		}
  		
		System.out.println("-->> The message is: " + pMessage.getMessage());
  		try
    	{
      		Properties props = System.getProperties();

      		// attaching to default session, or we could start a new one
      		props.put("mail.smtp.host", CommonUtilities.getProperty("smtpServer", null));  
      		Session session = Session.getInstance(props, null);

      		// create a new message
      		Message msg = new MimeMessage(session);
      		
      		// set the FROM
      		msg.setFrom(new InternetAddress(
      				CommonUtilities.getProperty("fromEmailAddress", null)));
      		
      		// set the TO
      		msg.setRecipients(Message.RecipientType.TO,
      				InternetAddress.parse(pMessage.getToEmailAddress(), false));

      		// set the subject and body text
      		msg.setSubject(pMessage.getMessageSubject());
      		msg.setText(pMessage.getMessage());

      		// set some other header information 
      		msg.setSentDate(new Date());

      		// send the message
      		Transport.send(msg);
    	}
    	catch(Exception ex) 
    	{
    		throw new uAALException("ERROR: Failed to send message to <" + 
    				pMessage.getToEmailAddress() + "> (" + ex.getMessage() + ")");
    	}
	}
}

