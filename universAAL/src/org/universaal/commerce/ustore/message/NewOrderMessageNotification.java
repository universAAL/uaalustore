/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.message;

import java.text.MessageFormat;
import java.util.Map;
import java.util.StringTokenizer;

import org.universaal.commerce.ustore.uAALException;

/**
 * This class holds the content of the message that will be sent to a service 
 * provider when an order that contains one of his services is made by a user in
 * uStore.
 * The message describes the required actions from the service provider to 
 * complete the order processing.
 * 
 * @author roni
 */
public class NewOrderMessageNotification 
	extends GenericMessageNotification
{
	/**
	 * The extracted parameters for the composition of the message to the 
	 * service provider
	 */
	public static final String PARAM_ADMIN_USER_NAME = "adminUserName";
	public static final String PARAM_ADMIN_PASSWORD = "adminPassword";
	public static final String PARAM_USER_NAME = "userName";
	public static final String PARAM_USER_IP_ADDRESS_AND_PORT = "userIpAddressAndPort";
	public static final String PARAM_USER_ADDRESS = "userAddress";
	public static final String PARAM_USER_PHONE = "userPhone";
	public static final String PARAM_USER_MOBILE_PHONE = "userMobilePhone";
	public static final String PARAM_SERVICE_NAME = "serviceName";
	public static final String PARAM_SERVICE_PROVIDER_NAME = "serviceProviderName";
	public static final String PARAM_SERVICE_PROVIDER_EMAIL = "serviceProviderEmail";
	public static final String PARAM_SERVICE_INCLUDE_HR = "serviceIncludeHR";
	public static final String PARAM_ORDER_ID = "orderId";
	public static final String PARAM_ORDER_TIME = "orderTime";
	
	/**
	 * The subject of the email message sent to the service provider
	 */
	private static final String DEFAULT_MESSAGE_SUBJECT = 
		"Purchase notication [uStore]";
	
	/**
	 * Place holder for the service provider name in the email message
	 */
	private static final String DEFAULT_SERVICE_PROVIDER_NAME = 
		"Service Provider";

	/**
	 * Place holder for the service name in the email message
	 */
	private static final String DEFAULT_UNKNOWN = "Unknown";
	
	/**
	 * The first part of message that is sent to the service provider
	 */
	private final static String MSG_PART1 =
		"Dear {0},\r\n\r\n"  +
		"We would like to inform you that your service {1} has been purchased " +
		"by user {2} on {3} (order #{4}).\r\n\r\n";

	/**
	 * The second part of message that is sent to the service provider (in case 
	 * the user information is available)
	 */
	private final static String MSG_PART2_1 =
		"Please install and configure the service remotely by accessing this " + 
		"address: http://{0}/ucc and use the following credentials:\r\n" +
		"Username: {1}\r\n" +
		"Password: {2}\r\n\r\n";

	/**
	 * The second part of message that is sent to the service provider (in case 
	 * the user information isn't available)
	 */
	private final static String MSG_PART2_2 =
		"The service currently can not be installed and configured remotely.\r\n\r\n";
   
	/**
	 * The third part of message that is sent to the service provider (in case 
	 * human resources are included in the service)
	 */
	private final static String MSG_PART3 =
		"This service includes human resources. Please contact the user to " +
		"schedule the arrival time:\r\n" +
  		"Phone number: {0}\r\n" +
  		"Mobile number: {1}\r\n" +
  		"Address: {2}\r\n\r\n";
   	
   /**
	 * The fourth part of message that is sent to the service provider
	 */
	private final static String MSG_PART4 =
		"Best Regards,\r\n" +
		"The uStore team.\r\n";

	/**
	 * The content of the message to be sent
	 */
	private String mMessage = null;
	
	/**
	 * The email address of the service provider
	 */
	private String mToEmailMessage = null;
	
	/**
	 * Compose a message to the service provider detailing order status and
	 * required actions.
	 * 
	 * @param pUserParams
	 * 		The ordering user information
	 * @param pOrderParams
	 * 		The order information
	 * @param pServiceParams
	 * 		The ordered service information.
	 * 
	 * @return a <code>String</code> containing the message
	 * 
	 * @throws uAALException
	 * 		If there is missing information required to sending the message
	 */
	public NewOrderMessageNotification(Map pUserParams, Map pOrderParams, Map pServiceParams)
		throws uAALException
	{
		String serviceProviderName = DEFAULT_SERVICE_PROVIDER_NAME;
		if(null != pServiceParams.get(PARAM_SERVICE_PROVIDER_NAME))
		{
			serviceProviderName = (String)pServiceParams.get(PARAM_SERVICE_PROVIDER_NAME); 
		}

		String serviceName = "<" + DEFAULT_UNKNOWN + ">";
		if(null != pServiceParams.get(PARAM_SERVICE_NAME))
		{
			serviceName = "<" + (String)pServiceParams.get(PARAM_SERVICE_NAME) + ">"; 
		}

		String[] valuesPart1 = {
				serviceProviderName, 
				serviceName,
				(String)pUserParams.get(PARAM_USER_NAME),
				(String)pOrderParams.get(PARAM_ORDER_TIME),
				(String)pOrderParams.get(PARAM_ORDER_ID)};
		
		String msgPart1 = MessageFormat.format(MSG_PART1, (Object[])valuesPart1);
		
		String msgPart2 = MSG_PART2_2;
		if(null != pUserParams.get(PARAM_USER_IP_ADDRESS_AND_PORT) &&
				null != pUserParams.get(PARAM_ADMIN_USER_NAME) &&
				null != pUserParams.get(PARAM_ADMIN_PASSWORD))
		{
			String ipAddressAndPort = (String)pUserParams.get(PARAM_USER_IP_ADDRESS_AND_PORT);
			StringTokenizer st = new StringTokenizer(ipAddressAndPort, ":");
			
			String[] valuesPart2 = {
					st.hasMoreTokens() ? st.nextToken() : ipAddressAndPort, 
					(String)pUserParams.get(PARAM_ADMIN_USER_NAME),
					(String)pUserParams.get(PARAM_ADMIN_PASSWORD)};
			msgPart2 = MessageFormat.format(MSG_PART2_1, (Object[])valuesPart2);
		}

		String msgPart3 = "";
		if(null != pServiceParams.get(PARAM_SERVICE_INCLUDE_HR))
		{
			String userPhone = DEFAULT_UNKNOWN;
			if(null != pUserParams.get(PARAM_USER_PHONE))
			{
				userPhone = (String)pUserParams.get(PARAM_USER_PHONE); 
			}
			String userMobilePhone = DEFAULT_UNKNOWN;
			if(null != pUserParams.get(PARAM_USER_MOBILE_PHONE))
			{
				userMobilePhone = (String)pUserParams.get(PARAM_USER_MOBILE_PHONE); 
			}
			String userAddress = DEFAULT_UNKNOWN;
			if(null != pUserParams.get(PARAM_USER_ADDRESS))
			{
				userAddress = (String)pUserParams.get(PARAM_USER_ADDRESS); 
			}
			String[] valuesPart3 = {userPhone, userMobilePhone, userAddress};
			msgPart3 = MessageFormat.format(MSG_PART3, (Object[])valuesPart3);
		}
		
		String msgPart4 = MSG_PART4;
		
		mMessage = msgPart1 + msgPart2 + msgPart3 + msgPart4;
		
		mToEmailMessage = (String)pServiceParams.get(PARAM_SERVICE_PROVIDER_EMAIL);
	}
	
	/**
	 * @return A <code>String</code> containing the message subject
	 */
	public String getMessageSubject()
	{
		return DEFAULT_MESSAGE_SUBJECT;
	}
	
	/**
	 * @return A <code>String</code> containing the message itself
	 */
	public String getMessage()
	{
		return mMessage;
	}
	
	/**
	 * @return A <code>String</code> containing the TO email address
	 */
	public String getToEmailAddress()
	{
		return mToEmailMessage;
	}
}

