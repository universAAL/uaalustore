/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.order.command;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.security.auth.callback.CallbackHandler;

import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.message.GenericMessageNotification;
import org.universaal.commerce.ustore.message.MessageSender;
import org.universaal.commerce.ustore.message.NewOrderMessageNotification;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.user.UserSessionInfo;
import org.universaal.commerce.ustore.utilities.CatalogUtilities;
import org.universaal.commerce.ustore.utilities.CommonUtilities;

import com.ibm.commerce.accesscontrol.AccessVector;
import com.ibm.commerce.accesscontrol.ResourceActionPair;
import com.ibm.commerce.catalog.facade.datatypes.CatalogDescriptionType;
import com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType;
import com.ibm.commerce.catalog.objects.CatalogEntryAccessBean;
import com.ibm.commerce.datatype.TypedProperty;
import com.ibm.commerce.exception.ECException;
import com.ibm.commerce.order.commands.OrderProcessCmd;
import com.ibm.commerce.order.commands.OrderProcessCmdImpl;
import com.ibm.commerce.order.objects.OrderAccessBean;
import com.ibm.commerce.order.objects.OrderItemAccessBean;
import com.ibm.commerce.user.objects.AddressAccessBean;
import com.ibm.commerce.user.objects.UserAccessBean;

/**
 * This class extends the actions taking place once an order is made by a user
 * in uStore, by sending emails to the service provider describing required 
 * actions to complete the order.
 * 
 * @author roni
 */
public class OrderProcessCommand 
	extends OrderProcessCmdImpl
	implements OrderProcessCmd
{
	/**
	 * Strings used to extract information from the response of the regular
	 * processing of an order
	 */
	private static final String ORDER_ID_KEY = "orderId";
	private static final String PAYMENT_STATUS_KEY = "paymentStatus";
	private static final String MSG_KEY = "msglistkey";
	private static final String SUCCESS_KEY = "success";
	private static final String ADMIN_USER_NAME_KEY = "adminUserName";
	private static final String ADMIN_PASSWORD_KEY = "adminPassword";
	
	/**
	 * Perform order processing
	 * 
	 * @throws ECException 
	 * 		If an error occurred during the order processing or the email 
	 * 		message	sending 
	 */
	public void performExecute()
    	throws ECException
    {
		// perform regular order processing
		super.performExecute();

		// extract information from regular processing response
		AccessVector resources = getResources();
		TypedProperty property = getResponseProperties();
		if(null == property || null == resources || resources.size() == 0 || null == getUser())
		{
			System.out.println("WARNING: There are no response properties, " +
					"resources or user to the OrderProcessCmd");
			return;
		}
		
		String orderId = property.getString(ORDER_ID_KEY);
		String status = property.getString(PAYMENT_STATUS_KEY);
		if(null == orderId || null == status || !status.equalsIgnoreCase(SUCCESS_KEY))
		{
			System.out.println("WARNING: The response properties are not valid");
			return;
		}

		try
		{
			UserSessionInfo info = 
				UserSessionHandler.getUserSessionInfoByUserName(getUser().getDisplayName());
			
			if(null == info) // user session info was not found 
			{
				System.out.println("WARNING: There is no user info");
				return;
			}
			
			Map userParams = extractUserInfo(getUser());

			// iterate through the resources
			for(int i=0; i<resources.size(); i++)
			{
				ResourceActionPair pair = (ResourceActionPair)resources.get(i);
				if(null != pair.getResource())
				{
					OrderAccessBean order = (OrderAccessBean)pair.getResource();
					try
					{
						Map orderParams = extractOrderInfo(order);
						
						// iterate through the purchased services in the order 
						OrderItemAccessBean[] items = order.getOrderItems();
						for(int j =0; j<items.length; j++)
						{
							try
							{
								Map serviceParams = extractServiceInfo(items[j], info.getCallbackHandler());
								sendMessage(userParams, orderParams, serviceParams);
							}
							catch(Exception ex)
							{
								System.out.println(ex.getMessage());
							}
						}
					}
					catch(Exception ex)
					{
						System.out.println(ex.getMessage());
					}
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
    }
	
	/**
	 * Extract completed order information into a map.
	 * 
	 * @param pOrder
	 * 		An access bean with completed order information
	 *            
	 * @return
	 * 		A <code>java.util.Map</code> containing the order ID and time
	 * 
	 * @throws uAALException
	 * 		If the order information does not contain expected data
	 */
	private Map extractOrderInfo(OrderAccessBean pOrder)
		throws uAALException
	{
		Map params = new HashMap();
		
		try
		{
			params.put(NewOrderMessageNotification.PARAM_ORDER_ID, pOrder.getOrderId());
			params.put(NewOrderMessageNotification.PARAM_ORDER_TIME, pOrder.getPlaceOrderTime());
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to extract information from the order");
		}
		
		System.out.println("-->> extractOrderInfo: params = " + params.toString());
		
		return params;
	}
	
	/**
	 * Extract purchasing user information from a completed order into a map.
	 * 
	 * @param pUser
	 * 		An access bean with user information
	 * 
	 * @return 
	 * 		A <code>java.util.Map</code> containing the user name and connection 
	 * 		data
	 * @throws uAALException
	 * 		If the user information does not contain expected data
	 */
	private Map extractUserInfo(UserAccessBean pUser)
		throws uAALException
	{
		Map params = new HashMap();

		try
		{
	  		params.put(NewOrderMessageNotification.PARAM_USER_NAME, pUser.getDisplayName());
	  		params.put(NewOrderMessageNotification.PARAM_USER_IP_ADDRESS_AND_PORT, pUser.getUserField2());
	
			String adminDetails = pUser.getUserField3();
			if(null != adminDetails)
			{
				int index1 = adminDetails.indexOf("<" + ADMIN_USER_NAME_KEY + ">");
				if(index1 > -1)
				{
					int index2 = adminDetails.indexOf("</" + ADMIN_USER_NAME_KEY + ">", index1+1);
					if(index2 > -1)
					{
						String userName = adminDetails.substring(index1+ADMIN_PASSWORD_KEY.length()+2, index2);
						params.put(NewOrderMessageNotification.PARAM_ADMIN_USER_NAME, userName);
					}
				}
				index1 = adminDetails.indexOf("<" + ADMIN_PASSWORD_KEY + ">");
				if(index1 > -1)
				{
					int index2 = adminDetails.indexOf("</" + ADMIN_PASSWORD_KEY + ">", index1+1);
					if(index2 > -1)
					{
						String password = adminDetails.substring(index1+ADMIN_PASSWORD_KEY.length()+2, index2);
						params.put(NewOrderMessageNotification.PARAM_ADMIN_PASSWORD, password);
					}
				}
			}
	
			String userAddress = null;
			String userPhone = null;
			String userMobilePhone = null;
	
			String[] keys = {"S", "B", "SB"};
	  		for(int i=0; i<keys.length; i++)
	  		{
				Enumeration addresses = pUser.getAddress(keys[i]);
				while(addresses.hasMoreElements())
				{
					AddressAccessBean addr = (AddressAccessBean)addresses.nextElement();
					userAddress = null == userAddress ? getAddress(addr) : userAddress;
					userPhone = null == userPhone ? addr.getPhone1() : userPhone;
					userMobilePhone = null == userMobilePhone ? addr.getMobilePhone1() : userMobilePhone;
				}
	  		}
	  		
	  		params.put(NewOrderMessageNotification.PARAM_USER_ADDRESS, userAddress);
	  		params.put(NewOrderMessageNotification.PARAM_USER_PHONE, userPhone);
	  		params.put(NewOrderMessageNotification.PARAM_USER_MOBILE_PHONE, userMobilePhone);
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to extract information from the user");
		}
		
  		System.out.println("-->> extractUserInfo: params = " + params.toString());
		
		return params;
	}
	
	/**
	 * Extract service information from a completed order item into a map.
	 * 
	 * @param pOrderItem
	 * 		An access bean with completed order item information
	 * @param pCallbackHandler
	 * 		A callback handler for issuing queries
	 * 
	 * @return 
	 * 		A <code>java.util.Map</code> containing details on the service and 
	 * 		service provider
	 * 
	 * @throws uAALException
	 * 		If errors occurred during data processing
	 */
	private Map extractServiceInfo(
			OrderItemAccessBean pOrderItem, CallbackHandler pCallbackHandler)
		throws uAALException
	{
		Map params = new HashMap();
		
		String serviceName = null;
		String serviceProviderName = null;
		String serviceProviderEmail = null;

		try
		{
			if(null != pOrderItem.getCatalogEntry())
			{
				CatalogEntryType entryType = CatalogUtilities.getCatalogEntryById(
						pOrderItem.getCatalogEntryId(), 
						String.valueOf(getStoreId().intValue()),
						null,
						CommonUtilities.getInstance().getBusinessContext(), 
						pCallbackHandler);
				
				if(null != entryType)
				{
					if(null != entryType.getDescription() && entryType.getDescription().size() > 0)
					{
						params.put(NewOrderMessageNotification.PARAM_SERVICE_NAME, 
								((CatalogDescriptionType)entryType.getDescription().get(0)).getName());
					}
					if(null != entryType.getUserData() && null != entryType.getUserData().getUserDataField())
					{
						params.put(NewOrderMessageNotification.PARAM_SERVICE_PROVIDER_EMAIL, 
								(String)entryType.getUserData().getUserDataField().get("serviceprovideremail"));
						params.put(NewOrderMessageNotification.PARAM_SERVICE_PROVIDER_NAME, 
								(String)entryType.getUserData().getUserDataField().get("serviceprovidername"));
					}
					
					// check if human resources are included in the service
					// retrieve the items from the FIELD4 for of the CATENTRY table
					// for the service record
					CatalogEntryAccessBean serviceBean = new CatalogEntryAccessBean();
					serviceBean.setInitKey_catalogEntryReferenceNumber(pOrderItem.getCatalogEntryId());
					serviceBean.refreshCopyHelper();
					String itemsList = serviceBean.getField4();

					boolean isHumanResourcesIncluded = false;
					StringTokenizer st = new StringTokenizer(itemsList, ";");
					while(st.hasMoreTokens())
					{
						String itemId = st.nextToken();
						if(null != itemId && itemId.length() > 0)
						{
							CatalogEntryType item = CatalogUtilities.getCatalogEntryById(
									itemId,
									String.valueOf(getStoreId().intValue()),
									null,
									CommonUtilities.getInstance().getBusinessContext(), 
									pCallbackHandler);
	
							if(null != item)
							{
								Map parameters = item.getUserData().getUserDataField();
								String itemType = (String)parameters.get("itemtype");
								if(null != itemType && itemType.equals("HUMAN_RESOURCE"))
								{
									isHumanResourcesIncluded = true;
									break;
								}
							}
						}
					}
					if(isHumanResourcesIncluded) 
					{
						params.put(NewOrderMessageNotification.PARAM_SERVICE_INCLUDE_HR, "true"); 
					}
				}
			}
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to extract information from the service");
		}
		
		System.out.println("-->> extractServiceInfo: params = " + params.toString());
		
		return params;
	}

  	/**
	 * Get a <code>String</code> containing the user address information.
	 * 
	 * @param pAddress
	 * 		An access bean containing the address information
	 * 
	 * @return 
	 * 		A <code>String</code> containing the users address information
	 * 
	 * @throws uAALException
	 * 		If the address access bean contains errors
	 */
  	private String getAddress(AddressAccessBean pAddress)
  		throws uAALException
  	{
  		StringBuffer sb = new StringBuffer();
  		try
  		{
	  		sb.append(null != pAddress.getAddress1() ? pAddress.getAddress1() : ""); 
	  		sb.append(null != pAddress.getCity() ? (sb.length() > 0 ? ", " + 
	  				pAddress.getCity() : pAddress.getCity()) : ""); 
	  		sb.append(null != pAddress.getState() ? (sb.length() > 0 ? ", " + 
	  				pAddress.getState() : pAddress.getState()) : ""); 
	  		sb.append(null != pAddress.getCountry() ? (sb.length() > 0 ? ", " + 
	  				pAddress.getCountry() : pAddress.getCountry()) : "");
  		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to return the user address information");
		}
  		
  		return sb.toString();
  	}
	
	/**
	 * Send a message to the service provider detailing order status and
	 * required actions.
	 * 
	 * @param pUserParams
	 * 		The ordering user information
	 * @param pOrderParams
	 * 		The order information
	 * @param pServiceParams
	 * 		The ordered service information
	 * 
	 * @throws uAALException
	 * 		If there is a problem with the sending process
	 */
  	private void sendMessage(Map pUserParams, Map pOrderParams, Map pServiceParams)
  		throws uAALException
  	{
  		GenericMessageNotification message = 
  			new NewOrderMessageNotification(pUserParams, pOrderParams, pServiceParams);
		MessageSender.sendMessage(message);
  	}
}

