/*	
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.application.AppHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.ibm.commerce.catalog.facade.datatypes.CatalogDescriptionType;
import com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType;

/**
 * This class builds and writes a .usrv file into the response output stream.
 * 
 * @author roni
 */
public class UsrvHandler 
{
	/**
	 * The name of the usrv template configuration file 
	 */
	private static final String USRV_TEMPLATE_CONFIG_FILE_NAME = 
		"/AAL-USRV-CONFIG-TEMPLATE.xml";
	
	/**
	 * The nmae of the uapp file schema
	 */
	private static final String UAPP_NAMESPACE = "uapp";
		
	/**
	 * The name of the elements in the service configuration file
	 */
	private static final String USRV_SRV_ELEMENT = "usrv:srv";
	private static final String USRV_SRV_NAME_ELEMENT = "usrv:name";
	private static final String USRV_SRV_VERSION_ELEMENT = "usrv:version";
	private static final String USRV_SRV_VERSION_MAJOR_ELEMENT = "usrv:major";
	private static final String USRV_SRV_VERSION_MINOR_ELEMENT = "usrv:minor";
	private static final String USRV_SRV_VERSION_MICRO_ELEMENT = "usrv:micro";
	private static final String USRV_SRV_ID_ELEMENT = "usrv:serviceId";
	private static final String USRV_SRV_DESCRIPTION_ELEMENT = "usrv:description";
	private static final String USRV_SRV_TAGS_ELEMENT = "usrv:tags";
	private static final String USRV_SRV_SERVICE_PROVIDER_ELEMENT = "usrv:serviceProvider";
	private static final String USRV_SRV_LICENSES_ELEMENT = "usrv:licenses";
	private static final String USRV_SRV_LICENSE_ELEMENT = "usrv:license";
	private static final String USRV_SRV_LICENSE_SLA_ELEMENT = "usrv:sla";
	private static final String USRV_SRV_NAME = "usrv:name";
	private static final String USRV_SRV_LINK = "usrv:link";
	private static final String USRV_SRV_CATEGORY = "usrv:category";
	private static final String USRV_SRV_CAPABILITIES_ELEMENT = "usrv:serviceCapabilities";
	private static final String USRV_SRV_REQUIREMENTS_ELEMENT = "usrv:serviceRequirements";
	private static final String USRV_SRV_COMPONENTS_ELEMENT = "usrv:components";
	private static final String USRV_SRV_APPLICATION_ELEMENT = "usrv:application";
	private static final String USRV_SRV_ARTIFACT_ID_ELEMENT = "usrv:artifactID";
	private static final String USRV_SRV_LOCATION_ELEMENT = "usrv:location";
	private static final String USRV_SRV_HARDWARE_ELEMENT = "usrv:hardware";
	private static final String USRV_SRV_HARDWARE_CATEGORY_ELEMENT = "usrv:category";
	private static final String USRV_SRV_HUMAN_RESOURCE_ELEMENT = "usrv:human";
	private static final String USRV_SRV_HUMAN_RESOURCE_ROLE_ELEMENT = "usrv:role";
	private static final String USRV_SRV_HUMAN_RESOURCE_COMPANY_ELEMENT = "usrv:company";
	private static final String USRV_SRV_HUMAN_RESOURCE_PHONE_ELEMENT = "usrv:phone";
	private static final String USRV_SRV_HUMAN_RESOURCE_REF_ID_ELEMENT = "usrv:refID";
	private static final String USRV_SRV_HUMAN_RESOURCE_FREQUENCY_ELEMENT = "usrv:frequency";
	private static final String USRV_SRV_HUMAN_RESOURCE_QUALIFICATION_ELEMENT = "usrv:qualification";
	private static final String USRV_SRV_HUMAN_RESOURCE_EXPERIENCE_ELEMENT = "usrv:experience";
	
	/**
	 * The output stream of the usrv file
	 */
	private ZipOutputStream mOutputStream = null;
	
	/**
	 * The service object
	 */
	private CatalogEntryType mService = null;
	
	/**
	 * The application items
	 */
	private CatalogEntryType[] mAppItems = null;
	
	/**
	 * The hardware items
	 */
	private CatalogEntryType[] mHardwareItems = null;
	
	/**
	 * The human resource items
	 */
	private CatalogEntryType[] mHumanResourceItems = null;
	
	/**
	 * The service configuration file as an XML document  
	 */
	private Document mUsrvConfigFile = null;
	
	/**
	 * The licenses node in the service configuration DOM tree 
	 */
	private Node mLicensesNode = null;

	/**
	 * The capabilities node in the service configuration DOM tree 
	 */
	private Node mCapabilitiesNode = null;

	/**
	 * The requirements node in the service configuration DOM tree 
	 */
	private Node mRequirementsNode = null;

	/**
	 * The part requirements node in the service configuration DOM tree 
	 */
	private Node mPartRequirementsNode = null;

	/**
	 * The components node in the service configuration DOM tree 
	 */
	private Node mComponentsNode = null;
	
	/**
	 * A list of all the names of the supported licenses
	 */
	private List mSupportedLicenses = new ArrayList();
	
	/**
	 * A list of all the supported capabilities in the format 
	 * <capability-name>=<capability-value>
	 */
	private List mSupportedCapabilities = new ArrayList();

	/**
	 * A list of all the supported requirements in the format 
	 * <capability-name>=<capability-value>
	 */
	private List mSupportedRequirements = new ArrayList();
	
	/**
	 * Initializes the handler and divides the included items to applications,
	 * hardware items and human resources.
	 * 
	 * @param pOutputStream
	 * 		The output stream of the usrv file to write the file into
	 * @param pService
	 * 		The service object as a <code>CatalogEntryType</code>
	 * @param pItems
	 * 		All the composed items as <code>CatalogEntryType</code>s
	 */
	public UsrvHandler(
			ZipOutputStream pOutputStream, 
			CatalogEntryType pService, 
			CatalogEntryType[] pItems)
	{
		if(null == pItems || null == pService || pItems.length == 0 || null == pOutputStream)
		{
			throw new IllegalArgumentException("ERROR: Invalid input parameters");
		}

		mOutputStream = pOutputStream;
		mService = pService;
		
		List appItems = new ArrayList();
		List hardwareItems = new ArrayList();
		List humanResourceItems = new ArrayList();
		
		for(int i=0; i<pItems.length; i++)
		{
			Map parameters = pItems[i].getUserData().getUserDataField();
			String itemType = (String)parameters.get("itemtype");
			if(null != itemType && itemType.equals("AAL_APPLICATION"))
			{
				appItems.add(pItems[i]);
			}
			else if(null != itemType && itemType.equals("HARDWARE"))
			{
				hardwareItems.add(pItems[i]);
			}
			else if(null != itemType && itemType.equals("HUMAN_RESOURCE"))
			{
				humanResourceItems.add(pItems[i]);
			}
		}
		
		if(appItems.size() + hardwareItems.size() + humanResourceItems.size() < pItems.length)
		{
			throw new IllegalArgumentException("ERROR: Unsupported input items");
		}
		
		mAppItems = new CatalogEntryType[appItems.size()];
		appItems.toArray(mAppItems);

		mHardwareItems = new CatalogEntryType[hardwareItems.size()];
		hardwareItems.toArray(mHardwareItems);

		mHumanResourceItems = new CatalogEntryType[humanResourceItems.size()];
		humanResourceItems.toArray(mHumanResourceItems);
	}
	
	/**
	 * Builds a usrv file from the service and all its composed items. 
	 *  
	 * @throw uAALException if an error occurred during the building process
	 */
	public void build()
		throws uAALException
	{
		try
		{
			String serviceName = 
				mService.getCatalogEntryIdentifier().getExternalIdentifier().getPartNumber();
	
			initUsrvTemplateConfigFile();
			
			// populate the template configuration file with content
			populateConfigFile();
			
			// go through the included applications
			for(int i=0; i<mAppItems.length; i++)
			{
				AppHandler handler = new AppHandler(mService, mAppItems[i]);
				
				String appName = mAppItems[i].getCatalogEntryIdentifier().getExternalIdentifier().getPartNumber();
	
				// write the application file
				mOutputStream.putNextEntry(new ZipEntry("bin/" + appName + "." + handler.getFileType()));
				writeInputStream(handler.getAppFileInputStream(), mOutputStream);
	
				// write the application component
				populateApplicationComponentInfo(handler);
				
				try
				{
					// initialize the settings of a uapp file 
					handler.init();
	
					// write the application docs
					Map docs = handler.getUappDocs();
					Iterator docsIter = docs.keySet().iterator();
					while(docsIter.hasNext())
					{
						String doc = (String)docsIter.next();
						
						mOutputStream.putNextEntry(new ZipEntry(doc));
						writeInputStream(new ByteArrayInputStream((byte[])docs.get(doc)), mOutputStream);
					}
				}
				catch(Exception ex) {} // not a uapp file - ignore
	
				// write the application licenses
				populateServiceLicensesInfo(handler);
	
				// write the application capabilities
				populateServiceCapabilitiesInfo(handler);
	
				// write the application requirements
				populateServiceRequirementsInfo(handler);
	
				// write the application part requirements
				populateServicePartRequirementsInfo(handler);
			}
			
			// go through the hardware items
			for(int i=0; i<mHardwareItems.length; i++)
			{
				// write the hardware component
				populateHardwareComponentInfo(mHardwareItems[i]);
			}
	
			// go through the human resources
			for(int i=0; i<mHumanResourceItems.length; i++)
			{
				// write the human resource component
				populateHumanResourceComponentInfo(mHumanResourceItems[i]);
			}
			
			// save the configuration file in the output stream
			mOutputStream.putNextEntry(new ZipEntry("config/" + serviceName + ".xml")); 
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			DOMSource source = new DOMSource(mUsrvConfigFile);
			StreamResult result =  new StreamResult(mOutputStream);
			transformer.transform(source, result);
			mOutputStream.closeEntry();
			
			mOutputStream.putNextEntry(new ZipEntry("data/")); 
			mOutputStream.closeEntry();
		}
		catch(uAALException ex)
		{
			throw ex;
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to build the usrv file (" + ex.getMessage() + ")");
		}
	}
	
	/**
	 * Initialize the usrv template configuration file that is located in the 
	 * local file system as a DOM XML.
	 * 
	 * @throw uAALException if an error occurred during the process
	 */
	private void initUsrvTemplateConfigFile()
		throws uAALException
	{
		InputStream input = null;
		try
		{
			input = getClass().getResourceAsStream(USRV_TEMPLATE_CONFIG_FILE_NAME);
			if(null == input)
			{
				throw new uAALException("ERROR: Failed to load the usrv template configuration file");
			}
			byte[] buffer = getInputStreamAsByteArray(input);
	        DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	        Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(buffer)));
	        doc.getDocumentElement().normalize();

	        mUsrvConfigFile = doc;
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to initialize the usrv template configuration file (" + 
					ex.getMessage() + ")");	
		}
		finally
		{
			if(null != input)
			{
				try
				{
					input.close();
				}
				catch(IOException ex) {} // ignore
			}
		}
	}
	
	/**
	 * Populate the config file with input from the service and its composed 
	 * items.
	 * 
	 * @throws uAALException fn an error occurred during the process
	 */
	private void populateConfigFile()
		throws uAALException
	{
		if(null == mUsrvConfigFile)
		{
			throw new uAALException("ERROR: Service configuration file was not initialized");
		}

		Map serviceParams = mService.getUserData().getUserDataField();

		String serviceName = mService.getCatalogEntryIdentifier().getExternalIdentifier().getPartNumber();
		String description = "";
		String keywords = "";
		if(!mService.getDescription().isEmpty())
		{
			serviceName = ((CatalogDescriptionType)mService.getDescription().get(0)).getName();
			description = ((CatalogDescriptionType)mService.getDescription().get(0)).getShortDescription();
			keywords = ((CatalogDescriptionType)mService.getDescription().get(0)).getKeyword();
		}

		NodeList nodes = mUsrvConfigFile.getDocumentElement().getChildNodes();
		for(int i=0; i<nodes.getLength(); i++)
		{
			Node node = nodes.item(i);
			if(node.getNodeName().equals(USRV_SRV_ELEMENT))
			{
				NodeList subnodes = node.getChildNodes();
				for(int j=0; j<subnodes.getLength(); j++)
				{
					Node subnode = subnodes.item(j);
					if(subnode.getNodeName().equals(USRV_SRV_NAME_ELEMENT))
					{
						subnode.setTextContent(serviceName);
					}
					else if(subnode.getNodeName().equals(USRV_SRV_ID_ELEMENT))
					{
						subnode.setTextContent(mService.getCatalogEntryIdentifier().getUniqueID());
					}
					else if(subnode.getNodeName().equals(USRV_SRV_DESCRIPTION_ELEMENT))
					{
						subnode.setTextContent(description);
					}
					else if(subnode.getNodeName().equals(USRV_SRV_TAGS_ELEMENT))
					{
						subnode.setTextContent(keywords);
					}
					else if(subnode.getNodeName().equals(USRV_SRV_VERSION_ELEMENT))
					{
						populateServiceVersionInfo(subnode);
					}
					else if(subnode.getNodeName().equals(USRV_SRV_SERVICE_PROVIDER_ELEMENT))
					{
						populateServiceProviderInfo(subnode);
					}
					else if(subnode.getNodeName().equals(USRV_SRV_LICENSES_ELEMENT))
					{
						populateServiceLicensesInfo(subnode);
					}
				}
			}
			else if(node.getNodeName().equals(USRV_SRV_CAPABILITIES_ELEMENT))
			{
				mCapabilitiesNode = node;
			}
			else if(node.getNodeName().equals(USRV_SRV_REQUIREMENTS_ELEMENT))
			{
				populateServiceRequirementsInfo(node);
			}
			else if(node.getNodeName().equals(USRV_SRV_COMPONENTS_ELEMENT))
			{
				mComponentsNode = node;
			}
		}
	}
	
	/**
	 * Populate the sub XML tree under the given node with the service version
	 * information. 
	 * 
	 * @param pVersionNode 
	 * 		The root <code>org.w3c.dom.Node</code> of the service version in the 
	 * 		service configuration file 
	 */
	private void populateServiceVersionInfo(Node pVersionNode)
	{
		Map parameters = mService.getUserData().getUserDataField();
		String version = (String)parameters.get("serviceversion");
		int index = version.indexOf(".");

		NodeList versionnodes = pVersionNode.getChildNodes();
		for(int k=0; k<versionnodes.getLength(); k++)
		{
			Node versionNode = versionnodes.item(k);
			if(versionNode.getNodeName().equals(USRV_SRV_VERSION_MAJOR_ELEMENT))
			{
				versionNode.setTextContent(index != -1 ? version.substring(0, index) : version);
			}
			else if(versionNode.getNodeName().equals(USRV_SRV_VERSION_MINOR_ELEMENT))
			{
				versionNode.setTextContent(index != -1 ? version.substring(index+1) : "0");
			}
			else if(versionNode.getNodeName().equals(USRV_SRV_VERSION_MICRO_ELEMENT))
			{
				versionNode.setTextContent("0"); 
			}
		}
	}
	
	/**
	 * Populate the sub XML tree under the given node with the service provider
	 * information. 
	 * 
	 * @param pProviderNode 
	 * 		The root <code>org.w3c.dom.Node</code> of the service provider in 
	 * 		the	service configuration file 
	 */
	private void populateServiceProviderInfo(Node pProviderNode)
	{
		Map parameters = mService.getUserData().getUserDataField();

		NodeList infonodes = pProviderNode.getChildNodes();
		for(int k=0; k<infonodes.getLength(); k++)
		{
			Node info = infonodes.item(k);
			if(info.getNodeName().indexOf(AppHandler.UAPP_ORGANIZATION_NAME) != -1)
			{
				info.setTextContent((String)parameters.get("serviceorganizationname"));
			}
			else if(info.getNodeName().indexOf(AppHandler.UAPP_CERTIFICATE) != -1)
			{
				info.setTextContent((String)parameters.get("serviceorganizationcertificate"));
			}
			else if(info.getNodeName().indexOf(AppHandler.UAPP_CONTACT_PERSON) != -1)
			{
				info.setTextContent((String)parameters.get("serviceprovidername"));
			}
			else if(info.getNodeName().indexOf(AppHandler.UAPP_EMAIL) != -1)
			{
				info.setTextContent((String)parameters.get("serviceprovideremail"));
			}
			else if(info.getNodeName().indexOf(AppHandler.UAPP_PHONE) != -1)
			{
				info.setTextContent((String)parameters.get("serviceproviderphone"));
			}
			else if(info.getNodeName().indexOf(AppHandler.UAPP_WEB_ADDRESS) != -1)
			{
				info.setTextContent((String)parameters.get("serviceorganizationurl"));
			}
		}
	}

	/**
	 * Populate the sub XML tree under the given node with the service licenses
	 * information and write the license files as new entries to the usrv file. 
	 * 
	 * @param pLicensesNode 
	 * 		The root <code>org.w3c.dom.Node</code> of the service licenses in 
	 * 		the	service configuration file 
	 * 
	 * @throws uAALException if an error occurred during the writing process
	 */
	private void populateServiceLicensesInfo(Node pLicensesNode)
		throws uAALException
	{
		mLicensesNode = pLicensesNode;
		
		Map serviceParams = mService.getUserData().getUserDataField();

		Element slaElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_LICENSE_SLA_ELEMENT); 
		mLicensesNode.appendChild(slaElement);
		Element nameElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_NAME);
		String serviceName = mService.getCatalogEntryIdentifier().getExternalIdentifier().getPartNumber();
		nameElement.setTextContent("SLA for " + serviceName);
		slaElement.appendChild(nameElement);
		Element linkElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_LINK);
		linkElement.setTextContent("file:../licenses/sla.txt");
		slaElement.appendChild(linkElement);

		String sla = (String)serviceParams.get("servicelevelagreement");
		sla = (null == sla) ? "" : sla;
		
		try
		{
			mOutputStream.putNextEntry(new ZipEntry("licenses/sla.txt")); 
			mOutputStream.write(sla.getBytes());
			mOutputStream.closeEntry();
		}
		catch(IOException ex)
		{
			throw new uAALException("ERROR: Failed to write the license file to the usrv file (" +
						ex.getMessage() + ")");
		}
	}

	/**
	 * Populate the service XML tree with the service licenses information hold 
	 * by the given application handler and write the license files as new 
	 * entries to the usrv file. 
	 * 
	 * @param pHandler
	 * 		The application handler that holds all the information on the 
	 * 		application 
	 * 
	 * @throws uAALException if an error occurred during the writing process
	 */
	private void populateServiceLicensesInfo(AppHandler pHandler)
		throws uAALException
	{
		Map params = pHandler.getUappConfigParameters();
		
		// populate the license parameters
		if(null != mLicensesNode)
		{
			boolean found = false;
			int counter = 1;
			String licenseName = (String)params.get(AppHandler.APP_LICENSES + "-name-" + counter);
			String licenseLink = (String)params.get(AppHandler.APP_LICENSES + "-link-" + counter);
			while(null != licenseName && null != licenseLink)
			{
				found = true;
				if(!mSupportedLicenses.contains(licenseName))
				{
					String updatedLicenseLink = licenseLink;
					boolean success = true;
					if(null != licenseLink)
					{
						if(licenseLink.startsWith("file:"))
						{
							licenseLink = licenseLink.substring(5);
	
							String licenseFileName = licenseLink;
							int index = licenseLink.lastIndexOf("/");
							if(index == -1)
							{
								index = licenseLink.lastIndexOf("\\");
							}
							if(index != -1)
							{
								licenseFileName = licenseLink.substring(index+1, licenseLink.length());
							}
							updatedLicenseLink = "file:../licenses/" + licenseFileName;
							
							InputStream input = pHandler.getUappLicenseInputStream(licenseName);
							if(null == input)
							{
								System.out.println("WARNING: Failed to find the uapp license file for " + licenseFileName);
								success = false;
							}
							try
							{
								mOutputStream.putNextEntry(new ZipEntry("licenses/" + licenseFileName));
							}
							catch(IOException ex)
							{
								throw new uAALException("ERROR: Failed to write the application license " +
										licenseFileName + " (" + ex.getMessage() + ")");
							}
							writeInputStream(input, mOutputStream);
						}
					}
				
					if(success)
					{
						Element licenseElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_LICENSE_ELEMENT);
						mLicensesNode.appendChild(licenseElement);
	
						Element nameElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_NAME);
						nameElement.setTextContent(licenseName);
						licenseElement.appendChild(nameElement);
						
						mSupportedLicenses.add(licenseName);
						
						String licenseCategory = (String)params.get(AppHandler.APP_LICENSES + "-category-" + counter);
						if(null != licenseCategory)
						{
							Element categoryElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_CATEGORY);
							categoryElement.setTextContent(licenseCategory);
							licenseElement.appendChild(categoryElement);
						}
						
						if(null != updatedLicenseLink)
						{
							Element linkElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_LINK);
							licenseElement.appendChild(linkElement);
							linkElement.setTextContent(updatedLicenseLink);
						}
					}
				}
				
				counter ++;
				licenseName = (String)params.get(AppHandler.APP_LICENSES + "-name-" + counter);
				licenseLink = (String)params.get(AppHandler.APP_LICENSES + "-link-" + counter);
			}
			
			if(!found)
			{
				String license = (String)params.get(AppHandler.APP_LICENSES);
				if(null != license && license.length() > 0)
				{
					Element licenseElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_LICENSE_ELEMENT);
					mLicensesNode.appendChild(licenseElement);
	
					Element nameElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_NAME);
					nameElement.setTextContent(pHandler.getAppName());
					licenseElement.appendChild(nameElement);
					
					Element categoryElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_CATEGORY);
					categoryElement.setTextContent("N/A");
					licenseElement.appendChild(categoryElement);
					
					Element linkElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_LINK);
					licenseElement.appendChild(linkElement);
					linkElement.setTextContent("file:../licenses/" + pHandler.getAppName() + ".txt");
	
					try
					{
						mOutputStream.putNextEntry(new ZipEntry("licenses/" + pHandler.getAppName() + ".txt"));
					}
					catch(IOException ex)
					{
						throw new uAALException("ERROR: Failed to write the service license (" + 
								ex.getMessage() + ")");
					}
					writeInputStream(new ByteArrayInputStream(license.getBytes()), mOutputStream);
				}
			}
		}
	}

	/**
	 * Populate the service XML tree with the service capabilities information 
	 * hold by the given application handler. 
	 * 
	 * @param pHandler
	 * 		The application handler that holds all the information on the 
	 * 		application capabilities 
	 */
	private void populateServiceCapabilitiesInfo(AppHandler pHandler)
	{
		Map params = pHandler.getUappConfigParameters();
	
		// populate the capabilities parameters
		if(null != mCapabilitiesNode)
		{
			int counter = 1;
			String name = (String)params.get(AppHandler.APP_CAPABILITIES + "-name-" + counter);
			String value = (String)params.get(AppHandler.APP_CAPABILITIES + "-value-" + counter);
			while(null != name && null != value)
			{
				if(!mSupportedCapabilities.contains(name + "=" + value))
				{
					Element capabilityElement = (Element)mUsrvConfigFile.createElement(AppHandler.UAAP_APP_CAPABILITY_ELEMENT);
					mCapabilitiesNode.appendChild(capabilityElement);
	
					Element nameElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_CAPABILITY_NAME_ELEMENT);
					nameElement.setTextContent(name);
					capabilityElement.appendChild(nameElement);
					
					Element valueElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_CAPABILITY_VALUE_ELEMENT);
					valueElement.setTextContent(value);
					capabilityElement.appendChild(valueElement);

					mSupportedCapabilities.add(name + "=" + value);

					counter ++;
					name = (String)params.get(AppHandler.APP_CAPABILITIES + "-name-" + counter);
					value= (String)params.get(AppHandler.APP_CAPABILITIES + "-value-" + counter);
				}
			}
		}
	}

	/**
	 * Populate the service XML tree with the service requirements information 
	 * hold by the given application handler. 
	 * 
	 * @param pHandler
	 * 		The application handler that holds all the information on the 
	 * 		application requirements 
	 */
	private void populateServiceRequirementsInfo(Node pRequirementsNode)
	{
		mRequirementsNode = pRequirementsNode;
		mPartRequirementsNode = pRequirementsNode;
		
		if(mAppItems.length > 1)
		{
			mPartRequirementsNode = buildRequirementsGroup(pRequirementsNode, "AND");
		}
	}

	/**
	 * Builds the requirements group element under the given node in the service
	 * configuration file.
	 * 
	 * @param pNode 
	 * 		The root <code>org.w3c.dom.Node</code> of the service requirements 
	 * 		in the service configuration file 
	 * @param pCondition 
	 * 		The condition between the requirements 
	 */
	private Node buildRequirementsGroup(Node pNode, String pCondition)
	{
		Element requirementElement = (Element)mUsrvConfigFile.createElement(
				UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_ELEMENT);
		pNode.appendChild(requirementElement);
		
		Element groupElement = (Element)mUsrvConfigFile.createElement(
				UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_GROUP_ELEMENT);
		requirementElement.appendChild(groupElement);
		
		Element logicalElement = (Element)mUsrvConfigFile.createElement(
				UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_LOGICAL_ELEMENT);
		logicalElement.setTextContent(pCondition);
		groupElement.appendChild(logicalElement);
		
		return groupElement;
	}
	
	/**
	 * Populate the service XML tree with the service requirements information 
	 * hold by the given application handler. 
	 * 
	 * @param pHandler
	 * 		The application handler that holds all the information on the 
	 * 		application requirements 
	 */
	private void populateServiceRequirementsInfo(AppHandler pHandler)
	{
		Map params = pHandler.getUappConfigParameters();
	
		// populate the requirements parameters
		if(null != mRequirementsNode)
		{
			boolean found = false;
			int counter = 1;
			String name = (String)params.get(AppHandler.APP_REQUIREMENTS + "-name-" + counter);
			String value = (String)params.get(AppHandler.APP_REQUIREMENTS + "-value-" + counter);
			while(null != name && null != value)
			{
				found = true;
				if(!mSupportedRequirements.contains(name + "=" + value))
				{
					Element requirementElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_ELEMENT);
					mRequirementsNode.appendChild(requirementElement);
	
					Element reqAtomElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_ATOM_ELEMENT);
					requirementElement.appendChild(reqAtomElement);
					
					Element nameElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_ATOM_NAME_ELEMENT);
					nameElement.setTextContent(name);
					reqAtomElement.appendChild(nameElement);
					
					Element valueElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_ATOM_VALUE_ELEMENT);
					valueElement.setTextContent(value);
					reqAtomElement.appendChild(valueElement);

					mSupportedRequirements.add(name + "=" + value);
					
					counter ++;
					name = (String)params.get(AppHandler.APP_REQUIREMENTS + "-name-" + counter);
					value= (String)params.get(AppHandler.APP_REQUIREMENTS + "-value-" + counter);
				}
			}
			
			if(!found)
			{
				String requirements = (String)params.get(AppHandler.APP_REQUIREMENTS);

				if(null != requirements && requirements.length() > 0)
				{
					Element requirementElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_ELEMENT);
					mRequirementsNode.appendChild(requirementElement);
	
					Element reqAtomElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_ATOM_ELEMENT);
					requirementElement.appendChild(reqAtomElement);
					
					Element nameElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_ATOM_NAME_ELEMENT);
					nameElement.setTextContent(pHandler.getAppName());
					reqAtomElement.appendChild(nameElement);
					
					Element valueElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_ATOM_VALUE_ELEMENT);
					valueElement.setTextContent(requirements);
					reqAtomElement.appendChild(valueElement);
				}
			}
		}
	}

	/**
	 * Populate the service XML tree with the service part requirements 
	 * information hold by the given application handler. 
	 * 
	 * @param pHandler
	 * 		The application handler that holds all the information on the 
	 * 		application part requirements 
	 */
	private void populateServicePartRequirementsInfo(AppHandler pHandler)
	{
		Map params = pHandler.getUappConfigParameters();
	
		// populate the part requirements parameters
		if(null != mPartRequirementsNode)
		{
			Node partRequirementsNode = mPartRequirementsNode;
			int numParts = pHandler.getNumAppParts();
			if(numParts > 1)
			{
				partRequirementsNode = buildRequirementsGroup(mPartRequirementsNode, "OR");
			}
			
			for(int i=1; i<=numParts; i++)
			{
				Node node = buildRequirementsGroup(partRequirementsNode, "AND");

				int counter = 1;
				String name = (String)params.get(AppHandler.APP_PART_REQUIREMENTS + "-" + i + "-name-" + counter);
				String value = (String)params.get(AppHandler.APP_PART_REQUIREMENTS + "-" + i + "-value-" + counter);
				while(null != name && null != value)
				{
					Element requirementElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_ELEMENT);
					node.appendChild(requirementElement);
	
					Element reqAtomElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_ATOM_ELEMENT);
					requirementElement.appendChild(reqAtomElement);
					
					Element nameElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_ATOM_NAME_ELEMENT);
					nameElement.setTextContent(name);
					reqAtomElement.appendChild(nameElement);
					
					Element valueElement = (Element)mUsrvConfigFile.createElement(
							UAPP_NAMESPACE + ":" + AppHandler.UAAP_APP_REQUIREMENT_ATOM_VALUE_ELEMENT);
					valueElement.setTextContent(value);
					reqAtomElement.appendChild(valueElement);
	
					counter ++;
					name = (String)params.get(AppHandler.APP_PART_REQUIREMENTS + "-" + i + "-name-" + counter);
					value = (String)params.get(AppHandler.APP_PART_REQUIREMENTS + "-" + i + "-value-" + counter);
				}
			}
		}
	}
	
	/**
	 * Populate the service XML tree with the application general information 
	 * hold by the given application handler. 
	 * 
	 * @param pHandler
	 * 		The application handler that holds all the general information on 
	 * 		the application 
	 */
	private void populateApplicationComponentInfo(AppHandler pHandler)
	{
		Element appElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_APPLICATION_ELEMENT);
		mComponentsNode.appendChild(appElement);

		Element nameElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_NAME);
		nameElement.setTextContent(pHandler.getAppName());
		appElement.appendChild(nameElement);

		int index = pHandler.getVersion().indexOf(".");
		Element versionElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_VERSION_ELEMENT);
		appElement.appendChild(versionElement);

		Element versionMajorElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_VERSION_MAJOR_ELEMENT);
		versionMajorElement.setTextContent(index != -1 ? pHandler.getVersion().substring(0, index) : pHandler.getVersion());
		versionElement.appendChild(versionMajorElement);

		Element versionMinorElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_VERSION_MINOR_ELEMENT);
		versionMinorElement.setTextContent(index != -1 ? pHandler.getVersion().substring(index+1) : "0");
		versionElement.appendChild(versionMinorElement);

		Element versionMicroElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_VERSION_MICRO_ELEMENT);
		versionMicroElement.setTextContent("0");
		versionElement.appendChild(versionMicroElement);

		Element artifactIdElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_ARTIFACT_ID_ELEMENT);
		artifactIdElement.setTextContent(pHandler.getAppId());
		appElement.appendChild(artifactIdElement);

		Element locationElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_LOCATION_ELEMENT);
		locationElement.setTextContent("file:../bin/" + pHandler.getAppName() + "." + pHandler.getFileType());
		appElement.appendChild(locationElement);
	}

	/**
	 * Populate the service XML tree with the hardware item general information 
	 * hold by the given catalog entry object. 
	 * 
	 * @param pItem 
	 * 		The hardware item as a <code>CatalogEntryType</code> that holds all 
	 * 		the general information on the hardware item 
	 */
	private void populateHardwareComponentInfo(CatalogEntryType pItem)
	{
		Map parameters = pItem.getUserData().getUserDataField();
		String category = (String)parameters.get("hardwarecategory");

		Element hardwareElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_HARDWARE_ELEMENT);
		mComponentsNode.appendChild(hardwareElement);
	
		Element nameElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_NAME);
		nameElement.setTextContent(pItem.getCatalogEntryIdentifier().getExternalIdentifier().getPartNumber());
		hardwareElement.appendChild(nameElement);

		Element artifactIdElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_ARTIFACT_ID_ELEMENT);
		artifactIdElement.setTextContent(pItem.getCatalogEntryIdentifier().getUniqueID());
		hardwareElement.appendChild(artifactIdElement);

		Element categoryElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_HARDWARE_CATEGORY_ELEMENT);
		categoryElement.setTextContent(category);
		hardwareElement.appendChild(categoryElement);
	}

	/**
	 * Populate the service XML tree with the human resource item general 
	 * information hold by the given catalog entry object. 
	 * 
	 * @param pItem 
	 * 		The human resource item as a <code>CatalogEntryType</code> that 
	 * 		holds all the general information on the human resource 
	 */
	private void populateHumanResourceComponentInfo(CatalogEntryType pItem)
	{
		Map parameters = pItem.getUserData().getUserDataField();
		String type = (String)parameters.get("humanresourcetype");
		String phone = (String)parameters.get("humanresourcephone");
		String company = (String)parameters.get("humanresourcecompany");
		String frequency = (String)parameters.get("frequency");
		String qualification = (String)parameters.get("qualification");
		String experience = (String)parameters.get("experience");

		Element humanResourceElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_HUMAN_RESOURCE_ELEMENT);
		mComponentsNode.appendChild(humanResourceElement);
	
		Element roleElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_HUMAN_RESOURCE_ROLE_ELEMENT);
		roleElement.setTextContent(type);
		humanResourceElement.appendChild(roleElement);

		Element companyElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_HUMAN_RESOURCE_COMPANY_ELEMENT);
		companyElement.setTextContent(company);
		humanResourceElement.appendChild(companyElement);

		Element phoneElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_HUMAN_RESOURCE_PHONE_ELEMENT);
		phoneElement.setTextContent(phone);
		humanResourceElement.appendChild(phoneElement);

		Element refElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_HUMAN_RESOURCE_REF_ID_ELEMENT);
		refElement.setTextContent(pItem.getCatalogEntryIdentifier().getUniqueID());
		humanResourceElement.appendChild(refElement);

		Element frequencyElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_HUMAN_RESOURCE_FREQUENCY_ELEMENT);
		frequencyElement.setTextContent(frequency);
		humanResourceElement.appendChild(frequencyElement);

		Element qualificationElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_HUMAN_RESOURCE_QUALIFICATION_ELEMENT);
		qualificationElement.setTextContent(qualification);
		humanResourceElement.appendChild(qualificationElement);

		Element experienceElement = (Element)mUsrvConfigFile.createElement(USRV_SRV_HUMAN_RESOURCE_EXPERIENCE_ELEMENT);
		experienceElement.setTextContent(experience);
		humanResourceElement.appendChild(experienceElement);
	}
	
	/**
	 * Return the input file as a byte array.
	 * 
	 * @param pInputStream 
	 * 		The input stream of the file
	 * 
	 * @return A byte array of the input file
	 * 
	 * @throws uAALException 
	 * 		If an error has occurred while reading the file
	 */
	private byte[] getInputStreamAsByteArray(InputStream pInput)
		throws uAALException
	{
		try
		{
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			byte[] buffer = new byte[2048];
			int len = 0;
	        while((len = pInput.read(buffer)) > 0)
	        {
	            output.write(buffer, 0, len);
	        }
	        return output.toByteArray();
		}
		catch(IOException ex)
		{
			throw new uAALException("ERROR: Failed to read the content of the input stream (" + 
					ex.getMessage() + ")");
		}
	}
	
	/**
	 * Write the content of the input stream into the output stream.
	 *  
	 * @param pInput
	 * 		The input stream that holds the content to be written
	 * @param pOutput
	 * 		The output stream to write into
	 * 
	 * @throws uAALException if an error occurred during the reading or the 
	 * 		writing processes 
	 */
	private void writeInputStream(InputStream pInput, ZipOutputStream pOutput)
		throws uAALException
	{
		if(null != pInput)
		{
			try
			{
				byte[] buff = new byte[2048];
				int bytesRead = -1;
				long total = 0;
					 
				// simple read/write loop
				while(-1 != (bytesRead = pInput.read(buff, 0, buff.length)))
				{
					pOutput.write(buff, 0, bytesRead);
					total += bytesRead;
				}
				System.out.println("-->> UsrvHandler.writeInputStream(): output size in bytes = " + total);
				pOutput.closeEntry();
			}
			catch(IOException ex)
			{
				throw new uAALException("ERROR: Failed to write the content of the input stream " +
						" into the output stream (" + ex.getMessage() + ")");
			}
			finally
			{
				try
				{
					pInput.close();
				}
				catch(IOException ex) {} // ignore
			}
		}
	}	
}
