/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.service.action;

import java.util.Map;

import javax.security.auth.callback.CallbackHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.space.SpaceHandler;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.user.UserSessionInfo;
import org.universaal.ucc.deploymanagerservice.DeployManagerServicePortType;

import com.ibm.commerce.foundation.client.facade.bod.servlet.struts.BusinessObjectDocumentAction;
import com.ibm.commerce.foundation.common.datatypes.BusinessContextType;

/**
 * This class is being called when there is a request for the installation, 
 * update or an un-installation of a service. The uCC running in the AAL space 
 * is being triggered for the actual operation. 
 * 
 * @author roni 
 */
public class InstallServiceFileAction extends BusinessObjectDocumentAction  
{
	/**
	 * The operations that are gandled by this class
	 */
    private final static String INSTALL_OPERATION = "install";
    private final static String UPDATE_OPERATION = "update";
    private final static String UNINSTALL_OPERATION = "uninstall";
    
    /**
	 * This method executes the action and and forward to the next page that 
	 * shows the error/success message. 
	 * 
	 * @param pActionMapping 
	 * 		The configuration of the struts action
	 * @param pActionForm 
	 * 		The action form of the struts action
	 * @param pRequest 
	 * 		The HTTP request object that contains the information pertaining to 
	 * 		the request
	 * @param pResponse 
	 * 		The HTTP response object that will contain the information 
	 * 		pertaining to the response
	 * 
	 * @return The ActionForward instance describing where and how control 
	 * 		should be forwarded
	 * 
	 * @throws <code>java.lang.Exception</code> in case a problem occurred while 
	 * 		parsing the HTTP request or executing the business object document. 
	 * 		These exceptions can range from server side validation which are 
	 * 		recoverable to server errors where are not recoverable
	 */
	 public ActionForward execute(
			 ActionMapping pActionMapping, 
			 ActionForm pActionForm, 
			 HttpServletRequest pRequest, 
			 HttpServletResponse pResponse) 
	 	throws Exception
	 {
		 ActionForward forward = super.execute(pActionMapping, pActionForm, pRequest, pResponse);
		 if(null != pRequest.getAttribute("errorMessage") || 
				 null != pRequest.getAttribute("successMessage"))
		 {
			return pActionMapping.findForward("RetrieveServiceStatusView");
		 } 
		 return forward;
	 }

	/**
	 * This method converts the HTTP request into the appropriate request 
	 * business object document and invoke the uCC with the requested operation.
	 * 
	 * @param pActionMapping 
	 * 		The configuration of the struts action
	 * @param pActionForm 
	 * 		The action form of the struts action
	 * @param pRequest 
	 * 		The HTTP request object that contains the information pertaining to 
	 * 		the request
	 * @param pResponse 
	 * 		The HTTP response object that will contain the information 
	 * 		pertaining to the response
	 * @param pBusinessContext 
	 * 		The business context of the current request 
	 * @param pCallbackHandler 
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return A <code>java.util.Map</code> that is used in the result of the 
	 * 		current request
	 * 
	 * @throws <code>java.lang.Exception</code> in case a problem occurred while 
	 * 		parsing the HTTP request or executing the business object document. 
	 * 		These exceptions can range from server side validation which are 
	 * 		recoverable to server errors where are not recoverable
     */
	 protected Map executeBusinessObjectDocument(
			 ActionMapping pActionMapping, 
			 ActionForm pActionForm, 
			 HttpServletRequest pRequest, 
			 HttpServletResponse pResponse, 
			 BusinessContextType pBusinessContext, 
			 CallbackHandler pCallbackHandler)
	 throws Exception
     {
		 String serviceId = pRequest.getParameter("serviceId");
		 String serviceURL = pRequest.getParameter("serviceURL");
		 String operationType = pRequest.getParameter("operationType");
		 String sessionId = pRequest.getSession().getId();
		 
		 System.out.println("-->>> InstallServiceFileAction: serviceId = " + 
				 serviceId + " serviceURL = " + serviceURL + " sessionId = " + 
				 sessionId + " operationType = " + operationType); 
		 
		 // no service or composed items
		 if(null == serviceId || null == serviceURL || null == sessionId ||
				 null == operationType)
		 {
			 pRequest.setAttribute("errorMessage", "ERROR: Invalid parameters");
			 return null;
		 }
		 
		 if(!operationType.equals(INSTALL_OPERATION) && !operationType.equals(UPDATE_OPERATION) &&
				 !operationType.equals(UNINSTALL_OPERATION))
		 {
			 pRequest.setAttribute("errorMessage", "ERROR: Invalid operation");
			 return null;
		 }
		 
		 UserSessionInfo info = UserSessionHandler.getUserSessionInfoBySessionKey(sessionId);
		 if(null == info) // user session info was not found 
		 {
			 pRequest.setAttribute("errorMessage", "ERROR: Session key is invalid");
			 return null;
		 }
		 
		 try
		 {
			 DeployManagerServicePortType managerPort = 
				 SpaceHandler.getInstance().getDeployManagerServicePortType(info.getUserId());
			
			 System.out.println("-->> going to invoke getSessionKey()");
			 String sessionKey = managerPort.getSessionKey(info.getUserName(), info.getPassword());
			 System.out.println("-->> session key = " + sessionKey);
			 if(null == sessionKey)
			 {
				 pRequest.setAttribute("errorMessage", 
						 "ERROR: Failed to retrieve the user session key");
			 }
			 
			 if(operationType.equals(INSTALL_OPERATION))
			 {
				 System.out.println("-->> going to invoke install()");
				 managerPort.install(sessionKey, serviceId, serviceURL);
				 System.out.println("-->> DONE!");
			 }
			 else if(operationType.equals(UPDATE_OPERATION))
			 {
				 System.out.println("-->> going to invoke update()");
				 managerPort.update(sessionKey, serviceId, serviceURL);
				 System.out.println("-->> DONE!");
			 }
			 else if(operationType.equals(UNINSTALL_OPERATION))
			 {
				 System.out.println("-->> going to invoke uninstall()");
				 managerPort.uninstall(sessionKey, serviceId);
				 System.out.println("-->> DONE!");
			 }
		 }
		 catch(uAALException ex)
		 {
			 ex.printStackTrace();
			 pRequest.setAttribute("errorMessage", ex.getMessage());
			 return null;
		 }
		 
		 pRequest.setAttribute("successMessage", "SUCCESS");
		 return null;
    }
}
