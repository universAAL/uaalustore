/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.service.action;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.zip.ZipOutputStream;

import javax.security.auth.callback.CallbackHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.application.AppHandler;
import org.universaal.commerce.ustore.service.UsrvHandler;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.user.UserSessionInfo;
import org.universaal.commerce.ustore.utilities.CatalogUtilities;

import com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType;
import com.ibm.commerce.catalog.objects.CatalogEntryAccessBean;
import com.ibm.commerce.foundation.client.facade.bod.servlet.struts.BusinessObjectDocumentAction;
import com.ibm.commerce.foundation.common.datatypes.BusinessContextType;

/**
 * This class is being called when there is a request for downloading/installing 
 * a complete AAL service which its payment was approved. A .usrv file (zip) is 
 * being created which includes all the artifacts of the service. 
 *  
 * @author roni 
 */
public class RetrieveServiceFileAction extends BusinessObjectDocumentAction  
{
	/**
	 * This method executes the action and if an error occurred forward to the
	 * error page. 
	 * 
	 * @param pActionMapping 
	 * 		The configuration of the struts action
	 * @param pActionForm 
	 * 		The action form of the struts action
	 * @param pRequest 
	 * 		The HTTP request object that contains the information pertaining to 
	 * 		the request
	 * @param pResponse 
	 * 		The HTTP response object that will contain the information 
	 * 		pertaining to the response
	 * 
	 * @return The ActionForward instance describing where and how control 
	 * 		should be forwarded
	 * 
	 * @throws <code>java.lang.Exception</code> in case a problem occurred while 
	 * 		parsing the HTTP request or executing the business object document. 
	 * 		These exceptions can range from server side validation which are 
	 * 		recoverable to server errors where are not recoverable
	 */
	 public ActionForward execute(
			 ActionMapping pActionMapping, 
			 ActionForm pActionForm, 
			 HttpServletRequest pRequest, 
			 HttpServletResponse pResponse) 
	 	throws Exception
	 {
		 ActionForward forward = 
			 super.execute(pActionMapping, pActionForm, pRequest, pResponse);
		 if(null != pRequest.getAttribute("errorMessage"))
		 {
			return pActionMapping.findForward("RetrieveServiceStatusView");
		 } 
		 return forward;
	 }

	/**
	 * This method converts the HTTP request into the appropriate request 
	 * business object document, extract the information on the service file
	 * that should be returned, build the file and write it to the response 
	 * output stream. 
	 * 
	 * @param pActionMapping 
	 * 		The configuration of the struts action
	 * @param pActionForm 
	 * 		The action form of the struts action
	 * @param pRequest 
	 * 		The HTTP request object that contains the information pertaining to 
	 * 		the request
	 * @param pResponse 
	 * 		The HTTP response object that will contain the information 
	 * 		pertaining to the response
	 * @param pBusinessContext 
	 * 		The business context of the current request 
	 * @param pCallbackHandler 
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return A <code>java.util.Map</code> that is used in the result of the 
	 * 		current request
	 * 
	 * @throws <code>java.lang.Exception</code> in case a problem occurred while 
	 * 		parsing the HTTP request or executing the business object document. 
	 * 		These exceptions can range from server side validation which are 
	 * 		recoverable to server errors where are not recoverable
     */
	 protected Map executeBusinessObjectDocument(
			 ActionMapping pActionMapping, 
			 ActionForm pActionForm, 
			 HttpServletRequest pRequest, 
			 HttpServletResponse pResponse, 
			 BusinessContextType pBusinessContext, 
			 CallbackHandler pCallbackHandler)
	 throws Exception
     {
		 String storeId = pRequest.getParameter("storeId");
		 String serviceId = pRequest.getParameter("serviceId");
		 String userSessionId = pRequest.getParameter("userSessionId");
		 
		 if(null == storeId || null == userSessionId || null == serviceId)
		 {
			 pRequest.setAttribute("errorMessage", "ERROR: input parameters are missing");
			 return null;
		 }

		 System.out.println("-->>> RetrieveServiceFileAction: storeId = " + 
				 storeId + " userSessionId = " + userSessionId + " serviceId = " + serviceId); 
		 
		 UserSessionInfo info = UserSessionHandler.getUserSessionInfoBySessionKey(userSessionId);
		 if(null == info) // user session info was not found 
		 {
			 pRequest.setAttribute("errorMessage", "ERROR: Session key is invalid");
			 return null;
		 }
		 
		 CatalogEntryType service = CatalogUtilities.getCatalogEntryWithInformationById(
				 serviceId,
				 storeId, 
				 pRequest.getSession().getServletContext(), 
				 pBusinessContext, 
				 pCallbackHandler);

		 // no service
		 if(null == service)
		 {
			 pRequest.setAttribute("errorMessage", "ERROR: Invalid parameters");
			 return null;
		 }
		 
		 // retrieve the items from the FIELD4 for of the CATENTRY table
		 // for the service record
		 CatalogEntryAccessBean serviceBean = CatalogUtilities.getCatalogEntryAccessBean(serviceId);
		 String itemsList = serviceBean.getField4();
		 System.out.println("-->> itemsList = " + itemsList);
		 
		 CatalogEntryType[] items = null;
		 if(null != itemsList)
		 {
			 List tmp = new ArrayList();
			 StringTokenizer st = new StringTokenizer(itemsList, ";");
			 while(st.hasMoreTokens())
			 {
				 String itemId = st.nextToken();
				 if(null != itemId && itemId.length() > 0)
				 {
					 CatalogEntryType item = CatalogUtilities.getCatalogEntryWithInformationById(
							 itemId,
							 storeId, 
							 pRequest.getSession().getServletContext(), 
							 pBusinessContext, 
							 pCallbackHandler);
					 System.out.println("-->> add item = " + itemId + " to the service");
					 tmp.add(item);
				 }
			 }
			 if(!tmp.isEmpty())
			 {
				 items = new CatalogEntryType[tmp.size()];
				 tmp.toArray(items);
			 }
		 }

		 // no composed items
		 if(null == items || items.length == 0)
		 {
			 pRequest.setAttribute("errorMessage", "ERROR: Invalid parameters");
			 return null;
		 }
		
		 // in case uaal file is the only item that is composed in the service, 
		 // return it (for backward compatibility)
		 if(items.length == 1)
		 {
			 Map parameters = items[0].getUserData().getUserDataField();
			 String itemType = (String)parameters.get("itemtype");
			 if(null != itemType && itemType.equals("AAL_APPLICATION"))
			 {
				 if(writeUaalFileToOutput(service, items[0], pResponse))
				 {
					 // success in writing the uaal file to the response
					 return null;
				 }
			 }
		 }
		
		 // return usrv file
		 buildAndWriteUsrvFileToOutput(service, items, pResponse);
		
		 return null;
     }
	 
	/**
	 * Set the content type and the header of the <code>HttpServletResponse</code>.
	 * 
	 * @param pFileName 
	 * 		The name of the service file
	 * @param pResponse 
	 * 		The servlet response to be set
	 */
	 private void setFileTypeInResponse(String pFileName, HttpServletResponse pResponse)
	 {
		 pFileName = pFileName.replaceAll(" ", "_");
		 System.out.println("-->> setFileTypeInResponse pFileName = " + pFileName);

		 String fileType = "uaal";
		 int index = pFileName.indexOf(".");
		 if(index != -1)
		 {
			 fileType = pFileName.substring(index+1);
		 }
		 if(fileType.equalsIgnoreCase("apk"))
		 {
			 pResponse.setContentType("application/vnd.android.package-archive");
		 }
		 else
		 {
			 pResponse.setContentType("application/" + fileType);
		 }
		 pResponse.setHeader("Content-disposition","attachment; filename=" + pFileName);
	 }

	 /**
	  * In case the application file was uploaded as a uaal file then write the
	  * uaal file to the given output stream.
	  * 
	  * @param pService 
	  *		The service as a <code>CatalogEntryType</code>
	  * @param pItem 
	  * 	The application item as a <code>CatalogEntryType</code>
	  * @param pOutput 
	  * 	The output stream to write the file into
	  * 
	  * @return <code>true</code> if a uaal file is associated with the item and
	  * 	was successfully written to the output stream. Otherwise, return 
	  * 	<code>false</code>
	  * 
	  * @throws uAALException in case a failure occurred during the retrieval of 
	  * 	the application file or during the writing process
	  */
	 private boolean writeUaalFileToOutput(
			 CatalogEntryType pService, 
			 CatalogEntryType pItem, 
			 HttpServletResponse pResponse)
	 	throws uAALException
	 {
		if(null == pItem || null == pService)
		{
			throw new IllegalArgumentException("ERROR: Invalid parameters");
		}
		
		OutputStream output = null;
		InputStream input = null;
		try
		{
			AppHandler handler = new AppHandler(pService, pItem);
			System.out.println("-->> handler.getFileType() = " + handler.getFileType());
			if(null != handler.getFileType() && handler.getFileType().equals("uaal"))
			{
				output = pResponse.getOutputStream();
				String appName = pItem.getCatalogEntryIdentifier().getExternalIdentifier().getPartNumber();
				setFileTypeInResponse(appName + ".uaal", pResponse);
				 
				input = handler.getAppFileInputStream();

				byte[] buff = new byte[2048];
				int bytesRead = -1;
				long total = 0;
				 
				// simple read/write loop
				while(-1 != (bytesRead = input.read(buff, 0, buff.length)))
				{
					output.write(buff, 0, bytesRead);
					total += bytesRead;
				}
				System.out.println("-->> RetrieveServiceFileAction: output size in bytes = " + total);
				return true;
			}
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to write file of type 'uaal' to the output stream (" + 
					ex.getMessage() + ")");	
		}
		finally
		{
			if(null != input)
			{
				try
				{
					input.close();
				}
				catch(IOException ex) {} // ignore
			}
			if(null != output) 
			{
				try
				{
					output.flush();
					output.close();
				}
				catch(IOException ex) {} // ignore
			}
		}
		return false;
	}
	
	/**
	 * Build a usrv file from the service information and all its composed items 
	 * and then write to file to the response output stream.
	 * 
	 * @param pService 
	 * 		The service as a <code>CatalogEntryType</code>
	 * @param pItems 
	 * 		All the composed items as <code>CatalogEntryType</code>s
	 * @param pOutput 
	 * 		The output stream to write the file into
	 * 
	 * @throws uAALException in case a failure occurred during the building or 
	 * 		writing of the file
	 */
	private void buildAndWriteUsrvFileToOutput(
			CatalogEntryType pService,
			CatalogEntryType[] pItems,
			HttpServletResponse pResponse)
		throws uAALException
	{
		if(null == pItems || null == pService || null == pResponse)
		{
			throw new IllegalArgumentException("ERROR: Invalid parameters");
		}

		String serviceName = 
			pService.getCatalogEntryIdentifier().getExternalIdentifier().getPartNumber();

		ZipOutputStream out = null; 
		try
		{
			out = new ZipOutputStream(pResponse.getOutputStream());
			
			setFileTypeInResponse(serviceName + ".usrv", pResponse);
			
			UsrvHandler handler = new UsrvHandler(out, pService, pItems);
			handler.build();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to build and write 'usrv' file to the output stream (" + 
					ex.getMessage() + ")");	
		}
		finally
		{
			if(null != out)
			{
				try
				{
					out.flush();
					out.close();
				}
				catch(IOException ex) {} // ignore
			}
		}
	}
}
