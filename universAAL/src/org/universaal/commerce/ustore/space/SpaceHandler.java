/*	
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.space;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.namespace.QName;

import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.application.AppHandler;
import org.universaal.commerce.ustore.user.UserSessionInfo;
import org.universaal.commerce.ustore.utilities.CatalogUtilities;
import org.universaal.commerce.ustore.utilities.CommonUtilities;
import org.universaal.commerce.ustore.utilities.OrderUtilities;
import org.universaal.ucc.deploymanagerservice.DeployManagerService;
import org.universaal.ucc.deploymanagerservice.DeployManagerServicePortType;

import com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType;
import com.ibm.commerce.catalog.objects.CatalogEntryAccessBean;
import com.ibm.commerce.order.objects.OrderItemAccessBean;
import com.ibm.commerce.user.objects.UserAccessBean;

/**
 * This class supports access to UniversAAL spaces (of users), and retrieval of 
 * data about services installed in that space.
 * 
 * @author roni
 */
public class SpaceHandler 
{
	/**
	 * The Qualified name of the Deploy Manager web service
	 */
    private final static QName DEPLOY_MANAGER_QNAME = 
    	new QName("http://deploymanagerservice.ucc.universAAL.org/", "DeployManagerService");

	/**
	 * The URI of the Deploy Manager web service
	 */
    private final static String DEPLOY_MANAGER_PATH = "/deploymanager?wsdl";

    /**
     * The elements in the XML snippets that describe the AAL space  
     */
    private static final String SERVICE_ID = "serviceId";
    private static final String UNIT_ID = "id";
    private static final String UNIT_VERSION = "version";
    
	/**
	 * The single instance of this class
	 */
	private static SpaceHandler mInstance = new SpaceHandler();
	
	/**
	 * @return The single instance of the space handler
	 */
	public static SpaceHandler getInstance()
	{
		return mInstance;
	}
	
	/**
	 * Private constructor.
	 */
	private SpaceHandler()
	{
	}
	
	/**
	 * Get the IP address and port that are stored for the given user in the 
	 * uStore DB.
	 * 
	 * @param pUserId
	 * 		The identifier of the user ID in uStore
	 * 
	 * @return 
	 * 		A <code>String</code> containing the IP address and port of the 
	 * 		given user that are stored in the uStore DB
	 *         
	 * @throws uAALException
	 * 		If an exception has occurred during the process
	 */
	private String getUserIPAddressAndPort(String pUserId)
		throws uAALException
	{
		 // retrieve the IP address & port of the user
		 String ipAddressAndPort = null;
		 try
		 {
			 UserAccessBean bean = new UserAccessBean();
			 bean.setInitKey_MemberId(pUserId);
			 bean.refreshCopyHelper();
			 ipAddressAndPort = bean.getUserField2();
		 }
		 catch(Exception ex)
		 {
			 throw new uAALException( 
					 "ERROR: Failed to retrieve the user connection data. " +
					 "Please register to uStore from the universAAL runtime platform.");
		 }

		 System.out.println("-->>> SpaceHandler: ipAddress and port = " + ipAddressAndPort); 
		 
		 // no IP address or port
		 if(null == ipAddressAndPort)
		 {
			 throw new uAALException( 
					 "ERROR: Failed to retrieve the user connection data. " +
					 "Please register to uStore from the universAAL runtime platform.");
		 }
		 return ipAddressAndPort;
	}
	
	/**
	 * Get a service port of the Deploy Manager web service for a given user.
	 * 
	 * @param pUserId
	 * 		The identifier of the user ID in uStore
	 * 
	 * @return 
	 * 		A <code>org.universaal.ucc.deploymanagerservice.DeployManagerServicePortType</code>
	 * 		for the given user
	 * 
	 * @throws uAALException
	 * 		If an exception has occurred during the process
	 */
	public DeployManagerServicePortType getDeployManagerServicePortType(String pUserId)
		throws uAALException
	{
		String ipAddressAndPort = SpaceHandler.getInstance().getUserIPAddressAndPort(pUserId);
		
		// setup the WSDL location, may need some host and port adjustments.
		String path = "http://" + ipAddressAndPort + DEPLOY_MANAGER_PATH;
		System.out.println("-->>> SpaceHandler.getDeployManagerServicePortType(): path = " + path); 

		try
		{
			DeployManagerService manager = new DeployManagerService(new URL(path), DEPLOY_MANAGER_QNAME);
			return manager.getDeployManagerServicePort();
		}
		catch(MalformedURLException ex)
		{
			throw new uAALException("ERROR: Failed to get the deploy manager service port (" + 
					ex.getMessage() + ")");
		}
	}
	
	/**
	 * Get the installed services in a given user's AAL space.
	 * 
	 * @param pUserInfo
	 * 		A {@link UserSessionInfo} holding information of the user's current 
	 * 		session
	 * 
	 * @return 
	 * 		A <code>java.util.Map</code> for each installed service contains the 
	 * 		IDs of installed services, mapped to description of installed units 
	 * 		for each service
	 * 
	 * @throws uAALException
	 * 		If an exception has occurred during the process
	 */
	public Map getUserInstalledServices(UserSessionInfo pUserInfo)
		throws uAALException
	{
		System.out.println("-->> SpaceHandler.getUserInstalledServices");
		
		Map services = new HashMap();
		
		DeployManagerServicePortType managerPort = getDeployManagerServicePortType(
				pUserInfo.getUserId());
		
		System.out.println("-->> going to invoke getSessionKey()");
		String sessionKey = 
			managerPort.getSessionKey(pUserInfo.getUserName(), pUserInfo.getPassword());
		System.out.println("-->> session key = " + sessionKey);
		if(null == sessionKey)
		{
			throw new uAALException("ERROR: Failed to retrieve the user session key");
		}

		System.out.println("----->>>>>> going to invoke getInstalledServices()");
		String installedServices = managerPort.getInstalledServices(sessionKey);
		System.out.println("-->> installedServices = " + installedServices);
		if(null != installedServices)
		{
			int index1 = installedServices.indexOf("<" + SERVICE_ID + ">");
			while(index1 > -1)
			{
				int index2 = installedServices.indexOf("</" + SERVICE_ID + ">", index1+1);
				if(index2 > -1)
				{
					String serviceId = installedServices.substring(index1 + SERVICE_ID.length()+2, index2);
					System.out.println("----->>>>>> serviceId = " + serviceId);
					
					Map units = new HashMap();
					
					String installedUnits = managerPort.getInstalledUnitsForService(sessionKey, serviceId);
					System.out.println("----->>>>>> installed installedUnits for service " + serviceId + ":" + installedUnits);
					
					if(null != installedUnits)
					{
						int index3 = installedUnits.indexOf("<" + UNIT_ID + ">");
						while(index3 > -1)
						{
							String unitId = null;
							String unitVersion = null;
							int index4 = installedUnits.indexOf("</" + UNIT_ID + ">", index3+1);
							if(index4 > -1)
							{
								unitId = installedUnits.substring(index3 + UNIT_ID.length()+2, index4);
								System.out.println("----->>>>>> unitId = " + unitId);
							}
							int index5 = installedUnits.indexOf("<" + UNIT_VERSION + ">", index3+1);
							if(index5 > -1)
							{
								int index6 = installedUnits.indexOf("</" + UNIT_VERSION + ">", index5+1);
								if(index6 > -1)
								{
									unitVersion = installedUnits.substring(index5 + UNIT_VERSION.length()+2, index6);
									System.out.println("----->>>>>> unitVersion = " + unitVersion);
								}
							}
							if(null != unitId && null != unitVersion)
							{
								units.put(unitId, unitVersion);
							}
							index3 = installedUnits.indexOf("<" + UNIT_ID + ">", index4);
						}
					}
					services.put(serviceId, units);
				}
				index1 = installedServices.indexOf("<" + SERVICE_ID + ">", index2);
			}
		}
		return services;
	}
	
	/**
	 * Compare the services available in the uStore catalog that have been 
	 * purchased by the given user to the services installed in that user's 
	 * space. The results of this comparison are stored in the uStore DB and 
	 * later reflected in the store front page with an 'info' icon:
	 * - USERS (FIELD1) - 0 indicates that there are no changes between the 
	 * user's services in the catalog versus his services at the space, 1 
	 * indicates that there are changes 
	 * - ORDERS (FIELD1) - 0 indicates that there are no changes between the 
	 * version of the services that have been purchased by the user in this 
	 * particular order, 1 indicates that there are changes between the version 
	 * available in the catalog to the version installed at the user's space 
	 * - ORDERITEMS (FIELD1) - 0 indicates that this service in the order was not 
	 * installed yet in the user's AAL space, 1 indicates that this service is 
	 * installed in the user's space but a new version of it exists in the 
	 * uStore catalog and 2 indicates that the service version in the catalog 
	 * and in the user's space are the same  
	 * 
	 * @param pUserInfo
	 * 		A {@link UserSessionInfo} holding information of the user's current 
	 * 		session
	 * 
	 * @throws uAALException
	 * 		If an exception has occurred during the process
	 */
	public void compareCatalogToSpace(UserSessionInfo pUserInfo)
		throws uAALException
	{
		String storeId = CommonUtilities.getProperty("storeId", "10001");
		
		System.out.println("++++++ compareCatalogToSpace userId = " + pUserInfo.getUserId() + " storeId = " + storeId);

		// retrieve the user's orders
		List orderIds = OrderUtilities.getOrdersByUserId(
				pUserInfo.getUserId(), 
				storeId,
				null,
				CommonUtilities.getInstance().getBusinessContext(),
				pUserInfo.getCallbackHandler());
		
		boolean isUpdateExist = false;

		try
		{
			if(null != orderIds)
			{
				System.out.println("++++++ compareCatalogToSpace: userId = " + pUserInfo.getUserId() + " orderIds.length = " + orderIds.size());
				Map installedServices = getUserInstalledServices(pUserInfo);
				
				for(int i=0; i<orderIds.size(); i++)
				{
					boolean isOrderUpdateExist = false;
					String orderId = (String)orderIds.get(i);
					
					List orderItemIds = OrderUtilities.getOrderItemsByOrderId(
							orderId,
							storeId,
							null,
							CommonUtilities.getInstance().getBusinessContext(),
							pUserInfo.getCallbackHandler());
	
					if(null != orderItemIds)
					{
						Iterator iter = orderItemIds.iterator();
						while(iter.hasNext())
						{
							String orderItemId = (String)iter.next();
							
							OrderItemAccessBean orderItemBean = OrderUtilities.getOrderItemAccessBean(orderItemId);
							String serviceId = orderItemBean.getCatalogEntryId();
							
							// service installed
							if(installedServices.containsKey(serviceId))
							{
								System.out.println("++++++ userId = " + pUserInfo.getUserId() + " serviceId = " + serviceId + " is installed in the AAL space");
								boolean isServiceUpdateExist = false;
								
								CatalogEntryType service = CatalogUtilities.getCatalogEntryById(
										serviceId, 
										storeId, 
										null, 
										CommonUtilities.getInstance().getBusinessContext(),
										pUserInfo.getCallbackHandler());
			
								Map units = (Map)installedServices.get(serviceId);
								
								CatalogEntryAccessBean catalogEntryBean = CatalogUtilities.getCatalogEntryAccessBean(serviceId);
								String items = catalogEntryBean.getField4(); 
								StringTokenizer st = new StringTokenizer(items, ";");
								while(st.hasMoreTokens() && !isServiceUpdateExist)
								{
									String itemId = st.nextToken();
			
									CatalogEntryType item = CatalogUtilities.getCatalogEntryById(
											itemId, 
											storeId, 
											null, 
											CommonUtilities.getInstance().getBusinessContext(),
											pUserInfo.getCallbackHandler());
									
									 Map parameters = item.getUserData().getUserDataField();
									 String itemType = (String)parameters.get("itemtype");
									 if(null != itemType && itemType.equals("AAL_APPLICATION"))
									 {
										 System.out.println("++++++ userId = " + pUserInfo.getUserId() + " serviceId = " + serviceId + " itemId = " + itemId + " is an AAL_APPLICATION");
										 AppHandler appHandler = new AppHandler(service, item);
											
										try
										{
											// initialize the settings of a uapp file 
											appHandler.init();
											
											System.out.println("++++++ userId = " + pUserInfo.getUserId() + " serviceId = " + serviceId + " itemId = " + itemId + " is a uapp file");
											Map params = appHandler.getUappConfigParameters();
											int counter = 1;
											String bundleId = (String)params.get(AppHandler.APP_PART_BUNDLE + "-id-" + counter);
											while(null != bundleId)
											{
												if(!units.containsKey(bundleId)) // unit not installed 
												{
													System.out.println("++++++ userId = " + pUserInfo.getUserId() + " serviceId = " + serviceId + " itemId = " + itemId + " bundleId = " + bundleId + " is not installed in the AAL space");
													isServiceUpdateExist = true;
													break;
												}
												else // unit installed
												{
													String bundleVersion = (String)params.get(AppHandler.APP_PART_BUNDLE + "-version-" + counter);
													String unitVersion = (String)units.get(bundleId);
													System.out.println("++++++ userId = " + pUserInfo.getUserId() + " serviceId = " + serviceId + " itemId = " + itemId + 
															" bundleId = " + bundleId + " bundleVersion = " + bundleVersion +
															" unitVersion in the AAL space = " + unitVersion);
													if(null != bundleVersion && null != unitVersion && !bundleVersion.equals(unitVersion)) // not the same version
													{
														System.out.println("++++++ userId = " + pUserInfo.getUserId() + " serviceId = " + serviceId + " itemId = " + itemId + 
																" bundleId = " + bundleId + " different version installed in the AAL space");
														isServiceUpdateExist = true;
														break;
													}
												}
												counter ++;
												bundleId = (String)params.get(AppHandler.APP_PART_BUNDLE + "-id-" + counter);
											}
										}
										catch(Exception ex) {} // not a uapp file - ignore
									 }
								}
								
								if(isServiceUpdateExist) // updates (status = 1) 
								{
									isOrderUpdateExist = true;
									OrderUtilities.updateOrderItemAccessBean(orderItemId, 1);
								}
								else // no updates (status = 2) 
								{
									OrderUtilities.updateOrderItemAccessBean(orderItemId, 2);
								}
							}
							else // service not installed (status = 0)
							{
								isOrderUpdateExist = true;
								System.out.println("++++++ userId = " + pUserInfo.getUserId() + " serviceId = " + serviceId + 
										" is NOT installed in the AAL space");
								OrderUtilities.updateOrderItemAccessBean(orderItemId, 0);
							}
						}
					}
					if(isOrderUpdateExist)
					{
						isUpdateExist = true;
					}
					OrderUtilities.updateOrderAccessBean(orderId, isOrderUpdateExist ? 1 : 0);
				}
			}
			UserAccessBean bean = new UserAccessBean();
			bean.setInitKey_MemberId(pUserInfo.getUserId());
			bean.refreshCopyHelper();
			bean.setUserField1(isUpdateExist ? "1" : "0");
			bean.commitCopyHelper();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to comapre the services in the catalog to those in the AAL space (" +
					ex.getMessage() + ")");
		}
	}
}
