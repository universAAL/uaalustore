/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.tools;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.application.AppHandler;
import org.universaal.commerce.ustore.application.NexusHandler;
import org.universaal.commerce.ustore.utilities.CatalogUtilities;
import org.universaal.commerce.ustore.utilities.CommonUtilities;

import com.ibm.commerce.catalog.facade.client.CatalogEntryException;
import com.ibm.commerce.catalog.facade.datatypes.AcknowledgeCatalogEntryDataAreaType;
import com.ibm.commerce.catalog.facade.datatypes.CatalogAttributeType;
import com.ibm.commerce.catalog.facade.datatypes.CatalogDescriptionType;
import com.ibm.commerce.catalog.facade.datatypes.CatalogEntryAttributesType;
import com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType;
import com.ibm.commerce.catalog.facade.datatypes.CatalogFactory;
import com.ibm.commerce.catalog.facade.datatypes.CatalogGroupType;
import com.ibm.commerce.catalog.facade.datatypes.StringValueType;
import com.ibm.commerce.catalog.objects.CatalogEntryAccessBean;
import com.ibm.commerce.command.CommandContext;
import com.ibm.commerce.component.contextservice.ActivityGUID;
import com.ibm.commerce.component.contextservice.ActivityToken;
import com.ibm.commerce.component.objects.WebAdapterServiceAccessBean;
import com.ibm.commerce.context.baseimpl.ContextHelper;
import com.ibm.commerce.datatype.TypedProperty;
import com.ibm.commerce.foundation.client.facade.bod.AbstractBusinessObjectDocumentException;
import com.ibm.commerce.foundation.client.facade.bod.ClientError;
import com.ibm.commerce.foundation.common.datatypes.CatalogEntryIdentifierType;
import com.ibm.commerce.foundation.common.datatypes.CatalogGroupIdentifierType;
import com.ibm.commerce.foundation.common.datatypes.CommerceFoundationFactory;
import com.ibm.commerce.foundation.common.datatypes.MonetaryAmountType;
import com.ibm.commerce.foundation.common.datatypes.PartNumberIdentifierType;
import com.ibm.commerce.foundation.common.datatypes.UnitPriceType;
import com.ibm.commerce.foundation.common.datatypes.UserDataType;
import com.ibm.commerce.oagis9.datatypes.ResponseActionCriteriaType;
import com.ibm.commerce.registry.UrlRegistry;
import com.ibm.commerce.registry.UrlRegistryEntry;
import com.ibm.commerce.server.ECConstants;
import com.ibm.commerce.server.MultipartInputStreamHandler;
import com.ibm.commerce.server.UploadedFile;

/**
 * This class provides methods for managing the AAL applications that are stored
 * in the uStore catalog.
 * 
 * @author roni
 */

@WebService
public class AALApplicationManager extends AbstractManager
	implements IAALApplicationManager
{
	/**
	 * Uploads an AAL Application of any type (except of uapp files) into 
	 * uStore catalog. If the identifier of the application is provided then 
	 * store this application as a new version and update its fields. Otherwise, 
	 * store this application as a new catalog entry.
	 * - Check the required fields
	 * - In case this is a new application then store the username that creates 
	 *   it in the CATENTRY table in 'FIELD5', otherwise verify that the user 
	 *   who updates the application is the one that created it, otherwise throw 
	 *   an exception
	 * - Check that the version field is a valid double and in case of an 
	 *   update, check that it was incremented
	 * - If the application has already been included in a service and it is 
	 *   being marked as 'not for purchase' then all the services that include 
	 *   this application are also marked as 'not for purchase'
	 * - All the fields that describe this application can be updated even if 
	 * 	 this application is already included in a service. The service provider 
	 *   is responsible to manage this information and to take into 
	 *   consideration the status of the application 
	 * For a new application, first save the application in the catalog and 
	 * then store the file in Nexus by using the application id and version. 
	 * If a failure occurs then remove it from the catalog.
	 * For an update of existing application, add the application file to the 
	 * Nexus server (under the same application id and a new version). If the 
	 * operation ended successfully then update the application parameters. 
	 *  
	 * @param pUserName 
	 * 		The developer's user name in uStore (required)
	 * @param pPassword 
	 * 		The developer's password in uStore (required)
	 * @param pApplicationId 
	 * 		The identifier of the application (if null a new catalog entry will 
	 * 		be created) 
	 * @param pName 
	 * 		The application's name (required)
	 * @param pShortDescription 
	 * 		A short description for the application 
	 * @param pLongDescription 
	 * 		A long description for the application
	 * @param pKeywords 
	 * 		Keywords separated by a comma (used by the search tool)
	 * @param pDeveloperName 
	 * 		The name of the developer 
	 * @param pDeveloperEmail 
	 * 		The email address of the developer 
	 * @param pDeveloperPhone 
	 * 		The phone number of the developer
	 * @param pOrganizationName 
	 * 		The name of the application vendor 
	 * @param pOrganizationURL 
	 * 		The URL to the security certificate (optional) 
	 * @param pOrganizationCertificate
	 * 		The organization certificate (optional)
	 * @param pURL 
	 * 		The URL that describes this application (provides additional 
	 * 		information)
	 * @param pParentCategoryId 
	 * 		The category that this application belongs to (required)       
	 * @param pFullImageFileName 
	 * 		The name of the full image that will be displayed for this 
	 * 		application       
	 * @param pFullImage 
	 * 		The full image that will be displayed for this application
	 * @param pThumbnailImageFileName 
	 * 		The name of the thumbnail image that will be displayed for this 
	 * 		application
	 * @param pThumbnailImage 
	 * 		The thumbnail image that will be displayed for this application
	 * @param pListPrice 
	 * 		The price of this application (in USD). If not provided then zero.             
	 * @param pVersion 
	 * 		The internal version of the application in the store (required)
	 * @param pVersionNotes 
	 * 		Description for this version (required)
	 * @param pFileName 
	 * 		The name of the application bundle file (required)
	 * @param pFileInputStream 
	 * 		The input stream of the bundle file (the bundle file will be stored 
	 * 		in the inner Nexus server (required)
	 * @param pServiceLevelAgreement 
	 * 		The service level agreement for this application
	 * @param pRequirements 
	 * 		Describes the general application requirements as free text (i.e. 
	 * 		requirements to the AAL Space) 
	 * @param pLicenses 
	 * 		The application licenses as free text
	 * @param pCapabilities 
	 * 		Describes what the application offers as free text
	 * @param pIsForPurchase 
	 * 		Indicates whether this application is ready for purchase
	 *  
	 * @return 
	 * 		The identifier of the application in the catalog. In case of an 
	 * 		update the given application identifier will be returned. 
	 *  
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * @throws <code>uAALException</code> if an error happens while performing a
	 * 		process operation on a single CatalogEntry 
	 */
	@WebMethod
	public String uploadAnyAALApplication(
			@WebParam(name = "username") String pUserName,
			@WebParam(name = "password") String pPassword,
			@WebParam(name = "applicationid") String pApplicationId,
			@WebParam(name = "applicationname") String pName,
			@WebParam(name = "shortdescription") String pShortDescription,
			@WebParam(name = "longdescription") String pLongDescription,
			@WebParam(name = "keywords") String pKeywords,
			@WebParam(name = "developername") String pDeveloperName,
			@WebParam(name = "developeremail") String pDeveloperEmail,
			@WebParam(name = "developerphone") String pDeveloperPhone,
			@WebParam(name = "organizationname") String pOrganizationName,
			@WebParam(name = "organizationURL") String pOrganizationURL,
			@WebParam(name = "organizationcertificate") String pOrganizationCertificate,
			@WebParam(name = "applicationURL") String pURL,
			@WebParam(name = "parentcategoryid") String pParentCategoryId,
			@WebParam(name = "fullimagefilename") String pFullImageFileName,
			@WebParam(name = "fullimage") byte[] pFullImage,
			@WebParam(name = "thumbnailimagefilename") String pThumbnailImageFileName,
			@WebParam(name = "thumbnailimage") byte[] pThumbnailImage,
			@WebParam(name = "listprice") String pListPrice,
			@WebParam(name = "version") String pVersion,
			@WebParam(name = "versionnotes") String pVersionNotes,
			@WebParam(name = "filename") String pFileName,
			@WebParam(name = "fileinputstream") byte[] pFileInputStream,
			@WebParam(name = "servicelevelagreement") String pServiceLevelAgreement,
			@WebParam(name = "requirements") String pRequirements,
			@WebParam(name = "licenses") String pLicenses,
			@WebParam(name = "capabilities") String pCapabilities,
			@WebParam(name = "isforpurchase") boolean pIsForPurchase)
		throws uAALException
	{
		// check the required parameters
		if(null == pUserName || null == pPassword || null == pName || 
				pName.length() == 0 || null == pParentCategoryId ||
				pParentCategoryId.length() == 0 || null == pVersion || pVersion.length() == 0 ||
				null == pVersionNotes || pVersionNotes.length() == 0 || 
				null == pFileName || pFileName.length() == 0 ||
				null == pFileInputStream)
		{
			throw new IllegalArgumentException("ERROR: An input parameter in NULL");
		}
		
		init(pUserName, pPassword);

		try
		{
			// extract the file type
			String appFileType = "uaal";
			int index = pFileName.lastIndexOf(".");
			if(index != -1)
			{
				appFileType = pFileName.substring(index+1);
			}
			
			if(null == pApplicationId) // new application as a catalog entry
			{
				CatalogEntryType catalogEntryToAdd = CatalogFactory.eINSTANCE.createCatalogEntryType();
				catalogEntryToAdd.setCatalogEntryTypeCode("ItemBean");
				
				// application identifier
				catalogEntryToAdd.setCatalogEntryIdentifier(createCatalogEntryIdentifier());

				// application description
				catalogEntryToAdd.getDescription().add(createCatalogDescription(
						pName, pShortDescription, pLongDescription, pKeywords, 
						pFullImageFileName, pFullImage, pThumbnailImageFileName, pThumbnailImage));

				// application price
				catalogEntryToAdd.setListPrice(CreateUnitPrice(pListPrice));
				
				// application attributes
				CatalogEntryAttributesType catalogEntryType = CatalogFactory.eINSTANCE.createCatalogEntryAttributesType();
				catalogEntryToAdd.setCatalogEntryAttributes(catalogEntryType);
				addAttribute(catalogEntryType, "url", pURL); // add the 'URL' attribute
				addAttribute(catalogEntryType, "buyable", pIsForPurchase ? "1" : "0"); // add the 'buyable' attribute
				
				// application additional information --> user data
				catalogEntryToAdd.setUserData(createUserData(
						pUserName, appFileType, pFileName, pFileInputStream, pVersion, pVersionNotes, 
						pRequirements, pLicenses, pCapabilities, pOrganizationName, pOrganizationURL, 
						pOrganizationCertificate, pDeveloperName, pDeveloperEmail, pDeveloperPhone, 
						pServiceLevelAgreement));

				// application parent category
				CatalogGroupIdentifierType parent = CommerceFoundationFactory.eINSTANCE.createCatalogGroupIdentifierType();
				parent.setUniqueID(pParentCategoryId);
				catalogEntryToAdd.setParentCatalogGroupIdentifier(parent);
				
				List list = new ArrayList();
				list.add(catalogEntryToAdd);
	
				// perform the add request
				AcknowledgeCatalogEntryDataAreaType respond = mCatalogEntryClient.createCatalogEntry(list);
				
				String newCatalogEntryId = getCatalogEntryIdFromRespond(respond);
				System.out.println("-->> newCatalogEntryId = " + newCatalogEntryId);
				if(null != newCatalogEntryId)
				{
					CatalogUtilities.updateCatalogEntryAccessBeanUserName(newCatalogEntryId, pUserName);
					CatalogUtilities.updateCatalogEntryDescriptionBean(newCatalogEntryId, true);
					
					// deploy the file in the Nexus server
					try 
					{
						String fileType = "uaal";
						NexusHandler.getInstance().deployArtifactToNexusServer(
								new ByteArrayInputStream(pFileInputStream), newCatalogEntryId, 
								newCatalogEntryId, pVersion, pVersionNotes, fileType);
					}
					catch(uAALException e) 
					{
						try
						{
							// failed to add the file to the Nexus server --> 
							// delete the new item from the catalog
							CatalogUtilities.deleteItem(newCatalogEntryId, 
									CommonUtilities.getInstance().getProperty("storeId", "10101"), 
									null, 
									mBusinessContext, 
									mCallbackHandler);
						}
						catch(uAALException ex)
						{
							throw new uAALException("ERROR: Can't delete the item from the catalog as a result " +
									"of failure of adding the file to Nexus (" + ex.getMessage() + ")"); 
						}
						throw new uAALException("ERROR: Can't deploy the application in the Nexus repository (" + 
								e.getMessage() + ")");
					}
					return newCatalogEntryId;
				}

				List clientErrors = getClientErrors(respond);
				throw new uAALException(clientErrors);
			}
			else // update the application
			{
				CatalogEntryAccessBean bean = CatalogUtilities.getCatalogEntryAccessBean(pApplicationId);
				String user = bean.getField5();
				if(null == user || !user.equals(pUserName))
				{
					throw new uAALException("ERROR: User [" + pUserName + "] is not authorized to update this application");
				}

				CatalogEntryType entry = CatalogUtilities.getCatalogEntryById(
						pApplicationId,
						CommonUtilities.getInstance().getProperty("storeId", "10101"), 
						null,
						mBusinessContext,
						mCallbackHandler);
				
				if(null == entry)
				{
					System.out.println("ERROR: Application [" + pApplicationId + "] was not found");	
		
					List clientErrors = new ArrayList();
					ClientError error = new ClientError();
					error.setErrorKey("_ERR_CATALOG_ENTRY_WAS_NOT_FOUND");
					error.setLocalizedMessage("Application [" + pApplicationId + "] was not found");
					clientErrors.add(error);
					throw new uAALException(clientErrors);
				}

				// deploy the file in the Nexus server
				String fileType = "uaal";
				System.out.println("-->> AALApplicationManager - Create in nexus server (" + 
						pApplicationId + " > " + pApplicationId + " > " + pVersion + ") file type is " + fileType);
				NexusHandler.getInstance().deployArtifactToNexusServer(
						new ByteArrayInputStream(pFileInputStream), pApplicationId, pApplicationId, 
						pVersion, pVersionNotes, fileType);

				// application price
				UnitPriceType price = CreateUnitPrice(pListPrice);
				price.getAlternativeCurrencyPrice().add(price.getPrice());
				price.setPrice(null);
				UnitPriceType[] listPrices = new UnitPriceType[1];
				listPrices[0] = price;
				System.out.println("-->> Update the list price to " + pListPrice);
				mCatalogEntryClient.updateCatalogEntryListPrice(entry.getCatalogEntryIdentifier(), listPrices);

				// application description
				CatalogDescriptionType description = createCatalogDescription(
						pName, pShortDescription, pLongDescription, pKeywords, 
						pFullImageFileName, pFullImage, pThumbnailImageFileName, pThumbnailImage);
				entry.getDescription().clear();
				entry.getDescription().add(description);
				CatalogEntryType[] catEntries = new CatalogEntryType[1];
				catEntries[0] = entry;
				mCatalogEntryClient.updateCatalogEntryDescription(catEntries);

				// application parent category
				String parentCategoryId = entry.getParentCatalogGroupIdentifier().getUniqueID();
				if(null == parentCategoryId ||
						(null != parentCategoryId && !parentCategoryId.equals(pParentCategoryId)))
				{
					CatalogGroupIdentifierType parent = CommerceFoundationFactory.eINSTANCE.createCatalogGroupIdentifierType();
					parent.setUniqueID(pParentCategoryId);
					System.out.println("-->> Update the parent catalog group to " + pParentCategoryId);
					mCatalogEntryClient.updateCatalogEntryParentCatGroup(entry.getCatalogEntryIdentifier(), parent);
				}
				
				// application additional information --> user data
				UserDataType userData = createUserData(
						pUserName, appFileType, pFileName, pFileInputStream, pVersion, pVersionNotes, 
						pRequirements, pLicenses, pCapabilities, pOrganizationName, pOrganizationURL, 
						pOrganizationCertificate, pDeveloperName, pDeveloperEmail, pDeveloperPhone, 
						pServiceLevelAgreement);

				entry.setUserData(userData);

				String[] actions = {"Change"};
			    String[] strXPaths = {"/CatalogEntry[1]"};
			    CatalogEntryType[] catentries = {entry};
			    mCatalogEntryClient.changeCatalogEntry(actions, catentries, strXPaths);

				// application attributes
			    bean = CatalogUtilities.getCatalogEntryAccessBean(pApplicationId);
				bean.setUrl(pURL);
				bean.setBuyable(pIsForPurchase ? "1" : "0");
				bean.commitCopyHelper();
				
				// in case the application is not for purchase the update all 
				// the services that include this application as 'not for purchase'
				if(!pIsForPurchase)
				{
					CatalogEntryType[] services = CatalogUtilities.getServicesContainReferenceToItemId(
							pApplicationId, 
							CommonUtilities.getInstance().getProperty("storeId", "10101"), 
							null,
							mBusinessContext,
							mCallbackHandler);

					if(null != services)
					{
						for(int i=0; i<services.length; i++)
						{
							CatalogEntryAccessBean serviceBean = 
								CatalogUtilities.getCatalogEntryAccessBean(services[i].getCatalogEntryIdentifier().getUniqueID());
							if(serviceBean.getBuyable().equals("1")) // the service is 'for purchase'
							{
								serviceBean.setBuyable("0"); // mark the service as 'not for purchase'
								serviceBean.commitCopyHelper();
								System.out.println("-->> Service " +
										services[i].getCatalogEntryIdentifier().getUniqueID() +
										" was updated as 'not for purchase'");
							}
						}
					}					
				}
				return pApplicationId;
			}
		} 
		catch(CatalogEntryException ex) 
		{
			System.out.println("ERROR: Failed to upload AAL application (" + ex.getMessage() + ")");
			throw new uAALException(ex);
		}
		catch(Exception ex) 
		{
			System.out.println("ERROR: Failed to upload AAL application (" + ex.getMessage() + ")");
			throw new uAALException(ex.getMessage());
		}
	}
	
	/**
	 * Uploads an AAL Application of a uapp type into uStore catalog. If the 
	 * identifier of the application is provided then store this application as 
	 * a new version and update its fields. Otherwise, store this application as
	 * a new catalog entry.
	 * - Check the required fields
	 * - In case this is a new application then store the username that creates 
	 *   it in the CATENTRY table in 'FIELD5', otherwise verify that the user 
	 *   who updates the application is the one that created it, otherwise throw 
	 *   an exception
	 * - Check that the version field is a valid double and in case of an 
	 *   update, check that it was incremented
	 * - If the application has already been included in a service and it is 
	 *   being marked as 'not for purchase' then all the services that include 
	 *   this application are also marked as 'not for purchase'
	 * - All the fields that describe this application can be updated even if 
	 * 	 this application is already included in a service. The service provider 
	 *   is responsible to manage this information and to take into 
	 *   consideration the status of the application 
	 * The parameters that describe the application such as name, description, 
	 * keywords, licenses, capabilities, requirements, etc, are being extracted
	 * from the uapp file and stored in the local DB.
	 * For a new application, first save the application in the catalog and 
	 * then store the file in Nexus by using the application id and version. 
	 * If a failure occurs then remove it from the catalog.
	 * For an update of existing application, add the application file to the 
	 * Nexus server (under the same application id and a new version). If the 
	 * operation ended successfully then update the application parameters.
	 *  
	 * @param pUserName 
	 * 		The developer's user name in uStore (required)
	 * @param pPassword 
	 * 		The developer's password in uStore (required)
	 * @param pApplicationId 
	 * 		The identifier of the application (if null a new catalog entry will 
	 * 		be created) 
	 * @param pLongDescription 
	 * 		A long description for the application
	 * @param pURL 
	 * 		The URL that describes this application (provides additional 
	 * 		information)
	 * @param pParentCategoryId 
	 * 		The category that this application belongs to (required)       
	 * @param pFullImageFileName 
	 * 		The name of the full image that will be displayed for this 
	 * 		application       
	 * @param pFullImage 
	 * 		The full image that will be displayed for this application
	 * @param pThumbnailImageFileName 
	 * 		The name of the thumbnail image that will be displayed for this 
	 * 		application
	 * @param pThumbnailImage 
	 * 		The thumbnail image that will be displayed for this application
	 * @param pListPrice 
	 * 		The price of this application (in USD). If not provided then zero             
	 * @param pVersion 
	 * 		The internal version of the application in the store (required)
	 * @param pVersionNotes 
	 * 		Description for this version (required)
	 * @param pFileName 
	 * 		The name of the application bundle file (required)
	 * @param pFileInputStream 
	 * 		The input stream of the bundle file (the bundle file will be stored 
	 * 		in the inner Nexus server (required)
	 * @param pIsForPurchase 
	 * 		Indicates whether this application is ready for purchase
	 *  
	 * @return 
	 * 		The identifier of the application in the catalog. In case of an 
	 * 		update the given application identifier will be returned 
	 *  
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * @throws <code>uAALException</code> if an error happens while performing a
	 * 		process operation on a single CatalogEntry 
	 */
	@WebMethod
	public String uploadUaapAALApplication(
			@WebParam(name = "username") String pUserName,
			@WebParam(name = "password") String pPassword,
			@WebParam(name = "applicationid") String pApplicationId,
			@WebParam(name = "longdescription") String pLongDescription,
			@WebParam(name = "applicationURL") String pURL,
			@WebParam(name = "parentcategoryid") String pParentCategoryId,
			@WebParam(name = "fullimagefilename") String pFullImageFileName,
			@WebParam(name = "fullimage") byte[] pFullImage,
			@WebParam(name = "thumbnailimagefilename") String pThumbnailImageFileName,
			@WebParam(name = "thumbnailimage") byte[] pThumbnailImage,
			@WebParam(name = "listprice") String pListPrice,
			@WebParam(name = "version") String pVersion,
			@WebParam(name = "versionnotes") String pVersionNotes,
			@WebParam(name = "filename") String pFileName,
			@WebParam(name = "fileinputstream") byte[] pFileInputStream,
			@WebParam(name = "isforpurchase") boolean pIsForPurchase)
		throws uAALException
	{
		// check the required parameters
		if(null == pUserName || null == pPassword || null == pParentCategoryId ||
				pParentCategoryId.length() == 0 || null == pVersion || pVersion.length() == 0 ||
				null == pVersionNotes || pVersionNotes.length() == 0 || 
				null == pFileName || pFileName.length() == 0 ||
				null == pFileInputStream)
		{
			throw new IllegalArgumentException("ERROR: An input parameter in NULL");
		}
		
		AppHandler handler = new AppHandler(new ByteArrayInputStream(pFileInputStream));
		handler.init();
		Map configParams = handler.getUappConfigParameters();
				
		return uploadAnyAALApplication(
				pUserName,
				pPassword,
				pApplicationId,
				(String)configParams.get(AppHandler.APP_NAME),
				(String)configParams.get(AppHandler.APP_DESCRIPTION),
				pLongDescription,
				(String)configParams.get(AppHandler.APP_KEYWORDS),
				(String)configParams.get(AppHandler.APP_DEVELOPER_NAME),
				(String)configParams.get(AppHandler.APP_DEVELOPER_EMAIL),
				(String)configParams.get(AppHandler.APP_DEVELOPER_PHONE),
				(String)configParams.get(AppHandler.APP_ORGANIZATION_NAME),
				(String)configParams.get(AppHandler.APP_ORGANIZATION_URL),
				(String)configParams.get(AppHandler.APP_ORGANIZATION_CERTIFICATE),
				pURL,
				pParentCategoryId, 
				pFullImageFileName,
				pFullImage,
				pThumbnailImageFileName,
				pThumbnailImage,
				pListPrice,
				pVersion,
				pVersionNotes,
				pFileName,
				pFileInputStream,
				(String)configParams.get(AppHandler.APP_SLA),
				(String)configParams.get(AppHandler.APP_REQUIREMENTS),
				(String)configParams.get(AppHandler.APP_LICENSES),
				(String)configParams.get(AppHandler.APP_CAPABILITIES),
				pIsForPurchase);
	}
	
	/**
	 * Deletes an application from the uStore catalog.
	 * NOTE: The application is not deleted from the catalog, it is marked as 
	 * deleted.
	 * - Verify that the user who deletes the application is the one that 
	 *   created it, otherwise throw an exception
	 * - If the application has already been included in a service then it can't 
	 *   be deleted
	 * - The file itself is being deleted from the Nexus server 
	 * 
	 * @param pUserName 
	 * 		The developer's user name in uStore (required)
	 * @param pPassword 
	 * 		The developer's password in uStore (required)
	 * @param pApplicationId 
	 * 		The identifier of the application to be deleted from the catalog 
	 * 		(required)
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		delete process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 */
	@WebMethod
	public void deleteAALApplication(
			@WebParam(name = "username") String pUserName,
			@WebParam(name = "password") String pPassword,
			@WebParam(name = "applicationid") String pApplicationId)
		throws uAALException
	{
		if(null == pUserName || null == pPassword || null == pApplicationId || 
				pApplicationId.length() == 0)
		{
			throw new IllegalArgumentException("ERROR: An input parameter in NULL");
		}

		init(pUserName, pPassword);
		
		try
		{
			CatalogEntryAccessBean bean = CatalogUtilities.getCatalogEntryAccessBean(pApplicationId);
			String user = bean.getField5();
			if(null == user || !user.equals(pUserName))
			{
				throw new Exception("ERROR: User [" + pUserName + "] is not authorized to delete this application");
			}
			
			CatalogEntryType entry = CatalogUtilities.getCatalogEntryById(
					pApplicationId,
					CommonUtilities.getInstance().getProperty("storeId", "10101"), 
					null, 
					mBusinessContext, 
					mCallbackHandler);
			
			if(null == entry)
			{
				System.out.println("ERROR: Application [" + pApplicationId + "] was not found");	

				List clientErrors = new ArrayList();
				ClientError error = new ClientError();
				error.setErrorKey("_ERR_CATALOG_ENTRY_WAS_NOT_FOUND");
				error.setLocalizedMessage("Application [" + pApplicationId + "] was not found");
				clientErrors.add(error);
				throw new uAALException(clientErrors);
			}
			
			CatalogEntryType[] services = CatalogUtilities.getServicesContainReferenceToItemId(
					pApplicationId, 
					CommonUtilities.getInstance().getProperty("storeId", "10101"), 
					null, 
					mBusinessContext, 
					mCallbackHandler);
			
			if(null != services)
			{
				System.out.println("-->> DeleteCatalogEntryAction #services contain a reference to item = " + services.length);
				if(services.length > 0)
				{
					throw new Exception("ERROR: Application can not be deleted since it is already included in a service");
				}
			}

			List list = new ArrayList();
			list.add(entry);
			
			AcknowledgeCatalogEntryDataAreaType result = mCatalogEntryClient.deleteCatalogEntry(list);
			
			boolean found = false;
			List catalogEntries = result.getCatalogEntry();
			Iterator iter = catalogEntries.iterator();
			while(iter.hasNext())
			{
				CatalogEntryType res = (CatalogEntryType)iter.next();
				if(res.getCatalogEntryIdentifier().getUniqueID().equals(pApplicationId))
				{
					found = true;
					break;
				}
			}
			if(!found)
			{
				System.out.println("ERROR: Failed to delete Application [" + pApplicationId + "]");	

				List clientErrors = new ArrayList();
				ClientError error = new ClientError();
				error.setErrorKey("_ERR_FAILED_DELETE_CATALOG_ENTRY");
				error.setLocalizedMessage("Failed to delete application [" + pApplicationId + "]");
				clientErrors.add(error);
				throw new uAALException(clientErrors);
			}

			// delete the file from the Nexus server
			Map parameters = entry.getUserData().getUserDataField();
			String version = (String)parameters.get("version");

			version = String.valueOf(Double.parseDouble(version));
			NexusHandler.getInstance().deleteArtifactFromNexusServer(pApplicationId, pApplicationId, version);
		}
		catch(AbstractBusinessObjectDocumentException ex)
		{
			System.out.println("ERROR: Failed to delete an application (" + ex.getMessage() + ")");
			throw new uAALException(ex);
		}
		catch(Exception e)
		{
			System.out.println("ERROR: Failed to delete an application (" + e.getMessage() + ")");
			throw new uAALException(e.getMessage());
		}
	}
	
	/**
	 * Returns the applications that are associated with the given user and 
	 * located in the given category in the uStore catalog.
	 * 
	 * @param pUserName 
	 * 		The name of the user (required)
	 * @param pPassword 
	 * 		The password of the user (required)
	 * @param pParentCategoryId 
	 * 		The category to look for. If null then the search will be done on 
	 * 		the full catalog      
	 *  
	 * @return 
	 * 		The applications that were found organized in the following 
	 * 		template:
	 * 		<applications>
	 * 			<application><id>application-id</id><name>application-name</name></application>
	 * 			<application><id>application-id</id><name>application-name</name></application>
	 * 			...
	 * 		</applications>
	 *  
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 */
	@WebMethod
	public String getAALApplications(
			@WebParam(name = "username") String pUserName, 
			@WebParam(name = "password") String pPassword,
			@WebParam(name = "parentcategoryid") String pParentCategoryId)
		throws uAALException
	{
		if(null == pUserName || null == pPassword)
		{
			throw new IllegalArgumentException("ERROR: An input parameter in NULL");
		}

		init(pUserName, pPassword);
		
		List apps = getUserApplications(pUserName, pParentCategoryId);

		// build XML from the Map
		String result = "<applications>";
		Iterator appsIter = apps.iterator();
		while(appsIter.hasNext())
		{
			CatalogEntryType catalogEntry = (CatalogEntryType)appsIter.next();
			CatalogEntryIdentifierType identifier = catalogEntry.getCatalogEntryIdentifier();
			Iterator iter = catalogEntry.getDescription().iterator();
			if(iter.hasNext())
			{
				CatalogDescriptionType description = (CatalogDescriptionType)iter.next();
				result += "<application>";
				result += "<id>" + identifier.getUniqueID() + "</id>";
				result += "<name>" + description.getName() + "</name>";
				result += "</application>";
			}
		}
		result += "</applications>";
		return result;
	}

	/**
	 * Returns applications from the uStore catalog in the given category and 
	 * that associated with the given user.
	 * This method is being called recursively in order to get all the 
	 * applications that are located in sub categories under the given 
	 * categories.  
	 * 
	 * @param pUserId 
	 * 		The identifier of the user that uploaded this application
	 * @param pParentCategoryId 
	 * 		The category to look inside               
	 *  
	 * @return 
	 * 		A <code>java.util.List</code> with the applications that were found 
	 * 		as <code>com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType</code>s 
	 * 
	 * @throws <code>uAALException</code> if an error has occurred
	 */
	private List getUserApplications(String pUserId, String pParentCategoryId)
		throws uAALException
	{
		List entries = new ArrayList();
		
		String categoryId = pParentCategoryId;
		if(null == categoryId)
		{
			categoryId = CommonUtilities.getInstance().getProperty("AALApplicationsCategoryId", "11001");
		}
		
		try
		{
			CatalogEntryType[] catalogEntries = CatalogUtilities.getCatalogEntriesByParentCatalogGroupId(
					categoryId,
					CommonUtilities.getInstance().getProperty("storeId", "10101"),
					null,
					mBusinessContext,
					mCallbackHandler);
			
			for(int i=0; i<catalogEntries.length; i++)
			{
				String id = catalogEntries[i].getCatalogEntryIdentifier().getUniqueID();
				Object typeCode = catalogEntries[i].getCatalogEntryTypeCode();
				if(null != typeCode && typeCode.equals("ItemBean"))
				{
					CatalogEntryAccessBean bean = CatalogUtilities.getCatalogEntryAccessBean(id);
					
					String user = bean.getField5();
					if(null != user && user.equals(pUserId))
					{
						CatalogEntryType entry = CatalogUtilities.getCatalogEntryById(
								catalogEntries[i].getCatalogEntryIdentifier().getUniqueID(),
								CommonUtilities.getInstance().getProperty("storeId", "10101"),
								null,
								mBusinessContext,
								mCallbackHandler);
	
						String itemType = (String)entry.getUserData().getUserDataField().get("itemtype");
						if(null != itemType && itemType.equals("AAL_APPLICATION"))
						{
							entries.add(catalogEntries[i]);
						}
					}
				}
			}
			
			CatalogGroupType[] groups = CatalogUtilities.getCatalogGroupsByParentCatalogGroupId(
					categoryId,
					CommonUtilities.getInstance().getProperty("storeId", "10101"),
					null,
					mBusinessContext,
					mCallbackHandler);
	
			for(int i=0; i<groups.length; i++)
			{
				entries.addAll(getUserApplications(pUserId, groups[i].getCatalogGroupIdentifier().getUniqueID()));
			}
			return entries;
		}
		catch(Exception ex)
		{
			System.out.println("ERROR: Failed to retrieve the AAL applications [user id =" + pUserId + 
					" parent category id=" + pParentCategoryId + "] (" + ex.getMessage() + ")");
			throw new uAALException("ERROR: Failed to retrieve the AAL applications (" + 
					ex.getMessage() + ")");
		}
	}
	
	/**
	 * Adds the given attribute name and value to the given attribute type 
	 * object.
	 * 
	 * @param pAttribute 
	 * 		The <code>com.ibm.commerce.catalog.facade.datatypes.CatalogEntryAttributesType</code>
	 * 		object to add the new attribute into
	 * @param pAttributeName
	 * 		The attribute name
	 * @param pAttributeValue
	 * 		The attribute value
	 */
	private void addAttribute(
			CatalogEntryAttributesType pAttribute, 
			String pAttributeName, 
			String pAttributeValue) 
	{
		CatalogAttributeType attr = CatalogFactory.eINSTANCE.createCatalogAttributeType();
		attr.setName(pAttributeName);
		StringValueType valueType = CatalogFactory.eINSTANCE.createStringValueType();
		valueType.setValue(pAttributeValue);
		attr.setStringValue(valueType);
		pAttribute.setAttribute(pAttributeName, attr);
	}

	/**
	 * Creates a new catalog entry identifier object.
	 * 
	 * @return 
	 * 		A <code>com.ibm.commerce.foundation.common.datatypes.CatalogEntryIdentifierType</code>
	 * 		with the current time as a part number
	 */
	private CatalogEntryIdentifierType createCatalogEntryIdentifier()
	{
		CatalogEntryIdentifierType identifier = 
			CommerceFoundationFactory.eINSTANCE.createCatalogEntryIdentifierType();
		PartNumberIdentifierType partNumber = 
			CommerceFoundationFactory.eINSTANCE.createPartNumberIdentifierType();
		partNumber.setPartNumber(String.valueOf(System.currentTimeMillis()));
		identifier.setExternalIdentifier(partNumber);
		
		return identifier;
	}
	
	/**
	 * Creates a new catalog entry description object.
	 * 
	 * @param pName 
	 * 		The application's name
	 * @param pShortDescription 
	 * 		A short description for the application 
	 * @param pLongDescription 
	 * 		A long description for the application
	 * @param pKeywords 
	 * 		Keywords separated by a comma (used by the search tool)
	 * @param pFullImageFileName 
	 * 		The name of the full image that will be displayed for this 
	 * 		application       
	 * @param pFullImage 
	 * 		The full image that will be displayed for this application
	 * @param pThumbnailImageFileName 
	 * 		The name of the thumbnail image that will be displayed for this 
	 * 		application
	 * @param pThumbnailImage 
	 * 		The thumbnail image that will be displayed for this application
	 * 
	 * @return 
	 * 		A <code>com.ibm.commerce.catalog.facade.datatypes.CatalogDescriptionType</code>
	 * 		holds the given values
	 * 
	 * @throws uAALException in an error has occurred during the process
	 */
	private CatalogDescriptionType createCatalogDescription(
			String pName,
			String pShortDescription,
			String pLongDescription,
			String pKeywords,
			String pFullImageName,
			byte[] pFullImage,
			String pThumbnailImageName,
			byte[] pThumbnailImage)
		throws uAALException
	{
		// application images
		String fullImageName = createImage(pFullImageName, pFullImage);
		String thumbnailImageName = createImage(pThumbnailImageName, pThumbnailImage);
		
		CatalogDescriptionType description = 
			CatalogFactory.eINSTANCE.createCatalogDescriptionType();
		description.setKeyword(pKeywords);
		description.setLanguage("-1");
		description.setLongDescription(pLongDescription);
		description.setName(pName);
		description.setShortDescription(pShortDescription);
		description.setFullImage(fullImageName);
		description.setThumbnail(thumbnailImageName);
		
		return description;
	}
	
	/**
	 * Creates a new catalog entry list price object.
	 * 
	 * @param pListPrice 
	 * 		The price of this application (in USD). If not provided then zero.
	 *              
	 * @return 
	 * 		A <code>com.ibm.commerce.foundation.common.datatypes.UnitPriceType</code>
	 * 		holds the given list price
	 */
	private UnitPriceType CreateUnitPrice(String pListPrice)
	{
		UnitPriceType price = CommerceFoundationFactory.eINSTANCE.createUnitPriceType();
		MonetaryAmountType amount = CommerceFoundationFactory.eINSTANCE.createMonetaryAmountType();
		double value = 0;
		if(null != pListPrice && pListPrice.length() > 0)
		{
			try
			{
				value = Double.parseDouble(pListPrice);
			}
			catch(NumberFormatException ex)
			{
				throw new IllegalArgumentException("ERROR: List price [" + pListPrice + "] in illegal");	
			}
		}
		amount.setValue(new BigDecimal(value));
		amount.setCurrency("USD");
		price.setPrice(amount);
		
		return price;
	}
	
	/**
	 * Creates a new catalog entry user data object.
	 * 
	 * @param pUserName 
	 * 		The developer's user name in uStore 
	 * @param pFileType
	 * 		The application's file type (uapp, uaal, zip, ...)
	 * @param pFileName 
	 * 		The name of the application bundle file 
	 * @param pFileInputStream 
	 * 		The input stream of the bundle file (the bundle file will be stored 
	 * 		in the inner Nexus server 
	 * @param pVersion 
	 * 		The internal version of the application in the store 
	 * @param pVersionNotes 
	 * 		Description for this version
	 * @param pRequirements 
	 * 		Describes the general application requirements as free text (i.e. 
	 * 		requirements to the AAL Space) 
	 * @param pLicenses 
	 * 		The application licenses as free text
	 * @param pCapabilities 
	 * 		Describes what the application offers as free text
	 * @param pOrganizationName 
	 * 		The name of the application vendor 
	 * @param pOrganizationURL 
	 * 		The URL to the security certificate 
	 * @param pOrganizationCertificate
	 * 		The organization certificate 
	 * @param pDeveloperName 
	 * 		The name of the developer 
	 * @param pDeveloperEmail 
	 * 		The email address of the developer 
	 * @param pDeveloperPhone 
	 * 		The phone number of the developer
	 * @param pServiceLevelAgreement 
	 * 		The service level agreement for this application
	 * 
	 * @return 
	 * 		A <code>com.ibm.commerce.foundation.common.datatypes.UserDataType</code>
	 * 		holds the given values
	 * 
	 * @throws uAALException in an error has occurred during the process
	 */
	private UserDataType createUserData(
			String pUserName,
			String pFileType,
			String pFileName,
			byte[] pFileInputStream,
			String pVersion,
			String pVersionNotes,
			String pRequirements,
			String pLicenses,
			String pCapabilities,
			String pOrganizationName,
			String pOrganizationURL,
			String pOrganizationCertificate,
			String pDeveloperName,
			String pDeveloperEmail,
			String pDeveloperPhone,
			String pServiceLevelAgreement)
	{
		UserDataType user = CommerceFoundationFactory.eINSTANCE.createUserDataType();
		user.getUserDataField().put("itemtype", "AAL_APPLICATION");
		user.getUserDataField().put("itemusername", pUserName);
		user.getUserDataField().put("versionnotes", pVersionNotes);
		user.getUserDataField().put("requirements", pRequirements);
		user.getUserDataField().put("licenses", pLicenses);
		user.getUserDataField().put("capabilities", pCapabilities);
		user.getUserDataField().put("filename", pFileName);
		user.getUserDataField().put("organizationname", pOrganizationName);
		user.getUserDataField().put("organizationurl", pOrganizationURL);
		user.getUserDataField().put("organizationcertificate", pOrganizationCertificate);
		user.getUserDataField().put("developername", pDeveloperName);
		user.getUserDataField().put("developeremail", pDeveloperEmail);
		user.getUserDataField().put("developerphone", pDeveloperPhone);
		user.getUserDataField().put("appservicelevelagreement", pServiceLevelAgreement);
		user.getUserDataField().put("filetype", pFileType);
		
		try
		{
			pVersion = String.valueOf(Double.parseDouble(pVersion));
		}
		catch(NumberFormatException ex)
		{
			throw new IllegalArgumentException("ERROR: Version [" + pVersion + "] in illegal");
		}
		user.getUserDataField().put("version", pVersion);
		ByteArrayInputStream is = new ByteArrayInputStream(pFileInputStream);
		user.getUserDataField().put("filesize", String.valueOf(is.available()));

		return user;
	}
	
	/**
	 * Creates a new image for the catalog entry.
	 * 
	 * @param pImageFileName 
	 * 		The name of the image 
	 * @param pImage 
	 * 		The image itself as a byte array
	 * 
	 * @return 
	 * 		The final file name that will be used to connect the image to the 
	 * 		catalog entry 
	 * 
	 * @throws uAALException if an erroe has occurred during the process
	 */
	private String createImage(String pImageName, byte[] pImage)
		throws uAALException
	{
		if(null == pImage || pImage.length == 0)
		{
			return "";
		}
		
		try
		{
			String interfaceName = "AttachmentUpload";
			String storeId = CommonUtilities.getInstance().getProperty("storeId", "10101");
		
		    ActivityToken token = new ActivityToken(
		    		new ActivityGUID(new Long(mIdentityTokenID)), mIdentityTokenSignature);
		    CommandContext cmdCtx = ContextHelper.createCommandContext(token);
			cmdCtx.setCommandName(interfaceName);
	
			String parentDirectoryName = "uStore";
			String fileName = String.valueOf(System.currentTimeMillis());
			if(null != pImageName)
			{
				fileName += "-" + pImageName;
			}
			int index = fileName.indexOf(".");
			if(index == -1)
			{
				fileName += ".jpg";
			}
			if(index == fileName.length() -1)
			{
				fileName += "jpg";
			}
			
			TypedProperty requestProperties = new TypedProperty();
			requestProperties.put("storeId", new String[] {storeId}); 
			requestProperties.put("objectStoreId", new String[] {storeId}); 
			requestProperties.put("identitySignature", new String[] {mIdentityTokenSignature}); 
			requestProperties.put("identityId", new String[] {mIdentityTokenID}); 
			requestProperties.put("locale", "en_US"); 
			requestProperties.put("isCMC", "true"); 
			requestProperties.put("operationType", new String[] {"Create"}); 
			requestProperties.put("fileSize", new String[] {String.valueOf(pImage.length)}); 
			requestProperties.put("Filename", new String[] {fileName}); 
			requestProperties.put("sourceFileName", new String[] {fileName}); 
			requestProperties.put("fileName", new String[] {fileName}); 
			requestProperties.put("parentDirectoryName", new String[] {parentDirectoryName}); 
			requestProperties.put("fullPath", "/" + parentDirectoryName + "/" + fileName); 

			UploadedFile uploadedFile = new UploadedFile(
					fileName, 
					"application/octet-stream", 
					new MyStreamHandler(new ByteArrayInputStream(pImage), null, pImage.length), 
					String.valueOf(pImage.length));
			
			uploadedFile.setLength(pImage.length);
			requestProperties.put("UpLoadedFile", uploadedFile); 
		
			cmdCtx.setRequestProperties(requestProperties);
		
			UrlRegistryEntry entry = UrlRegistry.singleton().find(interfaceName, ECConstants.EC_NO_STOREID);
			if(null != entry) 
			{
				interfaceName = entry.getInterfaceName();
			}
			WebAdapterServiceAccessBean service = new WebAdapterServiceAccessBean();
			Map responseMap = service.executeCommand(cmdCtx, requestProperties, interfaceName);

			return fileName;
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to create image (" + ex.getMessage() + ")");
		}
	}
	
	/**
	 * Extract the new catalog entry identifier from the respond.
	 * 
	 * @param pRespond
	 * 		The <code>com.ibm.commerce.catalog.facade.datatypes.AcknowledgeCatalogEntryDataAreaType</code>
	 * 		returned from the request to create a new catalog entry
	 * 
	 * @return
	 * 		The catalog entry identifier of this new entry, or NULL if such was 
	 * 		not found in the respond
	 */
	private String getCatalogEntryIdFromRespond(AcknowledgeCatalogEntryDataAreaType pRespond)
	{
		Iterator iter = pRespond.getCatalogEntry().iterator();
		if(iter.hasNext())
		{
			CatalogEntryType entry = (CatalogEntryType)iter.next();
			return entry.getCatalogEntryIdentifier().getUniqueID();
		}
		return null;
	}
	
	/**
	 * Extract the client errors from the respond.
	 * 
	 * @param pRespond
	 * 		The <code>com.ibm.commerce.catalog.facade.datatypes.AcknowledgeCatalogEntryDataAreaType</code>
	 * 		returned from the request to create a new catalog entry
	 * 
	 * @return 
	 * 		A <code>java.util.List</code> with all the 
	 * 		<code>com.ibm.commerce.foundation.client.facade.bod.ClientError</code>s
	 * 		stored in the respond  
	 */
	private List getClientErrors(AcknowledgeCatalogEntryDataAreaType pRespond)
	{
		List clientErrors = new ArrayList();
		Iterator iter = pRespond.getAcknowledge().getResponseCriteria().iterator();
		while(iter.hasNext())
		{
			ResponseActionCriteriaType type =(ResponseActionCriteriaType)iter.next(); 
			System.out.println("ERROR: Failed to create a new application [" + type.getChangeStatus().getReasonCode().getValue() + 
					"] " + type.getChangeStatus().getDescription().getValue());
			ClientError error = new ClientError();
			error.setErrorKey(type.getChangeStatus().getReasonCode().getValue());
			error.setLocalizedMessage(type.getChangeStatus().getDescription().getValue());
			clientErrors.add(error);
		}
		return clientErrors;
	}

	/**
	 * This inner class is being used by the <code>com.ibm.commerce.server.UploadedFile</code>
	 * to retrieve the image file.
	 * 
	 * @author roni
	 */
	class MyStreamHandler 
		extends MultipartInputStreamHandler
	{
		/**
		 * The image file input stream
		 */
		private InputStream mInput = null;
		
		/**
		 * The boundary of the image file
		 */
		private String mBoundary = null;

		/**
		 * The number of total bytes that are expected to be read
		 */
		private int mTotalExpected = -1;
		
		/**
		 * The number of total bytes read
		 */
		private int mTotalRead = 0;
		
		/**
		 * The buffer that is being used to read the image file
		 */
		private byte[] mBuffer = new byte[8192];

		/**
		 * Constructor.
		 * 
		 * @param pInput
		 * 		The image file input stream
		 * @param pBoundary
		 * 		The boundary of the image file
		 * @param pTotalExpected
		 * 		The number of total bytes that are expected to be read
		 */
		public MyStreamHandler(InputStream pInput, String pBoundary, int pTotalExpected)
		{
			super(null, pBoundary, pTotalExpected);
			
			mInput = pInput;
			mBoundary = pBoundary;
			mTotalExpected = pTotalExpected;
		}

		/**
		 * Reads a single line from the file.
		 * 
		 * @return 
		 * 		A single line from the file
		 * 
		 * @throws IOException in an error has occured during the reading 
		 * 		process
		 */
		public String readLine()
	    	throws IOException
	    {
			return readLine("UTF-8");
	    }

		/**
		 * Reads a single line from the file.
		 * 
		 * @param pEncoding
		 * 		The encoding to be used during the reading process
		 * 
		 * @return 
		 * 		A single line from the file
		 * 
		 * @throws IOException in an error has occured during the reading 
		 * 		process
		 */
		public String readLine(String pEncoding) 
			throws IOException 
		{
		    StringBuffer sbuf = new StringBuffer();

		    String encodedString = null;
		    int result;
		    do 
		    {
		    	result = readLine(mBuffer, 0, mBuffer.length);
		    	if(result != -1)
		    	{
		    		encodedString = new String(mBuffer, 0, result, pEncoding);
		    		sbuf.append(encodedString);
		    	}
		    	mBuffer = new byte[8192];
		    }
		    while(result == mBuffer.length);

		    if(sbuf.length() == 0)
		    {
		    	return null;
		    }
		    
		    sbuf.setLength(sbuf.length() - 2);
		    return sbuf.toString();
		}
	
		/**
		 * @return The number of bytes that are still reamining to be read
		 */
		public int getRemainingLength()
		{
			return mTotalExpected - mTotalRead;
		}

		/**
		 * Reads a single line from the file.
		 * 
		 * @param b 
		 * 		The buffer to write to it the line
		 * @param off
		 * 		The offest in the buffer to start the writing
		 * @param len
		 * 		The number of bytes to read
		 * 
		 * @return	
		 * 		The number of the actual  bytes that were read
		 * 
		 * @throws IOException in an error has occured during the reading 
		 * 		process
		 */
		public int readLine(byte[] b, int off, int len)
		    throws IOException
		{
			if(mTotalRead >= mTotalExpected) 
			{
				return -1;
		    }
			
			int counter = 0;
		    int i;
		    while(0 <= (i = mInput.read()) && counter < len) 
		    {
		    	b[off+counter] = (byte)i;
	    		counter ++;
	    		if(i == '\r' || i == '\n') 
	    		{
	    			break;
	    		}
		    }
		    if(counter > 0)
		    {
		    	mTotalRead += counter;
		    	return counter;
		    }
		    return -1;
		}
	}
}
