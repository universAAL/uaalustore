/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.tools;

import java.util.Map;

import javax.security.auth.callback.CallbackHandler;

import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.user.UserSessionInfo;
import org.universaal.commerce.ustore.utilities.CommonUtilities;

import com.ibm.commerce.catalog.facade.client.CatalogEntryFacadeClient;
import com.ibm.commerce.catalog.facade.client.CatalogFacadeClient;
import com.ibm.commerce.content.facade.client.AttachmentFacadeClient;
import com.ibm.commerce.content.facade.client.ManagedFileFacadeClient;
import com.ibm.commerce.foundation.common.datatypes.BusinessContextType;

/**
 * This class initializes the facades that are needed for accessing the data 
 * that is stored in uStore. 
 * 
 * @author roni
 */
public abstract class AbstractManager 
{
	/**
	 * The business context to associated with the request
	 */
	protected BusinessContextType mBusinessContext = null;
	
	/**
	 * The security callback handler that is passed to the invocation service
	 */
	protected CallbackHandler mCallbackHandler = null;
	
	/**
	 * Provides common methods to the catalog
	 */
	protected CatalogFacadeClient mCatalogClient = null;
	
	/**
	 * Provides common methods to the base catalog entry
	 */
	protected CatalogEntryFacadeClient mCatalogEntryClient = null;

	/**
	 * Provides common methods to create managed files
	 */
	protected ManagedFileFacadeClient mManagedFileFacadeClient = null;

	/**
	 * Provides common methods to create attachments
	 */
	protected AttachmentFacadeClient mAttachmentFacadeClient = null;
	
	/**
	 * The logged in user identifier
	 */
	protected String mUserId = null;
	
	/**
	 * The identity token identifier of the logged in user
	 */
	protected String mIdentityTokenID = null;

	/**
	 * The identity token signature of the logged in user
	 */
	protected String mIdentityTokenSignature = null;

	/**
	 * Initializes the facades. 
	 *   
	 * @param pUserName 
	 * 		The name of the user who invokes this call
	 * @param pPassword 
	 * 		The password of the user who invokes this call
	 * 
	 * @throws uAALException if an error has occurred during the initialization 
	 * 		process
	 * 		
	 */
	protected void init(String pUserName, String pPassword)
		throws uAALException
	{
		// get the business context
		mBusinessContext = CommonUtilities.getInstance().getBusinessContext();
		
		// authenticate the user
		Map userParams = CommonUtilities.getInstance().authenticate(pUserName, pPassword);
		mUserId = ((String[])userParams.get("userId"))[0];
		String[] identityTokenID = (String[])userParams.get("identityTokenID");
		String[] identityTokenSignature = (String[])userParams.get("identityTokenSignature");
		
		mIdentityTokenID = identityTokenID[0];
		mIdentityTokenSignature = identityTokenSignature[0];
		
		// get the callback handler
		mCallbackHandler = CommonUtilities.getInstance().getCallbackHandler(userParams);
	
		UserSessionInfo info = new UserSessionInfo(mUserId, pUserName, pPassword, mIdentityTokenSignature, mCallbackHandler);
		UserSessionHandler.addUserInfoToSession(info);
		
		// create the CatalogEntryFacadeClient client 
		// this client is used to send requests and receive responses from the server
		mCatalogEntryClient = new CatalogEntryFacadeClient(mBusinessContext, mCallbackHandler);

		// create the CatalogFacadeClient client
		// this client is used to send requests and receive responses from the server
		mCatalogClient = new CatalogFacadeClient(mBusinessContext, mCallbackHandler);
		
		// Create the AttachmentFacadeClient client
		mManagedFileFacadeClient = new ManagedFileFacadeClient(mBusinessContext, mCallbackHandler);
		mAttachmentFacadeClient = new AttachmentFacadeClient(mBusinessContext, mCallbackHandler);
	}
}
