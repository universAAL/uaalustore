/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.tools;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.utilities.CatalogUtilities;
import org.universaal.commerce.ustore.utilities.CommonUtilities;

import com.ibm.commerce.catalog.facade.datatypes.CatalogDescriptionType;
import com.ibm.commerce.catalog.facade.datatypes.CatalogGroupType;
import com.ibm.commerce.foundation.common.datatypes.CatalogGroupIdentifierType;

/**
 * This class provides methods for accessing the catalog structure of uStore.
 * 
 * @author roni
 */
@WebService
public class CatalogManager extends AbstractManager
	implements ICatalogManager
{
	/**
	 * Returns the categories under the 'AAL Applications' category from the 
	 * uStore catalog.
	 * 
	 * @param pUserName 
	 * 		The name of the user who invokes this call
	 * @param pPassword 
	 * 		The password of the user who invokes this call
	 * 
	 * @return The categories under the 'AAL Applications' category organized in
	 * the following template:
	 * <categories>
	 * 		<category><id>category-id</id><name>category-name</name></category>
	 * 		<category><id>category-id</id><name>category-name</name></category>
	 * 		...
	 * </categories>
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL 
	 * 		or the AAL Applications category id was not set in the properties 
	 * 		file
	 */
	@WebMethod
	public String getAALApplicationsCategories(
			@WebParam(name = "username") String pUserName, 
			@WebParam(name = "password") String pPassword)
		throws uAALException
	{
		// check the required parameters
		if(null == pUserName || null == pPassword)
		{
			throw new IllegalArgumentException("ERROR: An input parameter in NULL");
		}

		init(pUserName, pPassword);

		String categoryId = CommonUtilities.getInstance().getProperty("AALApplicationsCategoryId", "11001");
		if(null == categoryId)
		{
			throw new IllegalArgumentException(
					"ERROR: The AAL Applications category id was not set in the properties file");
		}
		
		System.out.println("-->> categoryId = " + categoryId);
		Map categories = getCategories(categoryId);

		// build XML from the Map
		String result = "<categories>";
		Iterator categoriesIter = categories.keySet().iterator();
		while(categoriesIter.hasNext())
		{
			String id = (String)categoriesIter.next();
			String name = (String)categories.get(id);
			result += "<category>";
			result += "<id>" + id + "</id>";
			result += "<name>" + name + "</name>";
			result += "</category>";
		}			
		result += "</categories>";
		return result;
	}
	
	/**
	 * Returns the categories under the parent category from the uStore catalog.
	 * 
	 * @param pParentCategoryId
	 * 		The parent category identifier in the uStore catalog
	 * 
	 * @return 
	 * 		A <code>java.util.Map</code> with the categories under the parent
	 * 		category from the uStore catalog
	 * 
	 * @throws <code>uAALException</code> if there was an error when performing
	 * 		the operation
	 */
	private Map getCategories(String pParentCategoryId) 
		throws uAALException
	{
		CatalogGroupType[] catalogGroups = CatalogUtilities.getCatalogGroupsByParentCatalogGroupId(
				pParentCategoryId, 
				CommonUtilities.getInstance().getProperty("storeId", "10101"),
				null, 
				mBusinessContext, 
				mCallbackHandler);
		
		System.out.println("-->> catalogGroups.size() = " + catalogGroups.length);
		
		Map categories = new HashMap();
		for(int i=0; i<catalogGroups.length; i++)
		{
			CatalogGroupType catalogGroup = catalogGroups[i];
			CatalogGroupIdentifierType identifier = catalogGroup.getCatalogGroupIdentifier();
			Iterator iter = catalogGroup.getDescription().iterator();
			if(iter.hasNext())
			{
				CatalogDescriptionType description = (CatalogDescriptionType)iter.next();
				System.out.println("-->> uniqueID = " + identifier.getUniqueID() + " name = " + description.getName());
				categories.put(identifier.getUniqueID(), description.getName());
			}
		}
		return categories;
	}
}
