/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.tools;

import org.universaal.commerce.ustore.uAALException;

/**
 * This class provides methods for managing the AAL applications that are stored
 * in the uStore catalog.
 * 
 * @author roni
 */
public interface IAALApplicationManager
{
	/**
	 * Uploads an AAL Application of any type (except of uapp files) into 
	 * uStore catalog. If the identifier of the application is provided then 
	 * store this application as a new version and update its fields. Otherwise, 
	 * store this application as a new catalog entry.
	 * - Check the required fields
	 * - In case this is a new application then store the username that creates 
	 *   it in the CATENTRY table in 'FIELD5', otherwise verify that the user 
	 *   who updates the application is the one that created it, otherwise throw 
	 *   an exception
	 * - Check that the version field is a valid double and in case of an 
	 *   update, check that it was incremented
	 * - If the application has already been included in a service and it is 
	 *   being marked as 'not for purchase' then all the services that include 
	 *   this application are also marked as 'not for purchase'
	 * - All the fields that describe this application can be updated even if 
	 * 	 this application is already included in a service. The service provider 
	 *   is responsible to manage this information and to take into 
	 *   consideration the status of the application 
	 * For a new application, first save the application in the catalog and 
	 * then store the file in Nexus by using the application id and version. 
	 * If a failure occurs then remove it from the catalog.
	 * For an update of existing application, add the application file to the 
	 * Nexus server (under the same application id and a new version). If the 
	 * operation ended successfully then update the application parameters. 
	 *  
	 * @param pUserName 
	 * 		The developer's user name in uStore (required)
	 * @param pPassword 
	 * 		The developer's password in uStore (required)
	 * @param pApplicationId 
	 * 		The identifier of the application (if null a new catalog entry will 
	 * 		be created) 
	 * @param pName 
	 * 		The application's name (required)
	 * @param pShortDescription 
	 * 		A short description for the application 
	 * @param pLongDescription 
	 * 		A long description for the application
	 * @param pKeywords 
	 * 		Keywords separated by a comma (used by the search tool)
	 * @param pDeveloperName 
	 * 		The name of the developer 
	 * @param pDeveloperEmail 
	 * 		The email address of the developer 
	 * @param pDeveloperPhone 
	 * 		The phone number of the developer
	 * @param pOrganizationName 
	 * 		The name of the application vendor 
	 * @param pOrganizationURL 
	 * 		The URL to the security certificate (optional) 
	 * @param pOrganizationCertificate
	 * 		The organization certificate (optional)
	 * @param pURL 
	 * 		The URL that describes this application (provides additional 
	 * 		information)
	 * @param pParentCategoryId 
	 * 		The category that this application belongs to (required)       
	 * @param pFullImageFileName 
	 * 		The name of the full image that will be displayed for this 
	 * 		application       
	 * @param pFullImage 
	 * 		The full image that will be displayed for this application
	 * @param pThumbnailImageFileName 
	 * 		The name of the thumbnail image that will be displayed for this 
	 * 		application
	 * @param pThumbnailImage 
	 * 		The thumbnail image that will be displayed for this application
	 * @param pListPrice 
	 * 		The price of this application (in USD). If not provided then zero.             
	 * @param pVersion 
	 * 		The internal version of the application in the store (required)
	 * @param pVersionNotes 
	 * 		Description for this version (required)
	 * @param pFileName 
	 * 		The name of the application bundle file (required)
	 * @param pFileInputStream 
	 * 		The input stream of the bundle file (the bundle file will be stored 
	 * 		in the inner Nexus server (required)
	 * @param pServiceLevelAgreement 
	 * 		The service level agreement for this application
	 * @param pRequirements 
	 * 		Describes the general application requirements as free text (i.e. 
	 * 		requirements to the AAL Space) 
	 * @param pLicenses 
	 * 		The application licenses as free text
	 * @param pCapabilities 
	 * 		Describes what the application offers as free text
	 * @param pIsForPurchase 
	 * 		Indicates whether this application is ready for purchase
	 *  
	 * @return 
	 * 		The identifier of the application in the catalog. In case of an 
	 * 		update the given application identifier will be returned. 
	 *  
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * @throws <code>uAALException</code> if an error happens while performing a
	 * 		process operation on a single CatalogEntry 
	 */
	public String uploadAnyAALApplication(
			String pUserName,
			String pPassword,
			String pApplicationId,
			String pName,
			String pShortDescription,
			String pLongDescription,
			String pKeywords,
			String pDeveloperName,
			String pDeveloperEmail,
			String pDeveloperPhone,
			String pOrganizationName,
			String pOrganizationURL,
			String pOrganizationCertificate,
			String pURL,
			String pParentCategoryId,
			String pFullImageFileName,
			byte[] pFullImage,
			String pThumbnailImageFileName,
			byte[] pThumbnailImage,
			String pListPrice,
			String pVersion,
			String pVersionNotes,
			String pFileName,
			byte[] pFileInputStream,
			String pServiceLevelAgreement,
			String pRequirements,
			String pLicenses,
			String pCapabilities,
			boolean pIsForPurchase)
		throws uAALException;
	
	/**
	 * Uploads an AAL Application of a uapp type into uStore catalog. If the 
	 * identifier of the application is provided then store this application as 
	 * a new version and update its fields. Otherwise, store this application as
	 * a new catalog entry.
	 * - Check the required fields
	 * - In case this is a new application then store the username that creates 
	 *   it in the CATENTRY table in 'FIELD5', otherwise verify that the user 
	 *   who updates the application is the one that created it, otherwise throw 
	 *   an exception
	 * - Check that the version field is a valid double and in case of an 
	 *   update, check that it was incremented
	 * - If the application has already been included in a service and it is 
	 *   being marked as 'not for purchase' then all the services that include 
	 *   this application are also marked as 'not for purchase'
	 * - All the fields that describe this application can be updated even if 
	 * 	 this application is already included in a service. The service provider 
	 *   is responsible to manage this information and to take into 
	 *   consideration the status of the application 
	 * The parameters that describe the application such as name, description, 
	 * keywords, licenses, capabilities, requirements, etc, are being extracted
	 * from the uapp file and stored in the local DB.
	 * For a new application, first save the application in the catalog and 
	 * then store the file in Nexus by using the application id and version. 
	 * If a failure occurs then remove it from the catalog.
	 * For an update of existing application, add the application file to the 
	 * Nexus server (under the same application id and a new version). If the 
	 * operation ended successfully then update the application parameters.
	 *  
	 * @param pUserName 
	 * 		The developer's user name in uStore (required)
	 * @param pPassword 
	 * 		The developer's password in uStore (required)
	 * @param pApplicationId 
	 * 		The identifier of the application (if null a new catalog entry will 
	 * 		be created) 
	 * @param pLongDescription 
	 * 		A long description for the application
	 * @param pURL 
	 * 		The URL that describes this application (provides additional 
	 * 		information)
	 * @param pParentCategoryId 
	 * 		The category that this application belongs to (required)       
	 * @param pFullImageFileName 
	 * 		The name of the full image that will be displayed for this 
	 * 		application       
	 * @param pFullImage 
	 * 		The full image that will be displayed for this application
	 * @param pThumbnailImageFileName 
	 * 		The name of the thumbnail image that will be displayed for this 
	 * 		application
	 * @param pThumbnailImage 
	 * 		The thumbnail image that will be displayed for this application
	 * @param pListPrice 
	 * 		The price of this application (in USD). If not provided then zero             
	 * @param pVersion 
	 * 		The internal version of the application in the store (required)
	 * @param pVersionNotes 
	 * 		Description for this version (required)
	 * @param pFileName 
	 * 		The name of the application bundle file (required)
	 * @param pFileInputStream 
	 * 		The input stream of the bundle file (the bundle file will be stored 
	 * 		in the inner Nexus server (required)
	 * @param pIsForPurchase 
	 * 		Indicates whether this application is ready for purchase
	 *  
	 * @return 
	 * 		The identifier of the application in the catalog. In case of an 
	 * 		update the given application identifier will be returned 
	 *  
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * @throws <code>uAALException</code> if an error happens while performing a
	 * 		process operation on a single CatalogEntry 
	 */
	public String uploadUaapAALApplication(
			String pUserName,
			String pPassword,
			String pApplicationId,
			String pLongDescription,
			String pURL,
			String pParentCategoryId,
			String pFullImageFileName,
			byte[] pFullImage,
			String pThumbnailImageFileName,
			byte[] pThumbnailImage,
			String pListPrice,
			String pVersion,
			String pVersionNotes,
			String pFileName,
			byte[] pFileInputStream,
			boolean pIsForPurchase)
		throws uAALException;
	
	/**
	 * Deletes an application from the uStore catalog.
	 * NOTE: The application is not deleted from the catalog, it is marked as 
	 * deleted.
	 * - Verify that the user who deletes the application is the one that 
	 *   created it, otherwise throw an exception
	 * - If the application has already been included in a service then it can't 
	 *   be deleted
	 * - The file itself is being deleted from the Nexus server 
	 * 
	 * @param pUserName 
	 * 		The developer's user name in uStore (required)
	 * @param pPassword 
	 * 		The developer's password in uStore (required)
	 * @param pApplicationId 
	 * 		The identifier of the application to be deleted from the catalog 
	 * 		(required)
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		delete process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 */
	public void deleteAALApplication(
			String pUserName,
			String pPassword,
			String pApplicationId)
		throws uAALException;
	
	/**
	 * Returns the applications that are associated with the given user and 
	 * located in the given category in the uStore catalog.
	 * 
	 * @param pUserName 
	 * 		The name of the user (required)
	 * @param pPassword 
	 * 		The password of the user (required)
	 * @param pParentCategoryId 
	 * 		The category to look for. If null then the search will be done on 
	 * 		the full catalog      
	 *  
	 * @return 
	 * 		The applications that were found organized in the following 
	 * 		template:
	 * 		<applications>
	 * 			<application><id>application-id</id><name>application-name</name></application>
	 * 			<application><id>application-id</id><name>application-name</name></application>
	 * 			...
	 * 		</applications>
	 *  
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 */
	public String getAALApplications(
			String pUserName, 
			String pPassword,
			String pParentCategoryId)
		throws uAALException;
}
