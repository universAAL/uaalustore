/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.tools;

import org.universaal.commerce.ustore.uAALException;


/**
 * This class provides methods for accessing the catalog structure of uStore.
 * 
 * @author roni
 */
public interface ICatalogManager  
{
	/**
	 * Returns the categories under the 'AAL Applications' category from the 
	 * uStore catalog.
	 * 
	 * @param pUserName 
	 * 		The name of the user who invokes this call
	 * @param pPassword 
	 * 		The password of the user who invokes this call
	 * 
	 * @return The categories under the 'AAL Applications' category organized in
	 * the following template:
	 * <categories>
	 * 		<category><id>category-id</id><name>category-name</name></category>
	 * 		<category><id>category-id</id><name>category-name</name></category>
	 * 		...
	 * </categories>
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL 
	 * 		or the AAL Applications category id was not set in the properties 
	 * 		file
	 */
	public String getAALApplicationsCategories(String pUserName, String pPassword)
		throws uAALException;
}
