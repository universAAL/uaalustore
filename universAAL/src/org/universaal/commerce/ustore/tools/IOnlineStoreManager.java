/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.tools;

import org.universaal.commerce.ustore.uAALException;

/**
 * This interface provides methods for any deployment manager that communicates 
 * with uStore.
 * 
 * @author roni
 */
public interface IOnlineStoreManager 
{
	/**
	 * Returns the session key of the given user for further communication with
	 * the online store manager.
	 *  
	 * @param pUserName 
	 * 		The name of the user in uStore
	 * @param pPassword 
	 * 		The password of the user in uStore
	 * 
	 * @return 
	 * 		The sessionKey for further interaction between uStore and the 
	 * 		deployment manager
	 *  
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	public String getSessionKey(String pUserName, String pPassword)
		throws uAALException;
	
	/**
	 * Registers the deployment manager that associates with the given 
	 * <IP-address, port> to the given user. 
	 * 
	 * @param pSessionKey 
	 * 		The user session key 
	 * @param pAdminUserName 
	 * 		The name of the AAL space administrator
	 * @param pAdminPassword 
	 * 		The password of the AAL space administrator
	 * @param pIpAddress 
	 * 		The IP address of the local deployment manager
	 * @param pPort 
	 * 		The port of the local deployment manager
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	public void registerDeployManager(
			String pSessionKey,
			String pAdminUserName,
			String pAdminPassword,
			String pIpAddress,
			String pPort)
		throws uAALException;

	/**
	 * Returns the user profile in uStore.
	 * 
	 * @param pSessionKey 
	 * 		The user session key 
	 * 
	 * @return 
	 * 		The user profile in the following template: <TO BE DEFINED>
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	public String getUserProfile(String pSessionKey)
		throws uAALException;
	
	/**
	 * Returns the free AAL services that are available in the uStore catalog 
	 * that have not been purchased yet by the user.
	 * The given boolean value specifies if only those services that fit the 
	 * target user and his space will be returned.
	 * 
	 * @param pSessionKey 
	 * 		The user session key  
	 * @param pIsFitToUser 
	 * 		If the value is <code>True</code> then only free AAL services that 
	 * 		fit the user and his space will be returned. Otherwise, all the free 
	 * 		AAL services that are available in the uStore catalog and have not 
	 * 		been purchased by the user will be returned
	 * 
	 * @return 
	 * 		The free AAL services in the following template:
	 * 		<services>
	 * 			<service><id>service-id</id><name>service-name</name></service>
	 * 			<service><id>service-id</id><name>service-name</name></service>
	 * 			...
	 * 		</services>
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	public String getFreeAALServices(String pSessionKey, boolean pIsFitToUser)
		throws uAALException;

	/**
	 * Purchases a free AAL service in uStore for a given user.
	 * 
	 * @param pSessionKey 
	 * 		The user session key  
	 * @param pServiceId 
	 * 		The identifier of the service to be purchased
	 * 
	 * @return 
	 * 		A link to retrieve the usrv file for the installation by the 
	 * 		deployment manager
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	public String purchaseFreeAALService(String pSessionKey, String pServiceId)
		throws uAALException;
	
	/**
	 * Returns all the AAL services that have been purchased by the user.
	 * 
	 * @param pSessionKey 
	 * 		The user session key  
	 * 
	 * @return 
	 * 		The AAL services in the following template:
	 * 		<services>
	 * 			<service>
	 * 				<name>service-name</name>
	 * 				<link>link to retrieve the .usrv file</link>
	 * 			</service>
	 * 			<service>
	 * 				<name>service-name</name>
	 * 				<link>link to retrieve the .usrv file</link>
	 * 			</service>
	 * 			...
	 * 		</services>
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	public String getPurchasedAALServices(String pSessionKey)
		throws uAALException;
	
	/**
	 * Returns all the AAL services that have been purchased by the user and an
	 * update exists for them in the uStore catalog.
	 * 
	 * @param pSessionKey 
	 * 		The user session key  
	 * 
	 * @return 
	 * 		The AAL services in the following template:
	 * 		<services>
	 * 			<service>
	 * 				<name>service-name</name>
	 * 				<link>link to retrieve the .usrv file</link>
	 * 			</service>
	 * 			<service>
	 * 				<name>service-name</name>
	 * 				<link>link to retrieve the .usrv file</link>
	 * 			</service>
	 * 			...
	 * 		</services>
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	public String getUpdatesForAALServices(String pSessionKey)
		throws uAALException;
}
