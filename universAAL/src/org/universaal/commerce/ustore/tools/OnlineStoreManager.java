/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.tools;

import java.text.MessageFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.security.auth.callback.CallbackHandler;

import org.universaal.commerce.ustore.uAALException;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.user.UserSessionInfo;
import org.universaal.commerce.ustore.utilities.CommonUtilities;

import com.ibm.commerce.catalog.facade.client.CatalogEntryException;
import com.ibm.commerce.catalog.facade.client.CatalogEntryFacadeClient;
import com.ibm.commerce.catalog.facade.client.CatalogFacadeClient;
import com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType;
import com.ibm.commerce.foundation.client.util.oagis.SelectionCriteriaHelper;
import com.ibm.commerce.oagis9.datatypes.ExpressionType;
import com.ibm.commerce.oagis9.datatypes.GetType;
import com.ibm.commerce.user.objects.UserAccessBean;

/**
 * This class provides methods for any deployment manager that communicates 
 * with uStore.
 * 
 * @author sivan
 * @author roni
 */
@WebService
public class OnlineStoreManager 
	implements IOnlineStoreManager
{
	/**
	 * Returns the session key of the given user for further communication with
	 * the online store manager.
	 *  
	 * @param pUserName 
	 * 		The name of the user in uStore
	 * @param pPassword 
	 * 		The password of the user in uStore
	 * 
	 * @return 
	 * 		The sessionKey for further interaction between uStore and the 
	 * 		deployment manager
	 *  
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	@WebMethod
	public String getSessionKey(
			@WebParam(name = "userName") String pUserName, 
			@WebParam(name = "password") String pPassword)
		throws uAALException
	{
		System.out.println("-->> OnlineStoreManager:getSessionKey() pUserName = " + pUserName + 
				" pPassword = " + pPassword);
		// check the required parameters
		if(null == pUserName || null == pPassword)
		{
			throw new IllegalArgumentException("ERROR: An input parameter in NULL");
		}
		
		UserSessionInfo info = UserSessionHandler.getUserSessionInfoByUserName(pUserName);
		if(null == info) // user session info was not found 
		{
			// authenticate the user
			Map userParams = CommonUtilities.getInstance().authenticate(pUserName, pPassword);
			String[] strUserId = (String[])userParams.get("userId");
			String[] identityTokenID = (String[])userParams.get("identityTokenID");
			String[] identityTokenSignature = (String[])userParams.get("identityTokenSignature");

			// get the callback handler
			CallbackHandler callbackHandler = CommonUtilities.getInstance().getCallbackHandler(userParams);

			info = new UserSessionInfo(strUserId[0], pUserName, pPassword, identityTokenSignature[0], callbackHandler);
			UserSessionHandler.addUserInfoToSession(info);
			return info.getSessionId();
		}
		
		// user session info was found 
		if(!pPassword.equals(info.getPassword()))
		{
			throw new uAALException("ERROR: Failure (password in invalid)");
		}
		return info.getSessionId();
	}
	
	/**
	 * Registers the deployment manager that associates with the given 
	 * <IP-address, port> to the given user. 
	 * 
	 * @param pSessionKey 
	 * 		The user session key 
	 * @param pAdminUserName 
	 * 		The name of the AAL space administrator
	 * @param pAdminPassword 
	 * 		The password of the AAL space administrator
	 * @param pIpAddress 
	 * 		The IP address of the local deployment manager
	 * @param pPort 
	 * 		The port of the local deployment manager
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	@WebMethod
	public void registerDeployManager(
			@WebParam(name = "sessionKey") String pSessionKey, 
			@WebParam(name = "adminUserName") String pAdminUserName, 
			@WebParam(name = "adminPassword") String pAdminPassword,
			@WebParam(name = "ipAddress") String pIpAddress,
			@WebParam(name = "port") String pPort)
		throws uAALException
	{
		System.out.println("-->> OnlineStoreManager:registerDeployManager() pIpAddress = " + pIpAddress + 
				" pPort = " + pPort);

		// check the required parameters
		if(null == pSessionKey || null == pIpAddress || null == pPort)
		{
			throw new IllegalArgumentException("ERROR: An input parameter in NULL");
		}
		
		UserSessionInfo info = UserSessionHandler.getUserSessionInfoBySessionKey(pSessionKey);
		if(null == info) // user session info was not found 
		{
			throw new uAALException("ERROR: Failure (session key in invalid)");
		}
		
		// user session info was found 
		try
		{
			UserAccessBean bean = new UserAccessBean();
			bean.setInitKey_MemberId(info.getUserId());
			bean.refreshCopyHelper();
			bean.setUserField2(pIpAddress + ":" + pPort);
			StringBuffer buffer = new StringBuffer();
			if(null != pAdminUserName)
			{
				buffer.append("<adminUserName>" + pAdminUserName + "</adminUserName>");
			}
			if(null != pAdminPassword)
			{
				buffer.append("<adminPassword>" + pAdminPassword + "</adminPassword>");
			}
			bean.setUserField3(buffer.toString());
			bean.commitCopyHelper();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to register deploy manager (" + ex.getMessage() + ")");
		}
	}

	/**
	 * Returns the user profile in uStore.
	 * This method was not implemented.
	 * 
	 * @param pSessionKey 
	 * 		The user session key 
	 * 
	 * @return 
	 * 		The user profile in the following template: <TO BE DEFINED>
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	@WebMethod
	public String getUserProfile(
			@WebParam(name = "sessionKey") String pSessionKey)
		throws uAALException
	{
		System.out.println("-->> OnlineStoreManager:getUserProfile() pSessionKey = " + pSessionKey);

		return null;
	}	

	/**
	 * Returns the free AAL services that are available in the uStore catalog 
	 * that have not been purchased yet by the user.
	 * The given boolean value specifies if only those services that fit the 
	 * target user and his space will be returned.
	 * This method was not implemented.
	 * 
	 * @param pSessionKey 
	 * 		The user session key  
	 * @param pIsFitToUser 
	 * 		If the value is <code>True</code> then only free AAL services that 
	 * 		fit the user and his space will be returned. Otherwise, all the free 
	 * 		AAL services that are available in the uStore catalog and have not 
	 * 		been purchased by the user will be returned
	 * 
	 * @return 
	 * 		The free AAL services in the following template:
	 * 		<services>
	 * 			<service><id>service-id</id><name>service-name</name></service>
	 * 			<service><id>service-id</id><name>service-name</name></service>
	 * 			...
	 * 		</services>
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	@WebMethod
	public String getFreeAALServices(
			@WebParam(name = "sessionKey") String pSessionKey,
			@WebParam(name = "isFitToUser") boolean pIsFitToUser)
		throws uAALException
	{
		System.out.println("-->> OnlineStoreManager:getFreeAALServices() pSessionKey = " + pSessionKey);

		return null;

		/*
		String xpath = "/CatalogEntry[FreeServices]";

		String accessProfile = "Universaal_All";
		Object[] idArray = {new String()};
		SelectionCriteriaHelper helper = new SelectionCriteriaHelper(MessageFormat.format(xpath, idArray));
		helper.addAccessProfile(accessProfile);
		ExpressionType getExpression = helper.getSelectionCriteriaExpression();
		GetType get = CatalogEntryFacadeClient.createGetVerb(getExpression);

		try 
		{
			// create the CatalogFacadeClient client
			// this client is used to send requests and receive responses from the server
			CatalogFacadeClient catalogClient = new CatalogFacadeClient(
					CommonUtilities.getInstance().getBusinessContext(), pUserInfo.getCallbackHandler());
			// get the list of free services 
			List catalogEntries = catalogClient.getCatalogEntry(get).getCatalogEntry();
			System.out.println("Number of entries with price 0 found: " + catalogEntries.size());
			HashSet<String> entryIDs = new HashSet<String>();
			for (Object object : catalogEntries) 
			{
				CatalogEntryType resultEntry = (CatalogEntryType) object;
				String uniqueID = resultEntry.getCatalogEntryIdentifier().getUniqueID();
				entryIDs.add(uniqueID);
			}			

			String result = getServicesXml(userAccessManager, entryIDs);
			return result;
		} 
		catch(Exception ex) 
		{
			throw new uAALException("[ERROR] Failed to retrieve services with price 0 " + ex.getMessage());
		}
		*/
	}

	/**
	 * Purchases a free AAL service in uStore for a given user.
	 * This method was not implemented.
	 * 
	 * @param pSessionKey 
	 * 		The user session key  
	 * @param pServiceId 
	 * 		The identifier of the service to be purchased
	 * 
	 * @return 
	 * 		A link to retrieve the usrv file for the installation by the 
	 * 		deployment manager
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	@WebMethod
	public String purchaseFreeAALService(
			@WebParam(name = "sessionKey") String pSessionKey, 
			@WebParam(name = "serviceId") String pServiceId)
		throws uAALException
	{
		System.out.println("-->> OnlineStoreManager:purchaseFreeAALService() pSessionKey = " + pSessionKey);

		return null;
	}
	
	/**
	 * Returns all the AAL services that have been purchased by the user.
	 * 
	 * @param pSessionKey 
	 * 		The user session key  
	 * 
	 * @return 
	 * 		The AAL services in the following template:
	 * 		<services>
	 * 			<service>
	 * 				<name>service-name</name>
	 * 				<link>link to retrieve the .usrv file</link>
	 * 			</service>
	 * 			<service>
	 * 				<name>service-name</name>
	 * 				<link>link to retrieve the .usrv file</link>
	 * 			</service>
	 * 			...
	 * 		</services>
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	@WebMethod
	public String getPurchasedAALServices(
			@WebParam(name = "sessionKey") String pSessionKey)
		throws uAALException
	{
		System.out.println("-->> OnlineStoreManager:getPurchasedAALServices() pSessionKey = " + pSessionKey);

		// check the required parameters
		if(null == pSessionKey)
		{
			throw new IllegalArgumentException("ERROR: An input parameter in NULL");
		}

		UserSessionInfo info = UserSessionHandler.getUserSessionInfoBySessionKey(pSessionKey);
		if(null == info) // user session info was not found 
		{
			throw new uAALException("ERROR: Failure (session key in invalid)");
		}
		
		// user session info was found 
		HashSet<String> entryIDs = getPurchasedAALServices(info);
		try 
		{
			String result = getServicesXml(info, entryIDs);
			System.out.println("-->> result: " + result);
			return result;
		} 
		catch(Exception ex) 
		{
			throw new uAALException("ERROR: Failed to retrieve purchased services");
		}
	}

	/**
	 * Returns all the AAL services that have been purchased by the user and an
	 * update exists for them in the uStore catalog.
	 * This method was not implemented.
	 * 
	 * @param pSessionKey 
	 * 		The user session key  
	 * 
	 * @return 
	 * 		The AAL services in the following template:
	 * 		<services>
	 * 			<service>
	 * 				<name>service-name</name>
	 * 				<link>link to retrieve the .usrv file</link>
	 * 			</service>
	 * 			<service>
	 * 				<name>service-name</name>
	 * 				<link>link to retrieve the .usrv file</link>
	 * 			</service>
	 * 			...
	 * 		</services>
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 * @throws IllegalArgumentException if one of the input parameters is NULL
	 * 		or invalid
	 */
	@WebMethod
	public String getUpdatesForAALServices(
			@WebParam(name = "sessionKey") String pSessionKey)
		throws uAALException
	{
		System.out.println("-->> OnlineStoreManager:getUpdatesForAALServices() pSessionKey = " + pSessionKey);

		return null;
	}
	
	/**
	 * Get the IDs of catalog entries which are services purchased by a certain
	 * user.
	 * 
	 * @param pUserInfo
	 * 		A {@link UserSessionInfo} holding information of the user's current 
	 * 		session
	 * 
	 * @return 
	 * 		The set of purchased services' catalog entry IDs
	 * 
	 * @throws <code>uAALException</code> if an exception occurs during the 
	 * 		process 
	 */
	private HashSet<String> getPurchasedAALServices(UserSessionInfo pUserInfo) 
		throws uAALException 
	{
		String xpath = "/CatalogEntry[UserOrders[ExternalIdentifier[(@ownerID={0})]]]";

		String accessProfile = "Universaal_All";
		Object[] idArray = {pUserInfo.getUserId()};
		SelectionCriteriaHelper helper = new SelectionCriteriaHelper(MessageFormat.format(xpath, idArray));
		helper.addAccessProfile(accessProfile);
		ExpressionType getExpression = helper.getSelectionCriteriaExpression();
		GetType get = CatalogEntryFacadeClient.createGetVerb(getExpression);

		try 
		{
			// create the CatalogFacadeClient client
			// this client is used to send requests and receive responses from the server
			CatalogFacadeClient catalogClient = new CatalogFacadeClient(
					CommonUtilities.getInstance().getBusinessContext(), pUserInfo.getCallbackHandler());
			
			// get the list of purchased services 
			List catalogEntries = catalogClient.getCatalogEntry(get).getCatalogEntry();
			System.out.println("-->> Number of services entries found: " + catalogEntries.size());
			HashSet<String> entryIDs = new HashSet<String>();
			for(Object object : catalogEntries) 
			{
				CatalogEntryType resultEntry = (CatalogEntryType)object;
				String uniqueID = resultEntry.getCatalogEntryIdentifier().getUniqueID();
				entryIDs.add(uniqueID);
			}
			System.out.println("-->> Done with SQL query, returning " + entryIDs.size() + " entries");
	
			return entryIDs;
		} 
		catch(CatalogEntryException ex) 
		{
			throw new uAALException("ERROR: Failed to retrieve purchased services for user " +
					pUserInfo.getUserId() + " (" + ex.getMessage() + ")");
		}
	}

	/**
	 * Build the services XML snippet to be returned by some of the methods.
	 * 
	 * @param pUserInfo
	 * 		A {@link UserSessionInfo} holding information of the user's current 
	 * 		session
	 * @param pEntryIDs 
	 * 		The set of the services' catalog entry IDs to be included in the 
	 * 		result
	 * 
	 * @throws uAALException if an error has occurred during the building process
	 */
	private String getServicesXml(UserSessionInfo pUserInfo, HashSet<String> pEntryIDs)
		throws uAALException 
	{
		String pathPrefix = CommonUtilities.getInstance().getProperty("linkToUsrvFile", null);
		if(null == pathPrefix)
		{
			throw new uAALException("ERROR: The prefix link to the usrv file was not defined");
		}

		String storeId = CommonUtilities.getInstance().getProperty("storeId", "10101");
		String catalogId = CommonUtilities.getInstance().getProperty("catalogId", "10101");

		// Build the result XML
		StringBuilder result = new StringBuilder("<services>");
		for(String uniqueID : pEntryIDs) 
		{
			result.append("<service>");
			result.append("<id>").append(uniqueID).append("</id>");
			StringBuilder link = new StringBuilder(pathPrefix);
			link.append("langId").append("=").append("-1").append("&");
			link.append("storeId").append("=").append(storeId).append("&");
			link.append("catalogId").append("=").append(catalogId).append("&");
			link.append("userSessionId").append("=").append(pUserInfo.getSessionId()).append("&");
			link.append("serviceId").append("=").append(uniqueID);
			result.append("<link>").append(link).append("</link>");
			result.append("</service>");
		}			
		result.append("</services>");
		return result.toString();
	}	
}
