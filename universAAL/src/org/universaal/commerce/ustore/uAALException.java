/*
	Copyright 2008-2014 ProSyst AG, http://www.prosyst.com
	 
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore;

import java.util.List;
import java.util.Locale;

import com.ibm.commerce.foundation.client.facade.bod.AbstractBusinessObjectDocumentException;
import com.ibm.commerce.foundation.client.facade.bod.ClientError;

/**
 * This exception is thrown by the classes in this project.  
 * 
 * @author roni
 * @author i.bakalov@prosyst.com
 */

public class uAALException extends Exception
{
	/**
	 * The exception message
	 */
	private String mMessage = null;
	
	/**
	 * Default constructor.
	 */
	public uAALException() 
	{
	}
	
    /**
     * Constructs a new exception with the specified detail message.
     *
     * @param pMessage 
     * 		The detail message
     */
	public uAALException(String pMessage) 
	{
		mMessage = pMessage;
    }
	
	/**
	 * Constructs a new exception from an
	 * <code>com.ibm.commerce.foundation.client.facade.bod.AbstractBusinessObjectDocumentException</code>
	 * 
	 * @param pException
	 * 		The input
	 * 		<code>com.ibm.commerce.foundation.client.facade.bod.AbstractBusinessObjectDocumentException</code>
	 */
	public uAALException(AbstractBusinessObjectDocumentException pException) 
	{
		StringBuffer buffer = new StringBuffer();
		List listErrors = pException.getClientErrors();
		if(null != listErrors) 
		{
			for(int i=0; i<listErrors.size(); i++) 
			{
				ClientError clientError = (ClientError)listErrors.get(i);
				buffer.append("ERROR: Failure [" + clientError.getErrorKey() + "] " +
					clientError.getLocalizedMessage(Locale.ENGLISH) + "\n");
			}
		}
		else
		{
			buffer.append("ERROR: Unknown Failure " + pException.getMessage());
		}
		mMessage = buffer.toString();
    }
	
	/**
	 * Constructs a new exception from a list of client errors
	 * 
	 * @param pErrors 
	 * 		A list of <code>com.ibm.commerce.foundation.client.facade.bod.ClientError</code>s
	 */
	public uAALException(List pErrors) 
	{
		StringBuffer buffer = new StringBuffer();
		if(null != pErrors) 
		{
			for(int i=0; i<pErrors.size(); i++) 
			{
				ClientError clientError = (ClientError)pErrors.get(i);
				buffer.append("ERROR: Failure [" + clientError.getErrorKey() + "] " +
					clientError.getLocalizedMessage(Locale.ENGLISH) + "\n");
			}
		}
		else
		{
			buffer.append("ERROR: Unknown Failure");
		}
		mMessage = buffer.toString();
    }

	/**
	 * @return The detail message string of this throwable. 
	 */
	public String getMessage()
	{
		return mMessage;
	}
}
