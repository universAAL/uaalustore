/*	
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.user;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * This class listens to session events of privileged users.
 * 
 * @author roni
 */
public class PriviligedUserSessionListener 
	implements HttpSessionListener
{
	/**
	 * Invoked when a session is created.
	 * No specific action is done. The user session will be stored in the 
	 * internal data structure only after the user authentication. 
	 *  
	 * @param pEvent 
	 * 		The session event for the created session
	 */
	public void sessionCreated(HttpSessionEvent pEvent)
	{
		System.out.println("-->> PriviligedUserSessionListener.sessionCreated(): sessionId = " + 
				pEvent.getSession().getId());
	}
	
	/**
	 * Invoked when a session is destroyed. The user information is being 
	 * removed from the internal data structure.
	 * 
	 * @param pEvent 
	 * 		The session event for the destroyed session
	 */
	public void sessionDestroyed(HttpSessionEvent pEvent)
	{
		System.out.println("-->> PriviligedUserSessionListener.sessionDestroyed(): sessionId = " + 
				pEvent.getSession().getId());
		UserSessionHandler.removeUserInfoFromSession(pEvent.getSession().getId());
	}
}
