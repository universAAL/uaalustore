/*	
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.user;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This class manages the user sessions for uStore.
 * 
 * @author roni
 */
public class UserSessionHandler 
{
	/**
	 * Holds the <code>UserSessionInfo</code> per username
	 */
	private static Map<String, UserSessionInfo> mUserNamesMap = 
		new HashMap<String, UserSessionInfo>();
	
	/**
	 * Holds the <code>UserSessionInfo</code> per session key
	 */
	private static Map<String, UserSessionInfo> mSessionKeysMap = 
		new HashMap<String, UserSessionInfo>();
	
	/**
	 * Returns the <code>UserSessionInfo</code> for the given user name.
	 * 
	 * @param pUserName 
	 * 		The user name to look for
	 * 
	 * @return 
	 * 		The <code>UserSessionInfo</code> for the given user name
	 */
	public static UserSessionInfo getUserSessionInfoByUserName(String pUserName)
	{
		synchronized(mUserNamesMap)
		{
			UserSessionInfo info = (UserSessionInfo)mUserNamesMap.get(pUserName);
			System.out.println("-->> UserSessionHandler.getUserSessionInfoByUserName(): " +
					"userName = " +	pUserName + " info = " + info);
			if(null != info)
			{
				info.resetTimeoutTimer();
			}
			return info;
		}
	}

	/**
	 * Returns the <code>UserSessionInfo</code> for the given session key.
	 * 
	 * @param pSessionKey 
	 * 		The session key to look for
	 * 
	 * @return 
	 * 		The <code>UserSessionInfo</code> for the given session key
	 */
	public static UserSessionInfo getUserSessionInfoBySessionKey(String pSessionKey)
	{
		synchronized(mSessionKeysMap)
		{
			UserSessionInfo info = (UserSessionInfo)mSessionKeysMap.get(pSessionKey);
			System.out.println("-->> UserSessionHandler.getUserSessionInfoBySessionKey(): " +
					"sessionKey = " + pSessionKey + " info = " + info);
			if(null != info)
			{
				info.resetTimeoutTimer();
			}
			return info;
		}
	}
	
	/**
	 * Adds a <code>UserSessionInfo</code> to the internal maps.
	 * 
	 * @param pInfo 
	 * 		The <code>UserSessionInfo</code> to add to the maps
	 */
	public static void addUserInfoToSession(UserSessionInfo pInfo)
	{
		synchronized(mSessionKeysMap)
		{
			Iterator iter = mSessionKeysMap.keySet().iterator();
			while(iter.hasNext())
			{
				String sessionKey = (String)iter.next();
				UserSessionInfo info = (UserSessionInfo)mSessionKeysMap.get(sessionKey);
				
				if((null != info.getUserName()) && info.getUserName().equals(pInfo.getUserName()))
				{
					mSessionKeysMap.remove(sessionKey);
					info.stopTimeoutTimer();
					System.out.println("-->> UserSessionHandler.addUserInfoToSession(): session (" + 
							sessionKey + ") is destroyed for user (" + info.getUserName() + ")");
					break;
				}
			}
			System.out.println("-->> UserSessionHandler.addUserInfoToSession(): session = " + 
					pInfo.getSessionId());
			mSessionKeysMap.put(pInfo.getSessionId(), pInfo);
		}
		synchronized(mUserNamesMap)
		{
			System.out.println("-->> UserSessionHandler.addUserInfoToSession(): userName = " + 
					pInfo.getUserName()); 
			UserSessionInfo info = mUserNamesMap.put(pInfo.getUserName(), pInfo);
			if(null != info)
			{
				info.stopTimeoutTimer();
				info.stopComapreCatalogToSpaceTimer();
			}
		}
		pInfo.startTimeoutTimer();
	}

	/**
	 * Removes a <code>UserSessionInfo</code> from the internal maps.
	 * 
	 * @param pInfo 
	 * 		The <code>UserSessionInfo</code> to remove from the maps
	 */
	public static void removeUserInfoFromSession(String pSessionKey)
	{
		synchronized(mUserNamesMap)
		{
			Iterator iter = mUserNamesMap.keySet().iterator();
			while(iter.hasNext())
			{
				String userName = (String)iter.next();
				UserSessionInfo info = (UserSessionInfo)mUserNamesMap.get(userName);
				
				if((null != info.getSessionId()) && info.getSessionId().equals(pSessionKey))
				{
					mUserNamesMap.remove(userName);
					System.out.println("-->> UserSessionHandler.removeUserInfoFromSession(): " +
							"session (" + pSessionKey +	") is destroyed for user (" + userName + ")");
					info.stopTimeoutTimer();
					break;
				}
			}
			System.out.println("-->> UserSessionHandler.removeUserInfoFromSession(): " +
					"mUserNamesMap.size() = " + mUserNamesMap.size());
		}

		synchronized(mSessionKeysMap)
		{
			System.out.println("-->> UserSessionHandler.removeUserInfoFromSession(): " +
					"session = " + pSessionKey); 
			UserSessionInfo info = mSessionKeysMap.remove(pSessionKey);
			if(null != info)
			{
				info.stopComapreCatalogToSpaceTimer();
			}
			System.out.println("-->> UserSessionHandler.removeUserInfoFromSession(): " +
					"mSessionKeysMap.size() = " + mSessionKeysMap.size());
		}
	}
}
