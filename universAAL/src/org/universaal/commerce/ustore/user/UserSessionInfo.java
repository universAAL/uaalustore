/*	
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.user;

import java.util.Timer;
import java.util.TimerTask;

import javax.security.auth.callback.CallbackHandler;

import org.universaal.commerce.ustore.space.SpaceHandler;
import org.universaal.commerce.ustore.utilities.CommonUtilities;

/**
 * This class holds the user information to be shared across the session 
 * requests.
 * 
 * @author roni
 */
public class UserSessionInfo
{
	/**
	 * The user identifier 
	 */
	private String mUserId = null;

	/**
	 * The user name
	 */
	private String mUserName = null;
	
	/**
	 * The user password
	 */
	private String mPassword = null;
	
	/**
	 * The session identifier
	 */
	private String mSessionId = null;
	
	/**
	 * The <code>javax.security.auth.callback.CallbackHandler</code> to be used 
	 * by the different facade clients
	 */
	private CallbackHandler mCallbackHandler = null;

	/**
	 * The timeout timer for the session to be expired
	 */
	private Timer mTimeoutTimer = null;
	
	/**
	 * The timer that compares the services installed at the AAL space to the
	 * services available in the catalog and marks in the DB if updates are 
	 * needed 
	 */
	private Timer mComapreCatalogToSpaceTimer = null;
	
	/**
	 * Constructor.
	 * 
	 * @param pUserId 
	 * 		The user identifier 
	 * @param pUserName 
	 * 		The user name
	 * @param pPassword 
	 * 		The user password
	 * @param pSessionId 
	 * 		The user session identifier
	 * @param pHandler 
	 * 		The <code>javax.security.auth.callback.CallbackHandler</code> to be 
	 * 		used by the different facade clients
	 */
	public UserSessionInfo(
			String pUserId, 
			String pUserName, 
			String pPassword, 
			String pSessionId, 
			CallbackHandler pHandler)
	{
		mUserId = pUserId;
		mUserName = pUserName;
		mPassword = pPassword;
		mSessionId = pSessionId;
		mCallbackHandler = pHandler;
	}

	/**
	 * @return The user identifier 
	 */
	public String getUserId()
	{
		return mUserId;
	}

	/**
	 * @return The user name 
	 */
	public String getUserName()
	{
		return mUserName;
	}

	/**
	 * @return The user password 
	 */
	public String getPassword()
	{
		return mPassword;
	}

	/**
	 * @return The user session identifier 
	 */
	public String getSessionId()
	{
		return mSessionId;
	}
	
	/**
	 * @return The <code>javax.security.auth.callback.CallbackHandler</code> to 
	 * 		be used by the different facade clients
	 */
	public CallbackHandler getCallbackHandler()
	{
		return mCallbackHandler;
	}
	
	/**
	 * Initiates a timer that will remove the user details from the session 
	 * after a fixed time that is defined in the configuration files. 
	 */
	public void startTimeoutTimer()
	{
		mTimeoutTimer = new Timer();
		String time = CommonUtilities.getProperty("sessionTimout", "60");
		mTimeoutTimer.schedule(new DestroySessionTask(),Long.parseLong(time)*1000);
		System.out.println("-->> startTimeoutTimer for user (" + mUserName + ") sessionId (" + 
				mSessionId + ") time " + time + ")");
	}

	/**
	 * Resets the timer that will remove the user details from the session.
	 */
	public void resetTimeoutTimer()
	{
		stopTimeoutTimer();
		startTimeoutTimer();
	}

	/**
	 * Stops the timer when the user session is expired.
	 */
	public void stopTimeoutTimer()
	{
		if(null != mTimeoutTimer)
		{
			mTimeoutTimer.cancel();
			System.out.println("-->> stopTimoutTimer for user " + mUserName + 
					" sessionId " + mSessionId);
		}
	}

	/**
	 * Initiates a timer that will compare the services installed at the AAL 
	 * space to the services available in the catalog and mark in the DB if 
	 * updates are needed. This timer executes every fixed time that is defined 
	 * in the configuration files. 
	 */
	public void startComapreCatalogToSpaceTimer()
	{
		mComapreCatalogToSpaceTimer = new Timer();
		String time = CommonUtilities.getProperty("comapreCatalogToSpaceTime", "60");
		mComapreCatalogToSpaceTimer.schedule(new CompareSessionTask(this), 0, Long.parseLong(time)*1000);
		System.out.println("-->> startComapreCatalogToSpaceTimer for user (" + mUserName + ") sessionId (" + 
				mSessionId + ") time " + time + ")");
	}
	
	/**
	 * Stops the timer when the user session is expired.
	 */
	public void stopComapreCatalogToSpaceTimer()
	{
		if(null != mComapreCatalogToSpaceTimer)
		{
			mComapreCatalogToSpaceTimer.cancel();
			System.out.println("-->> stopComapreCatalogToSpaceTimer for user " + mUserName + 
					" sessionId " + mSessionId);
		}
	}
	
	/**
	 * This task will be executed by the session's timeout timer.
	 */
	class DestroySessionTask extends TimerTask 
	{
		/**
		 * When this task is executed the user details will be removed from 
		 * the session.  
		 */
	    public void run() 
	    {
	    	UserSessionHandler.removeUserInfoFromSession(mSessionId);
	    }
	}

	/**
	 * This task will be executed by the session's compare timer.
	 */
	class CompareSessionTask extends TimerTask 
	{
		/**
		 * The user session information
		 */
		private UserSessionInfo mUserInfo;
		
		/**
		 * Constructor.
		 * 
		 * @param pUserInfo
		 * 		The user session information
		 */
		public CompareSessionTask(UserSessionInfo pUserInfo)
		{
			mUserInfo = pUserInfo;
		}
		
		/**
		 * When this task is executed the the services installed at the AAL 
		 * space will be compared to the services available in the catalog The
		 * comparison results will be marked in the DB.   
		 */
	    public void run() 
	    {
	    	try
	    	{
	    		SpaceHandler.getInstance().compareCatalogToSpace(mUserInfo);
	    	}
	    	catch(Exception ex) {} // ignore
	    }
	}
}
