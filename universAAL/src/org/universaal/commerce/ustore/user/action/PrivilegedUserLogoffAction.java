/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.user.action;

import java.util.Map;

import javax.security.auth.callback.CallbackHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.utilities.CommonUtilities;

import com.ibm.commerce.foundation.client.lobtools.actions.AuthenticationClientLibraryAction;
import com.ibm.commerce.foundation.common.datatypes.BusinessContextType;

/**
 * This action class removes the credentials of a privileged user from his
 * session.
 * 
 * @author roni
 */
public class PrivilegedUserLogoffAction extends AuthenticationClientLibraryAction 
{
	/**
	 * Override the default behavior.
	 * This method removes the user information from the internal data structure 
	 * in case of a successful logoff.
	 * 
	 * @param pActionMapping 
	 * 		The configuration of the struts action
	 * @param pActionForm 
	 * 		The action form of the struts action
	 * @param pRequest 
	 * 		The HTTP request object that contains the information pertaining to 
	 * 		the request
	 * @param pResponse 
	 * 		The HTTP response object that will contain the information 
	 * 		pertaining to the response
	 * @param pBusinessContext 
	 * 		The business context of the current request 
	 * @param pCallbackHandler 
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return 
	 * 		The response data as a <code>java.utilMap</code>, null for no 
	 * 		response
	 * 
	 * @throws <code>java.lang.Exception</code> in case a problem occurred while 
	 * 		parsing the HTTP request or executing the business object document. 
	 * 		These exceptions can range from server side validation which are 
	 * 		recoverable to server errors where are not recoverable
	 */
	protected Map executeBusinessObjectDocument(
			ActionMapping pActionMapping, 
			ActionForm pActionForm, 
			HttpServletRequest pRequest, 
			HttpServletResponse pResponse, 
			BusinessContextType pBusinessContext, 
			CallbackHandler pCallbackHandler)
		throws Exception
	{
		String sessionId = pRequest.getSession().getId();
		
		// carry on with BusinessObjectDocumentAction's executing
		Map result = super.executeBusinessObjectDocument(
				pActionMapping, 
				pActionForm, 
				pRequest,  
				pResponse, 
				pBusinessContext, 
				pCallbackHandler);
		
		String errorCode = CommonUtilities.getValue(result.get("ErrorCode"));
		System.out.println("-->> PrivilegedUserLogoffAction sessionId = " + sessionId +
				" error code = " + errorCode);

		if(null == errorCode) // successful logoff 
		{
			UserSessionHandler.removeUserInfoFromSession(sessionId);
		}
		return result;
	}
}
