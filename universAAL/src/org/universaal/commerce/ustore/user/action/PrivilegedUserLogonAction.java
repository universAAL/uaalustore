/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.user.action;

import java.util.Map;

import javax.security.auth.callback.CallbackHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.user.UserSessionInfo;
import org.universaal.commerce.ustore.utilities.CommonUtilities;

import com.ibm.commerce.foundation.client.lobtools.actions.AuthenticationClientLibraryAction;
import com.ibm.commerce.foundation.common.datatypes.BusinessContextType;
import com.ibm.wsspi.security.auth.callback.WSCallbackHandlerFactory;

/**
 * This action class stores the credentials of a privileged user in his session
 * in order to synchronize further accesses of this user from the different web
 * services.
 * 
 * @author roni
 */
public class PrivilegedUserLogonAction extends AuthenticationClientLibraryAction 
{
	/**
	 * Override the default behavior.
	 * This method stores the user information in the internal data structure in
	 * case of a successful login.
	 * 
	 * @param pActionMapping 
	 * 		The configuration of the struts action
	 * @param pActionForm 
	 * 		The action form of the struts action
	 * @param pRequest 
	 * 		The HTTP request object that contains the information pertaining to 
	 * 		the request
	 * @param pResponse 
	 * 		The HTTP response object that will contain the information 
	 * 		pertaining to the response
	 * @param pBusinessContext 
	 * 		The business context of the current request 
	 * @param pCallbackHandler 
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return 
	 * 		The response data as a <code>java.utilMap</code>, null for no 
	 * 		response
	 * 
	 * @throws <code>java.lang.Exception</code> in case a problem occurred while 
	 * 		parsing the HTTP request or executing the business object document. 
	 * 		These exceptions can range from server side validation which are 
	 * 		recoverable to server errors where are not recoverable
	 */
	protected Map executeBusinessObjectDocument(
			ActionMapping pActionMapping, 
			ActionForm pActionForm, 
			HttpServletRequest pRequest, 
			HttpServletResponse pResponse, 
			BusinessContextType pBusinessContext, 
			CallbackHandler pCallbackHandler)
		throws Exception
	{
		String sessionId = pRequest.getSession().getId();
		String userName = CommonUtilities.getValue(pRequest.getParameterMap().get("logonId"));
		String password = CommonUtilities.getValue(pRequest.getParameterMap().get("password"));
		
		// carry on with BusinessObjectDocumentAction's executing
		Map result = super.executeBusinessObjectDocument(
				pActionMapping, 
				pActionForm, 
				pRequest,  
				pResponse, 
				pBusinessContext, 
				pCallbackHandler);
		
		String userId = CommonUtilities.getValue(result.get("userId"));

		System.out.println("-->> PrivilegedUserLogonAction sessionId = " + sessionId + " userName = " +
				userName + " password = " + password + " userId = " + userId);

		CallbackHandler handler = WSCallbackHandlerFactory.getInstance().getCallbackHandler(
				userName, null, password, pRequest, pResponse, null);
		
		UserSessionInfo info = new UserSessionInfo(userId, userName, password, sessionId, handler);
		UserSessionHandler.addUserInfoToSession(info);
		
		return result;
	}
}
