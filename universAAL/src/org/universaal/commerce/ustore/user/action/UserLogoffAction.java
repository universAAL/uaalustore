/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.user.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.utilities.CommonUtilities;

import com.ibm.commerce.struts.LTPATokenGenerationEnabledBaseAction;
import com.ibm.commerce.webcontroller.exception.RequestException;

/**
 * This action class removes the credentials of the user from his session.
 * 
 * @author roni
 */
public class UserLogoffAction extends LTPATokenGenerationEnabledBaseAction 
{
	/**
	 * Override the default behavior.
	 * This method removes the user information from the internal data structure 
	 * in case of a successful logoff.
	 * 
	 * @param pActionMapping 
	 * 		The ActionMapping for this action and request
	 * @param pActionForm 
	 * 		The ActionForm bean for this request
	 * @param pInMap 
	 * 		The input parameters as a map
	 * @param pRequest 
	 * 		The HTTP servlet request being processed
	 * @param pResponse 
	 * 		The HTTP response being created
	 * 
	 * @return 
	 * 		The response data as a <code>java.utilMap</code>, null for no 
	 * 		response
	 * 
	 * @throws <code>com.ibm.commerce.webcontroller.exception.RequestException</code> 
	 * 		is thrown on any error encountered 
	 */
	protected Map invokeService(
			ActionMapping pActionMapping, 
			ActionForm pActionForm, 
			Map pInMap, 
			HttpServletRequest pRequest, 
			HttpServletResponse pResponse)
    	throws RequestException
   	{
		// carry on with LTPATokenGenerationEnabledBaseAction's executing
		Map outMap = super.invokeService(pActionMapping, pActionForm, pInMap, pRequest, pResponse);

		String sessionId = pRequest.getSession().getId();
		String errorCode = CommonUtilities.getValue(outMap.get("ErrorCode"));
		System.out.println("-->> UserLogoffAction.invokeService() sessionId = " + sessionId + 
				" error code = " + errorCode);
		
		if(null == errorCode) // successful logoff 
		{
			UserSessionHandler.removeUserInfoFromSession(sessionId);
		}
		return outMap;
    }
}
