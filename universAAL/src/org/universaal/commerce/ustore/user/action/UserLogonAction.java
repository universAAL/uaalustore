/*
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */
package org.universaal.commerce.ustore.user.action;

import java.util.Map;

import javax.security.auth.callback.CallbackHandler;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.universaal.commerce.ustore.user.UserSessionHandler;
import org.universaal.commerce.ustore.user.UserSessionInfo;
import org.universaal.commerce.ustore.utilities.CommonUtilities;

import com.ibm.commerce.command.ViewCommandContext;
import com.ibm.commerce.struts.LTPATokenGenerationEnabledBaseAction;
import com.ibm.commerce.webcontroller.RequestHandle;
import com.ibm.commerce.webcontroller.exception.RequestException;
import com.ibm.wsspi.security.auth.callback.WSCallbackHandlerFactory;

/**
 * This action class stores the credentials of the user in his session in order
 * to synchronize further accesses of this user from the different web services.
 * 
 * @author roni
 */
public class UserLogonAction 
	extends LTPATokenGenerationEnabledBaseAction 
{
	/**
	 * Override the default behavior.
	 * This method stores the user information in the internal data structure in
	 * case of a successful login.
	 * 
	 * @param pActionMapping 
	 * 		The ActionMapping for this action and request
	 * @param pActionForm 
	 * 		The ActionForm bean for this request
	 * @param pInMap 
	 * 		The input parameters as a map
	 * @param pRequest 
	 * 		The HTTP servlet request being processed
	 * @param pResponse 
	 * 		The HTTP response being created
	 * 
	 * @return 
	 * 		The response data as a <code>java.utilMap</code>, null for no 
	 * 		response
	 * 
	 * @throws <code>com.ibm.commerce.webcontroller.exception.RequestException</code> 
	 * 		is thrown on any error encountered 
	 */
	protected Map invokeService(
			ActionMapping pActionMapping, 
			ActionForm pActionForm, 
			Map pInMap, 
			HttpServletRequest pRequest, 
			HttpServletResponse pResponse)
    	throws RequestException
   	{
		// carry on with LTPATokenGenerationEnabledBaseAction's executing
		Map outMap = super.invokeService(pActionMapping, pActionForm, pInMap, pRequest, pResponse);
		
		String sessionId = pRequest.getSession().getId();
		String userName = CommonUtilities.getValue(outMap.get("logonId"));
		String password = CommonUtilities.getValue(outMap.get("logonPassword"));
		String errorCode = CommonUtilities.getValue(outMap.get("ErrorCode"));
		String userId = null; 

		RequestHandle handle = (RequestHandle)pRequest.getAttribute("EC_requestHandle");
		if(null != handle)
		{
			ViewCommandContext vContext = handle.getViewCommandContext();
			if(null != vContext)
			{
				userId = String.valueOf(vContext.getUserId());
			}
		}

		System.out.println("-->> UserLogonAction.invokeService() sessionId = " + sessionId + " userName = " +
				userName + " password = " + password + " error code = " + errorCode + " userId = " + userId);
		
		if(null == errorCode && null != userId) // successful login 
		{
			CallbackHandler handler = WSCallbackHandlerFactory.getInstance().getCallbackHandler(
					userName, null, password, pRequest, pResponse, null);
			
			UserSessionInfo info = new UserSessionInfo(userId, userName, password, sessionId, handler);
			UserSessionHandler.addUserInfoToSession(info);
			info.startComapreCatalogToSpaceTimer();
		}
		return outMap;
    }
}
