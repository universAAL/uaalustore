/*	
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.security.auth.callback.CallbackHandler;
import javax.servlet.ServletContext;

import org.universaal.commerce.ustore.uAALException;

import com.ibm.commerce.catalog.facade.client.CatalogEntryException;
import com.ibm.commerce.catalog.facade.client.CatalogEntryFacadeClient;
import com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType;
import com.ibm.commerce.catalog.facade.datatypes.CatalogGroupType;
import com.ibm.commerce.catalog.objects.CatalogEntryAccessBean;
import com.ibm.commerce.catalog.objects.CatalogEntryDescriptionAccessBean;
import com.ibm.commerce.catalog.objects.CatalogEntryRelationAccessBean;
import com.ibm.commerce.foundation.common.datatypes.BusinessContextType;
import com.ibm.commerce.foundation.common.datatypes.CommerceFoundationFactory;
import com.ibm.commerce.foundation.common.datatypes.ContextDataType;
import com.ibm.commerce.foundation.internal.client.taglib.GetDataHandler;

/**
 * This class provides utilities methods for the management of catalog entries 
 * in the catalog.
 * 
 * @author roni
 */
public class CatalogUtilities 
{
	/**
	 * The commerce configuration path
	 */
	private static String mCommerceConfigPath = null;
	
	/**
	 * Retrieve an item or a service from the store site.
	 * 
	 * @param pId
	 * 		The identifier of the item or the service (catalog entry) to be 
	 * 		retrieved
	 * @param pStoreId
	 * 		The identifier of the store
	 * @param pServletContext
	 * 		The servlet context
	 * @param pBusinessContext
	 * 		The business context of the current request
	 * @param pCallbackHandler
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return 
	 * 		The item or the service (catalog entry) that was retrieved
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during the retrieval
	 */
	public static CatalogEntryType getCatalogEntryById(
			String pId,
			String pStoreId,
			ServletContext pServletContext, 
			BusinessContextType pBusinessContext,
            CallbackHandler pCallbackHandler)
		throws uAALException
	{
		List contextParams = new ArrayList();
        ContextDataType contextData = CommerceFoundationFactory.eINSTANCE.createContextDataType();
        contextData.setName("storeId");
        contextData.setValue(pStoreId);
        contextParams.add(contextData);
		
		Map parameters = new HashMap();
		List params = new ArrayList();
		params.add(pId);
		parameters.put("catEntryId", params);
		
		GetDataHandler handler = new GetDataHandler();
		handler.setType("com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType");
		handler.setExpressionBuilder("getCatalogEntryFullDetailsByID");
		handler.setExpressionBuilderParameterMap(parameters);
		handler.setRecordSetReferenceId(pId);
		handler.setBusinessContext(pBusinessContext);
		handler.setCallbackHandler(pCallbackHandler);
		handler.setVar("catalogEntry");
		handler.setServletContext(pServletContext);
		handler.setContextDataList(contextParams);
		
		if(null == pServletContext)
		{
			handler.setConfigRoot(new File(getCommerceConfigDir()));
		}
		
		try
		{
			handler.execute();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve the catalog entry by id (" + 
					ex.getMessage() + ")");
		}
		System.out.println("-->> getCatalogEntryById data = " + handler.getData());
		
		return (CatalogEntryType)handler.getData();
	}

	/**
	 * Retrieve an item or a service with the user specific data from the store
	 * site.
	 * 
	 * @param pId
	 * 		The identifier of the item or the service (catalog entry) to be 
	 * 		retrieved
	 * @param pStoreId
	 * 		The identifier of the store
	 * @param pServletContext
	 * 		The servlet context
	 * @param pBusinessContext
	 * 		The business context of the current request
	 * @param pCallbackHandler
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return 
	 * 		The item or the service (catalog entry) that was retrieved
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during the retrieval
	 */
	public static CatalogEntryType getCatalogEntryWithInformationById(
			String pId,
			String pStoreId,
			ServletContext pServletContext, 
			BusinessContextType pBusinessContext,
            CallbackHandler pCallbackHandler)
		throws uAALException
	{
		List contextParams = new ArrayList();
        ContextDataType contextData = CommerceFoundationFactory.eINSTANCE.createContextDataType();
        contextData.setName("storeId");
        contextData.setValue(pStoreId);
        contextParams.add(contextData);

        Map parameters = new HashMap();
		List params = new ArrayList();
		params.add(pId);
		parameters.put("catalogEntryId", params);

		GetDataHandler handler = new GetDataHandler();
		handler.setType("com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType");
		handler.setExpressionBuilder("getCatalogEntryWithInformation");
		handler.setExpressionBuilderParameterMap(parameters);
		handler.setRecordSetReferenceId(pId);
		handler.setBusinessContext(pBusinessContext);
		handler.setCallbackHandler(pCallbackHandler);
		handler.setVar("catalogEntry");
		handler.setServletContext(pServletContext);
		handler.setContextDataList(contextParams);
		
		if(null == pServletContext)
		{
			handler.setConfigRoot(new File(getCommerceConfigDir()));
		}
		
		try
		{
			handler.execute();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve the catalog entry information by id (" +
					ex.getMessage() + ")");
		}
		System.out.println("-->> getCatalogEntryWithInformationById data = " + handler.getData());
		
		return (CatalogEntryType)handler.getData();
	}
	
	/**
	 * Retrieve a service with its composed items from the store site.
	 * 
	 * @param pServiceId
	 * 		The identifier of the service (catalog entry) to be retrieved
	 * @param pStoreId
	 * 		The identifier of the store
	 * @param pServletContext
	 * 		The servlet context
	 * @param pBusinessContext
	 * 		The business context of the current request
	 * @param pCallbackHandler
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return 
	 * 		The service (catalog entry) that was retrieved including its 
	 * 		component list
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during the retrieval
	 */
	public static CatalogEntryType getServiceWithComponentsById(
			String pServiceId,
			String pStoreId,
			ServletContext pServletContext, 
			BusinessContextType pBusinessContext,
            CallbackHandler pCallbackHandler)
		throws uAALException
	{
		List contextParams = new ArrayList();
        ContextDataType contextData = CommerceFoundationFactory.eINSTANCE.createContextDataType();
        contextData.setName("storeId");
        contextData.setValue(pStoreId);
        contextParams.add(contextData);
		
		Map parameters = new HashMap();
		List params = new ArrayList();
		params.add(pServiceId);
		parameters.put("catEntryId", params);
		
		GetDataHandler handler = new GetDataHandler();
		handler.setType("com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType");
		handler.setExpressionBuilder("getCatalogEntryComponentsByID");
		handler.setExpressionBuilderParameterMap(parameters);
		handler.setBusinessContext(pBusinessContext);
		handler.setCallbackHandler(pCallbackHandler);
		handler.setServletContext(pServletContext);
		handler.setContextDataList(contextParams);
		
		if(null == pServletContext)
		{
			handler.setConfigRoot(new File(getCommerceConfigDir()));
		}
		
		try
		{
			handler.execute();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve the service with components (" + 
					ex.getMessage() + ")");
		}
		
		System.out.println("-->> getServiceWithComponentsById data = " + handler.getData());
		
		return (CatalogEntryType)handler.getData();
	}
	
	/**
	 * Retrieve an array of services (catalog entries) that contain a reference
	 * to the input item.
	 * 
	 * @param pItemId
	 * 		The identifier of the reference item (catalog entry)
	 * @param pStoreId
	 * 		The identifier of the store
	 * @param pServletContext
	 * 		The servlet context
	 * @param pBusinessContext
	 * 		The business context of the current request
	 * @param pCallbackHandler
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return 
	 * 		An array of services (catalog entries) that contain a reference to 
	 * 		the input item
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during the retrieval
	 */
	public static CatalogEntryType[] getServicesContainReferenceToItemId(
			String pItemId,
			String pStoreId,
			ServletContext pServletContext, 
			BusinessContextType pBusinessContext,
            CallbackHandler pCallbackHandler)
		throws uAALException
	{
		String catEntryType = "PackageBean";
		
		List contextParams = new ArrayList();
        ContextDataType contextData = CommerceFoundationFactory.eINSTANCE.createContextDataType();
        contextData.setName("storeId");
        contextData.setValue(pStoreId);
        contextParams.add(contextData);
		
		Map parameters = new HashMap();
		List params = new ArrayList();
		params.add(pItemId);
		parameters.put("catEntryId", params);
		params = new ArrayList();
		params.add(catEntryType);
		parameters.put("catEntryType", params);
		
		GetDataHandler handler = new GetDataHandler();
		handler.setType("com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType[]");
		handler.setExpressionBuilder("getCatalogEntryKitReferencesById");
		handler.setExpressionBuilderParameterMap(parameters);
		handler.setBusinessContext(pBusinessContext);
		handler.setCallbackHandler(pCallbackHandler);
		handler.setServletContext(pServletContext);
		handler.setContextDataList(contextParams);
		
		if(null == pServletContext)
		{
			handler.setConfigRoot(new File(getCommerceConfigDir()));
		}
		
		try
		{
			handler.execute();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve the services that contain reference to the item (" +
					ex.getMessage() + ")");
		}
		System.out.println("-->> getServicesContainReferenceToItemId data = " + handler.getData());
		
		return (CatalogEntryType[])handler.getData();
	}
	
	/**
	 * Returns the catalog entries (items or services) under the parent category 
	 * from the uStore catalog.
	 * 
	 * @param pParentCatalogGroupId
	 * 		The parent category identifier in the uStore catalog
	 * @param pStoreId
	 * 		The identifier of the store
	 * @param pServletContext
	 * 		The servlet context
	 * @param pBusinessContext
	 * 		The business context of the current request
	 * @param pCallbackHandler
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return 
	 * 		An array of <code>com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType</code>
	 * 		objects containing data of the catalog entries under the parent 
	 * 		category
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if there was an error when performing the operation
	 */
	public static CatalogEntryType[] getCatalogEntriesByParentCatalogGroupId(
			String pParentCatalogGroupId,
			String pStoreId,
			ServletContext pServletContext, 
			BusinessContextType pBusinessContext,
            CallbackHandler pCallbackHandler)
		throws uAALException
	{
		List contextParams = new ArrayList();
        ContextDataType contextData = CommerceFoundationFactory.eINSTANCE.createContextDataType();
        contextData.setName("storeId");
        contextData.setValue(pStoreId);
        contextParams.add(contextData);
		
		Map parameters = new HashMap();
		List params = new ArrayList();
		params.add(pParentCatalogGroupId);
		parameters.put("catGroupId", params);
		
		GetDataHandler handler = new GetDataHandler();
		handler.setType("com.ibm.commerce.catalog.facade.datatypes.CatalogEntryType[]");
		handler.setExpressionBuilder("getCatalogEntryByParentCatalogGroupId");
		handler.setExpressionBuilderParameterMap(parameters);
		handler.setBusinessContext(pBusinessContext);
		handler.setCallbackHandler(pCallbackHandler);
		handler.setServletContext(pServletContext);
		handler.setContextDataList(contextParams);
		
		if(null == pServletContext)
		{
			handler.setConfigRoot(new File(getCommerceConfigDir()));
		}
		
		try
		{
			handler.execute();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve catalog entries by parent catalog group (" + 
					ex.getMessage() + ")");
		}
		return (CatalogEntryType[])handler.getData();
	}

	/**
	 * Returns the categories under the parent category from the uStore catalog.
	 * 
	 * @param pParentCategoryId
	 * 		The parent category identifier in the uStore catalog
	 * @param pStoreId
	 * 		The identifier of the store
	 * @param pServletContext
	 * 		The servlet context
	 * @param pBusinessContext
	 * 		The business context of the current request
	 * @param pCallbackHandler
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return 
	 * 		An array of <code>com.ibm.commerce.catalog.facade.datatypes.CatalogGroupType</code>
	 * 		objects containing data of the categories under the parent category
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if there was an error when performing the operation
	 */
	public static CatalogGroupType[] getCatalogGroupsByParentCatalogGroupId(
			String pParentCatalogGroupId,
			String pStoreId,
			ServletContext pServletContext, 
			BusinessContextType pBusinessContext,
            CallbackHandler pCallbackHandler)
		throws uAALException
	{
		List contextParams = new ArrayList();
        ContextDataType contextData = CommerceFoundationFactory.eINSTANCE.createContextDataType();
        contextData.setName("storeId");
        contextData.setValue(pStoreId);
        contextParams.add(contextData);
		
		Map parameters = new HashMap();
		List params = new ArrayList();
		params.add(pParentCatalogGroupId);
		parameters.put("catGroupId", params);
		
		GetDataHandler handler = new GetDataHandler();
		handler.setType("com.ibm.commerce.catalog.facade.datatypes.CatalogGroupType[]");
		handler.setExpressionBuilder("getCatalogGroupByParentCatalogGroupId");
		handler.setExpressionBuilderParameterMap(parameters);
		handler.setBusinessContext(pBusinessContext);
		handler.setCallbackHandler(pCallbackHandler);
		handler.setServletContext(pServletContext);
		handler.setContextDataList(contextParams);
		
		if(null == pServletContext)
		{
			handler.setConfigRoot(new File(getCommerceConfigDir()));
		}
		
		try
		{
			handler.execute();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve catalog groups by parent catalog group (" + 
					ex.getMessage() + ")");
		}
		System.out.println("-->> getCatalogGroupsByParentCatalogGroupId data = " + handler.getData());
		
		return (CatalogGroupType[])handler.getData();
	}
	
	/**
	 * Deletes an item from the catalog.
	 * 
	 * @param pItemId
	 * 		The identifier of the item in the catalog to be deleted
	 * @param pStoreId
	 * 		The identifier of the store
	 * @param pServletContext
	 * 		The servlet context
	 * @param pBusinessContext
	 * 		The business context of the current request
	 * @param pCallbackHandler
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during the delete operation
	 */
	public static void deleteItem(
			String pItemId, 
			String pStoreId,
			ServletContext pServletContext,
			BusinessContextType pBusinessContext,
            CallbackHandler pCallbackHandler)
		throws uAALException
	{
		CatalogEntryFacadeClient catalogEntryClient = 
			new CatalogEntryFacadeClient(pBusinessContext, pCallbackHandler);

		CatalogEntryType item = getCatalogEntryById(
				pItemId, pStoreId, pServletContext, pBusinessContext, pCallbackHandler);
		if(null == item)
		{
			throw new uAALException("ERROR: Can't find item [" + pItemId + "] in the catalog");
		}

		List list = new ArrayList();
		list.add(item);
		
		try
		{
			catalogEntryClient.deleteCatalogEntry(list);
		}
		catch(CatalogEntryException ex)
		{
			throw new uAALException(ex);
		}
	}

	/**
	 * Retrieves a populated
	 * <code>com.ibm.commerce.catalog.objects.CatalogEntryRelationAccessBean</code>
	 * that is associated with the given item id and service id.
	 * 
	 * @param pItemId
	 * 		The identifier of the included item
	 * @param pServiceId
	 * 		The identifier of the service
	 * 
	 * @return 
	 * 		A populated <code>com.ibm.commerce.catalog.objects.CatalogEntryRelationAccessBean</code>
	 * 		that is associated with the given item id and service id
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during the retrieval
	 */
	public static CatalogEntryRelationAccessBean getCatalogEntryRelationAccessBean(
			String pItemId, String pServiceId)
		throws uAALException
	{
		// retrieve the version and the file name of the application from the 
		// 'field1' and 'field3' columns
		CatalogEntryRelationAccessBean bean = new CatalogEntryRelationAccessBean();
		bean.setInitKey_catalogEntryIdChild(pItemId);
		bean.setInitKey_catalogEntryIdParent(pServiceId);
		bean.setInitKey_catalogRelationTypeId("PACKAGE_COMPONENT");

		try
		{
			bean.refreshCopyHelper();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve the catalog entry relation access bean (" + 
					ex.getMessage() + ")");
		}
		return bean;
	}

	/**
	 * Stores the file name of an application in the 'FIELD1' column and the
	 * version in the 'FIELD3' column, both fields used during the retrieval of
	 * the file
	 * 
	 * @param pItemId
	 * 		The identifier of the reference item (catalog entry)
	 * @param pServiceId
	 * 		The identifier of the parent service
	 * @param pFileName
	 * 		The file name
	 * @param pVersion
	 * 		The file version
	 *            
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during the update
	 */
	public static void updateCatalogEntryRelationAccessBean(
			String pItemId, String pServiceId, String pFileName, String pVersion)
		throws uAALException
	{
		CatalogEntryRelationAccessBean bean = getCatalogEntryRelationAccessBean(pItemId, pServiceId);
		bean.setField1(pFileName);
		bean.setField3(pVersion);

		try
		{
			bean.commitCopyHelper();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to update the catalog entry relation access bean (" + 
					ex.getMessage() + ")");
		}
	}
	
	/**
	 * Get a <code>com.ibm.commerce.catalog.objects.CatalogEntryAccessBean</code> 
	 * for a given catalog entry.
	 * 
	 * @param pCatEntryId
	 * 		The identifier of the item or the service (catalog entry) to be 
	 * 		accessed
	 * @return 
	 * 		A <code>com.ibm.commerce.catalog.objects.CatalogEntryAccessBean</code>
	 * 		for a given catalog entry
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during retrieval
	 */
	public static CatalogEntryAccessBean getCatalogEntryAccessBean(String pCatEntryId)
		throws uAALException
	{
		CatalogEntryAccessBean bean = new CatalogEntryAccessBean();
		bean.setInitKey_catalogEntryReferenceNumber(pCatEntryId);
		
		try
		{
			bean.refreshCopyHelper();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve the catalog entry access bean (" + 
					ex.getMessage() + ")");
		}
		return bean;
	}
	
	/**
	 * Stores a string representing items belonging to a service in the 'FIELD4'
	 * column of that service's catalog entry.
	 * 
	 * @param pCatEntryId
	 * 		The identifier of the service (catalog entry)
	 * @param pItemsList
	 * 		The string with service items (separated by ;)
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during update
	 */
	public static void updateCatalogEntryAccessBeanItemsList(
			String pCatEntryId, String pItemsList)
		throws uAALException
	{
		CatalogEntryAccessBean bean = getCatalogEntryAccessBean(pCatEntryId);
		bean.setField4(pItemsList);
		
		try
		{
			bean.commitCopyHelper();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to update the catalog entry access bean (" + 
					ex.getMessage() + ")");
		}
	}
	
	/**
	 * Stores the username of the developer uploading an application in the
	 * 'FIELD5' column of that application's catalog entry (CATENTRY table).
	 * 
	 * @param pCatEntryId
	 * 		The identifier of the application (catalog entry)
	 * @param pUserName
	 * 		The developer username
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during the update
	 */
	public static void updateCatalogEntryAccessBeanUserName(
			String pCatEntryId, String pUserName)
		throws uAALException
	{
		CatalogEntryAccessBean bean = getCatalogEntryAccessBean(pCatEntryId);
		bean.setField5(pUserName);

		try
		{
			bean.commitCopyHelper();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to update the catalog entry access bean (" +
					ex.getMessage() + ")");
		}
	}

	/**
	 * Get a <code>com.ibm.commerce.catalog.objects.CatalogEntryDescriptionAccessBean</code>
	 * for a given catalog entry.
	 * 
	 * @param pCatEntryId
	 * 		The identifier of the item or the service (catalog entry) to be accessed
	 * 
	 * @return 
	 * 		A <code>com.ibm.commerce.catalog.objects.CatalogEntryDescriptionAccessBean</code>
	 * 		for a given catalog entry
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during retrieval
	 */
	public static CatalogEntryDescriptionAccessBean getCatalogEntryDescriptionAccessBean(
			String pCatEntryId)
		throws uAALException
	{
		CatalogEntryDescriptionAccessBean bean = new CatalogEntryDescriptionAccessBean();
		bean.setInitKey_catalogEntryReferenceNumber(pCatEntryId);
		bean.setInitKey_language_id("-1");

		try
		{
			bean.refreshCopyHelper();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve the catalog entry description access bean (" + 
					ex.getMessage() + ")");
		}
		return bean;
	}

	/**
	 * Sets the "published" field of an application's catalog entry.
	 * 
	 * @param pCatEntryId
	 * 		The identifier of the application (catalog entry)
	 * @param pIsPublished
	 * 		The new value of the "published" field
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during the update
	 */
	public static void updateCatalogEntryDescriptionBean(
			String pCatEntryId, boolean pIsPublished)
		throws uAALException
	{
		CatalogEntryDescriptionAccessBean bean = getCatalogEntryDescriptionAccessBean(pCatEntryId);
		bean.setPublished(pIsPublished ? "1" : "0");

		try
		{
			bean.commitCopyHelper();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to update the catalog entry description access bean (" + 
					ex.getMessage() + ")");
		}
	}

	/**
	 * @return The path to the configuration directory
	 */
	private static String getCommerceConfigDir()
	{
		return mCommerceConfigPath;
	}

	/**
	 * Sets the the configuration directory path.
	 * 
	 * @param pCommerceConfigPath 
	 * 		The path to the configuration directory
	 */
	public static void setCommerceConfigPath(String pCommerceConfigPath) 
	{
		if(null == mCommerceConfigPath)
		{
			mCommerceConfigPath = pCommerceConfigPath;
		}
	}
}
