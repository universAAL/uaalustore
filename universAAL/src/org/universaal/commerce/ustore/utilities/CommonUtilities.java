/*	
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.security.auth.callback.CallbackHandler;

import org.universaal.commerce.ustore.uAALException;

import com.ibm.commerce.foundation.client.facade.bod.ClientError;
import com.ibm.commerce.foundation.client.lobtools.auth.IdentityTokenCallbackHandlerImpl;
import com.ibm.commerce.foundation.common.datatypes.BusinessContextType;
import com.ibm.commerce.foundation.common.datatypes.CommerceFoundationFactory;
import com.ibm.commerce.foundation.common.datatypes.ContextDataType;
import com.ibm.commerce.member.facade.client.MemberFacadeClient;
import com.ibm.commerce.member.facade.client.PersonException;

/**
 * This class provides utilities methods for processing requests from the store.
 * 
 * @author roni
 */
public class CommonUtilities 
{
	/**
	 * The name of the uStore configuration file
	 */
	private final String CONFIG_CFG_FILE = "/config.properties";
	
	/**
	 * The uStore configuration properties
	 */
	private static Properties mProperties = null;
	
	/**
	 * The business context to associate with the request
	 */
	private static BusinessContextType mBusinessContext = null;

	/**
	 * A single instance for common utilities
	 */
	private static CommonUtilities mInstance = new CommonUtilities();
	
	/**
	 * @return The single instance of the common utilities
	 */
	public static CommonUtilities getInstance()
	{
		return mInstance;
	}
	
	/**
	 * Constructor.
	 * Construct a {@link CommonUtilities} and load the configuration properties 
	 * and business context
	 */
	private CommonUtilities()
	{
		mProperties = new Properties();
		try 
		{
			// load the properties file from the classpath root
			InputStream input = getClass().getResourceAsStream(CONFIG_CFG_FILE);
			if(null != input)
			{
				mProperties.load(input);
			}
		} 
		catch(IOException ex) 
		{
			System.out.println("WARNING: Cannot open universAAL config configuration, using default one.");
		}

		String catalogId = mProperties.getProperty("catalogId", "10101");
		String storeId = mProperties.getProperty("storeId", "10101");

		CatalogUtilities.setCommerceConfigPath(mProperties.getProperty("commerceConfigPath"));

		// setup the language Id in the context  
		// the language Id is used to select the appropriate store description
		ContextDataType languageIdContext = CommerceFoundationFactory.eINSTANCE.createContextDataType();
		languageIdContext.setName("languageId");
		languageIdContext.setValue("-1");
		
		// setup the catalog Id in the context
		ContextDataType catalogIdContext = CommerceFoundationFactory.eINSTANCE.createContextDataType();
		catalogIdContext.setName("catalogId");
		catalogIdContext.setValue(catalogId);
		
		// setup the store Id in the context
		ContextDataType storeIdContext = CommerceFoundationFactory.eINSTANCE.createContextDataType();
		storeIdContext.setName("storeId");
		storeIdContext.setValue(storeId);
		
		mBusinessContext = CommerceFoundationFactory.eINSTANCE.createBusinessContextType();
		mBusinessContext.getContextData().add(languageIdContext);
		mBusinessContext.getContextData().add(catalogIdContext);
		mBusinessContext.getContextData().add(storeIdContext);
	}
	
	/**
	 * @return The business context
	 */
	public BusinessContextType getBusinessContext()
	{
		return mBusinessContext;
	}
	
	/**
	 * Returns the configuration property of a certain name. If there is no such
	 * property, returns the specified default value.
	 * 
	 * @param pName
	 * 		The property name
	 * @param pDefaultValue
	 * 		The default value.
	 * @return 
	 * 		The named property value
	 */
	public static String getProperty(String pName, String pDefaultValue)
	{
		return mProperties.getProperty(pName, pDefaultValue);
	}
	
	/**
	 * Authenticates a user for accessing the store.
	 * 
	 * @param pUserName
	 * 		The user's username
	 * @param pPassword
	 * 		The user's password
	 * 
	 * @return 
	 * 		A <code>java.util.Map</code> of String[] containing response 
	 * 		parameter <code>userID</code>
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if there was an error during the authentication process
	 */
	public Map authenticate(String pUserName, String pPassword)
		throws uAALException
	{
		// create the MemberFacade client 
		// this client is used to send requests and receive responses from the server
		MemberFacadeClient iMemberClient = new MemberFacadeClient(mBusinessContext, null);

		Map parameters = new HashMap();
		parameters.put("logonId", new String[] {pUserName});
		parameters.put("logonPassword", new String[] {pPassword});

		try 
		{
			return iMemberClient.authenticatePassword(parameters);
		}
		catch(PersonException e) 
		{
			// The following reason codes (consisting of message key and error 
			// code) are possible:
			// _ERR_AUTHENTICATION_ERROR+2030: The specified logon ID or 
			// password are not correct
			// _ERR_PERSON_ACCOUNT_DISABLED+2110: The person's account is 
			// disabled
			// _ERR_PERSON_ANCESTOR_ORG_LOCKED+2400: An ancestor organization is 
			// locked
			// _ERR_PERSON_HAS_NO_ROLES_IN_STORE+2410: The person does not have 
			// a role in the store's organizational hierarchy
			// _ERR_PERSON_REGISTRATION_PENDING_APPROVAL+2420: The registration 
			// is pending approval
			// _ERR_LOGIN_NOT_ALLOWED_NOW+2300: The person must wait before 
			// logging in again
			// _ERR_PASSWORD_EXPIRED+2170: The current password has expired. A 
			// new password must be specified
			// _ERR_PASSWORD_RESET+2430: Your password has been reset. Please 
			// retrieve the temporary password from your email and login again
			List listErrors = e.getClientErrors();
			if(null != listErrors) 
			{
				for(int i=0; i<listErrors.size(); i++) 
				{
					ClientError clientError = (ClientError)listErrors.get(i);
					System.out.println("ERROR: Failed to login [" + clientError.getErrorKey() + "] " +
							clientError.getLocalizedMessage(Locale.ENGLISH));
				}
			}
			throw new uAALException(e);
		}
	}

	/**
	 * Get a callback handler for a specific user.
	 * 
	 * @param pUserParams
	 * 		The user parameters obtains in {@link #authenticate(String, String)}
	 * 
	 * @return 
	 * 		A callback handler for the user
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if there was an error during retrieval
	 */
	public CallbackHandler getCallbackHandler(Map pUserParams)
		throws uAALException
	{
		String[] strUserId = (String[])pUserParams.get("userId");
		String[] identityTokenID = (String[])pUserParams.get("identityTokenID");
		String[] identityTokenSignature = (String[])pUserParams.get("identityTokenSignature");

		if(null == strUserId || strUserId.length == 0 || null == identityTokenID ||
				identityTokenID.length == 0 || null == identityTokenSignature || 
				identityTokenSignature.length == 0)
		{
			throw new IllegalArgumentException("ERROR: Invalid parameters");	
		}

		System.out.println("-->> pUserParams - userId: " + strUserId[0] + " " + 
				identityTokenID[0] + " " + identityTokenSignature[0]);
			
		return new IdentityTokenCallbackHandlerImpl(identityTokenID[0], identityTokenSignature[0]);
	}

	/**
	 * Convert the input value into array of Strings and return the first String 
	 * in the array.
	 * 
	 * @param pValue 
	 * 		The value as an <code>java.lang.Object</code>
	 * 	  
	 * @return 
	 * 		The first <code>java.lang.String</code> in case the given value is 
	 * 		an array of Strings, otherwise return null  
	 */
	 public static String getValue(Object pValue)
	 {
		 if(pValue instanceof String[] && ((String[])pValue).length > 0)
		 {
			 return ((String[])pValue)[0];
		 }
		 return null;
	 }
}
