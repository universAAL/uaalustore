/*	
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.utilities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;
import javax.security.auth.callback.CallbackHandler;
import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.universaal.commerce.ustore.uAALException;

import com.ibm.commerce.foundation.common.datatypes.BusinessContextType;
import com.ibm.commerce.foundation.common.datatypes.CommerceFoundationFactory;
import com.ibm.commerce.foundation.common.datatypes.ContextDataType;
import com.ibm.commerce.foundation.internal.client.taglib.GetDataHandler;
import com.ibm.commerce.order.facade.datatypes.OrderType;
import com.ibm.commerce.order.objects.OrderAccessBean;
import com.ibm.commerce.order.objects.OrderItemAccessBean;
import com.ibm.commerce.server.WcsApp;
import com.ibm.commerce.util.nc_crypt;

/**
 * This class provides utilities methods for the retrieval of specific orders. 
 * 
 * @author roni
 */
public class OrderUtilities 
{
	/**
	 * Retrieve an order from the store site.
	 * 
	 * @param pId 
	 * 		The identifier of the order to be retrieved 
	 * @param pStoreId 
	 * 		The identifier of the store 
	 * @param pServletContext 
	 * 		The servlet context
	 * @param pBusinessContext 
	 * 		The business context of the current request
	 * @param pCallbackHandler 
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return 
	 * 		The order that was retrieved 
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during the retrieval
	 */
	public static OrderType getOrderById(
			String pOrderId,
			String pStoreId,
			ServletContext pServletContext, 
			BusinessContextType pBusinessContext,
            CallbackHandler pCallbackHandler)
		throws uAALException
	{
		List contextParams = new ArrayList();
        ContextDataType contextData = CommerceFoundationFactory.eINSTANCE.createContextDataType();
        contextData.setName("storeId");
        contextData.setValue(pStoreId);
        contextParams.add(contextData);
		
		Map parameters = new HashMap();
		List params = new ArrayList();
		params.add(pOrderId);
		parameters.put("orderId", params);
		
		GetDataHandler handler = new GetDataHandler();
		handler.setType("com.ibm.commerce.order.facade.datatypes.OrderType");
		handler.setExpressionBuilder("findByOrderId");
		handler.setExpressionBuilderParameterMap(parameters);
		handler.setBusinessContext(pBusinessContext);
		handler.setCallbackHandler(pCallbackHandler);
		handler.setServletContext(pServletContext);
		handler.setContextDataList(contextParams);
		
		try
		{
			handler.execute();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve the order by id (" + 
					ex.getMessage() + ")");
		}
		
		return (OrderType)handler.getData();
	}

	/**
	 * Gets the orders of a user.
	 * 
	 * @param pUserId
	 * 		The identifier of the user
	 * @param pStoreId
	 * 		The identifier of the store
	 * @param pServletContext
	 * 		The servlet context
	 * @param pBusinessContext
	 * 		The business context of the current request
	 * @param pCallbackHandler
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return 
	 * 		The orders done by the store user
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during the retrieval
	 */
	public static List getOrdersByUserId(
			String pUserId,
			String pStoreId,
			ServletContext pServletContext, 
			BusinessContextType pBusinessContext,
            CallbackHandler pCallbackHandler)
		throws uAALException
	{
		List orders = new ArrayList();
		
		// use the JDBC approach since for some reason searching orders by 
		// status is not working 
		Connection con = null;
		try
		{
			InitialContext initialcontext = new InitialContext();
			DataSource datasource =	(DataSource)initialcontext.lookup("jdbc/"
					+ WcsApp.configProperties.getValue("Websphere/DatasourceName"));
			String passwd =	nc_crypt.decrypt(WcsApp.configProperties.getValue("Database/DB/DBUserPwd"), null);
			con = datasource.getConnection(WcsApp.configProperties.getValue("Database/DB/DBUserID"), passwd);
			PreparedStatement pstmt = con.prepareStatement("select DISTINCT orders_id from orders " +
					"where member_id = " + pUserId + " and orders.status='C'");
			
			ResultSet rs = pstmt.executeQuery();
			while(rs.next())
			{
				orders.add(rs.getString("orders_id"));
			}
			rs.close();
			pstmt.close();
		}
		catch(Exception ex)
		{
			System.out.println("ERROR: Failed to retrieve the orders (" + ex.getMessage() + ")");
			throw new uAALException("ERROR: Failed to retrieve the orders (" + ex.getMessage() + ")");
		}
		finally 
		{
			if(null != con) 
			{
				try 
				{
					con.close();
				}
				catch(Exception ex) {} // ignore
			}			 
		}
		return orders;
	}

	/**
	 * Gets the items included in an order.
	 * 
	 * @param pOrderId
	 * 		The identifier of the order
	 * @param pStoreId
	 * 	The identifier of the store
	 * @param pServletContext
	 * 		The servlet context
	 * @param pBusinessContext
	 * 		The business context of the current request
	 * @param pCallbackHandler
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return 
	 * 		The items included in the order
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during the retrieval
	 */
	public static List getOrderItemsByOrderId(
			String pOrderId,
			String pStoreId,
			ServletContext pServletContext, 
			BusinessContextType pBusinessContext,
            CallbackHandler pCallbackHandler)
		throws uAALException
	{
		List orderIds = new ArrayList();
		
		// use the JDBC approach since for some reason searching orders by 
		// status is not working 
		Connection con = null;
		try
		{
			InitialContext initialcontext = new InitialContext();
			DataSource datasource =	(DataSource)initialcontext.lookup("jdbc/"
					+ WcsApp.configProperties.getValue("Websphere/DatasourceName"));
			String passwd =	nc_crypt.decrypt(WcsApp.configProperties.getValue("Database/DB/DBUserPwd"), null);
			con = datasource.getConnection(WcsApp.configProperties.getValue("Database/DB/DBUserID"), passwd);
			PreparedStatement pstmt = con.prepareStatement("select DISTINCT orderitems_id from orderitems " +
					"where orders_id = " + pOrderId);
			
			ResultSet rs = pstmt.executeQuery();
			while(rs.next())
			{
				orderIds.add(rs.getString("orderitems_id"));
			}
			rs.close();
			pstmt.close();
		}
		catch(Exception ex)
		{
			System.out.println("ERROR: Failed to retrieve the order ids (" + ex.getMessage() + ")");
			throw new uAALException("ERROR: Failed to retrieve the order ids (" + ex.getMessage() + ")");
		}
		finally 
		{
			if(null != con) 
			{
				try 
				{
					con.close();
				}
				catch(Exception ex) {} // ignore
			}			 
		}
		return orderIds;
	}
	
	/**
	 * Checks if there are orders for the given service in the store.
	 *   
	 * @param pServiceId 
	 * 		The identifier of the reference service (catalog entry) 
	 * @param pStoreId 
	 * 		The identifier of the store 
	 * @param pServletContext 
	 * 		The servlet context
	 * @param pBusinessContext 
	 * 		The business context of the current request
	 * @param pCallbackHandler 
	 * 		The authentication callback handler to associate with the client 
	 * 		library
	 * 
	 * @return 
	 * 		True if there are orders that contain a reference to the input 
	 * 		service (catalog entry). Otherwise, returns False  
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during the process
	 */
	public static boolean isExistOrdersContainReferenceToServiceId(
			String pServiceId,
			String pStoreId,
			ServletContext pServletContext, 
			BusinessContextType pBusinessContext,
            CallbackHandler pCallbackHandler)
		throws uAALException
	{
		// use the JDBC approach since for some reason searching orders by 
		// status is not working 
		Connection con = null;
		try
		{
			InitialContext initialcontext = new InitialContext();
			DataSource datasource =	(DataSource)initialcontext.lookup("jdbc/"
					+ WcsApp.configProperties.getValue("Websphere/DatasourceName"));
			String passwd =	nc_crypt.decrypt(WcsApp.configProperties.getValue("Database/DB/DBUserPwd"), null);
			con = datasource.getConnection(WcsApp.configProperties.getValue("Database/DB/DBUserID"), passwd);
			PreparedStatement pstmt = con.prepareStatement("select DISTINCT orders.orders_id from orderitems, orders " +
					"where catentry_id = " + pServiceId + " and orderitems.orders_id=orders.orders_id and orders.status='C'");
			ResultSet resultset = pstmt.executeQuery();
			boolean exist = false;
			if(resultset.next())
			{
				exist = true;
			}
			resultset.close();
			pstmt.close();
			
			return exist;
		}
		catch(Exception ex)
		{
			System.out.println("ERROR: Failed to retrieve the orders (" + ex.getMessage() + ")");
			throw new uAALException("ERROR: Failed to retrieve the orders (" + ex.getMessage() + ")");
		}
		finally 
		{
			if(null != con) 
			{
				try 
				{
					con.close();
				}
				catch(Exception ex) {} // ignore
			}			 
		}
		
		/*
	    String[] statusArray = {"G"};
		
		// the access profile used in get requests
		String accessProfile = "IBM_Details IBM_Summary";
		
		// the XPath used to find orders
		String xpath = "/Order[OrderStatus[(Status={0})]]";

		SelectionCriteriaHelper helper = new SelectionCriteriaHelper(MessageFormat.format(xpath, statusArray));
		helper.addAccessProfile(accessProfile);
		ExpressionType getExpression = helper.getSelectionCriteriaExpression();
		GetType get = OrderFacadeClient.createGetVerb(getExpression);
		
		OrderFacadeClient orderFacadeClient = new OrderFacadeClient(pBusinessContext, pCallbackHandler);

		// perform the get request
		ShowOrderDataAreaType showOrderDataArea = orderFacadeClient.getOrder(get);
		System.out.println("-->> showOrderDataArea = " + showOrderDataArea);
			
		List orders = showOrderDataArea.getOrder();
		OrderType[] result = new OrderType[orders.size()];
		Iterator ordersIter = orders.iterator();
		while(ordersIter.hasNext()) 
		{
			OrderType order = (OrderType)ordersIter.next();
			if(null != order.getOrderItem())
			{
				Iterator iter = order.getOrderItem().iterator();
				while(iter.hasNext())
				{
					OrderItemType orderItemType = (OrderItemType)iter.next();
					if(orderItemType.equals(pServiceId))
					{
						return true;
					}
				}
			}
		}
		return false;
		*/
	}

	/**
	 * Get a <code>com.ibm.commerce.catalog.objects.OrderItemAccessBean</code>
	 * for a given order item.
	 * 
	 * @param pOrderItemId
	 * 		The identifier of the order item to be accessed
	 * 
	 * @return 
	 * 		A <code>com.ibm.commerce.catalog.objects.OrderItemAccessBean</code>
	 *      for a given order item
	 *      
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during retrieval
	 */
	public static OrderItemAccessBean getOrderItemAccessBean(String pOrderItemId)
		throws uAALException
	{
		OrderItemAccessBean bean = new OrderItemAccessBean();
		bean.setInitKey_orderItemId(pOrderItemId);
		
		try
		{
			bean.refreshCopyHelper();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve the order item access bean (" + 
					ex.getMessage() + ")");
		}
		return bean;
	}
	
	/**
	 * Stores a status of an order item in the 'FIELD1' column of that order's 
	 * entry.
	 * 
	 * @param pOrderItemId
	 * 		The identifier of the order item
	 * @param pStatus
	 * 		The status value (0 indicates that this service in the order was not 
	 * 		installed yet in the user's AAL space, 1 indicates that this service 
	 * 		is installed in the user's space but a new version of it exists in 
	 * 		the uStore catalog and 2 indicates that the service version in the 
	 * 		catalog and in the user's space are the same)  
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during update
	 */
	public static void updateOrderItemAccessBean(String pOrderItemId, int pStatus)
		throws uAALException
	{
		OrderItemAccessBean bean = getOrderItemAccessBean(pOrderItemId);
		bean.setField1(new Integer(pStatus));
		
		try
		{
			bean.commitCopyHelper();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to update the order item access bean (" + 
					ex.getMessage() + ")");
		}
	}

	/**
	 * Get a <code>com.ibm.commerce.catalog.objects.OrderAccessBean</code> for a
	 * given order.
	 * 
	 * @param pOrderId
	 * 		The identifier of the order to be accessed
	 * 
	 * @return
	 * 		A <code>com.ibm.commerce.catalog.objects.OrderAccessBean</code> for 
	 * 		a given order
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during retrieval
	 */
	public static OrderAccessBean getOrderAccessBean(String pOrderId)
		throws uAALException
	{
		OrderAccessBean bean = new OrderAccessBean();
		bean.setInitKey_orderId(pOrderId);

		try
		{
			bean.refreshCopyHelper();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to retrieve the order access bean (" + 
					ex.getMessage() + ")");
		}
		return bean;
	}

	/**
	 * Stores a status of an order in the 'FIELD1' column of that order's entry.
	 * 
	 * @param pOrderId
	 * 		The identifier of the order
	 * @param pStatus
	 * 		The status value (0 indicates that there are no changes between the 
	 * 		version of the services that have been purchased by the user in this 
	 * 		particular order, 1 indicates that there are changes between the 
	 * 		version available in the catalog to the version installed at the 
	 * 		user's space) 
	 * 
	 * @throws <code>org.universaal.commerce.ustore.tools.uAALException</code>
	 * 		if an exception has occurred during update
	 */
	public static void updateOrderAccessBean(String pOrderId, int pStatus)
		throws uAALException
	{
		OrderAccessBean bean = getOrderAccessBean(pOrderId);
		bean.setField1(new Integer(pStatus));

		try
		{
			bean.commitCopyHelper();
		}
		catch(Exception ex)
		{
			throw new uAALException("ERROR: Failed to update the order access bean (" + 
					ex.getMessage() + ")");
		}
	}
}
