/*	
	OCO Source Materials
	� Copyright IBM Corp. 2011
	
	See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership
	
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at
	
	  http://www.apache.org/licenses/LICENSE-2.0
	
	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/
package org.universaal.commerce.ustore.utilities;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * This class wraps the original <code>HttpServletRequestWrapper</code> class
 * in order to extract additional parameters that are added to the request 
 * after it was created. 
 * 
 * @author roni
 */
public class ServletRequestWrapper 
	extends HttpServletRequestWrapper
{
	/**
	 * The additional parameters to be added to the request
	 */
	private HashMap mParams = new HashMap();
	
	/**
	 * Constructs a request object wrapping the given request. 
	 * 
	 * @param pRequest 
	 * 		The HTTP servlet request being processed 
	 */
	public ServletRequestWrapper(HttpServletRequest pRequest) 
	{
		super(pRequest);
	}

	/**
	 * Adds an additional parameter to the request.
	 * 
	 * @param pName 
	 * 		The new parameter name
	 * @param pValue 
	 * 		The new parameter value
	 */
	public void addParameter(String pName, String pValue)
	{
		pValue = (null == pValue) ? "" : pValue; 
		mParams.put(pName, new String[] {pValue});
	}

	/**
	 * Returns all the parameters of this request.
	 * 
	 * @return 
	 * 		A <code>java.util.Map</code> that holds all the parameters of this 
	 * 		request  
	 */
	public Map getParameterMap()
	{
		Map params = new HashMap();
		params.putAll(super.getParameterMap());
		params.putAll(mParams);
		
		return params;
	}
	
	/**
	 * Return the value of the given parameter.
	 * 
	 * @param pName 
	 * 		The name of the parameter to be retrieved
	 * 
	 * @return 
	 * 		The value of the given parameter
	 */
	public String getParameter(String pName)
	{
		// if we added one, return that one
		String[] value = (String[])mParams.get(pName);
		if(null != value)
		{
			return value[0];
		}
		// otherwise return what's in the original request
		return super.getParameter(pName);
	}
	
	/**
	 * Returns all the request parameters.
	 * 
	 * @return 
	 * 		An <code>java.util.Enumeration</code> of all the request parameters
	 */
	public Enumeration getParameterNames()
	{
		return Collections.enumeration(getParameterMap().keySet());
	}
	
	/**
	 * Returns an array of objects containing the parameter's values.
	 * 
	 * @param pName 
	 * 		The name of the parameter to be retrieved
	 * 
	 * @return 
	 * 		An array of objects containing the parameter's values
	 */
	public String[] getParameterValues(String pName)
	{
		// if we added one, return that one
		String[] value = (String[])mParams.get(pName);
		if(null != value)
		{
			return value;
		}
		// otherwise return what's in the original request
		return super.getParameterValues(pName);
	}
}
